# Alti-Website
Website der Waldbesetzung im Altdorfer Wald [https://ravensburg.klimacamp.eu/](https://ravensburg.klimacamp.eu/)

## Setup
Die Seite ist in [Hugo](https://gohugo.io/) entwickelt. Hugo konvertiert die Ansammlung von Markdown (.md) Dateien in die HTML Webseite des Altis.

### Installation
Um die Webseite zu testen oder zu bauen werden die Tools Hugo-extended, Git und Go benötigt. Hugo kann nach den Informationen hier (https://gohugo.io/installation/) installiert werden. Die Hugo-extended Version ist erfahrungsgemäß am einfachsten über die prebuilt Binaries auf [GitHub](https://github.com/gohugoio/hugo/releases/latest) installierbar.


### Repository Klonen
Um die Webseite lokal zu bearbeiten muss dieses Git-Repository auf deinen Computer geklont werden.
```sh
git clone https://gitlab.com/McToel/alti-website.git
```

## Seite bearbeiten
Die Markdown-Dateien in `content` können einfach so bearbeitet und angepasst werden.

### Pressemitteilung
Besonders häufig müssen neue Pressemitteilungen veröffentlicht werden. Dazu per Hugo-CLI eine neue Seite erstellen:
```sh
hugo new --kind Presse Presse/Pressemitteilungen/name_der_pm.md
```
Pressemitteilungen (und der `type: post` generell) können Bilder zugeordnet haben. Diese können im Frontmatter im Kopf der Markdown-Datei eingefügt werden.

```yaml
img:
    src: Bildpfad
    alt: Bildbeschreibung
```

Die Bilder werden sowohl am Start der Pressemitteilung, als auch in der Übersicht über verschiedene Pressemitteilungen angezeigt.

Auch können extra Buttons hinzugefügt werden:

```yaml
buttons:
    - link: https://button-link
      text: Button text 
```

### Blog

Der Blog funktioniert eigentlich genau gleich wie die Pressemitteilungen. Um einen neuen Blogbeitrag zu erstellen, muss lediglich einen anderen Pfad auswählen:
```sh
hugo new --kind Presse Blog/name_des_blogs.md
```


### Bilder
Das Theme stellt einen sogenannten Shortcode bereit, um Bilder einzufügen. Die Bilder werden dadurch automatisch zugeschnitten und in ein effizientes Format konvertiert. Du solltest daher unbedingt diesen Shortcode verwenden:
```html
{{< img src="img/bild.jpg" alt="Beschreibung" >}}
```
### Übersetzungen
Die Seite unterstützt mehrsprachigkeit (Deutsch / Englisch). Auf Seiten mit Englischer Übersetzung wird automatisch ein Vermerk zur Übersetzung angezeigt. Die Übersetzung der Seite `Workshopwoche.md` wird in der Datei `Workshopwoche.en.md` gemacht.

Dennoch sind die Übersetzungen noch nicht optimal, was vor allem daran liegt, dass große Teile der Seite einfach nicht übersetzt sind.

### Navbar
Wenn eine Seite in der Navbar angezeigt werden soll, dann musst Du diese in die `config.yaml` hinzufügen. Das ist nur Notwendig, wenn Du eine neue Seite hinzufügst.

### Seite Testen
Teste die Seite lokal. Wer die Seite nicht testen, ist erfahrungsgemäß dazu verurteilt, die Fehler dann zeitintensiver im Nachhinein zu beheben.
```sh
hugo server -D --logLevel debug --disableFastRender
```

## Hugo Modules

Diese Seite basiert auf Hugo Modules, die wiederum auf Go Modules basieren (technisches Detail). 

### Update Modules

Du wirst ab und zu die Modules updaten müssen. Das geht wie folgt:

```sh
hugo mod get -u
hugo mod tidy
```

## Theme
Der Stil der Seite kommt vom Climatecrisis-Theme [https://gitlab.com/McToel/climatecrisis-theme](https://gitlab.com/McToel/climatecrisis-theme). Dort gibt es hoffentlich auch irgendwann Docs, wie es bearbeitet werden kann. Solange einfach wegen allem nachfragen, indem Du ein Issue aufmachst.
