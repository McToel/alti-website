---
title: "Film"
date: 2023-03-26T16:41:24+02:00
draft: false
description: Ein Film über Klettern, Selbstermächtigung und maximale Wirkung. Und über Wasser und viel Kies. Und über die Waldbesetzung im Altdorfer Wald (Alti)

---
# Von Menschen, die auf Bäume steigen
Ein Film über Klettern, Selbstermächtigung und maximale Wirkung. Und über Wasser und viel Kies. 

{{< img src="img/stoppt_den_klimahoellenplan.jpg" alt="Stoppt den Klimahöllenplan">}}

Die Filmemacher\*innen begleiten über 18 Monate lang Aktivist\*innen rund um das Klimacamp im Altdorfer Wald. Protest und ziviler Ungehorsam stoßen in Oberschwaben bei den meisten Bürger\*innen auf Unverständnis und offene Ablehnung, auch wenn es um den Erhalt der natürlichen Lebensgrundlagen geht. Nun regt sich im Kletteraktivismus eine neue Form des Widerstands, die aber gerichtlich unnachgiebig verfolgt wird.

Zunehmend erkennt die Bevölkerung, dass es um Kies, Trinkwasser und Mitbestimmung über den „Alti“ geht, der als Staatswald eigentlich allen gehört.

## Vorstellungstermine

| Termin               | Ort                  | Kino                                                         |
| -------------------- | -------------------- | ------------------------------------------------------------ |
| Fr. 01.03. 19 Uhr    | Tuttlingen           | BUND Naturschutzzentrum                                      |
| Sa. 02.03. 16 Uhr    | Ravensburg           | Filmprogramm KUNST LICHT NACHT beim Lichterfest              |
| Di. 05.03. 20 Uhr    | Ulm                  | Jazzkeller Sauschdall                                        |
| Di. 12.03. 18:30 Uhr | Göppingen            | Gemeindehaus Pavillon, präsentiert von FFF, P4F, u.a.        |
| Do. 16.03. 19:30 Uhr | Oderberg (BB)        | Altes Rathaus, 19. ÖKOFILMTOUR                               |
| Di. 19.03. 19 Uhr    | Reutlingen           | Haus der Jugend, präsentiert von FFF Reutlingen              |
| Mi. 20.03. 19 Uhr    | Tübingen             | Wohnprojekt Schellingstrasse                                 |
| Fr. 12.04. 20 Uhr    | Kirchzarten/Freiburg | Mietshaus Syndikat                                           |
| Sa. 11.05. 20 Uhr    | Kleinschönach        | Kunsthalle - im Rahmen des Buchvergnügens                    |
| Mi. 17.06. 18:30 Uhr | Oldenburg            | Kino Casablanca / Filmreihe „Alles Utopie- films for future“ |
| Do. 04.07.           | Wolfegg              | Bräuhaus Roßberg                                             |

_Frühere Vorführungen sind hier nicht mehr verzeichnet._

Kurzfristig angesetzte Filmvorführungen - auch anläßlich der Inhaftierung von Charlie und Samuel und anderen Aktivisti und anstehender Gesichtsverhandlungen auf Instagram: [@menschenaufbaeume.film](https://www.instagram.com/menschenaufbaeume.film/)

## Trailer
YouTube: [youtu.be/IfV8wKeFixo](https://youtu.be/IfV8wKeFixo)

## Rezensionen
* [Schwäbische Zeitung vom 27.2.2023](/img/rezension-film-schwaebische.jpg)
* [Bildschirmzeitung vom 27.2.2023](https://www.diebildschirmzeitung.de/blix/aktuell/14766-von-menschen-auf-baeumen)
* [Ersteindruck](https://www.youtube.com/watch?v=hW3vxY1skcY) (auf YouTube)

## Kontakt zum Filmteam
Der Film wurde von Bernadette Hauke und Christian Fussenegger produziert.
Weitere Filmvorführungen, auch in Schulen, können organisiert werden.
Dazu am besten direkten [Kontakt zum Filmteam
aufnehmen](mailto:christian.fussenegger@protonmail.com,bernadette@pangolin-doxx.com).
