---
title: "Anreise"
date: 2023-03-24T12:54:29+01:00
draft: false
description: Anreise in die Waldbesetzung im Altdorfer Wald (Alti)
---
# Anreise

Du möchtest uns im Alti besuchen kommen? Mega cool, wir freuen uns auf Dich!

## Viele Wege führen in den Alti
Der Alti ist gut mit dem ÖPNV zu erreichen. Du kannst auch mit dem Fahrrad oder Auto kommen. Wir können möglicherweise Deine Fahrtkosten übernehmen.

### Koordinaten
[47.810973, 9.76126](geo:47.810973,9.76126)

### Bushaltestelle direkt an der Besetzung (werktags)
Montag bis Freitag hält der Bus 7534 direkt an der Besetzung. Haltestelle [Vogt Abzw. Grund im Wald, Wolfegg](https://www.bahn.de/buchung/start#?ZO=Vogt%20Abzw.%20Grund%20im%20Wald%2C%20Wolfegg)

### Bushaltestelle in der Nähe der Besetzung (Wochenende)
Am Wochenende hält kein Bus direkt an der Besetzung. Die nächste Haltestelle [Grund, Vogt](https://www.bahn.de/buchung/start#?ZO=Grund%2C%20Vogt) ist in 20 Minuten zu Fuß erreichbar.

### Nächster Bahnhof
Wenn mal kein Bus fährt, ist der Alti auch vom Bahnhof [Wolfegg](https://www.bahn.de/buchung/start#?ZO=Wolfegg) (**Vorsicht Bedarfshalt![^bedarfshalt]**) aus in ca. 4,8 km zu Fuß erreichbar.

[^bedarfshalt]: Aus eigener Erfahrung

### Mitfahrbörse
Wenn mal kein Bus fährt, kannst Du vermutlich in der [Mitfahrbörse](https://t.me/MitfahrboerseAlti) ein Taxi finden.

### Kontakt

[Ruf gerne an](/kontakt/) oder frag auf Telegram oder WhatsApp nach 👋
