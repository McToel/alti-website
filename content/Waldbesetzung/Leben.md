---
title: "Leben"
date: 2023-03-24T12:59:21+01:00
draft: false
description: Leben in der Waldbesetzung im Altdorfer Wald (Alti)
---
# Im Wald leben

Du bist willkommen! Trotz Bedrohung durch die Klimakrise versuchen wir, eine gute Stimmung zu bewahren. 😊

{{< img src="img/schaukeln gegen kies.jpg" alt="Schaukeln gegen Kies" >}}

## Im Wald haben wir

- 🏕 bezugsfertige Baumhäuser
- 🚽 ein Kompostklo
- 🧶 genug Poly zum Bauen :-)
- 🧑‍🎓 gegenseitige Wissens- und Fertigkeitsweitergabe (insbesondere Baumhausbau)
- 👩‍💻 Strom (aus Batterie) und WLAN (aus LTE-Router) für Schule und Arbeiten
- 🥙 leckeres warmes veganes Essen (Wir fangen an zu kochen, sobald wir Hunger haben)
- 🍽️ Geschirr, Besteck, Töpfe, ...
- 👩‍🦳 Unterstützung durch Menschen vor Ort
- 🎨 eine dynamische unterstützende Kreativaktiviszene
- 🕊 bisher und auf absehbare Zeit wohl keine Polizeipräsenz
- 🔐 Möglichkeit, bei Unterstützer:innen vor Ort Dinge zu verwahren (etwa Ausweisdokumente)
- 🌳 einen wunderbaren Lebensraum, der dank uns bleiben wird!

Ob du nur für ein paar Stunden vorbeischauen willst oder unbefristet bleiben möchtest -- wir freuen uns auf dich!


## Mitbringen

Du brauchst daher eigentlich nur Schlafsack und Isomatte mitzubringen. (Können wir dir auch ausleihen, nimm dazu aber [vorab Kontakt mit uns auf](https://t.me/+Sv8dMIv1OEjRQ6DJ), damit wir ganz sicher was für dich da haben.)
