---
title: "Unterstützen"
date: 2023-03-24T13:00:20+01:00
draft: false
description: Wie kannst Du die Waldbesetzung im Altdorfer Wald (Alti) unterstützen?
---
# Die Waldbesetzung unterstützen

{{< img src="img/pride.jpg" alt="Pride Flagge über den Bäumen" >}}

Wir freuen uns, dass du die Waldbesetzung unterstützen möchtest! Wir wissen jeden Beitrag wahnsinnig zu schätzen und sind dir sehr dankbar!

So kannst du konkret helfen:

- 🗣 Im Freundes- und Bekanntenkreis über Klimagerechtigkeit, die dringend nötige Mobilitätswende sowie den Altdorfer Wald sprechen.
- ✉️ Leser\*innenbriefe schreiben.
- Komme vorbei und zeige, dass Du die Besetzung unterstützt. Menschen im Wald freuen sich immer über veganen Kuchen und wenn sie merken, dass es viel Unterstützung in der Bevölkerung gibt.
- 💶 [Geld spenden](/spenden/) (insbesondere für Baumhausmaterialien und etwaige Rechtskosten)