---
title: "Sanierungs- und Workshopwoche (26.10. bis 3.11.2024)"
date: 2024-10-15T22:30:29+02:00
type: post
draft: false
img:
    src: img/sanierungs_und_workshopwoche.png
    alt: Ankündigung der Sanierungs- und Workshopwoche im Alti von 26. Oktober bis 3. November
aliases:
    - /sanierungswoche
---

Vom 26.10. bis zum 3.11.2024 findet im Alti die Sanierungs- und Workshopwoche statt. Das Program findest du hier:

<a href="https://etherpad.wikimedia.org/p/r.d29cdb4f9fa1177839f4d8063cc251f5" class="btn btn-primary">Aktuelles Program</a>

Wir machen gemeinsam den Wald winterfest und bauen die Baumhäuser aus. Ihr könnt viele handwerkliche Fähigkeiten, Klettern und Skills für Aktionen dazu lernen. Außerdem gibt es gemütliche Austauschrunden, leckeres Essen und Tee.

Gemeinsam schützen wir den Altdorfer Wald vor der Zerstörung durch den Kiesabbau. Sanieren statt Neubauen!✊

Außer den großen Bauprojekten (Küchenboden, Baumhäuser unterfangen und Solarstrom-Anlage bauen) gibt es viele weitere kleine Projekte. Für alle ist etwas dabei. Es gibt auch zahlreiche Aufgaben ohne klettern. 

Wir freuen uns auch total über Menschen die Lust haben etwas zum Essen, ob Eintopf, Kuchen, Lebensmittel... 🌮🫕🍕vorbeizubringen ..
Dann können wir uns voll aufs Bauen konzentrieren🙏

Wenn Menschen noch Sperrmüllkarten übrig haben wäre uns damit auch total geholfen!🙏

🙋 Wenn ihr selbst eine Fähigkeit teilen wollt, meldet euch bei uns:

<a href="{{< ref "Kontakt.md">}}" class="btn btn-primary">Kontaktmöglichkeiten</a>

So kommst du in den Wald:

<a href="{{< ref "Anreise.md">}}" class="btn btn-primary">Anreise in den Alti</a>

☝️Hinweis: Falls in dieser Zeit eine Räumung in einer anderen Besetzung zu erwarten ist, mobilisieren wir selbstverständlich dort hin.
