---
title: "Workshop-Woche"
date: 2024-04-12T20:58:17+02:00
draft: false
description: "Workshop-Woche im besetzten Altdorfer Wald"
img:
    src: img/workshopwoche-2024.jpg
    alt: Workshop-Woche im besetzten Altdorfer Wald, 17. bis 26. Mai 2024
type: post
---


Das vorläufige Programm für die Workshop-Woche steht!  
Kommt vorbei - ab dem 17. Mai  im besetzten Altdorfer Wald bei Ravensburg!  
Offenes Ende ist der 26. Mai 2024. 🌳🌲🏠🌳

Protest, Utopie, Film, Musik, Wissen, Politik, Kunst und viiiiieles mehr  
Es gibt keinen Einsendeschluss für Programmpunkte und es können auch vor Ort spontan Programmpunkte an die Tafel geschrieben werden.  
Kommt vorbei, genießt das utopische Leben in der Natur und vernetzt euch für Aktionen!  

Wir haben Schlafplätze auf Baumhäusern, mit und ohne Leitern. ⛺️🏠🌲  
Das Essen organisieren alle, die da sind, gemeinsam selbst.  
Bringt unbedingt Schlafsack, Isomatte, warme wetterfeste Kleidung und Zelte mit!  
Alles wird zweisprachig sein. Deutsch und Englisch. Es gibt Flüsterübersetzung.

Workshops können unter [altibleibt@riseup.net](mailto:altibleibt@riseup.net) angemeldet werden.

Das Programm wurde zum Großteil aus dem Knast in Göppingen organisiert. ⛓  
Der Kampf für Klimagerechtigkeit geht weiter!

<a href="https://etherpad.wikimedia.org/p/r.7afd82efd5487cd2393288821639f365" class="btn btn-primary">Zum aktuellen Programm</a>