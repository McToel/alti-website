---
title: "Program Workshopwoche Februar 2025"
date: 2025-02-19T12:03:23+01:00
type: post
draft: false
buttons:
    - link: https://etherpad.wikimedia.org/p/r.5db2d2e4b5becd789ab204481527fe64
      text: Programm 
---

Vom 20.2. bis zum 25.2. lädt die Besetzung im Altdorfer Wald zu "Bauen, Sanieren, Lernen und Widerstand" ein.
Anlass ist das 4-jährige Jubiläum der Waldbesetzung. In zahlreichen Workshops zum Thema Protest und Baumhaus Bauen wird umfangreiches Wissen untereinander geteilt.

Am Wahlsonntag, den 23.2. um 14 Uhr wird die Band "Zimt und Zorn" mit Franzi Groß ein Konzert auf Spendenbasis in der Besetzung mitten im Wald geben. Ein ganz besonderes Ambiente. Außerdem gibt es ein Mitbring-Fingerfood-Buffet und die Möglichkeit, sich das Baumhausdorf bei einem Rundgang zeigen zu lassen.

## Program

Hier findest Du das Programm für die kommende Workshopwoche:

{{< img src="img/workshopwoche märz 2025/1.jpg" alt="Programm 1" >}}

{{< img src="img/workshopwoche märz 2025/2.jpg" alt="Programm 2" >}}

{{< img src="img/workshopwoche märz 2025/3.jpg" alt="Programm 3" >}}

{{< img src="img/workshopwoche märz 2025/4.jpg" alt="Programm 4" >}}

Falls es Änderungen und Ergänzungen zum Programm gebe sollte, findest Du diese hier: https://etherpad.wikimedia.org/p/r.5db2d2e4b5becd789ab204481527fe64
