---
title: "Wäre Jesus Klimaaktivist?"
date: 2024-07-02T22:44:06+02:00
type: post
draft: false
img:
    src: img/jesus-klimaaktivist.jpg
    alt: "100 m² großes Banner am Ulmer Münster fragt: Wäre Jesus Klimaaktivist?"
---

"Wäre Jesus Klimaaktivist?" stand Anfang Juli diesen Jahres am höchsten Kirchturm der Welt - dem Ulmer Münster. Eine Gruppe aus 20 Aktivist*innen hatten die Aktion vorbereitet und es fertig gebracht ihre Frage nach Gewissen und Verantwortung in Zeiten der Klimakrise dort nachts in 70m Höhe anzubringen. Sie riefen auch alle Kirchen dazu auf, sich zu fragen ob Klimaerhitzung und Artensterben eigentlich nur andere Worte für Schöpfungszerstörung sind.

Mehr Informationen auf [www.wäre-jesus-klimaaktivist.de](https://www.wäre-jesus-klimaaktivist.de/)