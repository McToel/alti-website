---
title: "Verkehrswendeplan fürs Schussental"
date: 2023-06-29T18:00:20+01:00
draft: false
description: Den Verkehrswendeplan mitgestalten
img:
    src: img/verkehrsschlilder schussental.jpg
    alt: Überhängte Verkehrsschilder weisen auf den Verkehrswendeplan hin
type: post
---
# Verkehrswendeplan fürs Schussental

Für das Schussental und die Anbindung der umliegenden Gemeinden soll ein Verkehrswende-Plan erarbeitet werden!

<!-- {{< img src="img/verkehrsschlilder schussental.jpg" alt="Überhängte Verkehrsschilder weisen auf den Verkehrswendeplan hin" >}} -->

Der Verkehrswendeplan soll konkrete Maßnahmen zur Verkehrswende im Schussental enthalten:
- Welche Straßen sollen Fahrradstraßen werden
- Welche Autostraßen sollen gesperrt werden
- Wo soll die Tram wieder eingeführt werden
- Wo und wie oft sollen Busse fahren
- Wo soll die autofreie Innenstadt beginnen
- Welche Bauprojekte sollen gestoppt werden, um unnötigen Autoverkehr zu vermeiden
- Der Nulltarif im öffentlichen Nahverkehr

Der Plan wird von allen die möchten mitgestaltet und wird keinen Konsens, sondern eine bunte Ideensammlung mit radikalen aber realistischen Vorschlägen werden.

Ziel ist, dass wir konkrete zusammenhängende Vorschläge für die Verkehrswende im Schussental und mit den umliegenden Gemeinden machen. Dadurch zeigen wir, dass alle Verkehrswendeaktionen miteinander verbunden sind und haben gleichzeitig die Möglichkeit deutlich konkreter, konstruktiver und mit mehr Nachdruck Aktionen zu machen.

## Erste Aktion hat bereits stattgefunden

Es gab eine erste Aktion, um auf den Verkehrswendeplan hinzuweisen. Wenn du bei zukünftigen Aktionen dabei sein möchtest, melde dich gerne oder fange an selber etwas zu organisieren.

<a href="{{< ref "Presse/Pressemitteilungen/Aktivisti gestalten Verkehrsschilder um – neues Fahrziel: Verkehrswendeplan fürs Schussental.md">}}" class="btn btn-primary">Pressemitteilung der Aktion</a>

## Mitgestalten

Alle sind eingeladen am Plan mitzuarbeiten. Bei Interesse einfach bei der [Baumbesetzung]({{< ref "Kontakt.md">}}) melden.
