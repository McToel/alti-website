---
title: "Shillshare-Week"
date: 2024-04-12T20:58:17+02:00
draft: false
description: "Shillshare-Week in the occupied Altdorf Forest"
img:
    src: img/workshopwoche-2024-english.jpg
    alt: Shillshare-Week in the occupied Altdorf Forest, 17 - 26 May 2024
type: post
---

The provisional program for the skillshare-week has been announced!  
Come along - from 17th of May in the occupied Altdorfer Wald near Ravensburg!  
The open end is 26th of May 2024 🌳🌲🏠🌳

Protest, utopia, film, music, knowledge, politics, art and much, much more  
There is no deadline for program items and program items can also be written spontaneously on the blackboard on site.  
Come along, enjoy the utopian life in nature and the networking for activities!

We have sleeping places in tree houses, with and without ladders. ⛺️🏠🌲  
We will be organising the food together with everyone who is there.  
Make sure you bring a sleeping bag, sleeping mat, warm weatherproof clothing and tents!  
Everything will be bilingual. German and English. There will be whispered translation for programs presented in German.

If you want to add Workshops, write to [altibleibt@riseup.net](mailto:altibleibt@riseup.net).

Big parts of the program were organised from the prison in Göppingen. ⛓  
The fight for climate justice continues!

<a href="https://etherpad.wikimedia.org/p/r.7afd82efd5487cd2393288821639f365" class="btn btn-primary">To the program</a>
