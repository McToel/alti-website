---
title: "Carousel1"
date: 2023-03-25T14:53:17+01:00
draft: false
img: img/menschen-auf-baeume_1400_07.jpg
type: carousel-content
---

# Von Menschen, die auf Bäume steigen

 Ein Film über Klettern, Selbstermächtigung und maximale Wirkung. Und über Wasser und viel Kies.

 <a href="/film/" class="btn btn-primary">Vorführungstermine</a>