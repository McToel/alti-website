---
title: "Spenden"
date: 2023-03-26T16:40:21+02:00
draft: false
description: Spenden für den Erhalt der Waldbesetzung im Altdorfer Wald (Alti)
---

# Spenden

Auch eine Baumbesetzung braucht Geld. Und das wächst leider nicht auf Bäumen. Spenden helfen uns vor allem dabei, Klettersachen für Menschen zu kaufen, die sich diese selber nicht leisten können, Flyer und Plakate zu drucken sowie Rechtskosten zu tragen.

Wir sind daher dringend auf Spenden angewiesen.

Anfangs machte auch das besondere baumschonende Seil für den Baumhausbau einen sehr großen Kostenpunkt aus, weil es nun schon viele mehrere Baumhausdörfer gibt, fallen diese Kosten aktuell nicht mehr so sehr ins Gewicht.

## Spendenkonto
```
Name: Spenden & Aktionen
IBAN: DE29 5139 0000 0092 8818 06
BIC: VBMHDE5F
Verwendungszweck: Ravensburg
```
(Verwendungszweck unbedingt angeben, sonst kann die Spende nicht zugeordnet werden!)

Hinweis: Wir können zwar Spendenbescheinigungen ausstellen -- [schreibt uns](/kontakt) dazu an -- Spenden an uns sind aber nicht steuerlich absetzbar.
