---
title: "Presse"
date: 2023-03-24T21:09:06+01:00
draft: false
description: Informationen für die Pressearbeit in der Waldbesetzung im Altdorfer Wald (Alti)
---
# Informationen für die Presse
Pressekontakt [<i class="bi bi-envelope-at"></i> baumbesetzung.ravensburg@gmail.com](mailto:baumbesetzung.ravensburg@gmail.com),
[<i class="bi bi-phone"></i> +4915908156028](tel:+4915908156028)
[<i class="bi bi-phone"></i> +4917695110311](tel:+4917695110311)
