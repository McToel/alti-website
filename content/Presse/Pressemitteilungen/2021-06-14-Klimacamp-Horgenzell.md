---
layout: page
title:  "14.06.2021: Klimagerechtigkeitsaktivist*innen eröffnen 50-Stunden-Klimacamp in Horgenzell"
date:   2021-06-14 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2106140
#          YYMMDDX
---

*Pressemitteilung des Ravensburger Klimacamps am 14.6.2021*

# Klimagerechtigkeitsaktivist\*innen eröffnen 50-Stunden-Klimacamp in Horgenzell

Am heutigen Montag (14.6.) errichten Klimagerechtigkeitsaktivist\*innen um 17:00
Uhr ein 50-Stunden-Klimacamp vor der Mehrzweckhalle in Horgenzell (Kornstraße
46/1, 88263 Horgenzell). Dort findet am 16.6. um 10:00 Uhr die Sitzung des
Regionalverbands statt, bei der die über 2.700 Einwendungen aus der
Zivilgesellschaft gegen den aktuellen Regionalplanentwurf abgewägt werden.

"In seiner aktuellen Fassung ist der Regionalplanentwurf ein Klima-Höllen-Plan",
erklärt Aktivist Samuel Bosch (18) die Aktion. "Wir möchten vor der
entscheidenden Ausschusssitzung ein letztes Mal auf die Verfehlungen des
undemokratischen Regionalverbands aufmerksam machen und mit den Verantwortlichen
in Kontakt kommen." Der Vorwurf der Demokratiefeindlichkeit bezieht sich darauf,
dass die Verbandsversammlung die tatsächlichen Mehrheitsverhältnissen in den
Gemeinderäten, wo die Grünen eine starke Kraft sind, gar nicht widerspiegelt,
sondern vor allem von Bürgermeistern und Altbürgermeistern von CDU und FWV
besetzt ist. Nur sieben der 56 Stimmberechtigten sind weiblich.

Au dem Klimacamp wird es Vorträge von diversen Wissenschaftler\*innen, Workshops
und Austauschrunden geben. Die Aktivist\*innen werden bis zur Entscheidung Tag
und Nacht vor der Mehrzweckhalle verweilen. Das Camp wird durch kleinere
Aktionen des zivilen Ungehorsams begleitet werden.