---
title: "Waldbesetzer*innen laden überregional zu Workshopwoche ein"
date: 2023-05-23T17:41:47+02:00
type: post
draft: false
---

Eine dedizierte Ermittlungsgruppe der Polizei, die "EG Baumwelt", verfolgt ihre Aktivitäten: Seit fast drei Jahren setzen die Besetzer\*innen des Altdorfer Walds mit ihren umstrittenen Aktionen immer wieder Klimagerechtigkeit und den umstrittenen Kiesabbau auf die Tagesordnung. Nun laden sie öffentlich zu einer Workshopwoche ein. Sowohl Familien mit ihren Kindern aus der Region als auch erfahrene Aktivist\*innen aus ganz Deutschland werden vom 27. Mai bis 4. Juni 2023 in der Waldbesetzung erwartet, der Eintritt ist frei.

"Wir müssen endlich aufhören alles zuzubauen. Weniger ist mehr, diese Philosophie muss auch im Regionalverband ankommen und in den Holzköpfen der Politiker\*nnen ankommen", erklärt Mitorganisator Samuel Bosch (20). Nur durch vielfältige Aktionen könne man eine Veränderung in Richtung Klimagerechtigkeit und eine zukunftsfähigen Regionalplan erreichen. So haben mehrere Workshops das Ziel, Bürger\*innen Protest zu ermöglichen: etwa wie Demonstrationen "geschickt" angemeldet werden, wie Grundlagen der Pressearbeit aussehen oder wie man auf Gebäude klettert, um dort Banner zu befestigen oder diese zu besetzen. Auch einen Workshop zum Anketten in so genannten Lock-Ons wird es geben.

Zu den mehreren externen Referent\*innen gehört auch der renommierte Verkehrswendeexperte und Publizist Jörg Bergstedt, der auf mehr als 40 Jahre Erfahrung in Protestbewegegungen zurückblicken kann [1]. Sein Vortrag "Provokante Aktionen und ihre Bedeutung für Protest" analysiert anhand geschichtlicher Beispiele verschiedene Qualitätsachsen von direkten Aktionen und nimmt dabei auch auf die umstrittenen Klebeaktionen der Letzten Generationen Bezug.

Daneben gibt es ein umfangreiches Rahmenprogramm: Etwa werden der Film "90 Meter" von Regisseur Claudio Brauchle, der auch auf dem SWR Doku Festival gespielt wurde [2], sowie "Finite: The Climate of Change" vorgeführt. Beide thematisieren die Herausforderungen ehrenamtlichen politischen Engagements im Kampf gegen scheinbar übermächtige Großkonzerne. Daneben gibt die Indie-Band "Gravel in the Basement" ein Konzert.

[1] https://de.wikipedia.org/wiki/J%C3%B6rg_Bergstedt

[2] https://www.swr.de/swr-doku-festival/doku-festival-22-90-meter-100.html

## Hinweis
Das vollständige Programm ist auf https://ravensburg.klimacamp.eu/ zu finden. Der Eintritt ist frei, eine Voranmeldung ist nicht erforderlich. Für Schlafplätze in Baumhäusern mit und ohne Leiter oder in Zelten ist gesorgt.

-------------

## <a href="/presse"> Pressekontakt </a>