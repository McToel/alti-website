---
layout: page
title:  "26.10.2021: Erster Strafprozess gegen Klimaaktivist Samuel Bosch wegen Gründung der \"Alti\"-Besetzung und vier Banneraktionen"
date:   2021-10-26 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2110260
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 26.10.2021*

# Erster Strafprozess gegen Klimaaktivist Samuel Bosch wegen Gründung der "Alti"-Besetzung und vier Banneraktionen

Nach nun zwei Jahren Klimagerechtigkeitsaktivismus steht Samuel Bosch am
Donnerstag (28.10.2021) ab 9:00 Uhr vor dem Amtsgericht Ravensburg Rede und
Antwort. Es ist der erste Strafprozess gegen Klimaaktivist\*innen in Ravensburg,
es geht um fünf Einzelvorwürfe zu vier Aktionen und dem Start der Besetzung des
Altdorfer Walds.

Die Einzelvorwürfe lauten:
1. Beginn des Baumhauscamps im Altdorfer Wald, Vorwurf: Versammlungsgesetz (§26 VersG)
2. "Stoppt den Klimahöllenplan"-Banner in Amtzell, Vorwurf: Hausfriedensbruch (§123 StGB)
3. Banner an Bagger der Kiesgrube Tullius bei Oberankrenreute, Vorwurf: Hausfriedensbruch (§123 StGB)
4. Baumbesetzung in der Bachstraße, Vorwurf: Versammlungsgesetz (§26 VersG)
5. Banner mit Prof. Dr. Ertel an der RWU in Weingarten, Vorwurf: Versammlungsgesetz (§26 VersG)

Die Klimaaktivist\*innen und deren anwältlicher Verteidiger Klaus Schulz
rechnen bei allen Vorwürfen mit einem Freispruch. Aus Sicht der
Aktivist\*innen seien die Vorwürfe "an den Haaren herbei gezogen" um
ihren legitimen Protest zu "kriminalisieren". Samuel Bosch (18): "Da
kein Mensch zu Schaden gekommen ist, bedauern wir, dass die Behörden die
Justiz mit unnötigen Prozessen belasten." Ein früheres
Ordnungswidrigkeitenverfahren gegen Bosch wurde eingestellt
([Artikel in der Schwäbischen Zeitung](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumkletterer-muss-kein-bussgeld-zahlen-_arid,11404370.html)),
zudem stellte auch schon die Staatsanwaltschaft zahlreiche polizeiliche
Ermittlungsverfahren ein, bevor sie Gerichte belasten musste.

Laut den Aktivist\*innen stimme die Aussage, sie hätten am 19.2.2021 ein
Banner über die L317 nahe des "Alti"-Camps gespannt, nicht, sie haben zu
diesem Zeitpunkt lediglich Plattformen und Baumhäuser im Wald errichtet.

Anwohner Manfred Scheurenbrand (66) aus Waldburg unterstützt eine von
Unterstützer\*innen organisierte parallel stattfindende Mahnwache vor dem
Amtsgericht: "Vor Gericht sollten die Umweltbrecher stehen, nicht die
jungen Menschen, die mit friedlichm zivilen Ungehorsam auf Missstände
hinweisen."


## Hinweise zu den Einzelvorwürfen

Die Einzelvorwürfe lauten:
- Beginn des Baumhauscamps im Altdorfer Wald, Vorwurf: Versammlungsgesetz (§26 VersG)
- "Stoppt den Klimahöllenplan"-Banner in Amtzell, Vorwurf: Hausfriedensbruch (§123 StGB) & (§26 VersG)
- Banner an Bagger der Kiesgrube Tullius bei Oberankrenreute, Vorwurf: Hausfriedensbruch (§123 StGB)
- Baumbesetzung in der Bachstraße, Vorwurf: Versammlungsgesetz (§26 VersG)
- Banner mit Prof. Dr. Ertel an der RWU in Weingarten, Vorwurf: Versammlungsgesetz (§26 VersG)

-----

1. Die Aktivist\*innen besetzen seit Februar ein bedrohtes Waldstück im
Altdorfer Wald um die geplante Rodung für den Kießabbau zu verhindern.
[Artikel zur Aktion](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-ein-zweiter-hambacher-forst-aktivisten-besetzen-den-altdorfer-wald-_arid,11333867.html)

2. Die Aktivist\*innen kletterten auf die Mehrzweckhalle in Amtzell um
dort mit einem Banner ("Stoppt den Klimahöllenplan") gegen den aus ihrer
Sicht nicht zukunftsfähigen Regionalplanentwurf zu protestieren.
Währenddessen stimmte der Gemeinderat-Amtzell in der Mehrzweckhalle über
den Entwurf ab.
Dieselbe Form von Aktion wurde einige Tage später auch bei der
entsprechenden Sitzung des Gemeinderats in Weingarten durchgeführt.
[Artikel zur Aktion](https://www.schwaebische.de/landkreis/landkreis-ravensburg/amtzell_artikel,-regionalplan-sorgt-fuer-unmut-bei-klimaaktivisten-_arid,11332447.html)

3. Die Aktion an der Hochschule stieß den Prozess an, ungenutzte Hörsäle
im Winter nicht unnötig zu beheizen.Das hatte zuvor Prof. Dr. Wolfgang
Ertel über zehn Jahre lang erfolglos versucht.
[Artikel zur Aktion](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-professor-protestiert-mit-baumbesetzern-an-hochschule-_arid,11362406.html)

1. Bei der Baumbesetzung in der Bachstraße wird den Aktivist\*innen
vorgeworfen, Vögel so sehr gestört zu haben, dass sie ihre Brut nicht
mehr versorgten. "Tatsächlich zeigten sich die Vögel von unserer
Hängematte sichtlich unbeeindruckt und flogen immer wieder ein uns aus",
so Bosch. Ein von einem Anwohner am 18. Maiaufgenommenes Video belegt,
dass sich die Vögel auch nach der Aktion weiterhin ihre Brut versorgten
und normal verhielten.
[Artikel zur Aktion](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaaktivisten-besetzen-erneut-baum-in-ravensburg-_arid,11363667.html)

5. Bei der Aktion in der Kiesgrube handelt es sich aus Sicht der
Aktivist\*innen nicht um Hausfriedensbruch. Ihrer Ansicht nach ist das
Betreten von offenem Gelände ist erlaubt und das Entfernen des Banners
durch die Arbeiter\*innen der Kiesgrube dauerte wahrscheinlich nur wenige
Sekunden. "Warum werden überhaupt Aktionen verfolgt, die niemanden
stören und bei denen niemand zu Schaden kommt?" so Bosch zu den
Vorwürfen.
[Artikel zur Aktion](https://wochenblatt-online.de/banner-an-bagger-der-kiesgrube-oberankenreute-mahnt-kiesunternehmen-und-politik/)