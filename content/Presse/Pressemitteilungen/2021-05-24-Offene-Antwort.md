---
layout: page
title:  "24.05.2021: Klimacamper*innen reagieren auf offenen Brief von Polizeipräsident Uwe Stürmer"
date:   2021-05-24 23:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2105240
#          YYMMDDX
---

*Pressemitteilung des Ravensburger Klimacamps am 24. Mai 2021*

# Klimacamper\*innen reagieren auf offenen Brief von Polizeipräsident Uwe Stürmer

Am Abend des heutigen Montags (24.5.2021) erreichte Ravensburgs
Klimacamper\*innen über Umwege ein offener Brief von Polizeipräsident Uwe
Stürmer, über den die Schwäbische am Nachmittag bereits berichtete [1]. Der
Volltext des Briefs ist auf [2] zu finden. Die offene Antwort der
Klimaaktivist\*innen steht unten. In diesem Zusammenhang
verweisen wir auch auf den offenen Brief von Eltern, Familien und sonstigen
Unterstützer\*innen vom 19.5.2021 [3].

[1] [https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-polizeipraesident-aeussert-sich-zu-umstrittenem-protest-der-klima-aktivisten-_arid,11367115.html](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-polizeipraesident-aeussert-sich-zu-umstrittenem-protest-der-klima-aktivisten-_arid,11367115.html)<br>
[2] [https://etherpad.wikimedia.org/p/r.fec2b7e354f3f601532f1550b3116fc9](https://etherpad.wikimedia.org/p/r.fec2b7e354f3f601532f1550b3116fc9)<br>
[3] [https://ravensburg.klimacamp.eu/Offener-Brief-PFF-2021-05-19.pdf](https://ravensburg.klimacamp.eu/Offener-Brief-PFF-2021-05-19.pdf)


## 1. Allgemeine übergeordnete Bemerkung

**Das große Ziel unseres Engagements ist, öffentlich zu machen, dass Ravensburgs Stadtregierung trotz ihrer gegenteiligen Beteuerungen unsere schöne Stadt nicht auf einen 1,5-Grad-Pfad lenkt.** Sie enthält Bevölkerung und Unternehmen Maßnahmen vor, die die Klimakrise abmildern und nebenbei unsere Lebensqualität steigern und soziale Gerechtigkeit stärken würden: etwa ein sicheres Radwegenetz oder ein ausgebautes und kostenloses Bus- und Tramnetz. Damit wären weniger Bürger\*innen genötigt, ein teures Auto unterhalten zu müssen.

Konflikte mit der Polizei sind nicht unser Ziel und werden von unserer Seite aus auch niemals in unseren Pressemitteilungen im Fokus stehen, da diese vom eigentlichen Skandal ablenken: dass die Stadtregierung den Schüler\*innen ihre Zukunft verbaut und jeden Tag die Klimakrise für die Menschen in den am meisten betroffenen Gebieten befeuert. Wie auch das Bundesverfassungsgericht jüngst bestätigte, verletzt eine Klimapolitik des Beschwichtigens und magischen Denkens in hypothetische Rettungstechnologie die Rechte der jungen Generation.

**Wir möchten besonders für die Gesprächsmuster konservativer Politiker\*innen sensibilisieren,** künstlich Reibung zwischen Klimaschutz und Wirtschaft zu konstruieren oder Klimapolitik zu privatisieren und ihre Verantwortung auf Privatpersonen abzuschieben. Unserer Auffassung muss die Politik Rahmenbedingungen schaffen, sodass sich alle Bürger\*innen klimafreundliches Verhalten finanziell und zeitlich leisten können. Appelle von Politiker\*innen an Privatpersonen, ihre Haare kürzer zu schneiden um die Umwelt mit weniger Shampoo zu belasten, lenken von dieser Verantwortung ab. Das Konzept des "CO2-Fußabdrucks" wurde von der Ölindustrie als Ablenkungsmanöver etabliert (Quelle: https://mashable.com/feature/carbon-footprint-pr-campaign-sham/).


## 2. Situationseinordnung in den größeren Kontext

Unser zweites Baumhaus bauten wir am 30.1.2021 ab, da wir wissen, dass Stadtverwaltungen komplexe Strukturen sind und Änderungen nicht von heute auf morgen beschließen können. Wir hinterließen der Stadt eine dreimonatige Frist, nach der wir die Situation neu begutachten wollten. Nach ihrem Ablauf mussten wir leider feststellen, dass die Stadt in den drei Monaten keine einzige Klimagerechtigkeitsmaßnahme beschloss. Allein der vierköpfige Klimarat wurde eingesetzt, der -- unter Ausschluss lokaler Wissenschaftler\*innen -- die Stadt in Klimafragen einige Male pro Jahr wissenschaftlich beraten soll. Professor Wolfgang Ertel, der auch am Samstag eine Rede hielt, bezeichnete das Verhalten der Stadt daher auch als "kriminell".

Wir planten daher, in der Nacht von Freitag (14.5.) auf Samstag (15.5.) mehrere Baumhäuser im Schussenpark zu errichten. Da wir diesen Plan über Instagram veröffentlichten, verhinderten Polizeistreifen allerdings dieses Vorhaben. Gegen Ende der Nacht entstand dann spontan die Idee, zumindest eine Bannertraverse über die Schussenstraße zu spannen, um den fließenden Verkehr auf die mobilitätspolitischen Verfehlungen unserer Stadtregierung hinzuweisen.

Die drei Aktivist\*innen hingen weit oberhalb des Lichtraums der Straße. Die Behörden hätten sich davon überzeugen können, dass Banner und Aktivist\*innen verkehrssicher fixiert waren und hätten nicht nur Busse, sondern auch den Individualverkehr durchleiten können. Allenfalls wäre eine Geschwindigkeitsbegrenzung geboten gewesen, eine Sperrung war unnötig.

Nach erfolgter Straßensperrung begaben sich weitere Aktivist\*innen sowie mehr und mehr Anwohner\*innen und Familien mit ihren Kindern auf die Schussenstraße und spielten oder malten mit Straßenkreide. Die autobefreite Schussenstraße wurde so für einige Stunden zu einem Symbol der Mobilitätswende. Eine Dauerlösung ist die Sperrung der Schussenstraße freilich nicht; dafür benötigt es eine kluge und sozial gerechte Planung des öffentlichen Raums.


## 3. Stellungnahme zu einzelnen Punkten des offenen Briefs von Polizeipräsident Uwe Stürmer

**3.1. Wir bedauern, dass wir Versammlungen nicht im Vorfeld anmelden können
und so der Polizei unnötig Arbeit machen.** Das Ordnungsamt hatte uns zuvor schon unmissverständlich mitgeteilt, dass es Versammlungen auf Bäumen grundsätzlich nicht genehmigen werde. So gab es im Vorfeld keine Kooperationsbasis. Übrigens verwunderte die Wortwahl unsere Anwält\*innen, da Versammlungen in Deutschland nicht genehmigungspflichtig sind.

**3.2. Am Samstagmorgen waren die Fronten schnell verhärtet.**
Das begann in der Früh, als die Polizei eine 15-jährige Aktivistin verhaftete, die gerade zu der Versammlung im Schussenpark dazustoßen wollte, die sich nach der Straßensperrung spontan bildete. Konfiszierung von Klettermaterial ohne Protokollaushändigung und der spätere Zwang, dass die drei verhafteten Aktivist\*innen die Zellennacht nur in Unterhose bekleidet schlafen durften, trugen nicht zur Entspannung bei. Eine der drei war eine Frau, auch ihr wurden keine weiteren Kleidungsstücke zugestanden.

**3.3. Banner im Altdorfer Wald.**
Die meisten Banner in der Altdorfer Waldbesetzungsgemeinschaft thematisieren direkt die umstrittenen Rodungspläne oder Klimagerechtigkeit und unmittelbar zusammenhängende Themen wie Feminismus und Antifaschismus. Zum Muttertag hängten wir auch ein Banner mit der Aufschrift "Danke Mama. Danke Erde. Wir werden immer für euch da sein" auf -- das wurde bislang nicht öffentlich rezipiert.

Einzelne Banner zeigen auch Aufschriften wie "No Cops". Hintergrund sind die teilweise traumatischen Erfahrungen, die letztes Jahr viele Aktivist\*innen bei ihren (Wochenend-)Besuchen im Dannenröder Wald in Hessen machten. Dort wurden etwa inhaftierten Frauen, die ihre Periode hatten, Hygieneprodukte verweigert. Einige Male durchtrennten Polizeikräfte sogar klar gekennzeichnete Sicherungsseile, woraufhin Aktivist\*innen zu Boden stürzten und längere Zeit im Krankenhaus behandelt werden mussten -- in manchen dieser Fälle ermittelt nun die Staatsanwaltschaft.

Wir möchten den Altdorfer Wald als den friedfertigen Ort bewahren, der er momentan ist, und eine Eskalation in jedem Fall vermeiden.

**3.4. Wir leisten zivilen Ungehorsam.**
Um die Öffentlichkeit nachhaltig darüber zu informieren, dass Ravensburgs Stadtregierung entgegen ihrer Beteuerungen noch keinen sozial gerechten Pfad zur 1,5-Grad-Grenze eingeschlagen hat, sind die Leser\*innenbriefe und Petitionen der vergangenen Jahre leider unzureichend. Dafür ist das Thema zu groß und der Zeitdruck, den die physikalischen Gegebenheiten der Klimakrise herstellen, zu hoch. Wir müssen daher auch zivilen Ungehorsam leisten. Eine gewisse Grenzüberschreitung ist dafür nötig, doch von kriminellen Aktionen distanzieren wir uns in aller Deutlichkeit. Die wird es von uns niemals geben.

**3.5. Besondere Situation von aktivistischen Lehramtsstudent\*innen.**
Viele von uns Aktivist\*innen sind Lehramtsstudent\*innen und nehmen in Kauf, dass ein Akteneintrag im Nachgang einer Aktion unsere Zukunft als Lehrkraft unumkehrbar zunichte machen könnte. Eine solch drastische Konsequenz erachten wir für Engagement um die Zukunft der Kinder als unverhältnismäßig. Um die Identitätsfeststellung zu erschweren, tragen daher manche von uns Sekundenkleber auf den Fingerkuppen auf. Einen echten Schutz bietet dieses Ritual freilich nicht, denn auch in Polizeirevieren gibt es Nagellackentferner.
