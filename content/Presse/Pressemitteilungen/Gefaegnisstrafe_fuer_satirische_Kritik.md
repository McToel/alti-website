---
title: "Gefängnisstrafe für satirische Kritik an Ausnahmegenehmigung für Lohwald-Rodung"
date: 2023-10-18T18:30:27+02:00
type: post
draft: false
---

# Gefängnisstrafe für satirische Kritik an Ausnahmegenehmigung für Lohwald-Rodung

**Richter hätte noch strenger verurteilen wollen, kritisiert Fridays for
Future**

[Fotos zur freien Verwendung](https://www.speicherleck.de/iblech/stuff/.rvs2)

[Umstrittene Ausnahmegenehmigung für die umstrittene Lohwald-Rodung im Volltext](https://www.lohwibleibt.de/)

Am heutigen Mittwoch (18.10.2023) standen die Ravensburger Klimaaktivist\*innen Samuel Bosch
(20) und Charlie Kiehne (21) in Augsburg vor dem Landgericht. Sie hatten im
Oktober 2022 gemeinsam mit dem ebenfalls Beschuldigten Ingo Blechschmidt (35)
satirische Kritik an der Fassade der Regierung von Schwaben platziert (siehe
unten stehende Pressemitteilung vom 17.10.2023). Der Vorsitzende Richter
bekräftigte nun das Urteil des Amtsgerichts aus dem Juni und verwarf die
Berufung. In seiner Urteilsbegründung bedauerte er, dass er keine noch höhere
Strafe verhängen könnte – das Verschlechterungsverbot verhinderte das, weil die
Staatsanwaltschaft vor Prozessbeginn ihre Berufung zurückgezogen hatte. Weil
das Jugendstrafrecht keine dritte Instanz vorsieht, sind Samuel Bosch und
Charlie Kiehne damit rechtskräftig zu drei bzw. einer Woche Gefängnis                                   
(vollstreckt als Jugendarrest, nicht zur Bewährung ausgesetzt) verurteilt.
Blechschmidt erwartet nun ebenfalls eine vollstreckte Haftstrafe, das Urteil
dazu wird am 26.10.2023 fallen. Weil bei ihm Erwachsenenstrafrecht zur
Anwendung kommt, werden es bei ihm voraussichtlich mehrere Monate.

Zentral ging es im Prozess um die Banneraufschriften "Lohwald-Rodung trotz
laufender Gerichtsverfahren? Frech!" und "Lohwald verhökern für 250 €? Frech!",
die zur Begründung des Straftatvorwurfs § 188 StGB (üble Nachrede bei Personen
des politischen Lebens) herangezogen wurden.

Anwohner\*innen des Lohwalds, die den Prozess verfolgten, kündigten direkt nach
der Verhandlung an, eben jene Bannersprüche auch bei der besonderen
Fridays-for-Future-Demonstration am 22.10.2023 (11:00, Augsburger Innenstadt)
zum Thema "Ein Jahr nach der vorgezogenen Lohwald-Rodung: Wird die Regierung
von Schwaben in Zukunft obergerichtliche Rodungsprüfungen respektieren?" zu
präsentieren. Ein Anwohner (knapp 60 Jahre, möchte seinen Namen nicht in der
Zeitung lesen) erklärte: "Erst die Ausnahmegenehmigung der Regierung von
Schwaben öffnete der Lohwald-Rodung Tür und Tor. Ohne sie hätten die
Lech-Stahlwerke die laufende obergerichtliche Prüfung ihres Rodungsvorhabens
abwarten müssen." Insofern seien die verfahrensgegenständlichen Äußerungen von
der Meinungsfreiheit gedeckt. Der Bayerische Verwaltungsgerichtshof legt bei
umstrittenen Rodungsvorhaben immer wieder die Notbremse ein, bewahrte in einem
[viel beachteten
Urteil](https://www.sueddeutsche.de/muenchen/vgh-entscheidung-kiesabbau-waldrodung-planegg-1.6093536)
zuletzt einen Bannwald bei Planegg (München) vor der Rodung. Entsprechend
könnte die Fridays-for-Future-Demonstration diesen Sonntag von den Behörden
gestoppt werden.

In der Urteilsbegründung äußerte der Vorsitzende Richter zudem allgemeine
Kritik an Fridays for Future. Aktivismus bedrohe die Lebensgrundlagen von acht
Milliarden Menschen; man könne nicht alles verbieten. Kurz vor diesen Vorwürfen
hatten Bosch und Kiehne in ihrem letzten Wort betont, wie sie sich in ihren
Heimatstädten in verschiedene städtische Gremien und Projekte einbringen, um
konstruktiv an Lösungen zur Klimakrise zu arbeiten, zudem Müll sammeln, Bäume
pflanzen und Workshops an Schulen leiten. Ingo Blechschmidt, der für
verschiedene Klimainitiativen den Telefondienst macht, erklärt: "Immer wieder
rufen Bürger\*innen bei mir an, weil sie gehört haben, dass Aktivist\*innen alles
verbieten wollen würden. Das Gegenteil ist der Fall: Wir stehen für die
Rückkehr des 9-Euro-Tickets ein, für die Erlaubnis von Balkonsolaranlagen, für
die Schaffung eines sicheren Radwegenetzes. Letztendlich geht es uns darum,
dass das vom Bundestag demokratisch beschlossene Klimaschutzgesetz umgesetzt
wird."

Klaus Schulz, erfahrener Anwalt im Strafrecht und Rechtsbeistand von Bosch,
erklärte: "Aus württembergischer Sicht ist das Urteil definitiv überzogen –  in
Bayern sind die drauf wie's Messer. Samuel und Charlie sind kaum vorbestraft
und trotzdem wurde hier gnadenlos fast die Höchststrafe verhängt. Ich schätze,
das ist im Strafmaß fast Faktor drei im Verhältnis zu Baden-Württemberg". Auch
die Situation im Gerichtsgebäude sei in Bayern grundlegend verschieden. In
flughafenähnlichen Polizeikontrollen wurden der Öffentlichkeit nahezu alle
mitgeführten Gegenstände, wie zum Beispiel Bleistifte verboten. Ein Zuschauer,
der auf einem Blatt mitschreiben wollte, lieh von einem der Prozessbeteiligten
einen Stift, woraufhin der Staatsanwalt erbost aufsprang und laut rufend in den
Nachbarraum lief. Dem Zuschauer wurde angedroht, aus dem Saal verwiesen zu
werden, wenn er den Stift nicht umgehend zurückgebe. Auch der Angeklagte Samuel
Bosch musste die Verwendung eines Stifts erst beantragen.

Das Urteil überraschte Kiki Köffler (19), eine Freundin der drei Angeklagten,
im Verhältnis zu den Vorwürfen und der Tat. Schließlich sei der Protest
zielgerichtet gewesen und habe auch keine Dritten tangiert. Die satirische
Kritik habe nichts mit Straßenblockaden der Letzten Generation zu tun. Aus
ihrer Sicht füge es sich in die zunehmende Kriminalisierung von
Klimaschutzprotesten ein. Das wurde im vergangenen September von Amnesty
International in deutlichen Worten kritisiert. In Deutschland würden Proteste
vonseiten der Behörden mitunter als Bedrohung wahrgenommen, statt sie als
Kernelement eines lebendigen gesellschaftlichen Diskurses zu ermöglichen und zu
schützen, [heißt es in dem Bericht](https://netzpolitik.org/2023/interaktive-karte-amnesty-kritisiert-einschraenkung-der-versammlungsfreiheit-in-deutschland/).
Vielmehr bestünde in Deutschland die Gefahr, Menschen davon abzuschrecken, zu
demonstrieren.

Bei aller Kritik an der Haftstrafe erklärt Bosch: "Das eigentlich Schlimme ist
nicht die Haftstrafe, sondern die Klimazerstörung, die wir im globalen Süden
verursachen. Wenn alle Bürger der Erde darüber entscheiden könnten, würden bei
uns im globalen Norden keine Wälder mehr gerodet werden würden."

Auf das schwere Urteil reagierten die den Prozess verfolgenden Anwohner\*innen,
Unterstützer\*innen und Aktivist\*innen unterschiedlich. Blechschmidt, der sein
Urteil am 26.10.2023 erfahren wird, erklärt: "Heute wurden zwei meiner besten
aktivistischen Freund\*innen zu Gefängnis verurteilt. Charlie und Samuel sind
für mich inspirierende Vorbilder. Wenn auch nur ein paar mehr Menschen sich
gelegentlich aktivistisch engagieren würden, wäre die Welt ein ganzes Stück
besser." Köffle erklärte: "Das Urteil ist unnötig hart und zeigt, dass es dem
Gericht um Einschüchterung und die Verhinderung von Protest im Allgemeinen
geht. Eine Gefängnisstrafe für drei Stunden in einem Behördenflur. So ein
freches Urteil sucht seinesgleichen." Bosch erlebte Rückhalt von seiner
Familie, seine Eltern drückten aus, dass sie weiter hinter Samuel stehen und
sein Engagement zu schätzen wissen. Eine Anwohnerin und gläubige Christin
betete nach der Verhandlung und bat um Verzeihung für das Unrecht, das wir in
Deutschland zulassen würden. Aktivist\*innen aus dem Klimacamp erklärten, für
die Dauer der Haft ein Protestcamp vor der Regierung von Schwaben zu errichten.


# Pressemitteilung am 17.10.2023

## Nach satirischer Kritik wegen Sonder-Rodungsgenehmigung des Lohwalds: Klimaaktivist\*innen sollen endgültiges Urteil erfahren

Diesen Mittwoch (18.10.2023, 9:00 Uhr) stehen die Klimaaktivist\*innen Samuel
Bosch (20) und Charlie Kiehne (21) vor dem Augsburger Landgericht (Gögginger
Str. 101). Zusammen mit dem ebenfalls angeklagten Augsburger Mathematiker Ingo
Blechschmidt (35) sowie weiteren Mitstreiter\*innen hatte das Team im Oktober
2022 satirische Kritik an der Regierung von Schwaben geübt [4,5,6,7,8]: Diese
hatte den Lech-Stahlwerken eine Ausnahmegenehmigung erteilt und so der
umstrittenen vorgezogenen Teilrodung des an das Konzerngelände angrenzenden
Lohwalds Tür und Tor geöffnet [1,2,10]. "Frech", befand die Aktivistengruppe,
denn Bayerns höchstes Verwaltungsgericht war zu diesem Zeitpunkt noch mit der
rechtlichen Prüfung der Rodungsabsicht beschäftigt.

Für ihre satirische Kritik wirft ihnen die Staatsanwaltschaft Hausfriedensbruch
(§ 123 StGB) und gegen Personen des politischen Lebens gerichtete üble Nachrede
(§ 188 StGB, Mindeststrafe drei Monate) vor. Da vermutlich wie schon in der
ersten Instanz nach Jugendstrafrecht verurteilt wird, wird das Urteil vom
Mittwoch endgültig sein, eine Revision zum Oberlandesgericht ist unmöglich (§
55 JGG). Auf Grundlage der Parallelverhandlung von Blechschmidt (am 10.10.2023,
[16,17]) ist eine vollstreckte Haftstrafe dabei ein wahrscheinlicher Ausgang
des Prozesses.

Anders als Straßenblockaden wie etwa von der Letzten Generation tangierte der
Protest keine Dritten, sondern nur die Regierung von Schwaben als unmittelbar
verantwortliche Stelle: Ihre Kritik brachten die drei in einer Kletteraktion
über Pappschilder mit Aufschriften wie "Lohwald-Rodung trotz laufender
Gerichtsverfahren? Frech!" an der Fassade des Regierungsgebäudes an.

Obwohl der Protest keine Dritten tangierte, entsprach das Augsburger
Amtsgericht in der ersten Instanz am 27.6.2023 unter Vorsitz von
Jugendrichterin Sandra Mayer dem Antrag der Staatsanwaltschaft auf Haftstrafe
und verurteilte Bosch und Kiehne zu drei bzw. einer Woche Gefängnis (sog.
Jugendarrest, vollstreckte Haftstrafe ohne Bewährung in einer speziellen
Jugendarrestanstalt) [11,12,13,14,15]. "Richterin fehlt politische Reife",
befand damals ein Prozessbeobachter [15], nachdem Mayer immer wieder abwertend
über das geistige Innenleben der Angeklagten spekulierte und in der
Urteilsbegründung ihren persönlichen Unmut gegen Bosch und Kiehne preisgab.
Gegen die Haftstrafe gingen die beiden in Berufung – nun wird der Prozess vor
dem Landgericht neu aufgerollt.

"Weil wir uns friedlich für die Beachtung rechtlicher Prüfungen durch Bayerns
höchstes Verwaltungsgericht eingesetzt haben, möchte uns die Staatsanwaltschaft
am liebsten wegsperren. Wem würde das nützen außer Großkonzernen wie den
Lech-Stahlwerken, die dann weiter ungehindert unsere Lebensgrundlagen zerstören
können?", fragt Bosch.


## Hintergründe

Der Lohwald ist ein nach dem Bayerischen Waldgesetz geschützter Bannwald bei
Biberbach und Langweid.

Ein öffentlich-rechtlicher Vertrag zwischen den Lech-Stahlwerken und der
Gemeinde Meitingen, die über den Wald verfügt, gestattete eine Teilrodung erst
nach erfolgreichem Abschluss gewisser artenschutzrechtlicher
Sicherungsmaßnahmen zur Rodungssaison im Herbst 2023 [10].

Diesen Vertrag ließen die an den Wald angrenzende Gemeinde Biberbach sowie der
BUND Naturschutz beim Bayerischen Verwaltungsgerichtshof in München prüfen.
Diese Prüfung dauert bis heute an. Die beiden Normenkontrollklagen entfalteten
keinen einstweiligen Rechtsschutz, da vermeintlich keine Eilbedürftigkeit
vorlag.

Diesen Umstand nutzten die Lech-Stahlwerke aus. Diese wollten die Attraktivität
ihres Betriebsgeländes durch die Rodung steigern – schwer ausbeutbarer
Waldboden sei weniger wertvoll als teure Industriefläche. Sie beantragten am
4.10.2022 eine Ausnahmegenehmigung bei der Regierung von Schwaben, um die
Rodung ein ganzes Jahr vorziehen zu können und so ein mögliches
Waldschutzurteil des Bayerischen Verwaltungsgerichtshof zu umgehen.

Nach nur wenigen Tagen Bearbeitungszeit erteilte die Regierung von Schwaben für
eine Routinegebühr von 250 € diese Genehmigung (Volltext der Genehmigung
verfügbar auf lohwibleibt.de). Und das, ohne die angrenzenden Gemeinden oder
den BUND Naturschutz darüber in Kenntnis zu setzen – diese hätten wegen der
dann bestehenden Eilbedürftigkeit einstweiligen Rechtsschutz beantragen können.
Thomas Frey, der Regionalreferent des BUND Naturschutz, sprach gegenüber BR von
einer "perfiden Aktion" und einer "Watschen für die engagierte Bürgerschaft"
[1].


## Hinweise

1. Samuel Bosch und Charlie Kiehne sind, so wie Ingo Blechschmidt, im Einklang
mit früherer Berichterstattung mit der Nennung ihrer vollen Namen
einverstanden. Bosch und Kiehne leben seit zwei Jahren im rodungsbedrohten
Altdorfer Wald bei Ravensburg. Zusammen mit anderen Klimaaktivist\*innen sowie
Anwohner\*innen errichteten sie dort nach dem Vorbild der erfolgreichen
Besetzung des Hambacher Walds mehrere Baumhausdörfer. Bosch und Kiehne kommen
zentral in dem Dokumentarfilm "Von Menschen, die auf Bäume steigen" der
Berliner Regisseure Bernadette Hauke und Christian Fussenegger vor [18].

2. Der zweite Prozesstag von Blechschmidt findet am 26.10.2023 um 9:00 Uhr
statt. Der erste Prozesstag am 10.10.2023 wurde nach einigen Stunden
Verhandlung unterbrochen. Wie Bosch und Kiehne ist auch bei Blechschmidt eine
Haftstrafe wahrscheinlich, Staatsanwältin Dorn-Haag hatte schon in der ersten
Instanz sechs Monate gefordert und mit dieser Forderung auch ihre Berufung
begründet. Nach Befragung der Zeug\*innen hätte Richter Natale einige der
Vorwürfe einstellen können, doch tat es nicht.

3. Am 22.10.2023 veranstaltet Fridays for Future um 11:00 Uhr in der Augsburger
Innenstadt eine Demonstration anlässlich des einjährigen Jubiläums der
vorgezogenen Lohwald-Teilrodung. Verschiedene Redebeiträge, auch von
Anwohner\*innen und Sachverständigen, sind angekündigt.

4. Der Volltext der Ausnahmegenehmigung, die der vorgezogenen Rodung Tür und
Tor öffnete, ist unter lohwibleibt.de abrufbar. Diese Ausnahmegenehmigung
existiert in zwei Fassungen. Die eine ist auf den 14.10.2022 datiert und hatte
somit eine Bearbeitungszeit von 10 Tagen. Die andere (Seite 44 der
Gerichtsakte) ist auf den 17.10.2022 datiert und hatte somit eine
Bearbeitungszeit von 13 Tagen. Wieso es zwei Abfassungen der
Ausnahmegenehmigung gibt, konnte bislang nicht geklärt werden.


## Referenzen

[1] https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV\
[2] https://www.br.de/nachrichten/bayern/bannwald-rodung-bei-stahlwerken-vor-gericht,TQRhKFH\
[3] https://www.lohwibleibt.de/\
[4] https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html\
[5] https://www.augsburg.tv/mediathek/video/aerger-um-protest-ermittlungen-gegen-augsburger-klimaaktivisten/\
[6] https://www.sueddeutsche.de/bayern/augsburg-lech-stahlwerke-klimacamp-polizei-regierung-von-schwaben-1.5682862\
[7] https://www.zeit.de/news/2022-10/27/ermittlungen-nach-protestaktion-bei-regierungsbehoerde\
[8] https://www.daz-augsburg.de/90948-2/\
[9] https://www.sueddeutsche.de/muenchen/muenchen-iaa-klimaaktivisten-germering-1.5761640\
[10] https://www.forumaugsburg.de/s_5region/Bezirk/210722_kampf-gegen-die-rodung-des-lohwalds-durch-die-lechstahlwerke-meitingen/index.htm\
[11] https://www.augsburg.tv/mediathek/video/lohwald-protest-klimaaktivisten-zu-dauerarrest-verurteilt/\
[12] https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/klimaschuetzer-vom-bodensee-muessen-nach-protestaktion-in-arrest;art410936,11623797\
[13] https://www.augsburger-allgemeine.de/augsburg/prozess-in-augsburg-klimaaktivisten-muessen-nach-abseil-aktion-in-arrest-id66971786.html\
[14] https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/haftbefehle-gegen-klimaaktivisten-aus-ravensburg-100.html\
[15] https://www.kontextwochenzeitung.de/gesellschaft/642/richterin-fehlt-die-politische-reife-8971.html\
[16] https://www.augsburger-allgemeine.de/augsburg/augsburg-nach-protest-gegen-abholzaktion-klimacamper-erneut-auf-der-anklagebank-id68133166.html\
[17] https://www.sueddeutsche.de/bayern/augsburg-regierungsgebaeude-besetzung-blechschmidt-1.5763823\
[18] https://classic-ravensburg.klimacamp-augsburg.de/pressespiegel/schwaebische-1423309.jpeg
