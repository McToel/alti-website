---
layout: page
title:  "21.01.2022: Aktivist*innen und Bürger*innen verteilen \"gerettete\" Lebensmittel in Ravensburg"
date:   2022-01-21 13:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2201211
#          YYMMDDX
---

*Pressemitteilung vom 21. Januar 2022*

# Aktivist\*innen und Bürger\*innen verteilen "gerettete" Lebensmittel in Ravensburg

Sperrfrist Freitag (21.01.2022) 13:30 Uhr
{: .label .label-red }

Am Freitag dem 21.01.2022 verteilen Akivist\*innen und Bürger\*innen im Rahmen
einer angemeldeten  Versammlung von 13:30 - 16:00 Uhr Lebensmittel an
Passant\*innen in Ravensburg. Die Lebensmittel hatten sie zuvor aus Mülltonnen
von Supermärkten gerettet. \
Das Bündnis "Aufstand der letzten Generation" hat bereits zusammen mit
Expertinnen eine Gesetzesvorlage erstellt, die genauso von der Bundespolitik
verabschiedet werden könnte, um "die Problematik der Lebensmittelverschwendung
in den Griff zu bekommen". Außerdem hat das Bündnis lokale Gruppen aufgerufen
ihre eigenen Aktionen zum Thema zu organisieren. "Mit unserer aktion wollen wir
von unten Druck auf die Bundespolitik ausüben" so Aktivist Samuel Bosch (19) aus
Schlier.

Weltweit hungern rund 811 Millionen Menschen, dass ist fast das Doppelte an
Menschen, die in der EU tagtäglich nicht ihren Bedarf an Essen stillen können.
In Deutschland werden jährlich pro Person und Jahr 75 kg Lebensmittel
weggeschmissen, dass sind insgesamt 12 mio Tonnen pro Jahr in Deutschland. Auch
global gesehen tragen Industrienationen wie Deutschland den größten Anteil an
der Verschwendung. Gleichzeitig sind diese am wenigsten von Hunger betroffen.
"Es kann nicht sein, dass wir Lebensmittel wegschmeißen und andere Menschen
stattdessen hungern"so Charlie Kiehne (19).   
Geretteten Lebensmittel werden auch als containerte Lebensmittel bezeichnet.
Dabei öffnen Aktivistinnen die Container der Supermärkte, um noch tadellose
Lebensmittel zu retten. Doch Aktivist\*innen drohen empfindliche, juristische
Strafen, denn containern ist in Deutschland immer noch Straftat und wird als
Diebstahl  oder sogar schwerer Diebstahl geahndet. "Es ist verrückt, was wir in
diesen Tonnen finden: Teilweise Säcke voll Äpfel, Kartoffeln, Karotten alle noch
in einwandfreiem Zustand. Oft finden wir auch zum Beispiel Joghurt  bei dem das
Mindesthaltbarkeitsdatum noch nicht abgelaufen ist oder ganze Kisten voll
Schokolade." so die Aktivistin Maraike Siebert (23) aus Ravensburg. Dabei sind
die einzelnen Supermärkte nicht in der Hauptverantwortung, denn auch sie sind an
Regelungen gebunden, wie beispielsweise das Mindesthaltbarkeitsdatum und die
Qualität der Produkte. Deswegen bedarf es laut der Aktivist\*innen vor allem
allgemeiner, bundesweiter Regelungen um die Verschwendung von Lebensmitteln zu
bekämpfen.

"Vielen Menschen ist überhaupt nicht bewusst, wie viel Essen tatsächlich in den
Tonnen landet." so die Aktivist Samuel Bosch (19). "An den Reaktionen der
Menschen, welche meist Fassungslosigkeit angesichts dieser Verschwendung zeigen,
sehen wir, dass unsere Aktion Früchte trägt." fügt Mitaktivistin Charlie Kiehne
(19) hinzu.