---
title: "Klimaschutz ist Heimatschutz - für ALLE"
date: 2025-02-20T22:42:00+01:00
type: post
draft: false
img:
    src: img/banner_untertor.jpg
    alt: Ein Banner mit der Aufschrift "Klimaschutz ist Heimatschutz für ALLE"
---
## Spektakuläres Banner am Ravensburger Untertor
3 Tage vor der Bundestagswahl platzieren Klimaaktivist\*innen durch eine gewagte Kletteraktion ein 20 Meter langes Spruchbanner am Untertor in Ravensburg. An dem 36 m hohen Turm steht seit heute Morgen: "Klimaschutz ist Heimatschutz für ALLE". Die Kletter\*innen wollen damit darauf aufmerksam machen, dass die Klimakrise weiterhin ein wichtiges und bislang ungelöstes Thema ist und bei der Bundestagswahl "gefährlich" unter den Tisch gefallen ist.

## Bildstarker Protest gegen rechte Diskursverschiebung 

Der bunte Protest problematisiert die gefährliche Diskursverschiebung, die aktuell politische Debatten vor der Bundestagswahl dominiert: "Während Merz, Scholz und Co nur weiter "die Ausländer" für alles verantwortlich machen, wird die eskalierende Klimakrise in naher Zukunft eine der Haupt-Fluchtursachen sein, wenn wir es nicht schaffen, das Klimaschutz-Ruder noch in diesem Jahrzehnt herumzureißen." so Samuel Bosch, einer der Kletter\*innen.
Ronja May ergänzt: "Seit den Hochwassern und Hitzewellen der letzten Jahre ist klar: Wir müssen jetzt aktiv werden und bei der Bundestagswahl ist die Klimakrise zentral. Es kann nicht sein, dass dieses extrem dringende Thema aus Talkshows und sogar dem Kanzlerduell in ZDF/ARD einfach verschwindet – in der Realität tut sie das nämlich nicht, sondern wird immer dringender."

## Die Klimakrise eskaliert Kriege und soziale Ungerechtigkeit

Klimaforscher\*innen und NGOs warnen schon seit Jahrzehnten davor, dass es einen kausalen Zusammenhang zwischen den Folgen der Klimakrise, Konflikten und Migration gibt [^1]. Durch Extremwetterereignisse und Trockenheit kommt es zu Ernteausfällen und Hunger, woraus Konflikte und Kriege entstehen [^2]. 
Die Klimakrise verschärft auch bereits bestehende Ungerechtigkeiten und schädigt die Wirtschaft [^3]. "Der Wiederaufbau von durch Klimakatastrophen zerstörte Häuser und Äcker ist teurer als jede Klimamaßnahme." so Ronja May.

"Die Klimakrise sorgt nicht nur für einen Anstieg des Meeresspiegels und der globalen Durchschnittstemperarur, sondern verschärft und multipliziert so ziemlich alle Krisen und Konflikte, die im Zukunft entstehen werden. Die Politik muss endlich Verantwortung für selbstgesetzte Klimaziele übernehmen, sonst müssen wir bald auch wegen Hochwasser auf Ravensburgs Türme steigen", so Bosch.

-------------

## <a href="/presse"> Pressekontakt </a>

[^1]: https://www.sueddeutsche.de/wissen/klimawandel-fluechtlinge-migration-1.4299438
[^2]: https://unric.org/de/steigender-hunger-auf-der-welt-kriege-und-klimachaos-als-hauptfaktoren/
[^3]: https://climate.ec.europa.eu/climate-change/consequences-climate-change_de