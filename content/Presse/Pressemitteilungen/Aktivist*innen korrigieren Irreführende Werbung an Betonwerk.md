---
title: "Aktivist*innen Korrigieren Irreführende Werbung an Betonwerk"
date: 2023-04-11T11:05:52+02:00
img:
    src: img/betonwerk_korrektur.png
    alt: Übermalte Werbung weißt nun darauf hin, dass Zement zwangsweise CO2 freisetzt
type: post
draft: false
---

<!-- # Aktivist*innen Korrigieren Irreführende Werbung an Betonwerk

{{< img src="img/betonwerk_korrektur.png" alt="Übermalte Werbung weißt nun darauf hin, dass Zement zwangsweise CO2 freisetzt" >}} -->

Am 9. April während der Mittagszeit haben Klimaaktivisten ein Werbebanner der Baufirma "Alfons Leuthe" in Wetzisreute beschrieben und damit auf die klimaschädlichen Auswirkungen der Baubranche aufmerksam gemacht. Die Aktivisten betonen, dass die Baufirma nicht klimafreundlich, sondern klimaschädlich agiert, wie im Bild zu sehen ist.

Um ihre Botschaft zu verdeutlichen, haben die Klimaaktivisten die vereinfachte chemische Formel der Zementherstellung ergänzt, bei der große Mengen an CO2 freigesetzt werden. Diese Freisetzung trägt maßgeblich zur Klimaerwärmung bei und ist somit ein wesentlicher Faktor im Kampf gegen den Klimawandel. Obwohl Beton nur für sehr wenige Bauten wirklich  notwendige ist und Obwohl es alternative CO2-neutrale Zementherstellungsmethoden gibt, werden diese von der Firma "Alfons Leuthe" nicht angewendet. „Ganz im Gegenteil zündet sie eine Nebelkerze und betont die angebliche Klimafreundlichkeit des Unternehmens mit der grundsätzlich löblichen, aber in der Bilanz kaum relevanten, Nutzung lokaler Ressourcen.“ so der an der Aktion beteiligte Akif Reemsund (26).

Der CO2-Fußabdruck des Betons ist größer als allgemein angenommen. Etwa 8% der weltweiten Emissionen entfällt auf sie. Dies zum einen weil die Herstellung sehr energieintensiv ist. Jedoch mengenmäßig noch entscheidender ist andererseits das Freiwerden exogenen CO2 bei der Zementherstellung aus dem Kalkstein. Hinzu kommt die Rodung klimaschützender Wälder für den Kies- und Kalkabbau, das gerade in unserer Region ein umstrittenes Thema ist.

Dabei gibt es viele Möglichkeiten die klimaschädliche Zementherstellung zu reduzieren, beispielsweise mit anderen Bindemitteln und der Vermeidung von Neubauten und nicht zuletzt die Verwendung anderer Baumaterialien.

Die Aktivistin Anna Weber (22) begründet ihre Aktion so: "Wir müssen endlich aufhören, die Augen vor den Auswirkungen unserer Handlungen auf das Klima zu verschließen. Die Baubranche ist einer der größten Verursacher von Treibhausgasemissionen und trägt damit maßgeblich zur Klimakrise bei. Wir fordern die Baufirma auf, Verantwortung zu übernehmen und sich für den Klimaschutz einzusetzen. Es ist an der Zeit, dass wir gemeinsam für eine nachhaltigere Zukunft kämpfen."

Die Klimaaktivisten sind entschlossen, weiterhin auf die dringende Notwendigkeit des Klimaschutzes aufmerksam zu machen und werden auch in Zukunft aktiv bleiben, um eine klimafreundlichere Zukunft zu gestalten.

## <a href="/presse"> Pressekontakt </a>