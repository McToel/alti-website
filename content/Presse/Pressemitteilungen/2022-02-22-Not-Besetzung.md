---
layout: page
title:  "22.02.2022: Neue Not-Besetzung aufgrund akuter Rodungsgefahr - Kampf um jeden Baum - geplante Kiesgrube bei Oberankrenreute"
date:   2022-02-22 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2202220
#          YYMMDDX
---

*Pressemitteilung aus dem Altdorfer Wald am 22.02.2022*

# Neue Not-Besetzung aufgrund akuter Rodungsgefahr - Kampf um jeden Baum - geplante Kiesgrube bei Oberankrenreute

---
***Pressekonferenz am heutigen Dienstag (22.2.2022) um 14:00 Uhr direkt in der
neuen Not-Besetzung***

---
Am heutigen Dienstagmorgen (22.2.2022) besetzen junge und alte Menschen ein Waldstück bei der geplanten Kiesgrubenvergrößerung bei Oberankenreute. Anlass ist, dass für dieses -- anders als für das Waldstück, das seit einem Jahr durch Klimaaktivist\*innen besetzt ist -- akute Rodungsgefahr besteht. Denn noch in dieser Rodungssaison (bis 28.2.2022, danach dürfen in Deutschland bis Oktober aufgrund von Brutzeit keine Wälder gerodet werden) soll das Waldstück gerodet werden. Die Aktivist\*innen spannen Hängematten in 4 bis 20 Meter Höhe, bauen ein komplexes Traversennetzwerk und verkünden mit Spruchbannern eine Not-Waldbesetzung.

[Da neue Markierungen an Bäumen, entsprechend der Umrisse der vergrößerten
Kiesgrube, aufgetaucht sind, vermuteten die Aktivist\*innen einen Rodungsbeginn
direkt am gestrigen Montag. Diese fand nicht statt, vermutlich aufgrund des
Sturms. Daher ist die Wahrscheinlichkeit hoch, dass am heutigen Dienstag gerodet
wird. Aktuelle Informationen zum Geschehen vor Ort gibt Samuel Bosch (+49 1590
8156028).]

Nach Aussage der Aktivist\*innen behaupten die Kiesfirmen immer wieder,
lediglich die Nachfrage nach Kies aus der Region zu bedienen. "Tatsächlich blieb
die Nachfrage in den letzten Jahren trotz Bauboom aber deutlich hinter den
Erwartungen zurück. Tullius vergrößert die Kiesgrube trotz ausbleibender
Nachfrage. Dem Konzern geht es nur darum, Profitrechte für die nächsten
Jahrzehnte auf Kosten der Umwelt zu sichern. Das ist ein Skandal!" so Anwohner
Martin Lang (55) aus Oberankenreute Laut Recherchen von SPD und des Altdorfer
Wald e.V. wird der Kies zu substanziellen Teilen in die Schweiz und nach
Österreich exportiert, wo Umweltauflagen den Kiesabbau im Vergleich zu
Baden-Württemberg unrentabler machen.

"Wir haben Angst um das Grundwasser, die Luftqualität und das Weltklima", so
Kletteraktivist Samuel Bosch (19) aus Schlier. "Wir werden nicht locker lassen,
bis das Thema Kiesabbau auf das ökologisch notwendige Maß reduziert ist."
ergänzt er. Dabei lässt er das Argument des Landratsamts zur Genehmigung der
vergleichsweise kleinen Kiesgrubenerweiterung nicht gelten: "Wir sehen doch in
den Gruben Grenis und Molpertshaus, dass stets mit kleinen Gruben begonnen wird
und Schritt für Schritt weitere Vergrößerungen beantragt und genehmigt werden.
Damit wird die Öffentlichkeit hintergangen, denn die großen Gruben sind von
Anfang das Ziel." Das Landratsamt gibt an, auf Grundlage eines zwar hauchdünnen,
aber positiven Beschlusses der Gemeinde Schlier entschieden zu haben. Jedoch ist
sich der Gemeinderat Schlier sicher, dass das Landratsamlt sowieso die
Abbaugenehmigung beschlossen hätte. Deswegen vermutet auch
Gemeinderatsmitglieder eine strategische Manipulation der Öffentlichkeit und
enthielt sich aus Protest der Abstimmung. Insgesamt ist der Gemeindebeschluss
zur neuen Kiesgrube mit 7 Ja und 6 Enthaltungen denkbar knapp ausgegangen. "Das
zeigt, dass der ehemals große regionale Rückhalt des Kiesabbaus schwindet." fügt
Kletteraktivist\*in Charlie Kiehne (19) aus Ulm hinzu. (Kiehne kann leider nicht
Vorort sein da auch in Ulm ein Waldstück gerodet werden soll.)

Dazu wird nach dem Willen der Baumkletter\*innen auch diese Besetzung beitragen.
Sie sind mit Schlafsäcken, Baumaterial, Essen, warmer Kleidung und Spruchbannern
ausgestattet und verlangen nach einem Gespräch mit dem Landrat und einer
Erklärung, weswegen im Zeitalter der Klimakrise immer noch Wälder gerodet
werden. "Die Rodungssaison geht nur noch genau eine Woche. Im Alti haben wir
sogar den Winter überdauert. Wir werden dem Wald bis Oktober Zeit verschaffen,
und im Oktober wird bereits ein ganz anderer politischer Wind wehen!", so Bosch.

## Ort
Kiesgrube "Tullius" bei Oberankenreute an der L317 (Auf der L317 von Weingarten
kommend direkt nach der Bestehenden Kiesgrube rechts. Das Waldstück zwischen
Grube und Waldwegeingang)

Koordinaten: 47.798154,9.72478