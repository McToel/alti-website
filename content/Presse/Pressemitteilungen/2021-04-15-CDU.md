---
layout: page
title:  "15.04.2021: Klimagerechtigkeitsaktivist*innen werfen mit Banner am Kreisverbandsgebäude CDU Korruption und Klimazerstörung vor"
date:   2021-04-15 08:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2104150
#          YYMMDDX
---

*Pressemitteilung der Altdorfer Waldbesetzung am 15. April 2021*

# Klimagerechtigkeitsaktivist\*innen werfen mit Banner am Kreisverbandsgebäude CDU Korruption und Klimazerstörung vor

Sperrfrist Freitag (16.4.2021) 7:00 Uhr
{: .label .label-red }

In der Nacht vom Donnerstag (15.4.) auf den Freitag (16.4.) brachten
Klimagerechtigkeitsaktivist\*innen ein Banner am Bürogebäude des
CDU-Kreisverbands Ravensburg (Bahnhofstraße 8, 88250 Weingarten)
mit der Aufschrift "76 Jahre Korruption & Klimaschmutz! Weg mit dem
Klima-Höllen-Plan!" an.

"Die CDU gibt es seit 76 Jahren, und seit 76 Jahren prägt Korruption die CDU",
begründet Aktivistin Emma Stadlbauer (17) die Aktion. "Die CDU betreibt mit ihrer
undemokratischen Dominanz im Regionalverband Klimazerstörung ungeahnten
Ausmaßes." Stadlbauers Kollege Kim Schulz (24) ergänzt: "Das größte
Straßenneubauprojekt der letzten Jahrzehnte auf den Weg zu bringen, wertvolle
Wälder zu roden und Flächen zu versiegeln -- das haben die allen ernstes vor!"

"Jeder Verstand, jedes wissenschaftliche Gutachten [1] spricht gegen den
Regionalplanentwurf. Nur mit Korruption ist das Verhalten der CDU erklärbar",
so Stadlbauer. Wie die SPD kürzlich veröffentlichte [2], ist das
Kiesabbauunternehmen Meichle+Mohr keinesfalls ein kleines eigenständiges
Familienunternehmen, sondern hat Verflechtungen bis nach Russland.

[1] https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf<br>
[2] https://satiresenf.de/ts26-21-regionalplan-und-so-kiesabbau-grenis-ein-russischer-oligarch-putin-navalny-und-trump/


## Fotos zur freien Verwendung

Hier herunterladen: https://www.speicherleck.de/iblech/stuff/.cdu-rv
(wird im Laufe des Morgens gefüllt)


## Kontakt

Ingo Blechschmidt (+49 176 95110311) stellt gerne den Kontakt zu den
Waldbesetzer\*innen her. Es gibt keine autorisierte Gruppe und kein
beschlussfähiges Gremium, das "offizielle Gruppenmeinungen" für die
Besetzung beschließen könnte. Die Menschen in der Besetzung und ihrem
Umfeld haben vielfältige und teils kontroverse Meinungen. Diese
Meinungsvielfalt soll nicht zensiert werden, sondern gleichberechtigt
nebeneinander stehen. Kein Text und keine Aktion spricht für die ganze
Besetzung oder wird notwendigerweise von der ganzen Besetzung gut geheißen.
