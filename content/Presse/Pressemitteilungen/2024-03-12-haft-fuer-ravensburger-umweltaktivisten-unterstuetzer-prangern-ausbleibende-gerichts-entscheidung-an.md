---
# Pressemitteilung automatisch erzeugt von pm-bot 5a8f785 am Donnerstag, 28. März 2024 um 12:31:44 GMT+1
title: "Haft für Ravensburger Umweltaktivisten – Unterstützer prangern ausbleibende Gerichts-Entscheidung an"
date: 2024-03-12T12:00:00.000Z
draft: false
---

Pressemitteilung vom 12.03.2024  
  
# Haft für Ravensburger Umweltaktivisten – Unterstützer prangern ausbleibende Gerichts-Entscheidung an
  
Am Mittwoch (13.03.2024) protestieren zahlreiche Unterstützer\*innen der Umweltaktivisten aus dem Altdorfer Wald von 17 bis 19 Uhr gegen eine ausbleibende Entscheidung des Ravensburger Amtsgerichts über die Haft von Samuel Bosch, die seit 3 Wochen aussteht. Ohne diese Entscheidung muss Bosch vom 14.3.-4.4. In Juggendarrest nach Göppingen. Die Kundgebung wurde sehr kurzfrisig organisiert, da bis Dienstagmittag immer noch keine Entscheidung des Gerichts kam. Die Kundgebung findet direkt vor dem Amtsgericht in der Herrenstraße statt.  
  
Im Oktober 2023 wurde Bosch wegen übler Nachrede des mittlerweile beförderten Präsidenten der Regierung von Schwaben in Augsburg zu drei Wochen Haft (Jugendarrest) verurteilt. Gemeinsam mit Charlie Kiehne und Dr. Ingo Blechschmidt hing er im Rahmen einer Kletteraktion ein Banner an die Gebäudefassade. "**Lohwald-Rodung genemigen troz laufender Gerichtsverfahren? Frech!**" war so in großen Lettern zu lesen. Das Banner bezieht sich auf einen nach dem Bayerischen Waldgesetz besonders geschützten Bannwald bei Biberbach, der damals trotz laufender Normenkontrollklage der Gemeinde für die Erweiterung eines Stahlwerks nahe Augsburg vorzeitig gerodet wurde. \[1,2,3\]  
  
Die Haft soll Bosch am Donnerstag 14.03.2024 für drei Wochen in Göppingen antreten. "Für mich ist es unsäglich, dass junge Menschen, die sich für unser aller Zukunft einsetzen und für die Lebensgrundlagen der Menschen im globalen Süden eintreten, heute weggesperrt werden sollen. Stattdessen sollten die angesprochenen Politiker endlich ins Handeln kommen", kritisiert Demo-Anmelder Martin Lang (56).  
  
Neben verschiedenen gesellschaftlichen Akteuren aus sozialen und ökologischen Bewegungen in der Region wird Bosch die Verbindungen zwischen der Aktion gegen das Stahlwerk in Augsburg und der Waldbesetzung im Altdorfer Wald gegen den Kies darlegen.  
  
"Stahl, Kies und Zement verursachen in der Bauindustrie die CO2-Emmissionen. Darum sind Widerstände gegen Stahl, Kies und Zement eng verbunden. Auch international reiht sich der Abbau von Ressourcen in der Region in den Extraktivismus den europäische Nationen im Globalen Süden betreiben ein. Wir bleiben aktiv und lassen uns nicht einschüchtern", erklärt Samuel Bosch (21).  
  
QUELLEN:  
\[1\]: [https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html](https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html)  
\[2\]: [https://www.kontextwochenzeitung.de/gesellschaft/642/richterin-fehlt-die-politische-reife-8971.html](https://www.kontextwochenzeitung.de/gesellschaft/642/richterin-fehlt-die-politische-reife-8971.html)  
\[3\]: [https://web.archive.org/web/20221030140916/https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV](https://web.archive.org/web/20221030140916/https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV)  
  
[https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/klimaaktivist-protest-ulm-gefaengnis-100.html](https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/klimaaktivist-protest-ulm-gefaengnis-100.html)  
  
\---  
  
BILDER ZUR FREIEN VERWENDUNG:  
[https://www.speicherleck.de/iblech/stuff/.ohifbver/](https://www.speicherleck.de/iblech/stuff/.ohifbver/)  
Fotos der Aktion, wegen der Bosch nun zur Haft verurteilt wurde: [https://speicherleck.de/iblech/stuff/.rvs2](https://speicherleck.de/iblech/stuff/.rvs2)  
  