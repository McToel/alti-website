---
# Pressemitteilung automatisch erzeugt von pm-bot 5a8f785 am Montag, 1. April 2024 um 15:47:48 GMT+2
title: "Karfreitagspicknick vor der Jugendarrestanstalt"
date: 2024-03-27T12:00:00.000Z
draft: false
---

Pressemitteilung vom 27.03.2024  
  
# Karfreitagspicknick vor der Jugendarrestanstalt
  
Eine Gruppe Aktivist\*innen lädt am Karfreitag (29.3.2024) um 16 Uhr zu einem gemeinsamen Solidaritätspicknick vor der Jugendarrestanstalt Göppingen ein. Sie möchten für die Gefangenen ein solidarisches Zeichen setzen und die langen, einsamen Feiertage verkürzen. Besonderer Anlass ist auch die Haft von Klimaaktivist Samuel Bosch, der aktuell für seine Kritik an einer bayrischen Bezirksregierung im Gefängnis sitzt \[1,2\].  
"Die Osterfeiertage, an denen die meisten Menschen sich mit ihren Familien treffen, in die Kirche gehen und die ersten Frühlingstage genießen, müssen die Gefangenen die meiste Zeit alleine in ihren Zellen verbringen", erklärt Sina Wagner (24), eine der Initiator\*innen. "Wir wollen heute zeigen, dass diese Menschen nicht alleine sind und nicht vergessen werden."  
Die Aktivist\*innen kritisieren auch die hohe Zahl armer Menschen in deutschen Gefängnissen, die wegen nicht gezahlter Geldstrafen oder wegen Schwarzfahrens einsitzen. In armen Großstädten wie Berlin sitzt jeder dritte Gefangene wegen Schwarzfahrens \[3\]. "Während sich wohlhabende Menschen auch bei schweren Straftaten einfach freikaufen können, werden arme Menschen, die von der Gesellschaft sowieso benachteiligt werden, wegen kleinen Geldstrafen ins Gefängnis gesteckt. Das ist nicht nur ungerecht, sondern bringt auch niemanden in irgendeiner Weise weiter", sagt Kiki Köffle (20), eine der Aktivist\*innen. Köffle und Wagner sind damit auf einer Linie mit dem ehemaligen Regierungsdirektor Thomas Galli, der nach einer Tätigkeit als Gefängnisleiter zum ausgesprochenen Kritiker des aktuellen Gefängnissystems wurde \[4\]. "Der institutionelle Gefängnisgedanke macht keinen Sinn", erklärte dieser gegenüber Deutschlandfunk \[4\].  
Am Freitag wollen die Aktivist\*innen nun ein Zeichen für Solidarität setzen und damit einen kleinen Beitrag leisten, um die Feiertage etwas heller zu gestalten. Speziell in Bezug auf die Inhaftierung von Samuel Bosch verweisen sie auf UN-Generalsekretär António Guterres, der erklärte: "Klimaaktivisten werden manchmal als ‚gefährliche Radikale‘ dargestellt. Aber die wirklich gefährlichen Radikalen sind die Länder, die die Produktion von fossilen Brennstoffen erhöhen" \[5a\] "Klimaaktivisten – angeführt von der moralischen Stimme junger Menschen – haben ihre Ziele auch in den dunkelsten Tagen weiterverfolgt. Sie müssen geschützt werden und wir brauchen sie jetzt mehr denn je." \[5b\]  
"Jedes ist herzlich eingeladen, etwas zum Picknick mitzubringen, sei es eine Decke zum Sitzen, Snacks zum Teilen oder Getränke zum Genießen! Eure Teilnahme bedeutet den Insassen viel und zeigt ihnen, dass sie nicht vergessen sind. Wir freuen uns darauf, euch dort zu sehen und gemeinsam ein Zeichen der Solidarität zu setzen!", schreibt die Gruppe auf Instagram \[6\].  
Wie ein Vertreter der Göppinger Polizei bei einem früheren Kooperationsgespräch explizit hinwies, besteht bei solchen Veranstaltungen grundsätzlich das Risiko der Verfolgung wegen "Verkehr mit Gefangenen" (§ 115 OWiG).  

QUELLEN:  
\[1\] [https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/klimaaktivist-protest-ulm-gefaengnis-100.html](https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/klimaaktivist-protest-ulm-gefaengnis-100.html)  
\[2\] [https://www.swp.de/lokales/goeppingen/klimaschuetzer-in-jugendarrestanstalt-\_luecke-im-terminplan\_-aktivist-samuel-bosch-hat-arrest-in-goeppingen-angetreten-73373989.html](https://www.swp.de/lokales/goeppingen/klimaschuetzer-in-jugendarrestanstalt-_luecke-im-terminplan_-aktivist-samuel-bosch-hat-arrest-in-goeppingen-angetreten-73373989.html)  
\[3\] Wegen Armut ins Gefängnis: Wie gerecht ist unsere Strafjustiz? | 11KM - der tagesschau-Podcast, [https://www.youtube.com/watch?v=McdzPMHNJtk](https://www.youtube.com/watch?v=McdzPMHNJtk)  
\[4\] [https://www.deutschlandfunk.de/gefaengnisreform-jurist-thomas-galli-ueber-eine-gesellschaft-ohne-knast-dlf-93066b4f-100.html](https://www.deutschlandfunk.de/gefaengnisreform-jurist-thomas-galli-ueber-eine-gesellschaft-ohne-knast-dlf-93066b4f-100.html)  
\[5a\] [https://www.rnd.de/politik/antonio-guterres-bezeichnet-neuen-klimabericht-als-dokument-der-schande-XLA2Z4M2HPT7WCMKNPVAYSNUMM.html](https://www.rnd.de/politik/antonio-guterres-bezeichnet-neuen-klimabericht-als-dokument-der-schande-XLA2Z4M2HPT7WCMKNPVAYSNUMM.html)  
\[5b\] [https://www.zeit.de/politik/ausland/2023-05/razzia-klimaschuetzer-un-schutz-antonio-guterres](https://www.zeit.de/politik/ausland/2023-05/razzia-klimaschuetzer-un-schutz-antonio-guterres)  
\[6\] Instagram: [https://www.instagram.com/baumbesetzung.ravensburg/](https://www.instagram.com/baumbesetzung.ravensburg/)  
  
BILDER  
zur freien Verwendung: [https://www.speicherleck.de/iblech/stuff/.kf](https://www.speicherleck.de/iblech/stuff/.kf) (wird im Laufe des Picknicks gefüllt)  
Fotos der Regierungskritik, wegen der Samuel Bosch in der JVA sitzt: [https://www.speicherleck.de/iblech/stuff/.rvs2](https://www.speicherleck.de/iblech/stuff/.rvs2)  
  