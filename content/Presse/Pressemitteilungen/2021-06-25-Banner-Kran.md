---
layout: page
title:  "25.06.2021: Klimacamper*innen kritisieren mit Banner am Aldi-Baukran in Baienfurt verantwortungslose Flächenversiegelung"
date:   2021-06-25 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2106250
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 25.6.2021*

# Klimacamper\*innen kritisieren mit Banner am Aldi-Baukran in Baienfurt verantwortungslose Flächenversiegelung

In der Nacht vom gestrigen Donnerstag auf heutigen Freitag (25.6.) brachten
Klimagerechtigkeitsaktivist\*innen in 25 Meter Höhe zwei Banner an einem Baukran
in Baienfurt an, wo gerade eine Aldi-Filiale entsteht: "Flächenversiegelung
stoppen", "Klimahöllenplan kippen".

"Wälder, Moore und Felder sind als CO₂-Senken unsere wichtigsten Verbündeten im
Kampf gegen die Klimakrise", erklärt Schüler Samuel Bosch (18). "Über die
nächsten Jahre müssen wir daher bereits versiegelte Flächen dringend wieder
entsiegeln. In Zeiten der Klimakrise neue Flächen für ein riesiges einstöckiges
Gebäude, das niemand benötigt, zu versiegeln, geht gar nicht."

Nach Aussage der Klimacamper\*innen ist die Mehrheit der Anwohner\*innen gegen
den Aldi-Neubau. "Aldi handelt mit dem Bauprojekt nicht im Sinne des
Gemeinwohls, sonndern folgt einzig und allein privaten Profitinteressen. Den
Raubbau an der Natur und unserer Zukunft kann die Greenwashing-Kampagne von
Aldi, auf dem Dach Solarzellen zu installieren, nicht verschleiern."

Die Klimacamper\*innen stellen sich aber auch allgemein gegen neue
Flächenversiegelung und den damit einhergehenden Entzug von Flächen für die
Landwirtschaft. "Verantwortungslose Flächenversiegelung", so Bosch, findet sich
auch im Regionalplanentwurf, der heute in Pfullendorf verabschiedet werden soll.
"Wenn die fast nur mit älteren Männern besetzte Verbandsversammlung in ihrer
zukunftsfeindlichen Ignoranz schon den unbezifferbaren Wert von unversiegelten
Flächen geringschätzt, tut sie gut daran, zumindest den Widerstand aus der
Zivilgesellschaft einzupreisen. Wenn der Regionalverband wirklich an seinen
Flächenversiegelungs- und Straßenneubauplänen festhalten wird, wird es auf den
Baustellen zu vielfältigen Aktionen des zivilen Ungehorsams kommen. In der
Überzeugung, dass es uns als Gesellschaft letztendlich nützt, werden wir diese
Bauprojekte richtig teuer machen."

## Ort
Ravensburger Str. 13 \
88255 Baienfurt \
47°49'29.0"N 9°38'56.9"E \
47.824708, 9.649137