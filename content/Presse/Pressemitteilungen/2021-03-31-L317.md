---
layout: page
title:  "31.03.2021: Klimagerechtigkeitsaktivist*innen machen mit Banner über Landstraße auf Klimazerstörung durch den RVBO aufmerksam"
date:   2021-03-31 01:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2103310
#          YYMMDDX
---

*Pressemitteilung der Altdorfer Waldbesetzung am 31. März 2021*

# Klimagerechtigkeitsaktivist\*innen machen mit Banner über Landstraße auf Klimazerstörung durch den RVBO aufmerksam

Sperrfrist Donnerstag (1.4.2021) 16:00 Uhr
{: .label .label-red }

Am morgigen Donnerstag (1.4.2021) spannen Aktivist\*innen der Altdorfer
Waldbesetzung in 14 Meter Höhe ein Banner über die angrenzende Landstraße L314,
um auf den "Klima-Höllenplan" des Regionalverbands Bodensee-Oberschwaben (RVBO)
hinzuweisen. "Wir lehnen den Regionalplanentwurf als ganzes ab", erklärt Samuel
Bosch (18) die Aktion. "Der Regionalverband und die CDU Ravensburg sollten sich
schämen, in Zeiten der Klimakrise nicht nur die Teilrodung des Altdorfer Walds,
sondern auch auf viele andere Arten Klimazerstörung ungeahnten Ausmaßes zu
planen."

Vorbereitungen für die Aktion beginnen um 14:00 Uhr mit der Errichtung von zwei
Traversen über die L314. Um 16:00 Uhr wird dann das Banner gehisst. Zu diesem
Zeitpunkt werden mehrere Aktivist\*innen in der Traverse hängen. Das verwendete
Traversenmaterial (PP-Splitfilm-Tauwerk mit Durchmesser 14 mm) hält Belastungen
von bis zu 3.000 kg stand. "Die Bannerbefestigung ist verkehrssicher", so
Bosch.


## Hinweis

Aktuelle Informationen über den Verlauf der Aktion gibt Ingo Blechschmidt (+48
176 95110311).


## Anfahrt

Auf dem Weg von Weingarten nach Wolfegg befindet sich das Baumhausdorf
kurz vor der Abzweigung Grund rechts im Wald. Parkplätze sind vorhanden.
Nah an der Besetzung hält die Buslinie 7534 (Haltestelle "Vogt Abzw.
Grund im Wald"). Koordinaten: 47.810973, 9.76126.
Karte: https://www.openstreetmap.org/node/8481053203


## Kontakt

Ingo Blechschmidt (+48 176 95110311) stellt gerne den Kontakt zu den
Waldbesetzer\*innen her. Es gibt keine autorisierte Gruppe und kein
beschlussfähiges Gremium, das "offizielle Gruppenmeinungen" für die
Besetzung beschließen könnte. Die Menschen in der Besetzung und ihrem
Umfeld haben vielfältige und teils kontroverse Meinungen. Diese
Meinungsvielfalt soll nicht zensiert werden, sondern gleichberechtigt
nebeneinander stehen. Kein Text spricht für die ganze Besetzung oder
wird notwendigerweise von der ganzen Besetzung gut geheißen.
