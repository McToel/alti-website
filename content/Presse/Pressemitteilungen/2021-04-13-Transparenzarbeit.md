---
layout: page
title:  "13.04.2021: Klimagerechtigkeitsaktivist*innen leisten ehrenamtlich Transparenzarbeit für Regionalverband"
date:   2021-04-13 00:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2104130
#          YYMMDDX
---

*Pressemitteilung des Ravensburger Klimacamps am 13. April 2021*

# Klimagerechtigkeitsaktivist\*innen leisten ehrenamtlich Transparenzarbeit für Regionalverband

Vor knapp zwei Monaten (am 20.2.2021) verteilten etwa 40
Klimagerechtigkeitsaktivist\*innen einen satirischen Rundbrief [1] an
zahlreiche Haushalte in der Region Bodensee-Oberschwaben (die Schwäbische
berichtete [2]). Der Rundbrief klärte über einige Inhalte des aktuellen
Regionalplanentwurfs auf und war dem Erscheinungsbild nach wie ein Brief des
Regionalverbands Bodensee-Oberschwaben verfasst, derjenigen Einrichtung, die
den Regionalplan verantwortet.

"Mit der Aktion halfen wir ehrenamtlich dem Regionalverband, das
demokratische Transparenzideal hochzuhalten", so Klimacamper Kim Schulz
(24). "Denn inhaltlich sind all unsere Aussagen über den
Regionalplanentwurf absolut richtig: das größte Straßenneubauprojekt der
letzten Jahrzehnte auf den Weg zu bringen, wertvolle Wälder zu roden und
Flächen zu versiegeln -- das hat der RVBO allen ernstes vor! Es ist
lediglich das erste Mal, dass unter dem Logo des RVBO der Entwurf
verständlich erklärt und so als Klimahöllenplan entlarvt wird." Auch
knapp zwei Monate nach der Aktion gab der RVBO noch nicht an, welche
Punkte des Informationsschreibens inhaltlich falsch seien; gegenüber der
Schwäbischen gab Verbandsdirektor Wilfried Franke nur an, dass
"wesentliche Aussagen" falsch seien [2], ohne Details zu nennen.

"Dabei hat der Regionalverband ein gewaltiges Demokratieproblem",
ergänzt Kollegin Johanna Wenz (17). In der Verbandsversammlung sind fast
ausschließlich Bürgermeister und Altbürgermeister von CDU und FWV
vertreten. "Die Grünen haben kaum Sitze -- obwohl sie in den
Gemeinderäten starke Kräfte sind. Auch andere Parteien sind kaum
vertreten. Unter den 56 Mitgliedern sind nur sieben Frauen [3]. Wir
bezweifeln die demokratische Legitimation des RVBO."

Passant\*innen, die die Aktivist\*innen in den Folgetagen wegen des
Rundschreibens ansprachen, zeigten sich erschüttert über das Ausmaß der
Klima- und Umweltzerstörung, die der RVBO plant. "Ich ging nicht davon
aus, dass der RVBO die Zukunft der Jugendlichen so geringschätzt",
kommentierte eine Anwohnerin.

[1] https://ravensburg.klimacamp.eu/pages/Pressemitteilungen/2021-04-13-Transparenzarbeit.pdf<br>
[2] https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-gefaelschter-rundbrief-zum-regionalplan-sorgt-fuer-furore-_arid,11331882.html<br>
[3] https://www.rvbo.de/Verband/Gremien-des-Regionalverbandes-Bodensee-Oberschwaben


## Hinweis

Der Aktion ging eine sorgfältige juristische Beratung voraus. Es handelt
sich nicht um eine Straftat. Details:

Als "Urkundenfälschung" werden Fälschungen zur Täuschung im
Rechtsverkehr bestraft. Wenn wir mit Regionalverband unterzeichnen, ist
das natürlich eine Fälschung. Aber die ist nicht zur Täuschung im
Rechtsverkehr. Täuschung im Rechtsverkehr wäre zum Beispiel, wenn man
einen Vertrag fälscht, um sich irgendeinen Vorteil zu verschaffen, oder
wenn man einen Führerschein fälscht, um der Polizei vorzutäuschen, man
hätte eine Fahrerlaubnis. Was wir machen ist eine politische Fälschung.
Das ist nicht strafbar. Im Übrigen ist jede einzelne unserer Aussagen
über den Regionalplanentwurf wahr.

Ein Betrug kann es nicht sein, weil es nicht um einen Vermögensvorteil
geht.

Eine Amtsanmaßung nach § 132 StGB ist es nicht, weil wir damit kein
öffentliches Amt ausüben und keine Handlung vornehmen, die nur ein
Amtsträger ausüben darf.
