---
title: "Umweltaktivist*innen und Stahlgegner*innen treten Haftstrafe an"
date: 2024-02-16T15:00:27+02:00
img:
    src: img/regierung-von-schwaben.jpg
    alt: Die Regierung von Schwaben stellte trotz laufender Rechtswidrigkeitsprüfung durch den Bayerischen Verwaltungsgerichtshof eine Ausnahmegenehmigung zur vorgezogenen Rodung eines Bannwalds aus.
type: post
draft: false
---

## Umweltaktivist\*innen und Stahlgegner\*innen treten Haftstrafe an

**Landgericht verhindert Rechtsmittel gegen Haft**

Nach einer satirischen Protestaktion gegen die Rodung eines Bannwalds
für Stahlproduktion [1-5] wurden die im süddeutschen Raum
bekannten Klimaaktivist\*innen Samuel Bosch (21) und Charlie Kiehne
(21) im vergangenen Oktober vor dem Augsburger Landgericht zu
Jugendarrest verurteilt. Nun soll der drei beziehungsweise eine Woche
andauernde Arrest vollstreckt werden — allerdings liegt ein
schriftliches Urteil weiterhin nicht vor. "Der Richter nimmt uns damit
nicht nur unsere Freiheit, sondern auch unsere Rechtsmittel", so Bosch.

Im Oktober 2022 genehmigte die Regierung von Schwaben die
vorgezogene Rodung eines Bannwalds für die Erweiterung der LechStahlwerke bei
Augsburg, obwohl der Bayerische
Verwaltungsgerichtshof, durch die Klagen angrenzender Gemeinden,
noch damit beschäftigt war, zu prüfen, ob die Rodung überhaupt
stattfinden durfte [6,7,8]. Daraufhin protestierten Bosch und Kiehne
zusammen mit mehreren anderen Aktivist\*innen mit einer
Kletteraktion am Regierungsgebäude gegen die Rodung und die
"rücksichtslos expandierende Stahlproduktion". Zwei von ihnen
wurden am Amts- und Landgericht Augsburg zu Jugendarrest
verurteilt. "Die Expansion der Stahlproduktion ist Teil unseres auf
Wachstumszwang basierten Wirtschaftssystems, das unsere Welt und
ihre Ökosysteme kaputt macht", kritisiert Bosch. "Die jungen Menschen
werden, ob das die Gesellschaft gut findet oder nicht, weiterhin gegen
den Wachstumswahn mit hemmungsloser Naturzerstörung und für
Klimagerechtigkeit aktiv bleiben", so Manfred Scheurenbrand (67), ein
Anwohner der Wahlheimat von Bosch und Kiehne, dem Altdorfer Wald
bei Ravensburg.

Charlie Kiehne ergänzt: "Die Stahlproduktion ist mit ihren riesigen CO2-
Emissionen für einen beträchtlichen Teil der Erdaufheizung
verantwortlich. Wir können es uns nicht leisten, Stahlwerke zu
vergrößern. Stattdessen müssen wir weniger Stahl verbrauchen."
Tatsächlich verursacht die Stahlproduktion sogar mehr CO2-Emissionen
als die Zementindustrie [S].


### Verhindert das Landgericht eine Verfassungsbeschwerde bewusst?

Dass die Haft zum jetzigen Zeitpunkt vollstreckt wird, ist ungewöhnlich,
denn ein schriftliches Urteil gibt es noch nicht. Üblicherweise wird das
Urteil geschrieben, bevor es vollstreckt wird. Ohne schriftliches Urteil
fehlt den Baumpfleger\*innen nun die Möglichkeit, ihre bereits im
Oktober 2023 angekündigte Verfassungsbeschwerde einzulegen und
damit die Gerichtsentscheidung aus Augsburg anzufechten. "Sind hier
nur Briefe durcheinandergeraten, oder unterdrücken sie eine
Verfassungsbeschwerde?", fragen sich die Aktivist\*innen über die
Beweggründe der Richter.

Auch Strafverteidiger Klaus Schulz findet die Vorgänge verwunderlich:
"Dass das Landgericht Augsburg zwei junge Menschen ohne relevante
Voreintragungen zu Arrest verurteilt, ist absolut unverständlich und
ungewöhnlich, so dass ich die erzieherischen Gründe gerne in der
Urteilsbegründung nachvollzogen hätte. In der Verhandlung gab es von
Seiten des Landgerichts zwar viele Emotionen und persönliche
Meinungen, wie Aktivismus auszusehen hätte, aber leider wenig
sachliche Erklärung."

Schon die Richterin am Amtsgericht irritierte mit dem Erlass eines
Haftbefehls, nachdem Schulz mehrmals aufgrund von eigenen
Terminen am Ravensburger Amtsgericht um Terminverlegung bat [9].
Ein Kommentator attestierte ihr "mangelnde politische Reife" [10].


### Einengung demokratischer Freiräume

"Dass wir bestraft werden statt der Regierung von Schwaben reiht sich
in die Ereignisse der letzten Monate ein", findet Bosch. "Wir erleben im
Zuge des fortschreitenden Rechtsrucks zusehends, dass wichtige
traditionelle demokratische Freiräume immer weiter beschnitten
werden. Rechte Motivationen im Rechtssystem schlagen um sich,
während die anderen scheinbar wegschauen. Am Ende verliert die
Demokratie." Kiehne pflichtet bei: "Anstatt den Aufschrei gegen Rechts
einfach verstummen zu lassen, müssen wir weiter für eine gerechte
Welt kämpfen. Es kann nicht sein, dass Aktivist\*innen weggesperrt
werden, die seit Jahren für soziale und klimagerechte Transformation
und damit auch gegen einen Rechtsruck kämpfen, während Parteien ihr
Wahlprogramm bei der AfD abschreiben und sich trotzdem auf Demos
gegen Rechts in den Himmel loben. Unsere Arbeit müsste unterstützt
und nicht bestraft werden!"

Seit letztem Jahr steht Deutschland aufgrund erheblicher
Einschränkungen der Versammlungsfreiheit auf der offiziellen
Ächtungsliste von Amnesty International [A1]. Insbesondere
Protestaktionen der Klimagerechtigkeitsbewegung seien immer wieder
von übermäßiger Polizeigewalt und Verboten betroffen, anstatt diese
als Kernelement eines lebendigen gesellschaftlichen Diskurses zu
ermöglichen und zu schützen [A2]. "Mit dem reflexartigen Unterbinden
und Kriminalisieren von ungewöhnlichen politischen Aktionen von
seiten verschiedenster Behörden verschwendet der Staat Energie und
Ressourcen für sinnlose Erziehungsmaßnahmen gegen
Klimaaktivist\*innen, statt sich endlich konstruktiv um
Klimagerechtigkeit zu bemühen", meint Kiehne.

Auch Jürgen Köhnlein, Vorsitzender der Deutschen Polizeigewerkschaft
Bayern, fordert gegenüber dem ZDF ein Umdenken von der Politik [K]. 
Weil sie davon überzeugt sind, zum Wohle der Gesellschaft zu handeln,
führen Gewahrsamnahmen bei Klimaaktivist\*innen nicht zu einer
dauerhaften Einschüchterung. Auch für die Zeit der Haft planen nach
eigener Aussage Unterstützer\*innen von Bosch und Kiehne
Protestaktionen.


### Hintergründe

An einem Samstagmorgen im Oktober 2022 (22.10.2022) kam es zur
Teilrodung des Lohwalds bei Langweid und Biberbach. Zu diesem
Zeitpunkt war der Bayerische Verwaltungsgerichtshof, wie auch jetzt
noch, mit der Rechtswidrigkeitsprüfung des Rodungsvorhabens
beschäftigt. Diese Rechtswidrigkeitsprüfung hatten die an den Lohwald
angrenzende Gemeinde Biberbach sowie der BUND Naturschutz mit
einer Normenkontrollklage auf den Weg gebracht.
Bannwälder sind nach dem Bayerischen Waldgesetz besonders
geschützt und dürfen nur unter ganz speziellen Bedingungen gerodet
werden, Art. 9 des Gesetzes setzt für ein solches Rodungsvorhaben
voraus, dass "zwingende Gründe des öffentlichen Wohls es erfordern" [G].

"Solange der Bayerische Verwaltungsgerichtshof noch mit der
Rechtswidrigkeitsprüfung beschäftigt ist, können wir keine
Genehmigung für eine vorgezogene Lohwald-Rodung ausstellen". Eine
Routineantwort wie diese hatten wohl die Gemeinden Langweid und
Biberbach von der Regierung von Schwaben erwartet, als die LechStahlwerke am
4.10.2022 dort einen speziellen Ausnahmeantrag
stellten. Doch es kam anders: Trotz der auch jetzt noch anhängigen
Normenkontrollklagen gestattete die Regierung von Schwaben die
umstrittene Rodung: Nach nur zehn Tagen Bearbeitungszeit hatten die
Lech-Stahlwerke die Ausnahmegenehmigung in der Tasche – ohne, dass
der Verwaltungsgerichtshof involviert wurde. Für die vom VGH 
ungeprüfte Genehmigung fiel für die Lech-Stahlwerke nur eine
Verwaltungsgebühr in Höhe von 250 € an [B].

Um eine größere Öffentlichkeit auf diese Unregelmäßigkeiten
hinzuweisen, nahmen mehrere Klimaaktivist\*innen mit einer
satirischen Kunstaktion die Regierung von Schwaben aufs Korn.
Schilder mit den Aufschriften "Lohwald-Rodung trotz laufender
Gerichtsverfahren? Frech!" sowie "Den Lohwald für 250 € verhökern?
Frech!" brachten sie kletternd an der Fassade des Regierungsgebäudes
an. Das Augsburger Amts- und Landgericht sah diese
Meinungsäußerung weder von der Meinungs- noch der
Versammlungsfreiheit gedeckt.

In ähnlich gelagerten Fällen hob der Bayerische Verwaltungsgerichtshof
Genehmigungen zur Bannwaldrodung immer wieder auf [13,14] und
Aktivist\*innen feierten vor Gericht Siege auf ganzer Linie [15].


### Referenzen

[1] https://www.augsburger-allgemeine.de/augsburg-land/augsburgmeitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufskorn-id64382891.html\
[2] https://www.augsburg.tv/mediathek/video/aerger-um-protestermittlungen-gegen-augsburger-klimaaktivisten/\
[3] https://www.sueddeutsche.de/bayern/augsburg-lech-stahlwerkeklimacamp-polizei-regierung-von-schwaben-1.5682862\
[4] https://www.zeit.de/news/2022-10/27/ermittlungen-nachprotestaktion-bei-regierungsbehoerde\
[5] https://www.daz-augsburg.de/90948-2/\
[6] https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mitbannwald-rodung-fuer-kritik,TLBZdnV\
[7] https://www.br.de/nachrichten/bayern/bannwald-rodung-bei-\
stahlwerken-vor-gericht,TQRhKFH\
[8] https://www.forumaugsburg.de/s_5region/Bezirk/210722_kampfgegen-die-rodung-des-lohwalds-durch-die-lechstahlwerke-meitingen/index.htm\
[9] https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/haftbefehle-gegen-klimaaktivisten-aus-ravensburg-100.html\
[10] https://www.kontextwochenzeitung.de/gesellschaft/642/richterinfehlt-die-politische-reife-8971.html\
[11] https://www.augsburger-allgemeine.de/augsburg/augsburg-nachprotest-gegen-abholzaktion-klimacamper-erneut-auf-der-anklagebankid68133166.html\
[12] https://www.sueddeutsche.de/bayern/augsburgregierungsgebaeude-besetzung-blechschmidt-1.5763823\
[13] https://www.sueddeutsche.de/muenchen/vgh-entscheidungkiesabbau-waldrodung-planegg-1.6093536\
[14] https://www.br.de/nachrichten/bayern/verwaltungsrichter-stoppenvorerst-bannwaldrodung-in-planegg,TltNDMx\
[15] https://www.sueddeutsche.de/muenchen/landkreismuenchen/forstkasten-neuried-amtsgericht-muenchen-klimaproteste-ingoblechschmidt-1.6301572\
[A1] https://www.fr.de/politik/klimaaktivisten-amnesty-internationaldeutschland-versammlungsfreiheit-repression-proteste-liste-zr92528884.html\
[A2] https://netzpolitik.org/2023/interaktive-karte-amnesty-kritisierteinschraenkung-der-versammlungsfreiheit-in-deutschland/\
[B] https://www.lohwibleibt.de/frech.pdf (Seite 4)\
[F] https://classic-ravensburg.klimacamp-augsburg.de/pressespiegel/\
schwaebische-1423309.jpeg\
[G] https://www.gesetze-bayern.de/Content/Document/BayWaldG-9\
[K] https://www.zdf.de/nachrichten/panorama/kriminalitaet/panoramapraeventiv-gewahrsam-klimaaktivisten-100.html (Zeitstempel 0:55)\
[S] https://www.klimaschutz-industrie.de/themen/branchen/stahlindustrie/
