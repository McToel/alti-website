---
title: "Aktivist*innen Gestalten Verkehrsschilder Um – Neues Fahrziel: Verkehrswendeplan Fürs Schussental"
date: 2023-07-07T14:38:26+02:00
img:
    src: img/verkehrsschlilder schussental.jpg
    alt: Überhängte Verkehrsschilder weisen auf den Verkehrswendeplan hin
type: post
draft: false
---
Pressemitteilung vom 7.7.2023

Wer am heutigen Freitagmorgen auf der Alten B30 zwischen Ravensburg und Weingarten unterwegs war, erlebte eine Überraschung: Was auf den ersten Blick aussieht wie eine ganz normale Schilderbrücke, entpuppt sich auf den zweiten Blick als raffinierter Wegweiser für klima- und menschenfreundliche Mobilität. Verkehrswendeaktivist\*innen hatten in den frühen Morgenstunden in einer unangemeldeten Kletteraktion die Informationsschilder zwischen Ravensburg und Weingarten (Koordinaten: 47.797464, 9.628136, Ravensburger Str. nahe Kreuzung Ulmer Str., Höhe Frick) mit eigenen Botschaften überklebt.

Aktivist\*innen rufen auf verkehrswende-rv.siehe.website zur Mitarbeit auf
Statt Orts- und Richtungsinformationen für Autofahrer\*innen stehen auf den Schildern nun die Eckpunkte eines umfassenden klima- und menschenfreundlichen Verkehrswendeplans fürs Schussental, dem ersten Plan dieser Art. 

"Die Gemeinden im Schussental und der Landkreis Ravensburg stehen in Sachen Mobilitätswende orientierungslos da, sie haben keine ambitionierten Konzepte für die Mobilitätswende", erklärt Sarah Wagner (17). "Daher wird die Zivilgesellschaft anpacken und einen umfassenden Plan ausarbeiten." Die Aktivist\*innen sind Teil eines größeren offenen Mobilitätswendebündnisses, das sich in den letzten Monaten im Raum Ravensburg formierte. In den nächsten Wochen und Monaten soll ein Bürgerpartizipationsprozess durchgeführt werden, bei dem der Verkehrswendeplan zunächst grob erarbeitet wird.

"Bei diversen Veranstaltungen werden alle Interessierten mit ihren Ideen und Vorstellungen den Plan dann verfeinern", so Wagner weiter. "Dann können alle die Radwege, Busverbindungen, Straßenbahnlinien und Geschwindigkeitsbegrenzungen einzeichnen, die sie möchten." ergänzt Samuel Bosch (20). So soll der erste Entwurf zu einem umfassenden Verkehrswendeplan werden, der nicht nur allgemeine Zielformulierungen enthält, sondern erstmals auch ganz konkrete Umsetzungsvorschläge macht.

Die Aktivist*innen planen noch diesen Sommer ein angemeldetes Straßenfest mit Vorträgen und Workshops auf einer Hauptverkehrsstraße in Ravensburg oder Weingarten durchzuführen. Auf dem Fest werde dann auch der grobe Entwurf des Plans vorgestellt und bearbeitet. 
Angemeldet und nicht angemeldet, Demonstrationen an mobilitätspolitischen Brennpunkten wie Unfallschwerpunkten oder Stellen, an denen sich die Abwesenheit eines geschlossenen Radwegenetzes besonders bemerkbar macht, soll es in nächster Zeit im Schussenbecken öfters geben. Die heutige Kletteraktion bilde den Auftakt dieser Aktionsserie.
"Wir werden diese Vorschläge mit den Aktionen in die öffentliche Wahrnehmung rücken und dadurch die Stadträte zur Umsetzung drängen." so Samuel Bosch (20).

"0€-Ticket" statt "Richtung Landratsamt"
Nach der unangemeldeten Kletteraktion steht auf den Schildern nun an der Stelle, an der gestern noch "Landratsamt" stand, heute "Straßenbahn reaktivieren" und "0€-Ticket". "Aktuell ist das Schussental noch eine reine Autowüste. Wo man hinschaut, überall sind die Straßen fast ausschließlich auf motorisierten Individualverkehr ausgelegt – richtiger Klimaschutz ist Fehlanzeige. Dabei wäre es doch gar nicht so schwer, das endlich zu ändern", meint der Klimaaktivist Samuel Bosch (20). "Die öffentlichen Verkehrsmittel sind in der Region noch mehr als ausbaufähig. Wir fordern die Reaktivierung der Straßenbahn und die Einführung eines 0-Euro-Tarifs in allen öffentlichen Verkehrsmitteln, um sie für alle zugänglich und nutzbar zu machen."

Alle Richtungen Fahrradstraße (statt (alle Richtungen) B30/B32)
Auf einem weiteren Schild wurde der Schriftzug "alle Richtungen zur B30/32" durch "alle Richtungen: Fahrradstraße" ersetzt. Die Aktivist\*innen haben dabei ein ernstes Anliegen: "Unsere Straßen müssen für Fahrradfahrer\*innen sicherer werden. Wir brauchen autofreie Zonen, Geschwindigkeitsbegrenzungen und Umverteilung von Verkehrsflächen an den Rad- und Fußverkehr. Das ist aktuell noch nicht der Fall, weshalb es immer wieder zu schrecklichen Unfällen wie am 23. Mai kommen wird, wenn sich nicht endlich etwas ändert", gibt die weitere Umweltaktivistin Sarah Wagner zu bedenken. Sie verweist damit auf den Zusammenstoß eines LKWs mit zwei Radfahrern auf der B32 bei Ravensburg, der einen der beiden Radfahrern das Leben kostete [6]. Wäre die B32 mit einer Geschwindigkeitsbegrenzung belegt gewesen, hätte der LKW-Fahrer vermutlich noch rechtzeitig bremsen können. Laut Statistiken [1] lässt sich das Unfallrisiko durch eine Geschwindigkeitsbegrezung drastisch reduzieren [2].

Mit der Aktion wollen die Aktivist\\*innen ein Umdenken in der Politik weg vom Auto hin zu ökologischer und klimafreundlicher Mobilität erreichen, die für alle bezahlbar ist. "Autos sind, was den Platzverbrauch angeht das schlechteste und ineffektivste Verkehrsmittel. 72 Personen zu transportieren, verbraucht mit Autos 1000 Quadratmeter voll versiegelte Fläche, während dieselben 72 Menschen mit Fahrrädern nur 90 und in einem Bus sogar nur 30 Quadratmeter Fläche benötigen [3]. Dazu kommt noch, dass für Autos Parkplätze benötigt werden, die zusätzlichen versiegelten Boden bedeuten." Es ist nicht das erste Mal, dass Klimaaktivist\*innen im Landkreis Ravensburg gezielt Verkehrsschilder manipulieren, um für mehr Klimaschutz im Verkehrssektor zu protestieren: Im Dezember 2022 wurden Autobahnschilder mit Tempo-100-Stickern überklebt (SWR berichtete [5]).

"Eine Verkehrswende hin zu mehr öffentlichen Verkehrsmitteln und Fahrradstraßen würde also nicht nur die allgemeine Sicherheit im Straßenverkehr erhöhen, sondern würde auch enorm Platz sparen, der für kühlende Grünflächen, Bäume und Wohnraum genutzt werden kann."


## Referenzen

1. https://de.statista.com/infografik/20761/anzahl-der-unfaelle-auf-bundesautobahnen-mit-und-ohne-tempolimit/
2. https://taz.de/Mobilitaetsforscher-ueber-Tempolimit/!5656151/
3. https://www.focus.de/auto/ratgeber/unterwegs/auto-fahrrad-bus-dieses-bild-wird-ihre-einstellung-zum-auto-veraendern_id_3844157.html
4. https://www.klimareporter.de/landwirtschaft/unterm-asphalt-kein-leben
5. https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/ravensburger-klimaaktivisten-ueberkleben-temposchilder-b30-100.html
6. https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/radfahrer-stirbt-bei-unfall-in-ravensburg-100.html

---------------------

## Fotos zur freien Verwendung
https://www.speicherleck.de/iblech/stuff/.vwr

-------------

## <a href="/presse"> Pressekontakt </a>