---
title: "Kunst Schaf Malerei Vor Dem Rathaus – Gegen Die Bebauung Der Schafsweide"
date: 2023-04-21T23:46:31+02:00
img:
    src: img/kunstschaf.png
    alt: Malerei Schafswiese muss grün bleiben
type: post
draft: false
---

Am Nachmittag des 21.4.2023 wurde ein, Schaf und der Spruch "Die Schafswiese soll grüne Wiese bleiben" auf den Vorplatz des Rathauses in Weingarten gemalt. Die Gruppe aus Künstler*innen  und Aktivist*innen macht mit ihrer Aktion gegen die aus ihrer Sicht "unnötige Bebauung" der sogenannten "Schafswiese", einer Wiese bei Weingarten, mobil.  

"Es darf nicht sein, dass wir immer neue Flächen versiegeln und immer weiter neue Wohnhäuser bauen, obwohl wir genügend Wohnraum haben. Kein Mensch braucht 130qm Wohnfläche, wir möchten eine sinnvolle Umorganisierung des bestehenden Wohnraums." [1] so Samuel Bosch (20) zu den Plänen der Stadtverwaltung.  

Anlass der Aktion ist die von Grünen und SPD initiierte Abstimmung über das Projekt im Gemeinderat in Weingarten. Das Projekt sieht vor, 1,7 ha Wiese zu versiegeln. [2] Durch Gespräche und das Verteilen von Flyern im Anschluss, informieren die Beteiligten Passant*innen vor Ort über das Projekt und ihren alternativen Vorschlag. Die Gruppe hofft durch ihre Aktion eine starke öffentliche Beteiligung an der Gemeinderatssitzung am kommenden Montag zu bewirken.

"Wir wissen, dass viele Bürger*innen auf unserer Seite sind und von der Politik mehr erwarten als ein fantasieloses und klimaschädliches Bauprojekt nach dem anderen" so Rosmarie Vogt aus Weingarten (70)

QUELLEN: 
[1] https://www.umweltbundesamt.de/daten/private-haushalte-konsum/wohnen/wohnflaeche#folgen-der-flachennutzung-durch-wohnen-fur-die-umwelt

[2] https://www.schwaebische.de/regional/oberschwaben/weingarten/raete-wollen-schafswiese-vor-bebauung-retten-1534517

-------------


## <a href="/presse"> Pressekontakt </a>