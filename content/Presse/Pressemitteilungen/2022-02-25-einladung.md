---
layout: page
title:  "25.02.2022: Ein Jahr Besetzung des Altdorfer Walds: Neue Impulse"
date:   2022-02-25 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2202251
#          YYMMDDX
---

*Presseeinladung vom 25.02.2022*

# Ein Jahr Besetzung des Altdorfer Walds: Neue Impulse

*Einladung zum Jubiläumswochenende und insbesondere zur Austauschrunde über die Zukunft der Besetzung am Sonntag*

Heute vor einem Jahr starteten eine Hand voll Aktvist\*innen aus Ravensburg und
Umgebung die Dauerbesetzung der Rodungsfläche im Altdorfer Wald bei Grund. Zu
diesem Anlass gibt es dieses Wochenende ein umfassendes Jubiläumsprogramm, zu
dem die Öffentlichkeit herzlich eingeladen ist. "Unser Ziel ist es, das
Waldstück vor der Zerstörung durch Profitgier zu beschützen", so Samuel Bosch
damals. An den Zielen hat sich bis heute nichts geändert, wohl aber an der
Besetzung selbst.

"Die ersten drei Menschen, die in den Wald zogen, waren wie ich 18 Jahre alt",
erinnert sich Bosch. "Heute ist es vor allem die ältere Generation aus
Anwohner\*innen der umliegend Orte, die das Projekt vorantreiben". Zwar würden
immer noch hauptsächlich junge Menschen im Wald übernachten und leben. Doch
organisierend seien mittlerweile vor allem Anwohner\*innen tätig. "Wir sind ja
unmittelbar von der Zerstörung unserer Trinkwasserquellen und von der täglichen
Lärmbelastung durch den Kiesexport betroffen", so Martin Lang (54) aus
Oberankenreute. "Wir Ältere organisieren Mahnwachen, leisten Vernetzungsarbeit,
organisieren Veranstaltungen wie kürzlich die Vernissage im Wald und vieles
andere mehr. Einige von uns haben bei den Jüngeren auch das Aktionsklettern
gelernt", so Lang weiter. Etwa war es Lang, der zusammen mit zwei jüngeren
Menschen an der Basilika in Weingarten ein CDU-kritisches Banner anbrachte.

Auch baulich veränderte sich die Besetzung: Statt einer provisorischen Plattform
sind es jetzt rund 20 Baumhäuser und Plattformen in bis zu 25 Meter höhe. "Wir
haben ein großes Küchenbaumhaus und viele Schlafplätze in den Bäumen", so
Charlie Kiehne (19) heute. Kiehne kommt ursprünglich aus Ulm. Sie zog im Sommer
in den Altdorfer Wald, um in der Besetzungsgemeinschaft zu leben. "Wir müssen
unser Denken grundsätzlich verändern. Es ist ein Symptom eines falschen Denkens,
wenn Menschen auf die Idee kommen, einen uralten Wald für Profite und Wachstum
zu roden. Wieso ist es legal, Bäume zu fällen, die weit älter als man selbst
ist, aber illegal, sie zu beschützen?", fragt Kiehne.

DIe Besetzer\*innen sehen ihr Projekt als eine Art Experiment nachhaltiger und
solidarischer Gesellschaftsorganisation. "Wir kamen wegen des undemokratischen
Kiesabbaus, stellten aber über die Zeit fest, dass viele Bürger\*innen sich
ebenfalls die Systemfrage stellen", erzählt dauerbesetzerin xxx xxx. 

"Was erlauben wir uns eigentlich? Angesichts der Erdaufheizung in großem Stil
Wald zu roden, nur um einen Rohstoff abzubauen, der nachweislich diese
Erdaufheizung befeuert und dank Alternativen auch eingespart werden könnte?",
ärgert sich Unterstützer Manfred Scheurenbrand (66) aus Waldburg. Er verfolgt
die Aktionen der Aktivist\*innen schon seit dem ersten Baumhaus in Ravensburg.
"Ich bin beeindruckt, wie diese jungen Menschen bei Wind und Wetter im Wald
sind. WIr brauchen die Menschen in den Bäumen!". Seit der Dauerbesetzung kommt
er regelmäßig in den Wald. "Ich unterstützte ab dem Moment, in dem ich merkte,
dass es sich bei den Besetzer\*innen um engagierte nachdenkliche junge Menschen
handelt." Seitdem bringt er regelmäßig warme Suppe oder hilft anderweitig, um so
seinen Beitrag zum Klimaprotest zu leisten.

## Jubiläumsprogramm
Freitag 25.2.
- 16:00 Gemeinsames Kochen
- 19:00 Filmpremiere: Jubiläumsfilm
- 20:00 Besetzungsanekdoten am Lagerfeuer

Samstag 26.2.
- 10:00 Kletterworkshop (auch Kinderklettern)
- 12:00 Direct Action Training (Austausch über kreative und subversive Aktionen)
- 15:00 Ausführliche Waldführungen
- 17:00 Kunstabend (Kleinkunstausstellung)
- 19:00 Jubiläumsfilm
- 20:00 Lagerfeuerromantik

Sonntag 27.2.
- 11:00 Brunch
- 12:00 Wie soll es weiter gehen? Ideenaustausch
- 14:00 Ausführlicher Waldspaziergang
- 15:00 Kinderklettern
- 19:00 Jubiläumsfilm

Journalist*innen laden wir insbesondere zu der Austauschrunde am Sonntagmittag
zur Zukunft des Widerstands im Altdorfer Wald ein.

## Ort
Wegbeschreibung: von Weingarten kommend auf der L317 in Richtung Wolfegg auf die L323 in Richtung Grund abbiegen. Dann nach 50m den ersten Waldweg nach rechts abbiegen.
Bushaltestelle: Abzw. Grund im Wald, Vogt
Koordinaten: 47.810973, 9.76126
