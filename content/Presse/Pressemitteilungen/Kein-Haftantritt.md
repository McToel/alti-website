---
title: "Trotz Verurteilung: Klimaaktivist Samuel Bosch bleibt Gefängnis fern"
date: 2024-03-14T07:00:27+02:00
img:
    src: img/regierung-von-schwaben.jpg
    alt: Die Regierung von Schwaben stellte trotz laufender Rechtswidrigkeitsprüfung durch den Bayerischen Verwaltungsgerichtshof eine Ausnahmegenehmigung zur vorgezogenen Rodung eines Bannwalds aus.
type: post
draft: false
---

**Wer hat die Macht, Gesetze zu brechen?**

Wie der SWR berichtete [0], hätte am heutigen Donnerstagmorgen (14.3.2024) der
im Ulmer und Ravensburger Raum bekannte Klimaaktivist Samuel Bosch (21)
eigentlich seine dreiwöchige Haft (Jugendarrest) antreten sollen. Er hatte im
Rahmen einer Kletteraktion in Augsburg die Regierung von Schwaben wegen einer
vorgezogenen Bannwaldrodung rechtswidrigerweise als "frech" bezeichnet
[1-5,6a,6b]. Nun kündigte er an, der Ladung der Göppinger Jugendarrestanstalt
vorerst nicht nachzukommen und sein soziales und politisches Engagement zu
priorisieren, während das Bundesverfassungsgericht noch eine von ihm eingelegte
Verfassungsbeschwerde gegen die Haftpläne prüft.


### Recht und unsere Ordnung sind in Gefahr, wenn Minister gegen das Klimaschutzgesetz verstoßen

"Staatsanwaltschaft, Richter\*innen und Polizei sind durch ihre
Einschüchterungsversuche mit dafür verantwortlich, dass sich große Teile
unserer Gesellschaft nicht trauen, die nötigen Veränderungen anzupacken", so
Bosch. "Wenn wir uns weiterhin hinter einem zu simpel gedachtem Prinzip von
'Recht und Ordnung' verstecken, werden wir die Klimakrise nicht lösen, sondern
uns nur immer wieder davor drücken, aktiv und konstruktiv an den desaströsen
Auswirkungen unseres Handelns zu arbeiten. Unser Recht und unsere Ordnung sind
in Gefahr, wenn Minister gegen das Klimaschutzgesetz verstoßen und
völkerrechtliche Übereinkünfte wie das Pariser Klimaabkommen nichts mehr
zählen."

Bosch bezieht sich dabei konkret auf ein Fachgutachten des wissenschaftlichen
Dienstes des Bundestags, das Bundesverkehrsminister Wissing aufgrund steigender
CO2-Emissionen im Verkehrssektor Rechtsbruch attestiert [7-9]. "Beim Brechen
von Verträgen oder Gesetzen wird in unserer Gesellschaft offensichtlich mit
zweierlei Maß gemessen", kritisiert Boschs Mitstreiterin Kiki Köffle (21) und
folgt damit Carola Rackete und Luisa Neubauer, die schon 2020 fragten: Wer hat
die Macht, Verträge zu brechen? [10]. UN-Generalsekretär António Guterres hat
dazu eine eindeutige Haltung, er erklärte: "Klimaaktivisten werden manchmal als
‚gefährliche Radikale‘ dargestellt. Aber die wirklich gefährlichen Radikalen
sind die Länder, die die Produktion von fossilen Brennstoffen erhöhen" [11a]
"Klimaaktivisten – angeführt von der moralischen Stimme junger Menschen – haben
ihre Ziele auch in den dunkelsten Tagen weiterverfolgt. Sie müssen geschützt
werden und wir brauchen sie jetzt mehr denn je." [11b]


### Bosch geht mit Angst um

Der Angst, nun jederzeit verhaftet zu werden, entgegnet Bosch die größere Angst
vor der Klimakrise: "Wir wissen, dass unser ehrenamtliches Engagement für
Klimagerechtigkeit große persönliche Konsequenzen haben kann", so Bosch. Seine
Haltung bleibt eindeutig: "Das eigentlich Gefährliche ist nicht meine
Inhaftierung, sondern die Klimakrise mit ihren verheerenden Auswirkungen auf
Ökosysteme und alle Lebensbereiche. Dieser Gefahr bin aber nicht nur ich mir
bewusst: Trotz der Kriminalisierung von Protest fragen sich immer mehr
Menschen, auf welche Weise sie gegen die Klimakrise aktiv werden können."


### Podiumsdiskussion im Ulmer Theater

Für den Freitagabend (15.3.) plant Bosch die Teilnahme an einer
Podiumsdiskussion im Ulmer Theater. Dort findet an diesem Abend die Dernière
des in Graz uraufgeführten Stücks "Was zündet, was brennt" statt [12]. "Was ist
die eine Geschichte, die erzählt werden müsste, um uns alle in Handlung zu
versetzen?", gibt das Theater als Leitfrage des Stücks an [12]. Inhaltlich geht
es um zwei Klimaaktivistinnen, die einen Wachmann von ihrer Haltung in Bezug
auf die Klimakrise überzeugen möchten [13]. Im Anschluss an die Aufführung gibt
es ein Publikumsgespräch, bei dem neben Bosch und Köffle auch Fabia
Tömösy-Moussong (20) auf dem Podium sitzen wird. Tömösy-Moussong studiert in
Heidelberg Physik und organisierte erst gestern eine Demonstration für eine
Umstellung auf die klimafreundlichere moderne industrielle Holzbauweise und
gegen HeidelbergCement [14].


### Referenzen

[0] https://www.ardmediathek.de/video/swr-aktuell-baden-wuerttemberg/ulmer-klimaaktivist-muss-drei-wochen-in-haft/swr-bw/Y3JpZDovL3N3ci5kZS9hZXgvbzIwMTIyMDU\
[1] https://www.augsburger-allgemeine.de/augsburg-land/augsburgmeitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufskorn-id64382891.html\
[2] https://www.augsburg.tv/mediathek/video/aerger-um-protestermittlungen-gegen-augsburger-klimaaktivisten/\
[3] https://www.sueddeutsche.de/bayern/augsburg-lech-stahlwerkeklimacamp-polizei-regierung-von-schwaben-1.5682862\
[4] https://www.zeit.de/news/2022-10/27/ermittlungen-nachprotestaktion-bei-regierungsbehoerde\
[5] https://web.archive.org/web/20211208150936/https://www.forumaugsburg.de/s_5region/Bezirk/210722_kampf-gegen-die-rodung-des-lohwalds-durch-die-lechstahlwerke-meitingen/index.htm\
[6a] https://web.archive.org/web/20221030140916/https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV\
[6b] https://web.archive.org/web/20211208150936/https://www.forumaugsburg.de/s_5region/Bezirk/210722_kampf-gegen-die-rodung-des-lohwalds-durch-die-lechstahlwerke-meitingen/index.htm\
[7] https://www.handelsblatt.com/politik/deutschland/klimaziele-gutachten-minister-wissing-verstoesst-gegen-das-klimaschutzgesetz/28899854.html\
[8] https://www.zdf.de/nachrichten/politik/klima-klimaschutzgesetz-treibhausgasemissionen-verkehr-100.html\
[9] https://www.vcd.org/service/presse/pressemitteilungen/rechtsbruch-beim-klimaschutzgesetz-verkehrsminister-wissing-bleibt-sofortprogramm-voraussichtlich-schuldig\
[10] https://www.spiegel.de/politik/deutschland/luisa-neubauer-und-carola-rackete-ueber-protest-im-dannenroeder-forst-a-7cae1bae-331f-48d3-a231-cf25dd076bcd\
[11a] https://www.rnd.de/politik/antonio-guterres-bezeichnet-neuen-klimabericht-als-dokument-der-schande-XLA2Z4M2HPT7WCMKNPVAYSNUMM.html\
[11b] https://www.zeit.de/politik/ausland/2023-05/razzia-klimaschuetzer-un-schutz-antonio-guterres\
[12] https://www.theater-ulm.de/spielplan/stuecke/was-zuendet-was-brennt (Höreinführung, Zeitstempel 2:04)\
[13] https://www.augsburger-allgemeine.de/neu-ulm/ulm-premiere-am-theater-ulm-was-zuendet-was-brennt-id69077521.html\
[14] https://www.rnz.de/region/heidelberg_artikel,-Heidelberg-Demonstration-gegen-Heidelberg-Materials-plus-Fotogalerie-_arid,1299485.html
