---
title: "Zeichen Für Vielfalt, Toleranz Und Miteinander Kein Rutenfest Ohne Regenbogen"
date: 2023-07-23T18:27:06+02:00
type: post
draft: false
---

Ravensburg, Rutenmontag 24. Juli / 21:00 Uhr:

Mehrere Bürger:innen setzen zum diesjährigen Rutenfest mit unterschiedlichen Regenbogenflaggen ein Zeichen für Vielfalt, Toleranz und Miteinander. Ungewöhnlich ist der Ort, den sie sich für ihre Aktion ausgesucht haben: Das große Kettenkarussel auf dem Festplatz.

Während der Fahrt werden mehrere verschiedene Regenbogenfahnen symbolisch für Vielfalt und gegen Ausgrenzung, gut sichtbar, in luftiger Höhe flattern.
"Wo, wenn nicht hier, im fröhlichen Treiben des Rutenfestes, ist der richtige Ort, um für eine bunte Gesellschaft, ungeachtet des Geschlechts, sexueller Ausrichtung, Herkunft, Alters und Beeinträchtigung, ein Zeichen zu setzen", betont Gudrun Bosch (47).

Der Charme unserer Stadt lebt durch das vielfältige Engagement und die bunten Unterschiede ihrer Bewohner:innen. Ob Klimaktivist:innen, die LTBTQ+ - Bewegung, Menschen z.B. aus der Ukraine, Syrien, Gambia, oder Menschen mit Beeinträchtigungen zeigen, dass wir alle von der "Regenbogen-Buntheit" profitieren. So Teilnehmer:innen an der Regenbogenfahrt in ihrem Statement.

"Ich freue mich seit jeher über jeden Menschen der seine Einzigartigkeit offen lebt und sich nicht in irgendwelche Konventionen oder Normen pressen lässt. Das empfinde ich beispielgebend für mein eigenes Leben", bekennt Martin Lang (56).

Mit ihrer Aktion solidarisieren sich die Teilnehmenden ausdrücklich mit allen Menschen, die im Alltag und strukturell von Ausgrenzung betroffen sind und wünschen sich mehr Offenheit und Selbstverständnis miteinander.

"Wie das Zusammenspiel der Farben eines Regenbogens, ist unsere Gesellschaft nur durch die unterschiedlichsten Lebensweisen so besonders!", unterstreicht Verena Müller (46)
 
## Hinweis

Wir laden Sie herzlich ein, die Fahrt live vor Ort mitzuverfolgen und mit den Teilnehmer:innen anschließend in Kontakt zu treten.
Start ist am Montag (24.7.2023) um 21:00 Uhr am Kettenkarussel auf dem Festplatz.

Fotos zur freien Verwendung:

https://www.speicherleck.de/iblech/stuff/.rf/


-------------

## <a href="/presse"> Pressekontakt </a>