---
layout: page
title:  "08.04.2021: Klimagerechtigkeitsaktivist*innen blockieren Einfahrt des Asphaltwerks Grenis"
date:   2021-04-08 08:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2104080
---

*Pressemitteilung der Altdorfer Waldbesetzung am 8. April 2021*

# Klimagerechtigkeitsaktivist\*innen blockieren Einfahrt des Asphaltwerks Grenis

In der Nacht von Mittwoch auf Donnerstag (8.4.2021) hat eine Gruppe von
Klimagerechtigkeitsaktivist:innen die Einfahrt des Asphaltwerks Grenis
bei Vogt (Ravensburg) mit einer Barrikade blockiert. Auf dem Teer vor
der Barrikade steht mit grellen Farben: "Ihr nehmt unsere Wälder"

Mit ihrer Aktion machen die Aktivist:innen auf die klimaschädliche
Wirkung der Bauindustrie aufmerksam. "Wir werden nicht hinnehmen, dass
weiterhin Natur und Klima für Profitinteressen einzelner zerstört
werden", so Aktivistin Clara Beuter (19). Die Barrikade aus vernagelten
Stämmen sei auf der Zufahrtsstraße vor dem Werk plaziert worden, um die
Asphaltproduktion für einige Stunden zu stoppen.
Die Anlage gehört Meichle und Mohr, welche unter anderem große Gebiete
des besetzten Altdorfer Waldes roden möchte um dort unter anderem Kies
für die Anlage in Grenis abzubaggern.

Der Keißabbau im Altdorfer Wald wird durch den neuen Regionlplan des von der CDU
dominierten Regionalverbands festgeschieben. Der Plan wird für die
nächsten 15--20 Jahre gelten und sieht in seiner bisherigen Form allein
im Altdorfer Wald die Rodung von 62 ha gesunden Mischwalds für den
Keisabbau vor.
"Wir werden den Klimahöllenplan kippen und mit ihm den Kiesabbau, die
Asphaltproduktion und die Rodung" so Beuters Mitstreiterin Smilla Feger
(17) weiter.

Die Produktion steht momentan still. Wie lange die Polizei mit der
Barikadenräumung beschäftigt sein wird, ist unklar.


## Wegbeschreibung

Werksabzwung an der Straße L324 bei Hannober südlich von Vogt
Koordinaten: 47.747459473608345, 9.765060066616408


## Kontakt

Es gibt keine autorisierte Gruppe und kein beschlussfähiges Gremium, das
"offizielle Gruppenmeinungen" für die Besetzung beschließen könnte. Die
Menschen in der Besetzung und ihrem Umfeld haben vielfältige und teils
kontroverse Meinungen. Diese Meinungsvielfalt soll nicht zensiert werden,
sondern gleichberechtigt nebeneinander stehen. Kein Text und keine Aktion
spricht für die ganze Besetzung oder wird notwendigerweise von der ganzen
Besetzung gut geheißen.

Diese Aktion wurde nicht mit der gesamten Besetzung abgesprochen.
