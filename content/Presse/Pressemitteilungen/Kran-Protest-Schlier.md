---
title: "Schlier: Kran-Protest – Renaturierung nur schwer möglich"
date: 2023-11-20T10:00:27+02:00
img:
    src: img/schlier_kran.jpg
type: post
draft: false
---

## Schlier: Kran-Protest -- Renaturierung nur schwer möglich

Flächenversiegelung durch "Gewerbegebiete auf der grünen Wiese" und "völlig
unzureichende Ausgleichsflächen" -- darauf machten am 20.11.2023 kletternde
Klimaaktivist\*innen aufmerksam. Ihre Kritik brachten sie auf einem Banner mit
der Aufschrift "Renaturierung?!" zum Ausdruck, das sie am Ausleger eines der
Baukräne im neu entstehenden Gewerbegebebiet Wetzisreute-Ost in der Gemeinde
Schlier [1a,1b] anbrachten. "Der geplante Natur-Ausgleich ist lange nicht
ausreichend - und was von den Plänen letztlich realisiert wird, ist offen",
sagt Hannah Schak (18), eine der Aktivistinnen.

In Wetzisreute sollen auf 58 000m² neue Gewerbe angesiedelt werden. Obwohl im
Vorhinein seitens der Gemeinden suggeriert wurde, dass der Bedarf an
Gewerbefläche riesig sei, scheint es derweil schwierig zu sein, überhaupt
Investor\*innen zu finden. "Auf allen Kanälen wirbt die Gemeinde Schlier mit dem
angeblich nachhaltigen Baugebiet und versucht sich als klimafreundlich
darzustellen, um Unternehmen zu überzeugen, in Schlier zu bauen. Angesichts der
Naturzerstörung, durch Flächenversiegelung und mehr Verkehr, ist das eine
Frechheit!", empört sich Janika Bändle (21).

Auch die sogenannten "Froschtümpel", die seit wenigen Wochen angelegt werden,
können nicht ansatzweise so viel CO2 binden, wie die ehemals intakten Böden,
auf denen das Gewerbegebiet entsteht. Zudem ist mehr als fraglich, ob die
selten gewordenen Laubfrösche die künstlich angelegten Minigewässer überhaupt
annehmen. Die Gemeinde Schlier schreibt dazu selbst, "der dann noch offene
Ausgleichsbedarf" werde "über das gemeindliche Ökokonto abgedeckt" [2]. Woraus
dies besteht ist unklar. "Bisschen grün, bisschen Wasser auffangen, bisschen
Baumpflanzexperimente starten und gut ist? Es werden 58 000 m² Fläche
versiegelt und bisher intakte Natur zerstört", fasst Bändle zusammen.

"Dass Gemeinden durch erhoffte Gewerbesteuereinnahmen dazu getrieben werden,
ihre Umwelt zu zerstören, darf nicht sein und ist ein Fehler im System", sagt
Bändle. "Dass es hier gerade den Laubfrosch trifft, scheint mir symbolisch: Um
den Klimawandel ignorieren zu können müssen die Wetterfrösche weichen." 

Der kürzlich vom RVBO beschlossene Regionalplan ermöglicht für die kommenden
Jahre noch viele weitere Gewerbegebiete und schreibt so die aus Sicht der
Aktivist\*innen maßlose Flächenversiegelung in ganz Oberschwaben fort - entgegen
allen Absichtserklärungen zum Klimaschutz [3a,3b,3c]. Verschiedene
Umweltgruppen haben dazu Proteste angekündigt.


## Referenzen

[1a] https://www.schwaebische.de/regional/oberschwaben/schlier/schlier-stimmt-fuer-umstrittenen-supermarkt-50099\
[1b] https://www.schwaebische.de/regional/oberschwaben/schlier/schlier-plant-oekologisches-gewerbegebiet-1282846\
[2] https://www.schlier.de/fileadmin/Dateien/Webseite/Dateien/PDF/Mitteilungsblatt/2022/Schlier_30_2022.pdf\
[3a] https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/klimaaktivisten-protestieren-in-kressbronn-gegen-regionalplan-100.html\
[3b] https://www.schwaebische.de/regional/bodensee/kressbronn/klimacamp-gegen-flaechenfrass-radikale-waldbesetzer-ziehen-nach-kressbronn-1998237\
[3c] https://www.schwaebische.de/regional/bodensee/kressbronn/protestcamp-und-demo-waldbesetzer-bringen-sich-in-kressbronn-in-stellung-2000966
