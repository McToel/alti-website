---
layout: page
title:  "19.02.2022: Aktivist*innen verschenken containerte Lebensmittel in Weingarten"
date:   2022-02-19 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2202190
#          YYMMDDX
---

*Pressemitteilung vom 19. Februar 2022*

# Aktivist\*innen verschenken containerte Lebensmittel in Weingarten

Am Samstag, den 19.02.2022 werden zwischen 10 und 14 Uhr auf dem Löwenplatz aus
dem Müll gerettete Lebensmittel an Passant\*innen verschenkt. Die
Aktivist\*innen wollen mit ihrer angemeldeten Versammlung auf die "unnötige"
Verschwendung von Lebensmitteln aufmerksam machen. In den vergangenen Wochen
hatte es ähnliche Aktionen bereits in Ravensburg gegeben, jetzt wollen die
Aktivist\*innen auch in anderen Städten beispielsweise Tettnang, Friedrichshafen
und Markdorf auf ihr Anliegen aufmerksam machen. "Es darf nicht sein das wir
Essen wegschmeißen und andere hungern müssen." kommentiert Charlie Kiehne (19)
ihre Aktion.

Die Lebensmittel wurden von Aktivist\*innen in den Nächten zuvor aus Mülltonnen
von Supermärkten in Ravensburg und Umgebung  gerettet. Wer sich hierbei
erwischen lässt kann wegen Diebstahl oder sogar schwerem Diebstahl
strafrechtlich verfolgt  werden. "Trotz des Risikos erwischt und strafrechtlich
verfolgt zu werden gehen wir containern. Wir können es nicht weiter tatenlos mit
ansehen, dass gute Lebensmittel im Müll landen anstatt gegessen zu werden." so
Samuel Bosch (19).

"Ich finde, dass das Retten von Lebensmitteln nicht bestraft werden sollte,
sondern das Wegschmeißen von noch haltbaren und guten genießbaren
Lebensmitteln." sagt Aktivistin und Mutter Mareike Siebert (23). 

"Während in anderen Ländern Millionen von Menschen hungern oder gar am
Hungertod oder den Auswirkungen von dauerhafter Mangelernährung sterben, werden
hier in Deutschland jährlich 18 Millionen Tonnen Lebensmittel weggeschmissen und
verbrannt, das ist über ein Drittel der Lebensmittel die Insgesamt produziert
werden. Diese Ungerechtigkeit muss aufhören und  dafür brauchen wir ein
Essen-Retten-Gesetz!" so Hannah Schak (22), eine der Mitstreiter\*innen. "Es
wird viel mehr produziert, als wirklich gebraucht wird. Und in der modernen
Landwirtschaft bedeutet das viel mehr Dünger, Pestizide und auch CO2, das
vollkommen zwecklos die Atmosphäre aufheizt." führt Kiehne weiter aus.
 
Die Aktivisten unterstützen  die Forderungen vom "Aufstand der letzten
Generation", diese haben  mithilfe von Expert\*innen eine Gesetzesvorlage
erstellt, die die Regierung genauso verabschieden könnte, um, nach französischem
Vorbild, "die Problematik der Lebensmittelverschwendung in den Griff zu
bekommen". Außerdem hat die "Letzte Generation" auch lokale Gruppen aufgerufen,
ihre eigenen Aktionen zu diesem Thema zu organisieren. Eine zweite Forderung an
die Bundesregierung ist, unverzüglich eine ökologische Agrarwende einzuleiten.
"Es brauche in der Klimakrise jetzt schnelles Handeln", fügt
Essenverteilerinnen Siebert hinzu.

Wir sehen uns nicht als Teil "Der letzten Generation", aber unterstützen die
wichtigen Anliegen, die sie vertreten. Die Bundesregierung muss endlich die
Verschwendung stoppen! Es darf nicht so weitergehen, dass 40% aller Lebensmittel
im Müll landen. Die großen Märkte müssen gezwungen werden, diese Lebensmittel an
soziale Einrichtungen weiterzugeben." so Manfred Scheurenbrand (66) aus
Waldburg.