---
layout: page
title:  "20.01.2022: Anwohner*innen und Aktivist*innen aus der Besetzung im Altdorfer Wald installieren Kunstaktion in Kiesgrube bei Oberankenreute"
date:   2022-01-20 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2201200
#          YYMMDDX
---

*Pressemitteilung vom 20. Januar 2022*

# Anwohner\*innen und Aktivist\*innen aus der Besetzung im Altdorfer Wald installieren Kunstaktion in Kiesgrube bei Oberankenreute

In der Nacht vom Mittwoch (19.01.) auf den Donnerstag (20.01.) installierten
Anwohner\*innen zusammen mit Aktivist\*innen aus der Altdorfer-Wald-Besetzung
eine Kunstaktion in und über der Kiesgrube bei Oberankenreute. Es wurde ein
großes Banner mit der Aufschrift "Renaturierung!?" aufgehängt und einige alte
Weihnachtsbäume darunter in den Kies "eingepflanzt". Mit ihrer Aktion wollen sie
darauf aufmerksam machen, dass der Umweltschaden, welcher durch den Kiesabbau
verursacht wird, nicht durch das "blosse  Wiederbegrünen" der Fläche
ausgeglichen werden kann.

"Die Zerstörung eines hunderte Jahre alten Mischwaldes kann nicht durch das
blosse Pflanzen einiger Baumreihen wettgemacht werden" so Anwohner Martin Lang
(55). "Bodenbildung dauert Jahrhunderte! Wälder sind komplexe Ökosysteme, welche
von unvorstellbaren Wert für die Artenvielfalt sind." so Lang weiter.

Es sind weiter Kiesgruben und Kiesgrubenerweiterungen mit einer Gesamtfläche von
62ha im Altdorfer Wald geplant. Hierfür ebnet der viel kritisierte, von
Aktivist\*innen und Kritiker\*innen "Klimahöllenplan" genannte Regionalplan den
Weg. Dieser wurde trotz großem Widerstand am 25.06.2021 durch Stimmen von
CDU,SPD und Freie Wähler verabschiedet.

"Das schier unbegrenzte Wachstum der Kiesabbauflächen im Altdorfer Wald
representiert das Streben nach unnendlichem Wachstum im Kapitalismus, welches
Profite über intakte Ökosysteme stellt. Angesichts der Klimakatastrophe müssen
wir uns von diesem Irrglauben verabschieden und endlich aufhören weiter
klimaschädliche Projekte umzusetzen." so Aktivist\*in Charlie Kiehne (19). "Den
Wald mit all seinen Facetten brauchen wir heute und nicht als renaturieretes,
ausgebeutetes Stück Erde in 30 Jahren." fügt Mitaktivisti Samuel Bosch (19)
hinzu.

## Koordinaten
(47.7976098, 9.7229269)