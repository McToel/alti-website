---
title: "Anstatt seines Haftantritts: Klimaaktivist Hängt Banner Vor Jugendarrestanstalt"
date: 2024-03-19T17:07:15+01:00
type: post
draft: false
---

Eine Gruppe Umweltaktivist\*innen protestierte am frühen Dienstagmorgen mit einer gewitzten Kletteraktion hinter der Jugendarrestanstalt Göppingen gegen die Verurteilung von Umweltschützer und Stahlgegner Samuel Bosch und die klimaschädliche Stahlproduktion.
"Klimaschützen ist kein Verbrechen" und "Lohwald-Rodung genehmigen trotz laufender Gerichtsverfahren? Frech!" ist nun auf zwei gespannten Bannern zwischen den Bäumen des Schloßparks zu lesen. 
Dabei ist das eine Banner identisch zu dem, das die Augsburger Jugendrichter\*innen im vergangenen Jahr für üble Nachrede hielten, als sie Samuel Bosch zu drei Wochen Jugendarrest verurteilten [1]. Der im süddeutschen Raum bekannte Umweltschützer, der eigentlich am vergangenen Donnerstag seine Haftstrafe hätte antreten sollen, bekennt sich dazu, an der Aktion beteiligt gewesen zu sein (Bild: siehe Cloudlink) und erklärt: "Wir sind uns sicher, dass es legitim ist, die Bannwaldrodung als "frech" zu benennen und die Öffentlichkeit über die klimaschädlichen Machenschaften von Max Aicher zu informieren. Es muss weiterhin möglich sein, Politiker\*innen zu kritisieren."
In Richtung der Jugendarrestanstalt senden die Aktivist\*innen eine Solidaritätsbotschaft an die Gefangen: "Ihr seid nicht allein" steht auf der Rückseite der beiden angebrachten Banner.

## Bosch entzieht sich Haft
Samuel Bosch entschied sich vergangenen Donnerstag, seiner Haftvorladung vorerst nicht nachzukommen. "Ich habe einen eigenen Terminplan und möchte weiter meinem politischen Engagment nachkommen. ", erklärte Bosch. "Wie normalerweise auch, versuche ich mit Menschen über soziale und klimagerechte Transformation ins Gespräch zu kommen, deshalb nahm ich an einem öffentlichen Podium teil und führte Interessierte am vergangenen Sonntag im Rahmen des wöchentlichen Waldrundgangs durch die Besetzung im Altdorfer Wald."
Bei der Podiumsdiskussion am Ulmer Theater war es Bosch trotz Polizeiüberwachung vor dem Gebäude gelungen, sich unbemerkt zu entfernen. Auch im Altdorfer Wald konnte die Polizei ihn trotz deutlich erhöhter Präsenz bisher nicht festsetzen.
Wann Samuel Bosch nun tatsächlich seine drei Wochen Jugendarrest antreten wird, ist derzeit noch unklar.

## Protest in Demokratie unerwünscht?
Die Menschrechtsorganisation Amnesty International kritisierte Deutschland im vergangenen Jahr für einen unverhältnismäßigen und einschüchternden Umgang mit Klimaaktivismus [2]. In Deutschland würden Proteste von staatlichen Behörden mitunter als „Bedrohung der öffentlichen Ordnung und Sicherheit“ wahrgenommen, [...] anstatt sie als Kernelement eines lebendigen gesellschaftlichen Diskurses zu ermöglichen und zu schützen, sagte Paula Zimmermann, Expertin für Meinungs- und Versammlungsfreiheit bei Amnesty International in Deutschland.
Boschs Mitstreiter\*innen kritisieren auch, dass dadurch Menschen von ihrem Engagement für die Lebensgrundlagen abgebracht werden könnten: "Die Justiz sendet damit ein völlig verdrehtes Signal in unsere Gesellschaft: Es sind vielmehr die Stahl-, Zement- und Autohersteller, die gigantische Verbrechen an der Zukunft unserer Kinder und an den aktuellen Lebensgrundlagen der Menschen im globalen Süden begehen", erklärt Sina Wagner (24), eine Unterstützerin der Aktivist\*innen.

Das fragliche Banner nun in Göppingen erneut zu platzieren, ist laut den Aktivist\*innen voraussichtlich weniger problematisch. Zu dem Augsburger Landgerichtsprozess sagte Anwalt Klaus Schulz: „Aus württembergischer Sicht ist das Urteil definitiv überzogen – in Bayern sind die drauf wie's Messer. Samuel und Charlie sind kaum vorbestraft und trotzdem wurde fast die Höchststrafe verhängt. Ich schätze, das ist im Strafmaß fast Faktor drei im Verhältnis zu Baden-Württemberg“. Dazu passen auch die Ergebnisse einer Studie von Volker Grundies, wonach Gerichte in Bayern (vor allem Augsburg und München) zu deutlich höheren Strafmaßen greifen [3] .

## Hintergrund
Das ursprüngliche Banner in Augsburg war Teil einer Protestaktion gegen die Rodung eines besonders geschützten Bannwaldes, bei Biberbach in der Region Augsburg. Dieser wurde im Herbst 2022, trotz laufender Klage der anliegenden Gemeinde und des BUND Naturschutz, für die Erweiterung eines Stahlwerks von Max Aicher, vorzeitig gerodet. Eine geheime und zweifelhafte Ausnahmegenehmigung, ausgestellt vom Regierungsbezirk Schwaben, machte die Rodung möglich [4].
Als Reaktion auf diese fragwürdigen Umstände, hingen Samuel Bosch, Charlie Kiehne und Ingo Blechschmidt im Rahmen einer Kletteraktion das satirische Banner  "Lohwald-Rodung genehmigen trotz laufender Gerichtsverfahren? Frech!" an die Fassade des Regierungsgebäudes [5].
Im Oktober 2023 wurden Kiehne und Bosch schließlich wegen übler Nachrede gegen den, mittlerweile beförderten, Präsidenten der Regierung von Schwaben in Augsburg zu einer, bzw. drei Wochen Haft (Jugendarrest) verurteilt.
Charlie Kiehne hat eine Woche Haft bereits im Februar abgesessen.
Eine Verfassungsbeschwerde gegen das Urteil wurde eingelegt und wird aktuell noch entschieden.

## Richter kritisiert Aktivismus
Während des Prozesses in Augsburg spielten auch persönliche Meinungen der Richter, wie Aktivismus auszusehen habe, eine Rolle: In der Urteilsbegründung des Jugendrichters am Augsburger Landgerichts äußerte dieser etwa pauschale Kritik an Fridays for Future: Aktivismus sei bedrohlich für die Lebensgrundlagen von acht Milliarden Menschen; man könne nicht alles verbieten.
"Es entsteht der Eindruck, dass nicht die juristische Einschätzung, sondern die Wut über die Aufmüpfigkeit der Aktion entscheidend waren", so Kiki Köffle (20), die den Prozess damals verfolgte. "Wir sollten uns als Gesellschaft darauf einigen, angesichts der Klimakrise endlich zu handeln, statt Aktivisti zu bestrafen."


## Quellen:
[1] https://www.augsburger-allgemeine.de/augsburg/augsburg-klimaaktivisten-scheitern-mit-berufung-und-muessen-in-den-arrest-id68217581.html
[2] https://netzpolitik.org/2023/interaktive-karte-amnesty-kritisiert-einschraenkung-der-versammlungsfreiheit-in-deutschland/
[3] https://www.spiegel.de/panorama/justiz/wo-deutschlands-strengste-richter-sitzen-a-1230399.html
[4] https://www.augsburger-allgemeine.de/augsburg-land/meitingen-lechstahlwerke-roden-lohwald-bei-meitingen-trotz-anhaengiger-klagen-id64346931.html
[5] https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html


## Bilder zur freien Verwendung:

https://drive.google.com/drive/folders/1-2U4ZKztTrkeXoPYgPNDZfy-1ZDNtQi2


-------------

## <a href="/presse"> Pressekontakt </a>