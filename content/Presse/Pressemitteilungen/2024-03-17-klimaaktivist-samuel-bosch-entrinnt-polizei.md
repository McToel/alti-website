---
# Pressemitteilung automatisch erzeugt von pm-bot 5a8f785 am Donnerstag, 28. März 2024 um 12:31:49 GMT+1
title: "Klimaaktivist Samuel Bosch entrinnt Polizei"
date: 2024-03-17T12:00:00.000Z
draft: false
---

Pressemitteilung am 17.03.2024  
  
# Klimaaktivist Samuel Bosch entrinnt Polizei
  
Bereits am Donnerstag (14.03.2024) hätte der im süddeutschen Raum bekannte Klimaaktivist Samuel Bosch (21) eigentlich seine dreiwöchige Haft (Jugendarrest) antreten sollen, hatte sich aber entschieden, der Ladung vorerst nicht nachzukommen.  
Stattdessen nahm er, gemeinsam mit zwei anderen Klimaaktivistinnen, am Freitag Abend bei einer öffentlichen Podiumsdiskussion des Ulmer Theaters teil. Trotz hoher Polizeipräsenz vor dem Theater gelang es ihm danach, unbemerkt das Gebäude zu verlassen. Er kündigt weitere Klimaschutzaktionen an und wird sein soziales und politisches Engagement fortsetzen.  
"Ich möchte mich nicht durch Kriminalisierung davon abhalten lassen, weiter auf Klimagerechtigkeit aufmerksam zu machen und mit Menschen über den gesellschaftlichen Wandel ins Gespräch zu kommen", so Bosch.  
  
Im Oktober 2023 wurde Bosch wegen übler Nachrede des mittlerweile beförderten Präsidenten der Regierung von Schwaben in Augsburg zu drei Wochen Haft (Jugendarrest) verurteilt. Gemeinsam mit Charlie Kiehne und Dr. Ingo Blechschmidt hing er im Rahmen einer Kletteraktion ein Banner an die Gebäudefassade. "**Lohwald-Rodung genemigen troz laufender Gerichtsverfahren? Frech!**" war so in großen Lettern zu lesen. Das Banner bezieht sich auf einen nach dem Bayerischen Waldgesetz besonders geschützten Bannwald bei Biberbach, der damals trotz laufender Normenkontrollklage der Gemeinde für die Erweiterung eines Stahlwerks nahe Augsburg vorzeitig gerodet wurde.  
  
In Ulm fand am Freitagabend die Dernière des in Graz uraufgeführten Stücks "Was zündet, was brennt" statt. "Was ist die eine Geschichte, die erzählt werden müsste, um uns alle in Handlung zu versetzen?", war die Leitfrage des Stücks . Inhaltlich geht es um zwei Klimaaktivistinnen, die einen Wachmann von ihrer Haltung in Bezug auf die Klimakrise überzeugen möchten. Im Anschluss an die Aufführung saßen im Rahmen des Publikumsgesprächs drei Aktivist\*innen, darunter Samuel Bosch, auf dem Podium.  
  
Zum Thema der Verfolgung von Klimaaktivisten sagte Aktivistin Fabia Tömösy-Moussong: "Die Aktivist\*innen im Stück, aber auch Samuel und Charlie, versuchen, den Lauf der Geschichte zum Positiven zu beeinflussen und dabei ihre Möglichkeiten auszuschöpfen. Dafür gehen sie hohe persönliche Risiken ein und brechen mit gesellschaftlichen Normen."  
  
BILDER:  
    siehe Anhang  
  