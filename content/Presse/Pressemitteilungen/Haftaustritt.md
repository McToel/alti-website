---
title: "Bundesverfassungsgericht: Ravensburger Klimaaktivist Samuel Bosch muss sofort aus Haft entlassen werden"
date: 2024-04-04T23:20:27+02:00
img:
    src: img/regierung-von-schwaben.jpg
    alt: Die Regierung von Schwaben stellte trotz laufender Rechtswidrigkeitsprüfung durch den Bayerischen Verwaltungsgerichtshof eine Ausnahmegenehmigung zur vorgezogenen Rodung eines Bannwalds aus.
type: post
draft: false
---

**Regierung von Schwaben muss sich Kritik gefallen lassen,
Inhaftierung von Samuel Bosch grundgesetzwidrig,
Bosch musste sofort entlassen werden**

+++ Pressekonferenz am morgigen Freitag (5.4.) um 11:00 Uhr in der Ravensburger
Innenstadt, genauer Ort wird auf [24] bekannt gegeben +++

„Lohwald-Rodung trotz laufender Gerichtsverfahren? Frech!“ Ein Banner mit
dieser Aufschrift hatten Aktivist\*innen und Unterstützer\*innen des Augsburger
Klimacamps im Oktober 2022 an der Fassade der Regierung von Schwaben angebracht
[1-5] – und wurden dafür vom Augsburger Amts- und Landgericht hart bestraft
[27-33], die dpa berichtete mehrmals über den Vorgang: drei Wochen Haft für
Samuel Bosch (21), eine Woche Haft für Charlie Kiehne (21) und ein halbes
Jahresgehalt Geldstrafe für Ingo Blechschmidt (35). Das
Bundesverfassungsgericht beschloss nun: Die Augsburger Urteile waren
rechtswidrig, das Grundrecht auf Meinungsfreiheit wurde von den Gerichten
missachtet. Damit musste die Jugendarrestanstalt Göppingen Bosch, in der er bis
heute Abend inhaftiert war, sofort entlassen.

„Eine finanzielle Haftentschädigung kann die zwei Wochen Haftzeit nicht wieder
gut machen“, erklärt Bosch. „Aber der eigentliche Schaden liegt in den Folgen
der Erdaufheizung. Es lohnt sich, für Klimagerechtigkeit zu kämpfen, auch wenn
dies bedeutet, eingesperrt zu werden.“

„Die Verfassungsbeschwerde ist [...] offensichtlich begründet.“ (RdNr. 11)
„Die angegriffenen Entscheidungen verletzen den Beschwerdeführer in seinem
Grundrecht auf Meinungsfreiheit aus Art. 5 Abs. 1 Satz 1 GG“ (RdNr. 11), so der
unanfechtbare höchstrichterliche Beschluss [25]. Bei Äußerungen wie der Kritik
an der Regierung von Schwaben sei zu beachten, wenn diese „nicht zum Zwecke
privater Auseinandersetzung“ getätigt werden, sondern „zur Bildung der
öffentlichen Meinung beitragen“ (RdNr. 14). Explizit erwähnt das Gericht, dass
der „Schutz der Meinungsfreiheit gerade aus dem besonderen Schutzbedürfnis der
Machtkritik erwachsen [ist]“ (RdNr. 12). „Die [Augsburger] Gerichte gehen mit
einer verfassungsrechtlichen Maßstäben nicht genügenden Begründung vom
Vorliegen einer Tatsachenbehauptung aus und verkürzen damit den
grundrechtlichen Schutz der Meinungsfreiheit. [...] Insbesondere erfolgt
jeweils keinerlei Einordnung in den Kontext“ (RdNr. 18).

+++ Pressekonferenz am morgigen Freitag (5.4.) um 11:00 Uhr in der Ravensburger
Innenstadt, genauer Ort wird auf [24] bekannt gegeben +++


### Rechtlich besonders geschützter Bannwald

Der Lohwald ist ein nach dem Bayerischen Waldgesetz rechtlich geschützter
Bannwald bei Biberbach und Langweid. Art. 9 dieses Gesetzes setzt für
Rodungsvorhaben bei Bannwäldern voraus, dass „zwingende Gründe des öffentlichen
Wohls es erfordern“ [6]. Ein Interesse an der Rodung des Lohwalds hatten die
angrenzenden Lech-Stahlwerke, die behaupteten, ihr Werksgelände vergrößern zu
wollen. Mehr als ein Jahr später liegt die Rodungsfläche indes immer noch
brach, wie eine Foto-Dokumentation von Fridays for Future Augsburg ergab [7].
Thomas Frey, der Regionalreferent des BUND Naturschutz, sprach in Bezug auf die
Rodung damals von einer „perfiden Aktion“ und einer „Watschen für die
engagierte Bürgerschaft“ [8].

Zum Zeitpunkt der Rodung war der Bayerische Verwaltungsgerichtshof mit einer
Rechtswidrigkeitsprüfung des Rodungsvorhabens beschäftigt – und ist es immer
noch [9,10]. Die an den Wald angrenzende Gemeinde Biberbach hatte nämlich, wie
auch der BUND Naturschutz, eine Normenkontrollklage eingereicht. Diese Klagen
entfalteten allerdings keinen einstweiligen Rechtsschutz, da die Rodung erst
für den Oktober 2023, nach Abschluss gewisser artenschutzrechtlicher Maßnahmen,
vorgesehen war und daher vermeintlich keine Eilbedürftigkeit bestand. Die
Aktivist\*innen kritisierten, dass die Regierung von Schwaben mit einer
Ausnahmegenehmigung der vorgezogenen Rodung des Lohwalds Tür und Tor öffnete,
ohne die Gemeinden darüber zu informieren, und sie so der Möglichkeit beraubte,
einstweiligen Rechtsschutz zu beantragen.


### Höchstrichterlicher Beschluss mit Strahlkraft

Der Beschluss des Bundesverfassungsgerichts bezieht sich zunächst nur auf das
Augsburger Urteil gegen Bosch, den die Arrestanstalt nun sofort aus der Haft
entlassen musste. Seine ebenfalls verurteilten Mitstreiter\*innen Charlie Kiehne
und Ingo Blechschmidt hatten bislang keine Verfassungsbeschwerde eingereicht.
Für Kiehne kommt der Beschluss auch zu spät: Kiehne saß die Haft bereits ab.
Kiehne kann höchstens Haftentschädigung von 75 Euro pro Tag beantragen.
Blechschmidt könnte sein als Strafe gezahltes halbes Jahresgehalt
zurückerhalten.

„Ein Beschluss mit Strahlkraft“, findet Blechschmidt. Die höchstrichterliche
Entscheidung wertet er nicht nur in Bezug auf ihre Kritik an der Regierung von
Schwaben als wichtiges positives Zeichen: Seit letztem Jahr ist Deutschland
aufgrund von immer weiter zunehmenden Einschränkungen des Versammlungsrechts
auf der internationalen Ächtungsliste der Menschenrechtsorganisation Amnesty
International gelandet – neben Ländern wie China und dem Iran [11,12]. In
Deutschland würden Proteste von staatlichen Behörden mitunter als „Bedrohung
der öffentlichen Ordnung und Sicherheit“ wahrgenommen, [...] anstatt sie als
Kernelement eines lebendigen gesellschaftlichen Diskurses zu ermöglichen und zu
schützen, sagte Paula Zimmermann, Expertin für Meinungs- und
Versammlungsfreiheit bei Amnesty International in Deutschland. „Heute ist ein
guter Tag für zivilgesellschaftliches Engagement. Mit seinem wegweisenden
Beschluss verteidigte das Bundesverfassungsgericht die dafür so wichtige
Versammlungs- und Meinungsfreiheit“, so Ingo Blechschmidt vom Augsburger
Klimacamp. Das Bundesverfassungsgericht folgt mit seinem Beschluss auch einer
Forderung der UN, Klimaaktivist\*innen stärker zu schützen [13].

Die Augsburger Staatsanwaltschaft steht überregional in der Kritik [14,15,16a],
sogar die New York Times berichtete schon über ihre Übergriffe [16b,17].
Charlie Kiehne kritisiert, dass dadurch Menschen von ihrem Engagement für die
Lebensgrundlagen abgebracht werden könnten: „Die Augsburger Justiz sendet ein
völlig verdrehtes Signal in unsere Gesellschaft: Es sind vielmehr die Stahl-,
Zement- und Autohersteller, die gigantische Verbrechen an der Zukunft unserer
Kinder und an den aktuellen Lebensgrundlagen der Menschen im globalen Süden
begehen“. Eine ähnliche Haltung vertritt auch UN-Generalsekretär António
Guterres, der in einer Rede erklärte: „Klimaaktivisten werden manchmal als
‚gefährliche Radikale‘ dargestellt. Aber die wirklich gefährlichen Radikalen
sind die Länder, die die Produktion von fossilen Brennstoffen erhöhen“ [18].

Während des Prozesses in Augsburg spielten auch persönliche Meinungen der
Richter, wie Aktivismus auszusehen habe, eine Rolle. In der Urteilsbegründung
des Jugendrichters am Augsburger Landgerichts äußerte Richter Rauh etwa
pauschale Kritik an Fridays for Future: Aktivismus sei bedrohlich für die
Lebensgrundlagen von acht Milliarden Menschen; man könne nicht alles verbieten.
„In der Verhandlung gab es von Seiten des Landgerichts zwar viele Emotionen und
persönliche Meinungen, wie Aktivismus auszusehen hätte, aber leider wenig
sachliche Erklärung“, kommentierte damals Boschs Strafverteidiger Klaus Schulz.


### Während Haft Workshop-Woche im besetzten Altdorfer Wald organisiert

Die Zeit in der Haft hat Bosch genutzt, um sein Engagement für
Klimagerechtigkeit auf andere Weise fortzuführen. Er richtete eine Art
zweigeteiltes Büro ein, per Post unterhielt er ausführlich Korrespondenz mit
Bürgerinitiativen, Freund\*innen und Aktivist\*innen. Auf diese Weise
organisierte er aus der Arrestanstalt heraus eine umfangreiche Workshop-Woche
in der von ihm mitinitiierten Besetzung im Altdorfer Wald bei Ravensburg: Vom
17. bis zum 26. Mai 2024 wird es in den Baumhausdörfern ein vielfältiges
Programm geben. Bürger\*innen können dort lernen, wie sie in Notsituationen
Bäume zu ihrem Schutz besetzen können, oder wie man Städte verklagen kann, wenn
sie Demonstrationen verbieten [19]. Daneben werden auch Dokumentarfilme rund um
die seit mehr als zwei Jahren bestehende Besetzung gezeigt [20-22].

Interessierte können auf www.ravensburg.klimacamp.eu weitere Details nachlesen.
Es werden Aktivist\*innen aus ganz Europa erwartet, die meisten Workshops werden
auf Deutsch und auf Englisch angeboten.


### Solidaritätsaktionen in Göppingen

Boschs Haft wurde von mehreren Solidaritätsaktionen begleitet, etwa am
gestrigen Mittwochnachmittag mit einem Banner mit der Aufschrift "Ihr seid
nicht alleine!", das eine Unterstützerin ohne Genehmigung in einem Baum direkt
vor der Jugendarrestanstalt anbrachte. Auch Boschs Mitgefangene haben sich sehr
über diese Aktion gefreut; Fotos der Aktion, die ihnen heute per Post
zugespielt wurden, mussten sie zwischenzeitlich aber auch wieder abgeben.

Davor gab es am Freitag (29.3.) ein öffentliches Picknick vor der
Arrestanstalt; der Zeitpunkt war mit Bedacht gewählt, da es über die Feiertage
kein anstaltsinternes Programm gibt.

Am heutigen Donnerstag brachten Unterstützer\*innen Flyer in der Regierung von
Schwaben an, die Mitarbeiter\*innen dazu aufriefen, zur Aufklärung der
vorgezogenen Bannwaldrodung beizutragen und mögliche bislang verschlossene
Informationen zu veröffentlichen [23].


### Referenzen

[1] https://www.augsburger-allgemeine.de/augsburg-land/augsburgmeitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufskorn-id64382891.html\
[2] https://www.augsburg.tv/mediathek/video/aerger-um-protestermittlungen-gegen-augsburger-klimaaktivisten/\
[3] https://www.sueddeutsche.de/bayern/augsburg-lech-stahlwerkeklimacamp-polizei-regierung-von-schwaben-1.5682862\
[4] https://www.zeit.de/news/2022-10/27/ermittlungen-nachprotestaktion-bei-regierungsbehoerde\
[5] https://www.daz-augsburg.de/90948-2/\
[6] https://www.gesetze-bayern.de/Content/Document/BayWaldG/true\
[7] https://www.lohwibleibt.de/fotoreportage_2023/\
[8] https://www.augsburger-allgemeine.de/augsburg-land/meitingen-lechstahlwerke-roden-lohwald-bei-meitingen-trotz-anhaengiger-klagen-id64346931.html\
[9] https://web.archive.org/web/20221024160357/https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV\
[10] https://web.archive.org/web/20230227175157/https://www.br.de/nachrichten/bayern/bannwald-rodung-bei-stahlwerken-vor-gericht,TQRhKFH\
[11] https://www.fr.de/politik/klimaaktivisten-amnesty-international-deutschland-versammlungsfreiheit-repression-proteste-liste-zr-92528884.html\
[12] https://www.amnesty.de/allgemein/pressemitteilung/versammlungsfreiheit-proteste-weltweit-interaktive-karte-protest-map\
[13] https://www.zeit.de/politik/ausland/2023-05/razzia-klimaschuetzer-un-schutz-antonio-guterres\
[14] https://de.wikipedia.org/wiki/Staatsanwaltschaft_Augsburg#%C3%96ffentliche_Kritik_an_der_Arbeitsweise\
[15] https://netzpolitik.org/2022/augsburg-gegen-demonstrationsfreiheit-mit-einer-liberalen-demokratie-nicht-zu-vereinbaren/\
[16a] https://www.stern.de/gesellschaft/janika-pondorf---ich-war-wegen-dieses-polizeieinsatzes-in-der-klinik--31895656.html\
[16b] https://www.pimmelgate-süd.de/\
[17] https://www.nytimes.com/2022/09/23/technology/germany-internet-speech-arrest.html\
[18] https://www.rnd.de/politik/antonio-guterres-bezeichnet-neuen-klimabericht-als-dokument-der-schande-XLA2Z4M2HPT7WCMKNPVAYSNUMM.html\
[19] https://www.augsburger-allgemeine.de/neu-ulm/ulm-wegen-kletterverbot-klimaaktivisten-verklagen-stadt-ulm-id69604311.html\
[20] https://classic-ravensburg.klimacamp-augsburg.de/pressespiegel/schwaebische-1423309.jpeg\
[21] https://www.pangolin-doxx.com/filme/menschen-die-auf-baeume-steigen/\
[22] https://www.swp.de/lokales/ulm/aktion-auf-adenauerbruecke-prozess-startet_-geld-oder-haftstrafe-fuer-klima-kletterer_-73214065.html\
[23] https://www.klimacamp-augsburg.de/pressemitteilungen/2024-04-04_Flyer\
[24] https://etherpad.wikimedia.org/p/r.4b6cfff13433e4336ae369655d900a7c\
[25] https://www.speicherleck.de/iblech/stuff/.rvs2/bverfg.pdf\
[26] https://www.swp.de/lokales/goeppingen/klimaschuetzer-in-jugendarrestanstalt-_luecke-im-terminplan_-aktivist-samuel-bosch-hat-arrest-in-goeppingen-angetreten-73373989.html\
[27] https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/klimaaktivist-protest-ulm-gefaengnis-100.html\
[28] https://www.schwaebische.de/regional/oberschwaben/ravensburg/samuel-bosch-und-charlie-kiehne-rechtskraeftig-verurteilt-1986484\
[29] https://www.schwaebische.de/regional/oberschwaben/vogt/klimaaktivist-samuel-bosch-muss-fuer-drei-wochen-in-jugendarrest-2344750\
[30] https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/nach-berufung-gericht-verurteilt-ravensburger-klimaaktivisten;art410936,11761080\
[31] https://www.augsburger-allgemeine.de/augsburg/augsburg-klimaaktivisten-scheitern-mit-berufung-und-muessen-in-den-arrest-id68217581.html\
[32] https://www.augsburger-allgemeine.de/augsburg/prozess-in-augsburg-klimaaktivisten-muessen-nach-abseil-aktion-in-arrest-id66971786.html\
[33] https://www.kontextwochenzeitung.de/gesellschaft/642/richterinfehlt-die-politische-reife-8971.html


### Hinweis

Bosch trat die Haft selbstbestimmt am 21. März an, nachdem er für den 13. März
geladen war [26]. „Ich hatte einige feste Termine für meinen Klimaaktivismus,
daher wäre ein pünktlicher Haftantritt unsinnig gewesen“, hatte Bosch in einer
Mitteilung angekündigt. In der Zeit zwischen angeordnetem Haftantritt und dem
21. März nahm Bosch an einer öffentlichen Podiumsdiskussion beim Ulmer Theater
teil, führte Interessierte im Rahmen des wöchentlichen Waldrundgangs durch die
Besetzung im Altdorfer Wald und beteiligte sich an einer Diskussion im
"Denksalon" des neuen Ravensburger Kunstvereins mit Oberbürgermeister Rapp über
Rechtsextremismus.

Bosch führte ein ausführliches Knasttagebuch, das in Auszügen veröffentlicht
werden wird. Auf Anfrage kann dieses geteilt werden. Bosch, Kiehne und
Blechschmidt sind im Einklang mit früherer Berichterstattung mit der Nennung
ihrer vollen Namen einverstanden. Fotos der zu Unrecht so verurteilten Aktion
zur freien Verwendung gibt es auf:
https://www.speicherleck.de/iblech/stuff/.rvs2 Der Beschluss des
Bundesverfassungsgerichts findet sich dort auch.

Pressekonferenz am morgigen Freitag (5.4.) um 11:00 Uhr in der Ravensburger
Innenstadt, genauer Ort wird auf [24] bekannt gegeben.
