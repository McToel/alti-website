---
layout: page
title:  "20.06.2021: Klimacamper*innen machen mit Fahrraddemo über die A96 auf fehlende Verkehrsplanung im Regionalplanentwurf aufmerksam"
date:   2021-06-20 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2106200
#          YYMMDDX
---

*Pressemitteilung des Ravensburger Klimacamps am 20.6.2021*

# Klimacamper\*innen machen mit Fahrraddemo über die A96 auf fehlende Verkehrsplanung im Regionalplanentwurf aufmerksam

Am heutigen Sonntag (20.6.2021) radelten Klimagerechtigkeitsaktivist\*innen um
kurz nach 12:00 Uhr über einen Teil der Autobahn A96. Zuvor hatte die Stadt die
Demonstration untersagt, das Verwaltungsgericht Sigmaringen befand das Verbot
allerdings für rechtswidrig.

In Bodensee-Oberschwaben verläuft nur eine einzige Autobahn, die A96. Samuel
Bosch (18): "Deswegen wäre es nicht zu viel erwartet gewesen, wenn der
Regionalplanentwurf zumindest für die A96 eine kluge Umgestaltung im Sinne einer
Mobilitätswende vorsehen würde. Stattdessen finden sich dort Waldrodungen für
das größte Straßenneubauprojekt der letzten Jahrzehnte. Unsere Steuermittel
sollen für Straßen ausgegeben werden, die in 10 Jahren niemand mehr braucht. Wir
stellen uns gegen diesen Klimahöllenplan!"

Die Stadt Wangen hatte, offenbar in Unkenntnis der zahlreichen
Fahrraddemonstrationen auf Autobahnen in den letzten Wochen und Monaten,
argumentiert, dass Autobahnen grundsätzlich nicht für Demonstrationen zur
Verfügung stünden und dass trotz des kurzen Abschnitts eine mehrstündige
Sperrung der Autobahn nötig sei. Dem war nicht so.