---
title: "Beton Werksleitung Lässt Banner Hängen"
date: 2023-06-26T23:11:50+02:00
type: post
draft: false
---

Klimaaktivisti haben am Montagvormittag den Betrieb des Betonwerks von "Heidelberg Beton" in Ravensburg über rund sieben Stunden blockiert.
"Jede Sekunde, die das Werk still steht, bedeutet weniger CO2-Ausstoß und ist ein Zeichen gegen den klimaschädlichen Baustoff Beton", sagte einer der Aktivisten, Samuel Bosch am Mittag. "Um die Blockierung ihres Betriebs zu verhindern war das Betonwerk gezwungen, unsere Kritik in Form des Banners zuzulassen und uns das sogar schriftlich zu bestätigen."
Gegen 12 Uhr hatten die Klimaaktivisti nach Verhandlungen mit der Polizei eingewilligt, selbstständig nach unten zu kommen. Im Gegenzug willigte die Geschäftsführung von Heidelberg Beton schriftlich ein, das Banner noch bis heute um 17 Uhr hängen zu lassen. So bleibt der ergänzte Schriftzug "Heidelberg Beton zerstört das Klima enorm" noch bis zum frühen Abend weithin sichtbar.
Die Ravensburger Polizei teilte dem eigentlich per Haftbefehl gesuchten Samuel Bosch vom Boden aus mit, dass der Haftbefehl des Amtsgerichts Augsburg außer Vollzug gesetzt sei und ließ ihn nach Feststellung seiner Personalien gehen. Das zweite Aktivisti wollte sich nicht kriminalisieren lasssen und wurde zur Festellung der Personalien von der Polizei in Gewahrsam genommen. Wann die Person frei kommt, ist noch unbekannt.


Mit der Abseilaktion protestierten die Umweltaktivist*innen gegen den klimaschädlichen Baustoff Beton und für eine Bauwende hin zu mehr Sanierung und weniger Neubau. Die Bauindustrie verarbeitet neben Beton auch viel Stahl und ist deshalb stark abhängig von Stahlwerken wie dem Lechstahlwerk, gegen das sich Charlie Kiehne und Samuel Bosch bei eben der Aktion im Oktober 2022 richteten, wegen der sie am Dienstag in Augsburg angeklagt sind.

Zu dem nur knapp entgangenen Gefängnisaufenthalt sagte Bosch: ,,Mir war klar, dass es bei dieser Aktion auch zu einer Festnahme kommen kann, vor allem, weil ich ja mit Haftbefehl gesucht wurde. Aber dieses persönliche Risiko ist im Vergleich zur Klimakatastrophe extrem gering. Eine Haftstrafe ist nichts dagegen, seine Heimat wegen Überschwemmungen zu verlieren oder wegen Dürren nichts mehr zu essen zu haben'', so der 20-Jährige.
"Es hat mich aber sehr überrascht, dass das Augsburger Amtsgericht nun offenbar zurückrudert und damit unsere Rechtseinschätzung implizit bestätigt."
 
Spontan organisieren AnwohnerInnen heute Abend um 18.30 Uhr eine Demonstration gegen das Betonwerk.
Eine Gruppe aus BürgerInnen aus der Region unterstützt die Kritik an Heidelberg Cement und organisiert heute Abend um 18. 30 Uhr eine Kundgebung vor dem Betonwerk in der Deisenfangstraße 43 in Ravensburg. "Dass Konzerne wie Heidelberg Cement ungestraft unsere Atmosphäre zerstören, finde ich ungeheuerlich. Ich unterstütze diese mutige und wirkungsvolle Aktion der Aktivisti ", sagt Jürgen Pfeffer aus Ravensburg zu den Geschehnissen.

[Siehe Pressemitteilung der Aktion am Betonwerk]({{< ref "Presse/Pressemitteilungen/Ravensburger Klimaaktivisten setzen bildstarkes Zeichen gegen klimaschädliche Betonproduktion – Inhaftierung eines per Haftbefehl gesuchten Aktivisten wahrscheinlich.md" >}})

[Bilder zur freien Verfügung](https://drive.google.com/drive/folders/1V8ynlCzGO9s_KSSqfY-ij2alr1YRpxex?usp=sharing)


-------------

## <a href="/presse"> Pressekontakt </a>