---
title: "Theaterstück über Waldbesetzung -   Aktivistengruppe, Meteorologe und Bürgermeister stehen mit auf der Bühne"
date: 2024-09-29T20:31:50+02:00
type: post
draft: false
img:
    src: img/theaterstück.png
    alt: Bild der Theateraufführung "Kiesgold" in der Kletterhalle
---

Im Altdorfer Wald, einem Naturjuwel Oberschwabens, ist eine weitere Kiesgrube geplant. Dagegen regt sich zunehmender Widerstand. Nachdem Anwohner\*innen schon seit Generationen demonstrieren, haben Umweltaktivisten im Wald ein Baumhüttendorf errichtet und wehren sich hartnäckig gegen die geplante Rodung für den Kiesabbau. Das Projekt sorgt öfter für Wirbel, so zum Beispiel als das Bundesverfassungsgericht involviert war, als es ein Hafturteil gegen einen der Besetzungsinitiator\*innen aufhob (siehe Referenzen unten). Manche Aktivist\*innen leben seit über drei Jahren dort. Inzwischen ist dieses Protestcamp nach dem Hambacher Forst im Rheinischen Braunkohlerevier, die am längsten existierende Waldbesetzung in Deutschland.

Die Städte Baienfurt und Baindt klagen vor dem Verwaltungsgerichtshof für die Aussetzung der Genehmigung eines Kiesabbaus. In Baienfurt steht der dortige Bürgermeister Günter Binder sogar selbst mit auf der Theaterbühne. Die Gemeinden sorgen sich um das Wasserreservoir unter dem Kies des Altdorfer Waldes und die zukünftige Wasserversorgung des Schussentals. In Ulm und Ausgsburg wird zusätzlich eine Szene über den Protest am Ulmer Münster ("Wäre Jesus Klimaaktivist?") gespielt.

Das Theaterstück „Kiesgold“ geht auf die politische Auseinandersetzung um den geplanten Abbau von Kies im Altdorfer Wald ein und appelliert an mehr Klima- und Umweltschutz angesichts der Risiken der zunehmenden Klimakrise. Zudem erklärt Wetterexperte Roland Roth auf der Bühne unser heimisches Wetter und den
Klimawandel. Musikalisch umrahmt wird das Theater durch die Musiker Peter Zoufal und Albert Buecheler.


## Aufführungen:

- Konzerthaus Ravensburg, Mittwoch 02.10.2024 19:30 Uhr
- Stadthaus Ulm, Sonntag 06.10.2024 18:30 Uhr
- Gemeindehalle Baienfurt, Samstag 12.10.2024 19:30 Uhr
- Kulturhaus Abraxas Augsburg, Donnerstag 17.10.2024 19:30 Uhr

Karten: Reservix, Stichwort Kiesgold oder Abendkasse


## Referenzen:
- https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/demo-im-altdorfer-wald-gegen-kiesabbau-104.html
- https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/klimaaktivist-protest-ulm-gefaengnis-100.html
- https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/klimaaktivisten-protestieren-in-kressbronn-gegen-regionalplan-100.html
- https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/haftbefehle-gegen-klimaaktivisten-aus-ravensburg-100.html
- https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/klimaaktivisten-befestigen-banner-an-turm-ulmer-muenster-100.html
- https://ravensburg.klimacamp.eu/presse/pressemitteilungen/haftaustritt/


-------------

## <a href="/presse"> Pressekontakt </a>