---
layout: page
title:  "24.07.2021: Radaktivist*innen installieren Kunstaktion im Lauratal"
date:   2021-07-24 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2107240
#          YYMMDDX
---

*Pressemitteilung vom Ravensburger Klimacamp am 24.7.2021*

# Radaktivist\*innen installieren Kunstaktion im Lauratal

In der Nacht vom Freitag (23.7.) auf den Samstag (24.7.) installierten
Radaktivist\*innen des Ravensburger Klimacamps eine Kunstaktion im Lauratal.
Quer über der Kreisstraße 7948 von Schlier nach Weingarten spannten sie in 6
Meter Höhe ein Seil, an dem sie zusammen mit einem Banner "Mehr Platz fürs" ein
Fahrrad befestigten. Dem "lange gehegten Wunsch der Schlierer und Weingarter"
[[1]] möchten sie damit mehr Sichtbarkeit verschaffen: Die einseitige politische
Bevorzugung des Autoverkehrs soll ein Ende haben. Bis zum Mittwoch soll die
Kunstaktion dauern, danach bauen die Aktivist\*innen sie wieder vollständig ab.

"Unsere Kunstaktion für eine klimagerechte Verkehrswende ist ein deutliches
Zeichen für einen sicheren Radverkehr dessen Ausbau oberste Priorität in der
heutigen Verkehrsplanung haben muss", erklärt Anwohner Martin Lang (54) aus
Oberankenreute sein Engagement. Die Aktion unterstützt er nicht vom Boden aus,
sondern aktiv beim Klettern. "Der Verkehrssektor ist der einzige Sektor, in dem
die CO2-Emissionen kontinuierlich steigen. So geht es nicht weiter! Wir Anwohner
möchten öfter aufs Fahrrad umsteigen, aber dazu braucht es mehr sichere
Fahrradwege!"

Langs Mitstreiter Samuel Bosch (18) ergänzt: "Bevor ich in den Alti zog, lebte
ich in Schlier. Das Lauratal war direkt vor meiner Haustür. Dass dort Autos
immer noch mit 60 km/h fahren dürfen, ist mir unbegreiflich."

Die Kunstaktion ergänzt einen Demonstrationszug der Grünen anlässlich eines
Pilotprojekts, bei dem die Kreisstraße 7948 für ein Wochenende für den
Autoverkehr (abgesehen Anwohner\*innen und Einsatzfahrzeuge) gesperrt wird. Der
Treffpunkt ist am 24. Juli um 18:00 Uhr am Ortsausgang Weingarten.

## Quellen
[1]: https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-das-lauratal-gehoert-ein-wochenende-lang-den-radlern-_arid,11387713.html
[1] [https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-das-lauratal-gehoert-ein-wochenende-lang-den-radlern-_arid,11387713.html][1]

## Politischer Hintergrund
In Weingarten bilden die Grünen zwar die stärkste Fraktion, werden jedoch oft
von den Mitgliedern von CDU, FWW und SPD überstimmt. Bosch kritisiert besonders,
dass bei einer vor kurzem erfolgten Sanierungsmaßnahme der Kreisstraße 7948, in
Zuge derer sogar Bäume am Straßenrand gefällt wurden, kein Fahrradweg
eingerichtet wurde. "Die soziale Mobilitätswende scheitert in Weingarten an CDU,
FWW und SPD", zieht Bosch da Fazit aus der politischen Debatte der letzten
Jahre. "Aus den ursprünglichen Vorschlägen der Grünen ist jetzt nur ein
Einzelwochenende für den Radverkehr geworden."

## Hinweis
Das Seil ist spezielles baumschonendes Polypropylen-Seil (kein Stahlseil) mit
einer Traglast von mindestens 560 kg. Bei der Traverse über die Straße handelt
es sich um eine professionelle, justierbare Bannertraverse. Fahrrad und Banner
sind verkehrssicher befestigt. Der Lichtraum der Straße, der sich bis 4,30 Meter
Höhe erstreckt, wird von der Kunstaktion nicht berührt.

## Koordinaten
47.796869,9.654705