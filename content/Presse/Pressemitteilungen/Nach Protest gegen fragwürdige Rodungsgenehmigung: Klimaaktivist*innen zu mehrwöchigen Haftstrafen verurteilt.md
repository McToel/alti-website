---
title: "Nach Protest Gegen Fragwürdige Rodungsgenehmigung: Klimaaktivist*innen Zu Mehrwöchigen Haftstrafen Verurteilt"
date: 2023-06-29T12:06:38+02:00
type: post
draft: false
---

Am gestrigen Dienstag (27.6.2023) fand am Amtsgericht Augsburg der Prozess gegen die zwei Klimaaktivist:innen Samuel Bosch (20) und Charlie Kiehne (21) statt, die im Oktober 2022 die Regierung von Schwaben aufs Korn genommen hatten [3]. Sie hatten gegen eine fragwürdige Rodungsgenehmigung der Regierung und für eine Reduktion der Stahlproduktion protestiert. Entgegen der Forderung der Staatsanwaltschaft wurden die Aktivist:innen statt zu sieben Monaten Haft ohne Bewährung zu ein und drei Wochen Jugendarrest verurteilt. Die Aktivist:innen legten Berufung ein und kündigten an, sich auch in Zukunft gegen Klimazerstörung zu stellen. Ein juristisches Nachspiel könnten wertende Äußerungen von Jugendrichterin Sandra Mayer und Staatsanwalt Lukas Peltsarszky haben.

## Wut über den Regelverstoß überwiegt

„Dieses Urteil zeigt, wie wenig ernst die Klimakrise immer noch genommen wird. Während es immer noch erlaubt ist, unsere Lebensgrundlagen zu zerstören – obwohl bereits Menschen wegen der Klimakatastrophe ihr Zuhause verlieren – werden hier mutige junge Menschen kriminalisiert, weil sie versuchen, im Grundgesetz verankerte Werte zu verteidigen. Die Forderung der Staatsanwaltschaft nach sieben Monaten Haft sind offensichtlich unverhältnismäßig im Bezug auf eine friedliche Protestaktion. Unsere Protestaktionen zeigen Wirkung. Darum ist die Staatsanwaltschaft gezwungen, zu härteren Strafen zu greifen um das zerstörerische Wachstum um jeden Preis weiter zu verteidigen. Wut über den bloßen Regelverstoß überwiegt und Sachargumente verlieren an Bedeutung“, empört sich Martin Lang. Der 56-jährige aus Ravensburg war extra mit der Bahn angereist, um den Prozess zu verfolgen.

Kiehne: „Stahl ist einer der klimaschädlichsten Baustoffe. Der Gebäude- und Bausektor trägt, laut einem UNO-Bericht [1] weltweit zu 38% aller CO2-Emissionen bei. Die Emissionen, die bei der Betonproduktion entstehen, lassen sich auch nicht wirklich reduzieren, weil dabei immer enorm viel Energie benötigt wird, die sich nicht allein aus Erneuerbaren decken lässt. Wir brauchen endlich eine Bauwende, weg von klimaschädlichen Materialien wie Beton und Stahl und hin zu viel weniger Neubauten und klimafreundlicheren Baustoffen.“

Diese Bauwende könne dabei auch soziale Fragen berücksichtigen. Zum Beispiel können Leerstände für günstigen Wohnraum genutzt und Sanierungsarbeiten mehr staatlich unterstützt werden.
„Von der Bauwende profitieren wir alle", so Bosch weiter. „Mit einer Bauwende können wir Lebensräume erhalten und massiv CO2-Emissionen vermeiden. Die einzigen, die dabei Verluste machen, sind klimaschädliche Konzerne wie die Lech-Stahlwerke. Es ist nicht überraschend, dass sie mit allen Mitteln versuchen, möglichst schnell Tatsachen zu schaffen und ihr gefährliches Geschäftsmodell zu retten.“

## Juristisches Nachspiel?

Der Prozess war begleitet von hohen Sicherheitsmaßnahmen, die es selbst bei Prozessen um schwerwiegende Verbrechen selten gibt. Die Aktivist:innen legten direkt nach der Verhandlung Berufung gegen das Urteil ein. Die Verhandlung könnte ein juristisches Nachspiel gegen Richterin und Staatsanwalt haben: Ein Prozessbeobachter lässt eine Anzeige wegen übler Nachrede rechtlich prüfen, nachdem Richterin und Staatsanwalt im Verlauf des Prozesses immer wieder abwertend über das geistige Innenleben der Angeklagten spekulierten.

Nachdem vor zwei Wochen Jugendrichterin Sandra Mayer eine routinemäßige Terminverschiebung nicht gewährte und überraschend zum schärfsten Mittel der Strafprozessordnung, einer bundesweiten Fahndung, griff, war heute in Absprache mit den Angeklagten Anwalt Klaus Schulz abwesend. In den Worten der Verteidigung seien am Augsburger Amtsgericht bei Prozessen gegen Klimaaktivist:innen Verurteilungen absehbar, das Urteil falle später in der zweiten Instanz am Landgericht.


## Fragwürdige Rodungsgenehmigung - Das Vorspiel des Prozesses

Den Anlass zu der Protestaktion gab die Regierung von Schwaben. Diese stellte am 14.10.2022, weniger als zwei Wochen nach Eingang des entsprechenden Antrags, eine Ausnahmegenehmigung aus, die den Lech-Stahlwerken bei Meitingen die vorgezogene Teilrodung des Lohwalds ermöglichte [4]. Der Lohwald ist ein besonders  Bannwald, der nach bayerischem Waldgesetz als besonders geschützt und unersetzlich gilt. Für dessen Erhalt kämpfen die angrenzenden Gemeinden Biberbach und Langweid sowie mehrere Bürger:inneninitiativen seit knapp 20 Jahren. Ein Vertrag zwischen den Lech-Stahlwerken und der Gemeinde Meitingen, die das Verfügungsrecht hat, obwohl sie nicht an den Lohwald angrenzt, sah die abschnittsweise Rodung nach erfolgreichem Abschluss gewisser artenschutzrechtlicher Maßnahmen vor. Das wäre frühestens im Herbst 2023 gewesen. Um diese abzuwenden, reichten BUND Naturschutz sowie die Gemeinde Biberbach vor Bayerns höchstem Verwaltungsgericht Normenkontrollklagen ein.

Die Ausnahmegenehmigung hatte für die Regierung von Schwaben ein juristisches Nachspiel [5], sogar die Anordnung einer Sanierung wie beim Rappenalpbach steht im Raum: Die Regierung von Schwaben stellte die Ausnahmegenehmigung in voller Kenntnis der zwei anhängigen Klagen aus und tat dies im Geheimen, sodass die Gemeinden und die Bürgerinitiativen keine Möglichkeit hatten, einstweiligen Rechtsschutz zu beantragen. Die beiden Normenkontrollklagen selbst entfalteten keinen einstweiligen Rechtsschutz, da angesichts des ursprünglichen Rodungstermins frühestens im Herbst 2023 vermeintlich keine Eilbedürftigkeit bestand.

Der damalige Präsident der Regierung von Schwaben, Erwin Lohner, erweckte mit dem Vorfall nicht den besten Eindruck. Zunächst macht seine Regierung mit der umstrittenen Genehmigung dem größten Spender der CSU den Weg für seine Rodung frei [6]. Die gegen die Rodung klagenden Parteien werden dazu im Dunklen gelassen. Wenig später wird er von einem CSU-Minister ins Innenministerium befördert [7]. Bosch und Kiehne wird vorgeworfen, diesbezüglich Aussagen getätigt zu haben, die den Vorwurf der üblen Nachrede erfüllen sollen.

## Referenzen

1. https://drive.google.com/file/d/1k2X0oASPl-RUsi90RdKLMkrBfalv29yW/view (Seite 4)
2. https://www.klimaschutz-industrie.de/themen/branchen/stahlindustrie/
3. https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html
4. https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV
5. https://www.br.de/nachrichten/bayern/bannwald-rodung-bei-stahlwerken-vor-gericht,TQRhKFH
6. https://lobbypedia.de/wiki/CSU#Top-Spender (Eintrag "Max Aicher-Firmen (Stahl, Entsorgung)" plus Lobbyverband "VBM Verband der Bayerischen Metall- und Elektroindustrie")
7. https://www.aichacher-zeitung.de/vorort/augsburg/schretter-ersetzt-lohner;art21,235795


## Hinweis
Kiehne und Bosch sind mit der Nennung ihren Namen in Bezug auf den Prozess einverstanden. Über beide wurde auch schon mehrmals bei früheren Gerichtsprozessen mit vollem Namen berichtet.


[Fotos und Videos zur freien Verwendung](https://www.speicherleck.de/iblech/stuff/.rvs2)

-------------

## <a href="/presse"> Pressekontakt </a>