---
title: "Bundesweite Fahndung nach Klimaaktivisten: Landgericht deckt umstrittenen Haftbefehl des Augsburger Amtsgerichts"
date: 2023-06-20T13:00:06+02:00
type: post
draft: false
---

## Fahndung nach Klimaaktivist*innen: Landgericht Augsburg deckt Jugendrichterin Sandra Mayer

Heute vor einer Woche (am Dienstag, 13.6.2023) veranlasste Jugendrichterin Sandra Mayer am Augsburger Amtsgericht eine bundesweite Fahndung nach den Klimaaktivist*innen Samuel Bosch (20) und Charlie Kiehne (21) [6,7,8,9, dpa:230613-99-39901/2]. Die beiden hatten Mitte Oktober vergangenen Jahres die Regierung von Schwaben aufs Korn genommen, nachdem diese trotz laufender Gerichtsverfahren eine umstrittene Rodungsgenehmigung ausstellte [1,2,3,4,5]. Bosch und Kiehne wollten nicht ohne ihren Anwalt, der zeitgleich einen anderen Prozess wahrnehmen musste [12], zu einem Gerichtstermin kommen. Der hatte im Vorfeld aus diesem Grund zwei Mal vergeblich um Terminverlegung gebeten. Nun bestätigt das Augsburger Landgericht die umstrittene Fahndung.


## Erstaunen bei der Strafverteidigung

"Ich bin seit 28 Jahren im Strafrecht tätig und habe das noch nie erlebt", erklärte Verteidiger Klaus Schulz vor einer Woche gegenüber dem Südkurier [10]. Erwartet hatte Schulz eine einfache Terminverlegung oder allenfalls einen Vorführbefehl, bei dem die Polizei die Angeklagten zum nächsten Prozesstermin zu Hause abholt. Stattdessen griff Jugendrichterin Sandra Mayer zum schwersten Grundrechtseingriff, den das deutsche Rechtssystem kennt, den Freiheitsentzug.

"Nun entschied das Landgericht über die Beschwerde, ohne dass mir der Haftbefehl mit Begründung zugestellt worden wäre", so Rechtsanwalt Schulz, "obwohl ich das Amtsgericht dazu aufgefordert hatte. Das ist rechtsstaatlich unterirdisch".

Der Beschluss des Landgerichts argumentiert lapidar, dass sich die Angeklagten auch einen anderen Anwalt hätten suchen können, der zum Verhandlungstermin nicht verhindert gewesen wäre [16, Seite 2, zweitletzter Absatz].


## "Uns geht es noch gut"

Inzwischen erreichen die beiden Beschuldigten, Familien und nahestehende Freunde täglich Solidaritätsbekundungen aus dem gesamten Bundesgebiet. Am Freitag gab es eine Solidaritätsdemonstration in Augsburg [14,15], am Sonntag solidarisierten sich Vertreter*innen von Weltläden bei ihrer bundesweiten Jahreskonferenz mit einer Fotoaktion (siehe Foto). "Es tut allen Betroffenen gut zu spüren, dass so viele Menschen unser Unverständnis mit der bayerischen Justiz teilen und uns jetzt sogar noch mehr den Rücken stärken", so Klimaaktivist Martin Lang (56) aus Schlier.

Vor diesem Hintergrund erklärt Bosch: "Uns geht es noch gut!" Hinter Bosch und Kiehne steht ein breites Unterstützernetzwerk, das mit ihnen auch während eines Gefängnisaufenthalts Kontakt halten würde. Inzwischen steht der neue Termin des Prozesses fest: 27.6.2023, 13:00 Uhr, Amtsgericht Augsburg. Sollten Bosch und Kiehne vorher noch aufgefunden werden, würden sie somit längstens eine Woche in Haft verbringen müssen.

"Diesen 'Luxus' haben viele andere Betroffene unseres Justizsystems nicht", so Bosch weiter. Er spielt damit auf  tausende Menschen an, die jedes Jahr im Gefängnis landen, weil sie sich kein Ticket für den öffentlichen Nahverkehr leisten konnten und auch kein Geld für verhängte Strafen haben. Eine vergleichsweise geringe Geldstrafe von wenigen hundert Euro muss dann in Form einer mehrmonatigen Ersatzfreiheitsstrafe abgesessen werden. Nach Tagesschau-Recherchen ist in manchen Justizvollzugsanstalten jeder Dritte aus diesem Grund der Freiheit beraubt [11, "Wegen Armut ins Gefängnis: Wie gerecht ist unsere Strafjustiz?"].

Kiehne und Bosch schließen sich damit amtierenden und ehemaligen Gefängnisleitungen wie Dr. Uwe Meyer-Odewald oder Thomas Galli an, die sich rigoros gegen die Ersatzfreiheitsstrafe aussprechen [11, Zeitstempel 11:00]. "Diese Leute gehören hier nicht her", so Meyer-Odewald. Das Gefängnissystem steht aktuell nicht nur aufgrund der Ersatzfreiheitsstrafe in der Kritik, Forderungen nach Alternativen wie "dezentrale freie Formen" des Justizvollzugs werden immer lauter [13]. Ein Grundproblem: Soziale Probleme werden nur verschärft, wenn Menschen weggesperrt werden, die zwar dem Wortlaut nach Straftaten begangen haben, aber keine Gefahr für die Gesellschaft darstellen. Durch einen Gefängnisaufenthalt verlieren viele Menschen ihr soziales Umfeld und Anschluss auf dem Arbeitsmarkt. Eine Wiedereingliederung wird dadurch erheblich erschwert.


## Referenzen

1. https://www.br.de/br-fernsehen/sendungen/quer/221110quer-100.html
2. https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV
3. https://www.augsburger-allgemeine.de/augsburg-land/meitingen-lechstahlwerke-roden-lohwald-bei-meitingen-trotz-anhaengiger-klagen-id64346931.html
4. https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html
5. https://www.augsburg.tv/mediathek/video/aerger-um-protest-ermittlungen-gegen-augsburger-klimaaktivisten/
6. https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/haftbefehle-gegen-klimaaktivisten-aus-ravensburg-100.html
7. https://www.wochenblatt-news.de/region-ravensburg/ravensburg/ravensburger-klimaaktivisten-erscheinen-nicht-zu-gerichtstermin-haftbefehl-ausgestellt/
8. https://www.augsburger-allgemeine.de/augsburg/augsburg-augsburger-klimaaktivisten-demonstrieren-und-kritisieren-die-justiz-id66849876.html
9. https://www.aichacher-zeitung.de/nach-haftbefehl-gegen-aktivisten-augsburger-klimacamp-will-vor-dem-amtsgericht-demonstrieren/cnt-id-ps-ee2b16bf-cb9d-47d9-848a-71f4a524e78a
10. https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/haftbefehl-jungen-klimaaktivisten-droht-das-gefaengnis;art410936,11607351
11. https://www.youtube.com/watch?v=McdzPMHNJtk, ZDF-Podcast
12. Die entsprechenden Schriftsätze des Anwalts an das Amtsgericht schicken wir auf Anfrage unmittelbar und ungeschwärzt zu.
13. https://www.sueddeutsche.de/politik/justiz-gehe-nicht-ins-gefaengnis-1.4979766
14. https://www.augsburger-allgemeine.de/augsburg/augsburg-augsburger-klimaaktivisten-demonstrieren-und-kritisieren-die-justiz-id66849876.html
15. https://www.aichacher-zeitung.de/klimaschuetzer-demonstrieren-gegen-haftbefehl/cnt-id-ps-1569ad62-5068-4063-938c-d5eaf8850292
16. Der Beschluss des Landgerichts gehört zur Gerichtsakte, sodass wir diesen nicht pauschal veröffentlichen dürfen. Auf Anfrage schicken wir den Beschluss unmittelbar und ungeschwärzt zu, am schnellsten nach SMS-Nachricht an +4917695110311.


## Fotos zur freien Verwendung

Fotos und Videos der Protestaktion an der Regierung von Schwaben gibt es zur freien Verwendung hier: https://www.speicherleck.de/iblech/stuff/.rvs2

Solidaritätsfoto: https://www.speicherleck.de/iblech/stuff/.rvs-weltladen.jpeg
(Fotograf: C. Albuschkat)

## Termininfo für Augsburg
- Dienstag 27.6. 13:00: Neue Verhandlung Kiehne und Bosch am Amtsgericht
- Donnerstag 22.6. 13:00: Verhandlung wegen eines Banners am Augsburger Rathaus, an dem auch Bosch beteiligt war, am Augsburger Landgericht


-------------

## <a href="/presse"> Pressekontakt </a>

- Samuel Bosch (aktuell nur über WhatsApp erreichbar: +4915908156028)
- Charlie Kiehne (aktuell nur über WhatsApp erreichbar: +4915788463579)
- Ingo Blechschmidt (über Handynetz erreichbar: +4917695110311) 