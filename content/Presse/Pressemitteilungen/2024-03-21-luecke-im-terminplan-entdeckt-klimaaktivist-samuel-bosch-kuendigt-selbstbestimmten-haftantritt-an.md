---
# Pressemitteilung automatisch erzeugt von pm-bot 5a8f785 am Donnerstag, 28. März 2024 um 12:31:39 GMT+1
title: "Lücke im Terminplan entdeckt: Klimaaktivist Samuel Bosch kündigt selbstbestimmten Haftantritt an"
date: 2024-03-21T12:00:00.000Z
draft: false
---

Pressemitteilung vom 21.3.2024  
  
# Lücke im Terminplan entdeckt: Klimaaktivist Samuel Bosch kündigt selbstbestimmten Haftantritt an
  
Nachdem der Waldschützer der Haftvorladung aus Göppingen am vergangenen Donnerstag nicht nachgekommen war, kündigt er nun einen selbstbestimmten Haftantritt eine Woche später am 21.3.2024 an. "Ich hatte einige feste Termine für meinen Klimaaktivismus, daher wäre ein pünktlicher Haftantritt unsinnig gewesen. Da ich in drei Wochen, im April, weitere Termine habe, werde ich am Donnerstag gegen 13 Uhr meine Haft selbstbestimmt antreten", so Bosch.  
  
Die Zeit seit dem 14.3., an dem Bosch ursprünglich die Haftstrafe hätte antreteten sollen, verbrachte er, trotz Festnahmerisiko, mit der Fortführung seines Aktivismus. "Wie normalerweise auch, bin ich mit Menschen über soziale und klimagerechte Transformation ins Gespräch gekommen, deshalb war ich bei einer öffentlichen Podiumsdiskussion, führte Interessierte im Rahmen des wöchentlichen Waldrundgangs durch die Besetzung im Altdorfer Wald und hörte mir eine Diskussion im "Denksalon" des neuen Ravensburger Kunstvereins mit Oberbürgermeister Rapp über Rechtsextremismus an."   
Bei der Podiumsdiskussion am Ulmer Theater vergangenen Freitag, war es Bosch trotz Polizeiaufgebot vor dem Gebäude gelungen, sich unbemerkt zu entfernen. Auch im Altdorfer Wald konnte die Polizei ihn trotz deutlich erhöhter Präsenz am Wochenende nicht festsetzen. Zudem hängte er mit einigen anderen Aktivist\*innen zwei Protestbanner vor die Jugendarrestanstalt in Göppingen: "Klimaschützen ist kein Verbrechen" war dort zu lesen, daneben das Banner von der Aktion, für die Bosch verurteilt wurde: "Lohwald-Rodung genehmigen trotz laufender Gerichtsverfahren? Frech!"  
  
**Kritik am Gericht**  
Unterstützer\*innen von Bosch werfen dem Gericht die Behinderung der Verteidigung vor. So wurden bis zuletzt wichtige Unterlagen und Informationen zurückgehalten, Beweisanträge vor Gericht nicht zugelassen, sowie das Urteil nicht zugestellt. Erst mit monatelanger Verzögerung tauchte es schließlich in den Gerichtsakten auf. Damit wurde der weitere Rechtsweg, in Form einer Verfassungsbeschwerde erst nach der Haft von Boschs Mitstreiter\*in, Charlie Kiehne, möglich.  
 "Das Urteil ist in meinen Augen ungerecht und unverhältnismässig. Ich weiß, dass die Klimabewegung mit diesem Urteil eingeschüchtert werden sollte. Es wurde ein Exempel an Samuel statuiert, aber gemeint sind wir alle." so Martin Lang (57), ein Unterstützer der Aktivist\*innen.  
 "Es entsteht der Eindruck, dass nicht die juristische Einschätzung, sondern die Wut über die Aufmüpfigkeit der Aktion entscheidend waren", so Kiki Köffle (20), die den Prozess damals verfolgte. "Wir sollten uns als Gesellschaft darauf einigen, angesichts der Klimakrise endlich zu handeln, statt Aktivisti zu bestrafen."  
   
Die Probleme im Justizsystem sehen die Aktivist\*innen aber nicht auf diesen Prozess beschränkt: Bei einer Aktion zum öffentlichen Nahverkehr kritisierte Bosch zusammen mit Mitstreiter:innen die hohe Zahl armer Menschen in deutschen Gefängnissen, die aufgrund nicht gezahlter Geldstrafen wegen Schwarzfahrens, einsitzen. Insgesamt handelt es sich jährlich um 7000 Menschen in Deutschland. In armen Großstädten wie Berlin sitzt jeder dritte Gefangene wegen Schwarzfahrens. \[1\].   
  
**Hintergrund**  
Das ursprüngliche Banner in Augsburg war Teil einer Protestaktion gegen die Rodung eines besonders geschützten Bannwaldes, bei Biberbach in der Region Augsburg. Dieser wurde im Herbst 2022, trotz laufender Klage der anliegenden Gemeinde und des BUND Naturschutz, für die Erweiterung eines Stahlwerks von Max Aicher, vorzeitig gerodet. Eine geheime und zweifelhafte Ausnahmegenehmigung, ausgestellt vom Regierungsbezirk Schwaben, machte die Rodung möglich \[2\].  
Als Reaktion auf diese fragwürdigen Umstände, hingen Samuel Bosch, Charlie Kiehne und Ingo Blechschmidt im Rahmen einer Kletteraktion das satirische Banner  "Lohwald-Rodung genehmigen trotz laufender Gerichtsverfahren? Frech!" an die Fassade des Regierungsgebäudes \[3\].  
Im Oktober 2023 wurden Kiehne und Bosch schließlich wegen übler Nachrede gegen den, mittlerweile beförderten, Präsidenten der Regierung von Schwaben in Augsburg zu einer, bzw. drei Wochen Haft (Jugendarrest) verurteilt.  
Charlie Kiehne hat eine Woche Haft bereits im Februar abgesessen.  
Über eine Verfassungsbeschwerde zu dem Urteil wurde noch nicht entschieden.  
  
  
Unterstützer\*innen kündigten am Donnerstag um 12 Uhr eine Spontanversammlung zur Begleitung des Haftantritts an.  
  
QUELLEN  
\[1\] [https://www.youtube.com/watch?v=McdzPMHNJtk](https://www.youtube.com/watch?v=McdzPMHNJtk)  
\[2\] [https://www.augsburger-allgemeine.de/augsburg-land/meitingen-lechstahlwerke-roden-lohwald-bei-meitingen-trotz-anhaengiger-klagen-id64346931.html](https://www.augsburger-allgemeine.de/augsburg-land/meitingen-lechstahlwerke-roden-lohwald-bei-meitingen-trotz-anhaengiger-klagen-id64346931.html)  
\[3\] [https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html](https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html)  
  
\--------------  
  
BILDER ZUR FREIEN VERWENDUNG:  
[https://drive.google.com/drive/folders/1-2U4ZKztTrkeXoPYgPNDZfy-1ZDNtQi2](https://drive.google.com/drive/folders/1-2U4ZKztTrkeXoPYgPNDZfy-1ZDNtQi2)  
  