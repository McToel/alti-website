---
title: "Radau im Gemeinderat – Aktivist*innen Machen Auf Flächenversiegelung Aufmerksam"
date: 2023-04-24T22:29:11+02:00
type: post
draft: false
---

Als der Gemeinderat von Weingarten am 24.4.2023 den Sitzungspunkt über die Bebauung der sogenannten "Schafswiese" beginnen wollte, kam es zu einer Aktion von Aktivist\*innen indem sie zahlreiche ausgeschnittene Schmetterlinge aus buntem Papier auf den Tischen der Fraktionen CDU,Freien Wähler und der Stadtverwaltung mit den Sprüchen "Flächenversiegelung endlich stoppen" und  "Schafswiese/Schmetterlingswiese muss grüne Wiese bleiben!" verteilten.
Zudem äußerte einer der Aktivist\*innen, bis er unterbrochen wurde, an den Gemeinderat:" Keine weitere Versiegelung, es reicht einfach!

Die Fraktionen Grüne und SPD brachten diese Tagesordnung am heutigen Montag ein, in der es darum ging, ob nochmals über das Projekt entschieden werden soll. [1]

"Es darf nicht sein, dass wir immer neue Flächen versiegeln und immer weiter neue Wohnhäuser bauen, obwohl wir genug Wohnraum haben. Kein Mensch braucht allein 130qm Wohnfläche, wir möchten eine sinnvolle Umorganisierung des bestehenden Wohnraums." [2] so Aktivist Samuel Bosch (20) zu den Plänen der Stadtverwaltung. 

Zur Sitzung kamen auch zahlreiche Bürger*innen, die unter anderem durch die Kunstaktion vergangenen Freitag vor dem Rathaus, auf die Abstimmung heute im Gemeinderat aufmerksam wurden und beobachteten das Geschehen aufmerksam. [3]
Am Ende der Diskussion wurde aber doch knapp mit 13/12 Stimmen für die Bebauung der Wiese gestimmt. 

"Wir signalisieren, dass viele Menschen solche Projekte doof finden. Die CDU und die Freien Wähler in Weingarten sollen ihre Blockadehaltung ein für alle Mal aufgeben. Auch sie sind verantwortlich zukunftsorientierte Entscheidungen zu treffen." ergänzt Sina Wagner (23) aus Weingarten.


## Quellen: 
[1] https://www.schwaebische.de/regional/oberschwaben/weingarten/raete-wollen-schafswiese-vor-bebauung-retten-1534517

[2] https://www.umweltbundesamt.de/daten/private-haushalte-konsum/wohnen/wohnflaeche#folgen-der-flachennutzung-durch-wohnen-fur-die-umwelt

[3] https://www.schwaebische.de/regional/oberschwaben/weingarten/aktivisten-malen-schafe-vor-das-rathaus-in-weingarten-1558411?lid=true




-------------

## <a href="/presse"> Pressekontakt </a>