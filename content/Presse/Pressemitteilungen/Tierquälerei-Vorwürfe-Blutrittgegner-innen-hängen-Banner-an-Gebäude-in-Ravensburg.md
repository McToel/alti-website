---
title: "Tierquälerei Vorwürfe: Blutrittgegner*Innen Hängen Banner an Gebäude in Ravensburg"
date: 2023-05-18T17:49:04+02:00
type: post
draft: false
---

Am Morgen des 18.5. hängten Blutrittgegner*innen ein Banner mit der Aufschrift "Pferdequälen der Staat macht mit" auf das Dach des Leerstehenden "Raiffeisen Lagerhauses".
Das Banner ist von der Meersburger Brücke und vom Bahnhof in Ravensburg gut sichtbar für Passant*innen.

"Beim Blutritt in Weingarten werden jedes Jahr aufs neue tausende Pferde in einer langen Prozession durch die Stadt getrieben. Diese Tierquälerei für Tradition und Unterhaltung ist inakzeptabel!" so Johanna Remscheck (21).

Mit der Aktion wollen die Tierschützer*innen die Kritik am Blutritt auch nach Ravensburg tragen.

"In Weingarten ist sehr vielen bereits klar das der Blutritt dringend verändert werden muss. Diese Erkenntnis wollen wir jetzt auch in die Nachbarstädte tragen." so Remscheck weiter.


-------------

## <a href="/presse"> Pressekontakt </a>