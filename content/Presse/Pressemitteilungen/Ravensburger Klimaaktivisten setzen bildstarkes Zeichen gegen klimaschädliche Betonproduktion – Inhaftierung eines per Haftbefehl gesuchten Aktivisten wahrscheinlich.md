---
title: "Ravensburger Klimaaktivisten Setzen Bildstarkes Zeichen Gegen Klimaschädliche Betonproduktion – Inhaftierung Eines per Haftbefehl Gesuchten Aktivisten Wahrscheinlich"
date: 2023-06-26T23:06:35+02:00
type: post
draft: false
---

[Ravensburg], [26.06.2023] - In den frühen Morgenstunden des heutigen Montags platzierten Ravensburger Klimaaktivist*innen - darunter der stadtbekannte Samuel Bosch (20) - ein deutliches Statement gegen die klimaschädliche Betonproduktion von HeidelbergBeton in Ravensburg. (Deisenfangstraße 43, 88212 Ravensburg). Mehrere junge Menschen kletterten auf die Zementsilos und befestigten ein eindrucksvolles Banner am örtlichen Betonwerk. Damit möchten sie auf die verheerenden Auswirkungen dieser Industrie auf das Klima hinweisen.

Das Banner mit der Aufschrift "Heidelberger Beton zerstört das Klima enorm" wurde sorgfältig an der Anlage angebracht und war für alle Passant*innen und für die Autofahrer*innen auf der nahegelegenen B32 gut sichtbar. Die Klimaaktivisten wählen diese mutige Aktion als friedlichen Protest, um auf die dringende Notwendigkeit hinzuweisen, die Betonproduktion zeitnah auf umweltfreundlichere Alternativen umzustellen.

Die Betonindustrie ist nach ARD-Recherchen weltweit für einen Anteil von 8% der Treibhausgasemissionen verantwortlich [1] und trägt damit maßgeblich zur Klimakrise bei. Bei der Produktion von Beton werden große Mengen an CO2 freigesetzt, sowohl durch den Energieverbrauch als auch durch die chemischen Prozesse zur Herstellung des Zements. Die Auswirkungen auf die Umwelt sind verheerend und tragen zur globalen Erwärmung und damit auch zu Umweltkatastrophen, wie beispielsweise dem Tornado in Köln und den Überschwemmungen in Kassel, bei.

## Besonderes Aktionsziel: Mehr CO2-Emissionen als ganz Österreich

Der offiziell "HeidelbergCement AG" heißende, sich seit einiger Zeit aber öffentlich auch unter dem Namen "HeidelbergMaterials" präsentierende Konzern ist weltweit der viertgrößte in seiner Branche [2]. Durch die Produktion von jährlich mehr als 120 Millionen Tonnen Zement verantwortet der Großkonzern mehr als 70 Millionen Tonnen CO2 [3]. Das ist mehr als die jährlichen Gesamtemissionen von ganz Österreich [4] und fast ein Drittel der jährlichen Emissionen des Kohlekonzerns RWE [5].

Die "HeidelbergCement AG" gehört damit zu den größten Treibhausgasemittenten Europas und provozierte immer wieder Proteste, zuletzt vor einem Monat von pax christi, Fridays for Future und anderen Initiativen [6,7].

Die beiden Aktivist*innen, haben durch die Besetzung des Altdorfer Walds bereits die ebenfalls für die Betonherstellung notwendige Kiesproduktion ins Visier genommen und fordern, den Schutz unseres Planeten zur obersten Priorität zu machen. "Wir benötigen dringend eine nachhaltige Baumittelproduktion. Das setzt die schnelle Umstellung auf alternative Baustoffe, ebenso wie die Reduktion des Bedarfs voraus. Nur so kann das Klima, unsere Wälder und unser Trinkwasser geschützt werden", so Bosch.

Besonders brisant: Bosch wird gerade mit einem bundesweiten Haftbefehl gesucht. Er erschien vorletzte Woche nicht vor dem Augsburger Amtsgericht, da sein Anwalt aufgrund eines anderen Prozesses verhindert war und dies bereits Wochen zuvor dem Gericht mitgeteilt hatte. Normalerweise wird eine Verhandlung dann verschoben - doch nicht so in Augsburg. Stattdessen erließ die Richterin einen Haftbefehl gegen Samuel und seine Mitstreiter*in Charlie Kiehne (21). Daher könnte Bosch nun bis zum nächsten Gerichtstermin in Haft sitzen. Die Verhandlung wurde inzwischen auf den 27.6. um 13 Uhr in Augsburg terminiert.

Bei dem Prozess in Augsburg geht es um die rechtlich fragwürdige Rodung eines Bannwaldes [8] zu Gunsten einer Fabrikerweiterung der Lechstahl-Werke nördlich von Augsburg. "Auch hier habe ich gegen ein energieintensives und daher klimaschädliches Element der Bauindustrie protestiert. Die Tatsache, dass das Gericht sich geweigert hat einen Alternativtermin anzubieten, bei dem ich mein Recht auf Verteidigung wahrnehmen konnte, obwohl ich sonst immer zuverlässig vor Gericht erscheine, hat mich in die merkwürdige Situation gebracht, dass ich sowieso irgendwann festgenommen werde. Insofern hat mich das noch mehr motiviert, eine Aktion gegen lokale Klimazerstörung umzusetzen, um endlich auf die Klimazerstörung der Bauindustrie und ihre vergessene Verantwortung aufmerksam zu machen." Seine Mitaktivistin "Luchs" ergänzt: "HeidelbergCement steht dabei nicht nur symbolisch für solch eine lokale Größe der Klimazerstörung, sondern ist sich dessen auch bewusst. Das dürfte vermutlich ein Grund für die vielen Formen des Greenwashings sein, das der weltweit zweitgrößten Zementhersteller in Werbung, Internet und Lobbyarbeit in letzter Zeit verbreitet."


## Alternativen vorhanden

Klimafreundlicher Betonherstellung sind aufgrund physikalischer Gegebenheiten enge Grenzen gesetzt. Was die Aktivist*innen stattdessen fordern, ist eine drastische Reduktion des Neubauens und ein systematischer Umstieg auf alternative Baustoffe, etwa Holz, im Rahmen moderner industrieller Holzbauweise. 
Neubauten müssten generell kritisch hinterfragt werden, denn Sanierung sei fast immer klimafreundlicher als CO2-intensiver Neubau. Die Aktivist*innen folgen mit dieser Position der Einschätzung eines Architektenverbands [9].


## Referenzen

1. https://www.ardalpha.de/wissen/umwelt/klima/klimawandel/beton-emissionen-klimafreundlich-bauen-umwelt-loesungen-klimakrise-100.html
2. https://blog.bizvibe.com/blog/top-10-cement-companies-world
3. https://www.handelsblatt.com/unternehmen/energie/klimaschutz-klimakiller-beton-so-will-die-deutsche-zementindustrie-co2-neutral-werden-/26652040.html (Umrechnungsfaktor Tonne Zement zu Tonne CO2-Ablagerung in der Atmosphäre: durch 1,64 teilen)
4. https://en.wikipedia.org/wiki/List_of_countries_by_carbon_dioxide_emissions
5. https://energyload.eu/energiewende/deutschland/rwe-co2-emissionen/
6. https://www.swr.de/swraktuell/baden-wuerttemberg/mannheim/klimademo-gegen-die-zementproduktion-bei-hauptversammlung-von-heidelberg-materials-100.html
7. https://www.labournet.de/branchen/chemie/hauptversammlung-der-heidelbergcement-ag-2023-am-11-mai-klimasuender-heidelberg-materials-muss-sich-schneller-transformieren/
8. https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV
9. https://www.architects4future.de/forderungen


Weitere Hintergrundinformationen zu Heidelberger Zement:
https://extinctionrebellion.de/og/heidelberg/hd-cement/

## Hinweis
Die Aktion startete um 4 Uhr morgens und ist derzeit im Gange. Der weitere Verlauf hängt von der Art polizeilicher Intervention ab.

## Fotos zur freien Verwendung

Aufgrund des erheblichen Charakters der Aktion, der erhöhte Sicherheitsstandards erfordert, ist unklar, ob es uns gelingen wird, selbst Fotos anzufertigen. Falls ja, werden diese zur freien Verwendung schrittweise hochgeladen unter:
https://drive.google.com/drive/folders/1V8ynlCzGO9s_KSSqfY-ij2alr1YRpxex?usp=sharing

-------------

## <a href="/presse"> Pressekontakt </a>