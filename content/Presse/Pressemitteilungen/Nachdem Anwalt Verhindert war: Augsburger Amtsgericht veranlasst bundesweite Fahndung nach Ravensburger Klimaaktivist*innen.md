---
title: "Nachdem Anwalt Verhindert War: Augsburger Amtsgericht Veranlasst Bundesweite Fahndung Nach Ravensburger Klimaaktivist*innen"
date: 2023-06-13T13:00:06+02:00
type: post
draft: false
---

Immer wieder nutzen die überregional bekannten Klimaaktivist*innen Charlie Kiehne (21) und Samuel Bosch (20) aus dem besetzten Altdorfer Wald bei Ravensburg Gerichtsverhandlungen, um dort ihre symbolischen Aktionen zu erklären und weitere Öffentlichkeit für Missstände in der Politik zu schaffen. Heute hätten sie wegen einer Protestaktion zu einer umstrittenen Waldrodung vor dem Augsburger Amtsgericht stehen sollen. Da ihr Anwalt Klaus Schulz verhindert war und die beiden daher nicht kamen, erließ das Gericht kurzerhand einen Haftbefehl. Sobald die beiden aufgegriffen werden, kommen sie bis zu einem Folgetermin für mehrere Wochen in Haft.

## Mit symbolischen Aktionen für Klimagerechtigkeit

Kiehne und Bosch aus dem besetzten Altdorfer Wald bei Ravensburg engagieren sich seit mehreren Jahren in ihrer Freizeit ehrenamtlich für Klimagerechtigkeit und machen dazu immer wieder symbolische Aktionen. Etwa haben sie schon mehrmals Essen aus Supermarktmülltonnen gerettet und am Folgetag öffentlich verschenkt; dafür standen sie wegen "Diebstahl" vor Gericht [1,2,3,4]. Oft werden Verfahren gegen die beiden eingestellt, manchmal gibt es sogar Freisprüche, meistens leisten sie Sozialstunden ab, etwa bei Vereinen wie Foodsharing [5].

Am heutigen Dienstag (13.6.2023) wäre es um eine symbolische Protestaktion bei der Regierung von Schwaben gegangen. Gemeinsam mit dem Augsburger Klimaaktivisten Ingo Blechschmidt (34), der kürzlich auch Betroffener der umstrittenen bundesweiten Razzia war [6,7], kreideten die beiden das Vorgehen der Regierung von Schwaben an [8]: Binnen nur zehn Tagen hatte diese eine Ausnahmegenehmigung erteilt [9,10,11], die eine vorgezogene Teilrodung eines rechtlich besonders geschützten Bannwalds bei Meitingen bewilligte – obwohl gegen diese Rodungspläne noch an Bayerns höchstem Verwaltungsgericht Verfahren von angrenzenden Gemeinden und dem BUND Naturschutz anhängig waren.

Als Strafe für die Aktion stehen Sozialstunden oder eine Geldstrafe im Raum. So wurde Blechschmidt in derselben Sache zur Zahlung von etwa 2.500 Euro verurteilt [12].


## Anwalt verhindert - Bundesweite Fahndung

Auch heute wollten Bosch und Kiehne vor Gericht wieder ihre friedliche Aktion erklären. Doch dazu kam es nicht: Ihr Verteidiger Klaus Schulz aus Ravensburg war verhindert. Zweimal bat er das Gericht aus diesem Grund um eine Terminverschiebung – vergeblich. Zudem reagierte das Gericht auf die vor jedem Verfahren übliche Beantragung der Akteneinsicht zunächst gar nicht, nach erneuter Anfrage dann unter Verweis auf Zeitnot sogar ablehnend.

Ohne Anwalt wollten Bosch und Kiehne heute aber nicht das Verfahren antreten – und blieben daher der Verhandlung fern. Nachdem sie auch 15 Minuten nach Beginn nicht erschienen waren, beantragte die Staatsanwaltschaft Haftbefehl nach § 230 StPO, was die Richterin sofort bestätigte.

"Dem Gericht ist es offensichtlich egal, wenn sich die beiden Klimaaktivist*innen ohne Anwalt verteidigen müssen. Das ist nicht meine Auffassung von einem fairen Gerichtsprozess", kommentiert Kiki Köffle (19, Konstanz), eine Freundin von Samuel Bosch, das Vorgehen des Augsburger Amtsgerichts.

Schulz gab telefonisch an, dass er in seiner langjährigen Erfahrung als Rechtsanwalt ein solches Vorgehen noch nie erlebte. Üblich sei zunächst, dass bei anwaltlicher Verhinderung im Vorfeld ein neuer Termin vereinbart werden würde. In Fällen, in denen zwar der Anwalt, aber nicht der Angeklagte erscheint, werde auch zunächst ein neuer Termin anberaumt. In besonderen Fällen wird ein Vorführungsbefehl erlassen, sodass die Polizei einige Stunden vor dem neuen Verhandlungstermin den Angeklagten abholt. Noch nie habe Schulz erlebt, dass direkt ein Haftbefehl erlassen wurde.


## Nehmen persönliche Nachteile in Kauf

Ingo Blechschmidt (34): "Als Klimaaktivist*innen nehmen wir immer wieder persönliche Nachteile in Kauf, um zu erreichen, dass die Bundesregierung das demokratisch beschlossene Pariser Klimaabkommen und ihr eigenes Klimaschutzgesetz einhält. Unverhältnismäßige Übergriffe der staatlichen Behörden bringen Bürger immer wieder zum Nachdenken: Wieso hat die Regierung die Klimakrise nicht im Griff und legt stattdessen ein solch immensen Verfolgungseifer an den Tag?"

Auch Bosch und Kiehne gaben an, sich trotz des nun beschlossenen Haftbefehls weiter für Klimagerechtigkeit engagieren zu wollen.


## Referenzen

[1] https://www.kontextwochenzeitung.de/gesellschaft/597/diebe-oder-wohltaeterinnen-8401.html

[2] https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/umweltaktivisten-vor-gericht-wegen-diebstahls-von-lebensmitteln;art410936,11206300

[3] https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/klimaaktivist-amtsgericht-ravensburg-sozialstunden-baumbesetzung-100.html

[4] https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/gerettete-lebensmittel-ravensburg-prozess-100.html

[5] https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/11127-aktivist-innen-nehmen-strafe-im-essen-retten-prozess-an

[6] https://www.br.de/nachrichten/bayern/razzia-bei-letzter-generation-das-sagt-ein-beschuldigter,TfB6J4D

[7] https://www.augsburger-allgemeine.de/augsburg/augsburg-razzia-bei-der-letzten-generation-wer-ist-ingo-blechschmidt-aus-augsburg-id66599826.html

[8] https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html

[9] https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV

[10] https://www.br.de/nachrichten/bayern/naturschuetzer-klagen-gegen-bannwaldrodung-bei-lech-stahlwerken,TJa04mR

[11] https://www.br.de/nachrichten/bayern/bannwald-rodung-bei-stahlwerken-vor-gericht,TQRhKFH

[12] https://www.sueddeutsche.de/bayern/augsburg-regierungsgebaeude-besetzung-blechschmidt-1.5763823


## Rechtlicher Hinweis

Da es sich nicht um ein Urteil, sondern um einen Beschluss handelt, entfaltet die Verfügung des Amtsgerichts sofort Wirkung. Urteile werden dagegen erst nach Ablauf von Fristen rechtskräftig. Gegen den Beschluss legte Verteidiger Klaus Schulz Beschwerde ein, doch bis das Amtsgericht diesem abhilft oder das Landgericht den Beschluss aufhebt, kommen Bosch und Kiehne sofort in Haft, wenn diese aufgegriffen werden.

Es ist gesetzlich vorgesehen, dass zeitnah ein neuer Verhandlungstermin angesetzt wird, sobald Bosch und Kiehne inhaftiert sind. So könnten Bosch und Kiehne mehrere Wochen im Gefängnis verbringen.

-------------

## <a href="/presse"> Pressekontakt </a>

Klaus Schulz (Anwalt, +4975132059)

Sobald Bosch und Kiehne inhaftiert sind, ist Ingo Blechschmidt (+4917695110311) der Primärkontakt.

## Nachtrag

1. Die Angeklagten sind, wie auch schon bei allen anderen früheren Gerichtsterminen und entsprechend früherer Berichterstattung, mit der Nennung ihrer vollen Namen einverstanden.
2. Kiehne ist aktuell nicht telefonisch erreichbar; Bosch nicht über Handynetz, dafür aber über WhatsApp.
3. Wenn Angeklagte nicht zu einem Verhandlungstermin kommen (zumal, wie hier, der Verteidiger verhindert ist und dies dem Gericht zweimal mitteilte), stehen dem Gericht grundsätzlich drei Möglichkeiten zur Verfügung. Üblich sind sowohl Sitzungsstrafbefehle (ein schriftlich niedergelegtes Urteil ohne mündliche Verhandlung, das dann innerhalb von zwei Wochen angefochten werden kann) als auch Vorführungsbefehle (Polizei sucht den Angeklagten vor zum neuen Prozesstermin auf und bringt ihn zum Gericht). Pauschale Haftbefehle, die zu einer wochenlangen Haft bis zum neuen Prozesstermin führen, stellen das letzte Mittel dar. Zu diesem letzten Mittel griff das Amtsgericht. Gründe dafür sind uns keine ersichtlich, zumal wie geschildert der Verteidiger zweimal unter Angabe von Gründen um Terminverschiebung bat und Bosch und Kiehne bislang jeden Gerichtstermin wahrgenommen haben (und dies auch gerne tun, um Öffentlichkeit für ihre Anliegen herzustellen).
4. Einspruch gegen den Haftbefehl konnte Verteidiger Klaus Schulz noch nicht einlegen, da ihm der schriftliche Beschluss noch nicht zugestellt wurde.

Quellen hierfür:
- Rechtsanwalt Klaus Schulz
- https://www.burhoff.de/veroeff/aufsatz/zap_F22_S939.htm, Abschnitt V.4 ("Haftbefehl")
- https://gramm-recht.de/was-geschieht-wenn-der-angeklagte-nicht-zur-hauptverhandlung-erscheint/
- https://strafverteidiger-berlin.info/gerichtstermin-verpasst-und-jetzt/
