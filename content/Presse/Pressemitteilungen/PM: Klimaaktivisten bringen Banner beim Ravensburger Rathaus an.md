---
title: "PM: Klimaaktivisten Bringen Banner Beim Ravensburger Rathaus An"
date: 2023-07-26T18:19:27+02:00
type: post
draft: false
---

Die öffentliche Kritik an Ravensburgs Stadtspitze um Oberbürgermeister Rapp reißt nicht ab. Erst attestierten ihr Wissenschaftler aus der Region schwere Versäumnisse beim Klimaschutz [1], dann kritisierten auch Stadträte das Schneckentempo in Klimafragen [2]. Mit einer Kletteraktion beim Ravensburger Rathaus verdeutlichten am heutigen Mittwochmittag um 12:05 Uhr nun auch Klimaaktivist\*innen die Untätigkeit der Stadt. Neben einer Statistik der steigenden CO2-Ablagerungen in der Atmosphäre zieren leere Worthülsen der Stadtspitze ein in unmittelbarer Rathausnähe angebrachtes Banner.

## Stadtspitze ingoriert Kritik

Bald drei Jahre ist es her, als zum fünfjährigen Jubiläum des Pariser Klimaabkommens mit einer viele Wochen andauernden Baumbesetzung in Ravensburg Aktivist\*innen auch mit zivilem Ungehorsam auf fehlende Klimagerechtigkeitsmaßnahmen der Stadt Ravensburg hinwiesen. Seitdem gab es zahlreiche Aktionen, an denen sich immer mehr Bürger\*innen beteiligten, etwa verschenkten sie aus Supermarktmülltonnen gerettetes Essen, brachten Banner am CDU-Büro und beim Büro des Regionalverbands an, luden zu Informationsvorträgen ein und sammelten Unterschriften.

Oberbürgermeister Rapp reagierte auf diese Aktionen immer nach dem gleichen Muster: Er verwies auf bereits getroffene Beschlüsse, insbesondere zum "Ravensburger Klimakonsens". Sein Parteikollege August Schuler folgt derselben Linie, meint, dass für Klimaschutz bereits einiges von der Stadt geleistet worden sei [2]. Dabei zeigt ein Blick auf die Statistik, dass von dem im "Klimakonsens" vorgeschriebenen CO2-Emissionspfad massiv abgewichen wird, in einigen Sektoren die Emissionen sogar steigen [3]. So erhöhte zuletzt die Stadtverwaltung ihren Energieverbrauch, um ganze 8,5%, anstatt ihn zu senken [3].

## Neuen Menschen das Feld überlassen

"Ein Oberbürgermeister, der zu allem hauptsächlich schweigt, und führende Politiker, die ihr eigenes Versagen marketingmäßig in ein beseres Licht rücken wollen. Deutlicher kann man wohl nicht scheitern. Höchste Zeit endlich andere ran zu lassen, die handeln statt nur labern", findet Martin Lang (56), der heute bei der Kletteraktion mit dabei war. "Aktionäre hätten Unternehmenschefs mit so geringen Leistungen bereits lange abgesägt", ergänzt Langs Mitstreiter Samuel Bosch (20). Nur bei der Stadtspitze werde Versäumnis in dieser Größenordnung geduldet. "Drei Jahre lang nur heiße Luft und steigende CO2-Emissionen! Wie sollen wir jüngeren die Stadt und die Politik da noch ernst nehmen? Ihr verzockt gerade nichts Geringeres als unsere Zukunft und schämt euch nicht mal – einfach dreist", so Bosch.

## Ravensburgs Klimapolitik Geschichte des Scheiterns

Dabei sei Klimaschutz aktuell dringender denn je. "Mittlerweile pfeifen es die Spatzen von den Ravensburger Dächern", meint Lang. Der Sturm am 10. Juli stimmte viele Menschen nachdenklich, ob alle Schäden behoben werden können, ist unklar. Im Ferienparadies Rhodos zwingen Waldbrände 19.000 Menschen zur Flucht, provisorische Notunterkünfte werden eingerichtet, Flüge nach Rhodos annulliert. Ravensburg ist Mitbetroffener und Mitverursacher der Klimakrise. Ravensburgs Klimapolitik dagegen ist "eine Geschichte des Scheiterns", resümiert Bosch.

Wie das Klimaschutzgesetz die Bundesregierung beim Abweichen vom demokratisch beschlossenen CO2-Reduktionspfad zwingt, Sofortmaßnahmen zu ergreifen, erwarten das auch Ravensburgs Klimaaktivist\*innen von Ravensburgs Stadtspitze. Der städtische Handlungsspielraum müsse voll ausgeschöpft werden. Mit weiteren Aktionen werden sie immer wieder auf den Widerspruch zwischen den Beschlüssen des Stadtrats und der fehlenden Umsetzung aufmerksam machen.


## Referenzen

1. https://www.schwaebische.de/regional/oberschwaben/ravensburg/klimaschutz-wissenschaftler-werfen-stadt-versaeumnisse-vor-43547
2. https://www.schwaebische.de/regional/oberschwaben/ravensburg/ravensburg-macht-beim-klimaschutz-fuer-stadtraete-keine-gute-figur-1777982
3. https://www.schwaebische.de/regional/oberschwaben/ravensburg/ravensburger-klimakonsens-sorgt-seit-drei-jahren-fuer-streit-1762863


## Hinweis

Die Aktion findet am Baum vor dem Rathaus statt, nicht am Rathaus selbst. Klimaaktivist\*innen werden diesen mit einer besonderen Klettertechnik besteigen und dann ein Banner anbringen. Die Aktion findet am heutigen Mittwochmittag (26.7.2023) um genau 12:05 Uhr statt, um auf die Redewendung "es ist fünf nach zwölf" symbolisch Bezug zu nehmen. Polizeiliche Intervention kann den Aktionsverlauf gegebenenfalls ändern.

Fotos zur freien Verwendung: https://www.speicherleck.de/iblech/stuff/.rb
(wird kurz nach 12:05 Uhr befüllt, aktuell ist dort nur ein Foto des Banners selbst hinterlegt)

-------------

## <a href="/presse"> Pressekontakt </a>