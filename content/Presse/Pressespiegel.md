---
title: "Pressespiegel"
date: 2023-03-24T14:13:22+01:00
draft: false
summary: Presseartikel, die über die Waldbesetzung im Altdorfer Wald (Alti) berichten
---
# Pressespiegel
Das sagen andere über uns (unvollständig, zahlreiche aktuelle Artikel bislang
nicht eingepflegt):

**Siehe auch: [Leser\*innenbriefe](https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp-briefe)**


<h2>2.7.2024 / 3.7.2024 / 4.7.2024 (Ulmer Münster)</h2>
<ul>
  <li><a href="https://www.kath.net/news/85008">kath.net</a></li>
  <li><a href="https://www.rnd.de/panorama/klimaaktivisten-klettern-auf-ulmer-muenster-banner-in-70-meter-hoehe-YYNYR27D4ZGJXOP32A5QUVVEPQ.html">rnd.de</a></li>
  <li><a href="https://www.augsburger-allgemeine.de/neu-ulm/ulm-klima-kletterer-auf-den-grosseinsatz-am-ulmer-muenster-folgt-die-dicke-rechnung-102842167">augsburger-allgemeine.de</a></li>
  <li><a href="https://www.regio-tv.de/mediathek/video/spektakulaere-protestaktion-klimaaktivisten-beziehen-stellung/">regio-tv.de</a></li>
  <li><a href="https://www.schwaebische.de/regional/ulm-alb-donau/ulm/wuerden-fuer-unsere-strafe-pleitegehen-klima-kletterer-verteidigen-aktion-2667197">schwäbische.de</a></li>
  <li><a href="https://www.idea.de/artikel/aktion-am-ulmer-muensterturm-waere-jesus-klimaaktivist">idea.de</a></li>
  <li>
    <a href="https://www.augsburger-allgemeine.de/baden-wuerttemberg/demonstrationen-klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf-id71250411.html">augsburger-allgemeine.de</a> (Nr.&nbsp;1),
    <a href="https://www.augsburger-allgemeine.de/neu-ulm/ulm-ulmer-muenster-dekan-ueber-klima-kletterer-ihr-seid-doch-voellig-irre-id71252901.html">augsburger-allgemeine.de</a> (Nr.&nbsp;2),
    <a href="https://www.augsburger-allgemeine.de/neu-ulm/ulm-klimaaktivisten-in-70-metern-hoehe-auf-dem-ulmer-muenster-verhaftet-id71252656.html">augsburger-allgemeine.de</a> (Nr.&nbsp;3)
  </li>
  <li>
    <a href="https://aussiedlerbote.de/en/climate-activists-hang-up-protest-banners-at-ulm-minster/">aussiedlerbote.de</a> (Nr.&nbsp;1),
    <a href="https://aussiedlerbote.de/en/would-jesus-be-a-climate-activist-protest-banner-causes-firefighting-operation-at-ulm-minster/">aussiedlerbote.de</a> (Nr.&nbsp;2)
  </li>
  <li><a href="https://www.bild.de/regional/baden-wuerttemberg/protest-in-ulm-klimaaktivisten-haengen-banner-am-muenster-auf-6683dbe52f16c412acd2997d">bild.de</a></li>
  <li><a href="https://www.br.de/nachrichten/meldung/klimaaktivisten-klettern-auf-ulmer-muenster,30068c6de">br.de</a></li>
  <li><a href="https://cc4f-soest.org/waere-jesus-klimaaktivist/">cc4f-soest.org</a></li>
  <li><a href="https://x.com/CClimateAction/status/1808208412485845454">christianclimateaction.org</a></li>
  <li><a href="https://www.come-on.de/deutschland-welt/drei-klimaaktivisten-illegal-auf-ulmer-muenster-geklettert-botschaft-enthuellt-zr-93164211.html">come-on.de</a></li>
  <li>
    <a href="https://archiv.epd.de/detail/live.621418">eps.de</a> (Nr.&nbsp;1),
    <a href="https://archiv.epd.de/detail/live.621485">eps.de</a> (Nr.&nbsp;2)
  </li>
  <li><a href="https://www.evangelisch.de/inhalte/231450/02-07-2024/klimaaktivisten-klettern-auf-ulmer-muenster">evangelisch.de</a></li>
  <li><a href="https://www.ffh.de/video/mediathek/405639-klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf.html">ffh.de</a></li>
  <li><a href="https://www.kath.ch/medienspiegel/klimaaktivisten-klettern-auf-ulmer-muenster-und-hissen-riesiges-banner/">kath.ch</a></li>
  <li><a href="https://www.merkur.de/deutschland/baden-wuerttemberg/drei-klimaaktivisten-illegal-auf-ulmer-muenster-geklettert-botschaft-enthuellt-zr-93164211.html">merkur.de</a></li>
  <li><a href="https://nachrichten.ag/deutschland/baden-wuerttemberg/ulm/aktivisten-klettern-auf-turm-des-ulmer-muensters-waere-jesus-klimaaktivist/">nachrichten.ag</a></li>
  <li><a href="https://www.nonstopnews.de/meldung/44648">nonstopnews.de</a></li>
  <li><a href="https://www.ref.ch/news/klimaaktivisten-klettern-auf-ulmer-muenster/">ref.ch</a></li>
  <li><a href="https://www.regio-tv.de/mediathek/video/ulmer-muenster-klimaaktivisten-klettern-auf-kirchturm-und-entrollen-banner/">regio-tv.de</a></li>
  <li><a href="https://www.schwaebische.de/regional/ulm-alb-donau/ulm/klimaaktivisten-klettern-auf-ulmer-muenster-und-hissen-riesiges-banner-2663258">schwäbische.de</a></li>
  <li><a href="https://www.sonntagsblatt.de/artikel/epd/klimaaktivisten-klettern-auf-ulmer-muenster">sonntagsblatt.de</a></li>
  <li>
    <a href="https://www.swp.de/lokales/ulm/polizeieinsatz-am-ulmer-muenster-christliche-klimaaktivisten-klettern-auf-kirchturm-und-bringen-banner-an-74122347.html">swp.de</a> (Nr.&nbsp;1),
    <a href="https://www.swp.de/lokales/ulm/sek-einsatz-am-ulmer-muenster-klimaaktivisten-klettern-aufs-muenster-das-sagen-polizei-und-dekan-77363480.html">swp.de</a> (Nr.&nbsp;2),
    <a href="https://www.swp.de/lokales/ulm/sek-einsatz-am-ulmer-muenster-diese-form-von-protest-braucht-keiner-77362216.html">swp.de</a> (Nr.&nbsp;3)
  </li>
  <li>
    <a href="https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/klimaaktivisten-befestigen-banner-an-turm-ulmer-muenster-100.html">swr.de</a> (Artikel),
    <a href="https://www.ardmediathek.de/video/swr-aktuell-baden-wuerttemberg/sendung-16-00-uhr-vom-2-7-2024/swr-bw/Y3JpZDovL3N3ci5kZS9hZXgvbzIwNzQ5MDk">swr.de</a> (Fernsehbericht, Zeitstempel 9:07)
  </li>
  <li><a href="https://www.tagesschau.de/inland/regional/badenwuerttemberg/swr-klimaaktivisten-entrollen-banner-an-ulmer-muensterturm-100.html">tagesschau.de</a></li>
  <li><a href="https://www.ulm-news.de/weblog/ulm-news/view/dt/3/article/96044/.html">ulm-news.de</a></li>
  <li>
    Sowie:
    <a href="https://www.antenne1.de/p/Klimaaktivisten-hangen-Protestbanner-am-Ulmer-Munster-auf-1A22mCGBb1cy5uAFpV6Hqk">antenne1.de</a> &bull;
    <a href="https://www.badische-zeitung.de/klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf">badische-zeitung.de</a> &bull;
    <a href="https://www.fraenkischertag.de/ueberregional/vorlaeufige-festnahmen-nach-protestaktion-am-ulmer-muenster-art-366045">fränkischertag.de</a> &bull;
    <a href="https://www.krzbb.de/inhalt.protest-in-ulm-klimaaktivisten-haengen-protestbanner-am-muenster-auf.93a4e0b2-2ed2-4c17-909a-8036fd9f2399.html">krzbb.de</a> &bull;
    <a href="https://www.landtag-bw.de/home/aktuelles/dpa-nachrichten/2024/Juli/KW27/Dienstag/97ebc6b9-a046-4749-ba9c-b1b6b94a.html">landtag-bw.de</a> &bull;
    <a href="https://www.morgenpost.de/video/article406708733/klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf.html">morgenpost.de</a> &bull;
    <a href="https://www.radio7.de/nachrichten/regionales/klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf">radio7.de</a> &bull;
    <a href="https://www.radio-bamberg.de/vorlaeufige-festnahmen-nach-protestaktion-am-ulmer-muenster-785340/">radio-bamberg.de</a> &bull;
    <a href="https://www.radioeins.com/vorlaeufige-festnahmen-nach-protestaktion-am-ulmer-muenster-574125/">radioeins.com</a> &bull;
    <a href="https://www.rtl.de/regionale-nachrichten/bayern/vorlaeufige-festnahmen-nach-protestaktion-am-ulmer-muenster-id1692098.html">rtl.de</a> &bull;
    <a href="https://www.tagblatt.de/Nachrichten/Klimaaktivisten-haengen-Protestbanner-am-Ulmer-Muenster-auf-632704.html">tagblatt.de</a> &bull;
    <a href="https://www.msn.com/de-de/nachrichten/politik/klimaaktivisten-h%C3%A4ngen-protestbanner-am-ulmer-m%C3%BCnster-auf/ar-BB1pfRcJ">msn.de</a> &bull;
    <a href="https://www.nussbaum.de/entdecken/bw-news/polizei-holt-klimaaktivisten-vom-ulmer-muenster-mit-video-2015557">nussbaum.de</a> &bull;
    <a href="https://www.spin.de/news/drei_menschen_klettern_auf_ulmer_muenster_und_entrollen_banner">spin.de</a> &bull;
    <a href="https://www.stern.de/gesellschaft/regional/baden-wuerttemberg/demonstrationen--klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf-34845200.html">stern.de</a> &bull;
    <a href="https://www.stuttgarter-zeitung.de/inhalt.protest-in-ulm-klimaaktivisten-haengen-protestbanner-am-muenster-auf.93a4e0b2-2ed2-4c17-909a-8036fd9f2399.html">stuttgarter-zeitung.de</a> &bull;
    <a href="https://www.ka-news.de/nachrichten/baden-wuerttemberg/klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf-art-3200403">ka-news.de</a> &bull;
    <a href="https://www.sueddeutsche.de/politik/demonstrationen-klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf-dpa.urn-newsml-dpa-com-20090101-240702-930-161072">süddeutsche.de</a> (Nr.&nbsp;1) &bull;
    <a href="https://www.sueddeutsche.de/bayern/demonstrationen-vorlaeufige-festnahmen-nach-protestaktion-am-ulmer-muenster-dpa.urn-newsml-dpa-com-20090101-240702-930-161072">süddeutsche.de</a> (Nr.&nbsp;2) &bull;
    <a href="https://live.vodafone.de/regional/badenwuerttemberg/klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf/12710382">vodafone.de</a> &bull;
    <a href="https://www.welt.de/regionales/baden-wuerttemberg/article252300130/Vorlaeufige-Festnahmen-nach-Protestaktion-am-Ulmer-Muenster.html">welt.de</a> &bull;
    <a href="https://www.wochenblatt-news.de/nachbarschaft/ravensburger-klimaaktivisten-haengen-banner-am-ulmer-muenster-auf/">wochenblatt-news.de</a> &bull;
    <a href="https://www.zeit.de/news/2024-07/02/klimaaktivisten-haengen-protestbanner-am-ulmer-muenster-auf">zeit.de</a>
  <li><a href="https://aussiedlerbote.de/en/police-prevent-climbing-action-at-ulm-minster/">aussiedlerbote.de</a></li>
  <li><a href="https://www.staatsanzeiger.de/nachrichten/politik-und-verwaltung/sind-wir-nicht-alle/">staatsanzeiger.de</a></li>
  <li><a href="https://www.augsburger-allgemeine.de/neu-ulm/ulm-waere-jesus-klimaschuetzer-banner-aktion-am-ulmer-muenster-gescheitert-id71185701.html">augsburger-allgemeine.de</a></li>
  <li>
    <a href="https://www.regio-tv.de/mediathek/video/klimaaktivisten-in-ulm-aktion-abgebrochen/">regio-tv.de</a> (Nr.&nbsp;1),
    <a href="https://www.regio-tv.de/mediathek/video/polizei-verhindert-kletteraktion-von-klimaaktivisten/">regio-tv.de</a> (Nr.&nbsp;2)
  </li>
  <li><a href="https://www.schwaebische.de/regional/ulm-alb-donau/ulm/klimaaktion-gestoppt-polizei-verhindert-kletteraktion-am-ulmer-muenster-2645740">schwäbische.de</a></li>
  <li>
    <a href="https://www.swp.de/lokales/ulm/ulmer-muenster-kletteraktion-verhindert-polizei-kommt-klimaaktivisten-zuvor-74081279.html">swp.de</a> (Nr.&nbsp;1),
    <a href="https://www.swp.de/lokales/ulm/ulmer-muenster-illegale-aktion-aufgeflogen-74081525.html">swp.de</a> (Nr.&nbsp;2)
  </li>
  <li><a href="https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/aktion-von-klimaaktivisten-am-ulmer-muenster-gescheitert-100.html">swr.de</a></li>
  <li><a href="https://www.tag24.de/thema/klimaaktivisten-letzte-generation/klimaaktivisten-wollen-in-ulm-hoch-hinaus-polizei-verhindert-kletteraktion-3228080">tag24.de</a></li>
  <li><a href="https://www.tagesschau.de/inland/regional/badenwuerttemberg/swr-aktion-von-kimaaktivisten-am-ulmer-muenster-gescheitert-100.html">tagesschau.de</a></li>
  <li>Sowie:
<a href="https://live.vodafone.de/regional/bayern/polizei-verhindert-kletteraktion-am-ulmer-muenster/12702190">live.vodafone.de</a> &bull;
  <a href="https://newstral.com/de/article/de/1254803768/aktion-in-ulm-war-durchgesickert-polizei-verhindert-kletteraktion-von-klimaaktivisten-am-ulmer-m%C3%BCnster#google_vignette">newstral.com</a> &bull;
  <a href="https://www.allgaeuer-zeitung.de/welt/polizei-verhindert-kletteraktion-von-klimaaktivisten-am-ulmer-muenster_arid-761664">allgaeuer-zeitung.de</a> &bull;
  <a href="https://www.antenne.de/nachrichten/bayern/polizei-verhindert-kletteraktion-von-klimaaktivisten-am-ulmer-muenster">antenne.de</a> &bull;
  <a href="https://www.berchtesgadener-anzeiger.de/startseite_artikel,-polizei-verhindert-kletteraktion-am-ulmer-muenster-_arid,882597.html">berchtesgadener-anzeiger.de</a> &bull;
  <a href="https://www.esslinger-zeitung.de/inhalt.aktion-in-ulm-war-durchgesickert-polizei-verhindert-kletteraktion-von-klimaaktivisten-am-ulmer-muenster.a41cf914-483b-4e88-be23-8ac5606a62e7.html">esslinger-zeitung.de</a> &bull;
  <a href="https://www.flz.de/polizei-verhindert-kletteraktion-von-klimaaktivisten-am-ulmer-muenster/cnt-id-ps-15615c85-756e-4e8c-9943-f318717f37ef">flz.de</a> &bull;
  <a href="https://www.idea.de/artikel/ulm-polizei-verhindert-aktion-von-klima-aktivisten-am-muenster">idea.de</a> &bull;
  <a href="https://www.landtag-bw.de/home/aktuelles/dpa-nachrichten/2024/Juni/KW26/Mittwoch/23fdccf8-79a6-4390-b182-676c4436.html">landtag-bw.de</a> &bull;
  <a href="https://www.pnp.de/nachrichten/panorama/polizei-verhindert-kletteraktion-am-ulmer-muenster-16425485">pnp.de</a> &bull;
  <a href="https://www.radioeins.com/polizei-verhindert-kletteraktion-von-klimaaktivisten-am-ulmer-muenster-561948/">radioeins.com</a> &bull;
  <a href="https://www.rtl.de/regionale-nachrichten/bayern/polizei-verhindert-kletteraktion-am-ulmer-muenster-id1681626.html">rtl.de</a> &bull;
  <a href="https://www.stern.de/gesellschaft/regional/baden-wuerttemberg/demonstrationen--polizei-verhindert-kletteraktion-von-klimaaktivisten-am-ulmer-muenster-34830248.html">stern.de</a> &bull;
  <a href="https://www.stuttgarter-nachrichten.de/inhalt.aktion-in-ulm-war-durchgesickert-polizei-verhindert-kletteraktion-von-klimaaktivisten-am-ulmer-muenster.a41cf914-483b-4e88-be23-8ac5606a62e7.html">stuttgarter-nachrichten.de</a> &bull;
  <a href="https://www.sueddeutsche.de/bayern/demonstrationen-polizei-verhindert-kletteraktion-am-ulmer-muenster-dpa.urn-newsml-dpa-com-20090101-240626-99-537339">süddeutsche.de</a> &bull;
  <a href="https://www.tagblatt.de/Nachrichten/Wohl-wieder-illegales-Klettern-am-Ulmer-Muenster-632214.html">tagblatt.de</a> &bull;
  <a href="https://www.ulm-news.de/weblog/ulm-news/view/dt/3/article/82825/Kletter-_und_Klimaaktivisten_am_Ulmer_Muenster_von_Polizei_gestoppt.html">ulm-news.de</a> &bull;
  <a href="https://www.welt.de/regionales/baden-wuerttemberg/article252214184/Polizei-verhindert-Kletteraktion-am-Ulmer-Muenster.html">welt.de</a> &bull;
  <a href="https://www.zeit.de/news/2024-06/26/polizei-verhindert-kletteraktion-von-klimaaktivisten-am-ulmer-muenster">zeit.de</a>
</ul>

## 03.05.2024

- Schwäbische: [„Bürokratischer Wahnwitz“: Molldietetunnel kommt nicht voran](https://www.schwaebische.de/regional/oberschwaben/ravensburg/buerokratischer-wahnwitz-molldietetunnel-kommt-nicht-voran-2488998)

## 01.05.2024

- BLIX: [Meinung statt üble Nachrede](https://www.diebildschirmzeitung.de/blix/aktuell/meinung-statt-ueble-nachrede-50840/)

## 17.04.2024

- Südkurier: [Klimaaktivist bekommt Recht: Höchstes Gericht ordnet Freilassung aus der Haft an](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/klimaaktivist-bekommt-recht-hoechstes-gericht-ordnet-freilassung-aus-der-haft-an;art410936,11980719)

## 12.04.2024

- Filstalwelle: [Göppingen: Haftstrafe des Klimaaktivisten Samuel Bosch - Mutter schreibt offenen Brief](https://filstalwelle.de/video/2024-04-12-goeppingen-haftstrafe-des-klimaaktivisten-samuel-bosch-mutter-schreibt-offenen-brief)
- Filstalexpress: [Offener Brief an Justizministerin Gentges: Zellen in der Jugendarrestanstalt Göppingen](https://filstalexpress.de/lokalnachrichten/168016/)

## 10.04.2024

- Kontext Wochenzeitung: [Die Solidarität trägt](https://www.kontextwochenzeitung.de/gesellschaft/680/die-solidaritaet-traegt-9467.html)
- Kontext Wochenzeitung: [Ein Urteil für die Meinungsfreiheit](https://www.kontextwochenzeitung.de/gesellschaft/680/ein-urteil-fuer-die-meinungsfreiheit-9466.html)

## 09.04.2024

- augsburg.tv: [Lohwaldrodung: Erneute Proteste des Klimacamps](https://www.augsburg.tv/mediathek/video/lohwaldrodung-erneute-proteste-des-klimacamps/)
- Zwiebel: [Wir sind im Herzen bei dir, Samuel!](https://zwiebel-es.de/wir-sind-im-herzen-bei-dir-samuel/)
- myheimat: [Klimacamper\*innen kündigen Wiederholung ihrer satirischen Kritik an der Regierung von Schwaben an](https://www.myheimat.de/ravensburg/c-politik/klimacamperinnen-kuendigen-wiederholung-ihrer-satirischen-kritik-an-der-regierung-von-schwaben-an_a3505018)

## 08.04.2024

- Filstalexpress: [Solidaritätsaktion an der Jugendarrestanstalt nach gewonnenem Verfassungsgerichtsurteil](https://filstalexpress.de/lokalnachrichten/167741/)

## 05.04.2024

- LTO: [BVerfG kassiert Urteil gegen Klimaaktivisten Samuel Bosch](https://www.lto.de/recht/nachrichten/n/bverfg-verfassungsbeschwerde-klimaaktivist-klimaschutz-verurteilung-ueble-nachrede-meinungsfreiheit-samuel-bosch/)
- SWP: [Sieg vor Gericht für Klimaaktivisten – Samuel Bosch kommt aus Arrest frei](https://www.swp.de/baden-wuerttemberg/erfolg-vor-bundesverfassungsgericht-klimaaktivist-samuel-bosch-siegt-mit-verfassungsbeschwerde-in-karlsruhe-73487279.html)
- SWR: [Urteil aufgehoben: Ravensburger Klimaaktivist kommt frei](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/bundesverfassungsgericht-kippt-urtei-oberschwaebischer-klimaaktivist-kommt-frei-100.html)
- Süddeutsche: [Bayern: Verfassungsgericht kassiert Urteil gegen Klimaaktivisten](https://www.sueddeutsche.de/bayern/bayern-augsburg-klimaprotest-verfassungsgericht-1.6522557)
- Tagesschau.de: [Urteil aufgehoben: Ravensburger Klimaaktivist kommt frei](https://www.tagesschau.de/inland/regional/badenwuerttemberg/swr-urteil-aufgehoben-ravensburger-klimaaktivist-kommt-frei-100.html)
- BR24: [Verfassungsgericht hebt Urteil auf: Klimaaktivist kommt frei](https://www.br.de/nachrichten/bayern/verfassungsgericht-hebt-urteil-auf-klimaaktivist-kommt-frei,U94XcIL)
- Augsburger Allgemeine: [Aktion in Augsburg: Verfassungsgericht kassiert Urteil gegen Klimaaktivisten](https://www.augsburger-allgemeine.de/augsburg/augsburg-aktion-in-augsburg-verfassungsgericht-kassiert-urteil-gegen-klimaaktivisten-id70354226.html)
- Schwäbische: [Bundesverfassungsgericht kippt Urteil gegen Samuel Bosch](https://www.schwaebische.de/regional/oberschwaben/ravensburg/bundesverfassungsgericht-kippt-urteil-gegen-klimaaktivisten-2408838)
- Bundesverfassungsgericht: [Beschluss vom 04. April 2024 - 1 BvR 820/24](https://www.bverfg.de/e/rk20240404_1bvr082024.html)

## 27.03.2024
- Ravensburger Spectrum: [Aktuell mit Fotos vom Sit-in 🚩Aktivist/innen aus Süd-West wollen am Karfreitag in Göppingen "Sit-in" vor der Jugend-Arrest-Anstalt durchführen ...](https://servus-ravensburg.mozello.com/klima/params/post/4482902/aktivistinnen-aus-sud-west-wollen-am-karfreitag-in-goppingen-sit-in-vor-der)

## 21.03.2024

- Südwest Presse: [„Lücke im Terminplan“: Aktivist Samuel Bosch tritt Arrest in Göppingen an](https://www.swp.de/lokales/goeppingen/klimaschuetzer-in-jugendarrestanstalt-_luecke-im-terminplan_-aktivist-samuel-bosch-hat-arrest-in-goeppingen-angetreten-73373989.html)
- Schwäbische: [Klimaaktivist Samuel Bosch tritt Haftstrafe nun doch an](https://www.schwaebische.de/regional/oberschwaben/ravensburg/klimaaktivist-samuel-bosch-tritt-haftstrafe-an-2367270)

## 19.03.2024

- Filstalexpress: [Anstatt seiner Haftantritt: Klimaaktivist hängt Banner vor Jugendarrestanstalt in Göppingen](https://filstalexpress.de/lokalnachrichten/166652/)
- Regio TV: [Samuel Bosch - Klimaschützer auf der Flucht vor der Justiz](https://www.regio-tv.de/mediathek/video/samuel-bosch-klimaschuetzer-auf-der-flucht-vor-der-justiz/)
- Südwest Presse: [Protest statt Arrest: Aktivist Samuel Bosch entwischt der Polizei](https://www.swp.de/lokales/goeppingen/jugendarrestanstalt-goeppingen-protest-statt-arrest_-aktivist-samuel-bosch-entwischt-der-polizei-73356945.html)

## 18.03.2024

- [Regio TV Bodensee Journal 18.03.2024](https://www.regio-tv.de/mediathek/video/regio-tv-bodensee-journal-18-03-2024/)

## 17.03.2024

- Augsburger Allgemeine: [Klimaaktivist tritt Haftstrafe nicht an und geht zu Podiumsdiskussion in Ulm](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimaaktivist-tritt-haftstrafe-nicht-an-id70173706.html)

## 14.03.2024

- Schwäbische: [Klimaaktivist Samuel Bosch muss für drei Wochen in Jugendarrest](https://www.schwaebische.de/regional/oberschwaben/vogt/klimaaktivist-samuel-bosch-muss-fuer-drei-wochen-in-jugendarrest-2344750)
- BLIX: [Samuel Bosch will Jugendarrest nicht antreten](https://www.diebildschirmzeitung.de/allgaeu-oberschwaben/allgaeu-oberschwaben/samuel-bosch-will-jugendarrest-nicht-antreten-42320/)

## 13.03.2024

- [Samuel Bosch (21) geht für das Klima in den Jugendarrest](https://www.youtube.com/watch?v=Its7z6q6SiQ)

## 01.03.2024

- SWR: [Warum ein Aktivist fürs Klima ins Gefängnis geht](https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/klimaaktivist-protest-ulm-gefaengnis-100.html)

## 17.02.2024

- Ravensburger Spectrum: [Skandal im Süden: Ravensburger Aktivist/innen, die für soziale und klimagerechte Transformation und damit auch gegen den Rechtsruck kämpfen, müssen hinter "Gitter" ...](https://servus-ravensburg.mozello.com/klima/params/post/4449245/skandal-im-suden-ravensburger-aktivistinnen-die-fur-soziale-und-klimagerech)

## 22.12.2023

- Schwäbische: [Ravensburger Klimaschützer zeigen wieder Flagge](https://www.schwaebische.de/regional/oberschwaben/ravensburg/ravensburger-klimaschuetzer-zeigen-wieder-flagge-2148761)

## 25.10.2023

- Regio TV: [Klimaprotest in Kressbronn: Die Aktivisten aus dem Altdorfer Wald wehren sich gegen neuen Regionalplan](https://www.regio-tv.de/mediathek/video/klimaprotest-in-kressbronn-die-aktivisten-aus-dem-altdorfer-wald-wehren-sich-gegen-neuen-regionalplan/)

## 24.10.2023

- SWR: [Klimaaktivisten demonstrieren gegen Regionalplan](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/klimaaktivisten-protestieren-in-kressbronn-gegen-regionalplan-100.html)
- Schwäbische: [Klimacamp gegen Flächenfraß: Radikale Waldbesetzer ziehen nach Kressbronn](https://www.schwaebische.de/regional/bodensee/kressbronn/klimacamp-gegen-flaechenfrass-radikale-waldbesetzer-ziehen-nach-kressbronn-1998237)
- Schwäbische: [Waldbesetzer bringen sich in Kressbronn in Stellung](https://www.schwaebische.de/regional/bodensee/kressbronn/protestcamp-und-demo-waldbesetzer-bringen-sich-in-kressbronn-in-stellung-2000966)

## 28.06.2023

- [Klimaschützer vom Bodensee müssen nach Protestaktion in Arrest](https://newsapp.suedkurier.de/region/bodenseekreis/bodenseekreis/klimaschuetzer-vom-bodensee-muessen-nach-protestaktion-in-arrest;art410936,11623797)

## 22.07.2023

- [Klimaaktivisten vom Bodensee sind nach Haftbefehl weiter untergetaucht](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/klimaschuetzer-nach-dem-haftbefehl-haben-wir-sofort-die-handys-auf-flugmodus-geschaltet;art410936,11615151)
- [Augsburger Amtsgericht veranlasst Bundesweite Fahndung nach
Ravensburger Klimaaktivisten](https://www.wochenblatt-news.de/region-ravensburg/ravensburg/nach-haftbefehl-untergetaucht-klimaaktivisten-aus-ravensburg-weiterhin-auf-der-flucht/)
- [Klimaaktivisten per Haftbefehl gesucht: Es gibt einen neuen Prozesstermin](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimaaktivisten-per-haftbefehl-gesucht-es-gibt-einen-neuen-prozesstermin-id66892961.html)

## 21.04.2023

- [Aktivisten malen Schafe vor das Rathaus in Weingarten](https://www.schwaebische.de/regional/oberschwaben/weingarten/aktivisten-malen-schafe-vor-das-rathaus-in-weingarten-1558411)

## 27.02.2023

- [Von Menschen auf Bäumen](https://www.diebildschirmzeitung.de/blix/aktuell/14766-von-menschen-auf-baeumen)

- [Film über Besetzung im Altdorfer Wald ausverkauft](https://www.schwaebische.de/regional/oberschwaben/weingarten/filmdoku-ueber-besetzung-im-altdorfer-wald-ausverkauft-1423309)

## 23.02.2023

- [Dokumentarfilm über Waldbesetzung kommt ins Kino](https://www.schwaebische.de/regional/oberschwaben/vogt/dokumentarfilm-ueber-waldbesetzung-kommt-in-weingarten-ins-kino-1413884)

## 15.02.2023

- [Ein Hort der Hoffnung](https://www.kontextwochenzeitung.de/gesellschaft/620/ein-hort-der-hoffnung-8699.html)

## 12.02.2023

- [Windräder im Altdorfer Wald: Das Dilemma der Klimaschützer](https://www.schwaebische.de/regional/baden-wuerttemberg/das-dilemma-der-klimaschuetzer-1377940?lid=true)

## 11.02.2023

- [Protest in Stuttgart gegen Kiesabbau in Oberschwaben](https://www.schwaebische.de/regional/baden-wuerttemberg/protest-in-stuttgart-gegen-kiesabbau-in-oberschwaben-1377493)

## 11.01.2023

- [Steht eine Räumung der Waldbesetzung an? Amt nimmt Stellung zu Gerüchten](https://www.schwaebische.de/regional/oberschwaben/vogt/waldbesetzer-wappnen-sich-fuer-raeumung-1284022)

## 09.01.2023

- [Wie weit würden die Waldbesetzer für ihren Umweltaktivismus gehen?](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/wie-weit-wuerden-die-waldbesetzer-fuer-ihren-umweltaktivismus-gehen;art410936,11423872)

- [Mehr als 400 Menschen demonstrierten im Altdorfer Wald für Waldschutz, Trinkwasserschutz, Klimaschutz](https://www.diebildschirmzeitung.de/diebildschirmzeitung/bad-waldsee/bad-waldsee-le/13877-mehr-als-400-menschen-demonstrierten-im-altdorfer-wald-fuer-waldschutz-trinkwasserschutz-klimaschutz)

## 08.01.2023

- [Protest gegen Kiesabbau im Altdorfer Wald](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/demo-im-altdorfer-wald-gegen-kiesabbau-104.html)

## 19.12.2022

- [Prozess gegen Baumbesetzer in Ravensburg](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/landkreis-ravensburg-le/13598-prozess-gegen-baumbesetzer-in-ravensburg)

## 17.12.2022

- [Gehzeug-Aktivist*innen legen Verkehr im Schussental lahm](https://regionbodenseeoberschwaben.blogspot.com/2022/12/gehzeug-aktivistinnen-legen-verkehr-im.html)

## 17.5.2022
* Südzeit: Wir leben in den Bäumen ([Foto](/pressespiegel/suedzeit.jpeg))

## 14.5.2022
* Süddeutsche: [Kran-Protest gegen Stuttgart 21 in luftiger Höhe](https://www.sueddeutsche.de/politik/demonstrationen-stuttgart-kran-protest-gegen-stuttgart-21-in-luftiger-hoehe-dpa.urn-newsml-dpa-com-20090101-220514-99-287261)

## 10.5.2022
* BLIX: [Protestaktion auf Dach von Gemeindehalle in Wetzisreute: Muss jetzt Bürgermeisterin Katja Liebmann aussagen?](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/landkreis-ravensburg-le/9666-protestaktion-auf-dach-von-gemeindehalle-in-wetzisreute-muss-jetzt-buergermeisterin-katja-liebmann-aussagen)

## 5.4.2022
* BLIX: [Freispruch für Klimaktivist](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/9083-freispruch-fuer-klimaktivist)

## 3.4.2022
* Schwäbische: [170 Menschen demonstrieren in Ankenreute](https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-170-menschen-demonstrieren-in-ankenreute-_arid,11491971.html)

## 25.3.2022
* Schwäbische: [Rechtsbruch für den Klimaschutz](https://www.pressreader.com/germany/schwaebische-zeitung-ehingen/20220325/281599538998906) ([Foto](/pressespiegel/schwaebische-2022-03-25.jpeg))

## 2.3.2022
* Kontext Wochenzeitung: [Raus aus dem Wachstumswahn](https://www.kontextwochenzeitung.de/gesellschaft/570/raus-aus-dem-wachstumswahn-8041.html)

## 1.3.2022
* Südkurier: [Klimaaktivist verurteilt: Verständnis für die Botschaft, Sozialstunden für die Vorgehensweise](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/klimaaktivist-verurteilt-verstaendnis-fuer-die-botschaft-sozialstunden-fuer-die-vorgehensweise;art410936,11060634)

## 28.2.2022
* BLIX: [„Die Klinik ist unserer Meinung“](https://www.diebildschirmzeitung.de/blix/aktuell/8531-die-klinik-ist-unserer-meinung)

## 25.2.2022
* Schwäbische: [Protest aus dem Wald: Seit einem Jahr leben Aktivisten in Baumhäusern](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-protest-aus-dem-wald-seit-einem-jahr-leben-aktivisten-in-baumhaeusern-_arid,11476467.html)
* Ravensburger Spectrum: [Altdorfer Wald: "Ich bin überzeugt, dass die Polizei gute Arbeit leistete. Aber wer steckt hinter diesen unverhältnismäßigen Maßnahmen?" -- Trauernacht bei den gerodeten Bäumen ... Uralte Eichen und Buchen ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3997128/altdorfer-wald-ich-bin-uberzeugt-dass-die-polizei-gute-arbeit-leistete-aber)

## 24.2.2022
* Schwäbische: [Baumbesetzer sind wieder auf freiem Fuß](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-baumbesetzer-sind-wieder-auf-freiem-fuss-_arid,11476451.html)

## 23.2.2022
* Schwäbische: [Nach SEK-Einsatz im Altdorfer Wald: Ermittlungsverfahren wegen Nötigung eingeleitet](https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-nach-sek-einsatz-im-altdorfer-wald-ermittlungsverfahren-wegen-noetigung-eingeleitet-_arid,11476025.html)
* Schwäbische: [Kletterei über der Straße: Strafe und Standpauke für Klimaaktivisten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-kletterei-ueber-der-strasse-strafe-und-standpauke-fuer-klimaaktivisten-_arid,11475576.html)
* Schwäbische: [Spezialkommando räumt Baumbesetzer - Rodung im Altdorfer Wald hat begonnen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-altdorfer-wald-bei-oberankenreute-polizei-loest-protestaktion-auf-_arid,11475694.html)
* Schwäbische: [Polizei holt im Altdorfer Wald Aktivisten vom Baum](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-polizei-holt-aktivisten-im-altdorfer-wald-von-baeumen-_arid,11475829.html)
* Zeit Online: [Polizei holt im Altdorfer Wald Aktivisten vom Baum](https://www.zeit.de/amp/news/2022-02/23/polizei-holt-aktivisten-im-altdorfer-wald-von-baeumen)

## 22.2.2022
* Schwäbische: [Klimaaktivisten besetzen weiteren Teil des Altdorfer Waldes](https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-klimaaktivisten-besetzen-weiteren-teil-des-altdorfer-waldes-_arid,11475259.html)
* Schwäbische: [Waldbesetzung bei Oberankenreute - Wie geht es jetzt weiter?](https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-waldbesetzung-bei-oberankenreute-das-ist-der-aktuelle-stand-_arid,11475514.html)
* SWR: [Klimaaktivist Samuel Bosch soll Sozialstunden leisten](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/klimaaktivist-amtsgericht-ravensburg-sozialstunden-baumbesetzung-100.html)
* Wochenblatt: [Wegen Protestbanner: Klimaaktivist wieder vor Gericht](https://www.wochenblatt-news.de/wegen-protestbanner-klimaaktivist-wieder-vor-gericht/)

## 21.2.2022
* Schwäbische: [Baumbesetzer setzen Uniklinik unter Druck](https://www.schwaebische.de/landkreis/alb-donau-kreis/ulm_artikel,-jetzt-hat-auch-die-uniklinik-ein-baumbesetzer-problem-_arid,11475063.html)

## 18.2.2022
* Schwäbische: [Wegen Kletteraktion an der Basilika: 19-Jährige Aktivistin zu 30 Arbeitsstunden verurteilt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-wegen-banner-an-der-basilika-19-jaehrige-aktivistin-zu-30-arbeitsstunden-verurteilt-_arid,11473661.html)

## 17.2.2022
* Die Bildschirmzeitung: [30 Sozialstunden für Hausfriedensbruch an der Basilika](https://www.diebildschirmzeitung.de/diebildschirmzeitung/diebildschirmzeitung-le/8383-30-sozialstunden-fuer-hausfriedensbruch-an-der-basilika)

## 16.2.2022
* Schwäbische: [Polizeieinsatz bei Waldbesetzern im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-landratsamt-laesst-dreibein-im-baumhauscamp-im-altdorfer-wald-absaegen-_arid,11473082.html)

## 15.2.2022
* BLIX: [Kunst im und für den Wald](https://www.diebildschirmzeitung.de/blix/aktuell/8351-kunst-im-und-fuer-den-wald)

## 14.2.2022
* Südkurier: [Als der Professor auf einen Baum kletterte – weil Wolfgang Ertel für Nachhaltigkeit demonstrierte, muss er jetzt vor Gericht](https://www.suedkurier.de/baden-wuerttemberg/als-der-professor-auf-einen-baum-kletterte-weil-wolfgang-ertel-fuer-nachhaltigkeit-demonstrierte-muss-er-jetzt-vor-gericht;art417930,11045126)

## 10.2.2022
* Schwäbische: [Aktivisten laden zu Vernissage ins Baumhauscamp „Alti“](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-aktivisten-laden-zu-vernissage-ins-baumhauscamp-alti-_arid,11470184.html)
* Schwäbische: [Klimaaktivisten protestieren auf dem Dach der Wetzisreuter Halle gegen Kiesabbau](https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-klimaaktivisten-protestieren-auf-dem-dach-der-wetzisreuter-halle-_arid,11470612.html)

## 8.2.2022
* SWR: [Sendung 14:00 Uhr vom 8.2.2022 (Ab Minute 4)](https://www.ardmediathek.de/video/swr-aktuell/sendung-14-00-uhr-vom-8-2-2022/swr/Y3JpZDovL3N3ci5kZS9hZXgvbzE2MTAyNzU)

## 5.2.2022
* Wochenblatt: [Klimaaktivisten beim Containern in Weingarten erwischt: Polizei beschlagnahmt Essen](https://www.wochenblatt-news.de/klimaaktivisten-beim-containern-in-weingarten-erwischt-polizei-beschlagnahmt-essen/)

## 2.2.2022
* Schwäbische: [Ravensburger CDU-Stadtrat Rolf Engler kritisiert Protestaktionen der Baumbesetzer](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-cdu-stadtrat-rolf-engler-kritisiert-protestaktionen-der-baumbesetzer-_arid,11466745.html)
* Kontext Wockenzeitung: ["Wir behalten uns natürlich vor, auch andere Flächen zu besetzen"](https://www.kontextwochenzeitung.de/schaubuehne/566/wir-behalten-uns-natuerlich-vor-auch-andere-faechen-zu-besetzen-7983.html)

## 1.2.2022
* Wochenblatt: [Nach Banneraktion an Basilika Weingarten: Klimaaktivisten und Kirche wollen zusammenarbeiten](https://www.wochenblatt-news.de/nach-banneraktion-an-basilika-weingarten-klimaaktivisten-und-kirche-wollen-zusammenarbeiten/)

## 24.1.2022
* Ravensburger Spectrum: [Die vergangene Woche im Klima-Rückspiegel - Es war viel los, auch ohne Moos!!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3971466/die-vergangene-woche-im-klima-ruckspiegel---es-war-viel-los-auch-ohne-moos)
* Die Bildschirmzeitung: [Klimaaktivismus: Rückblick auf Aktionen der letzten Tage](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/8123-klimaaktivismus-rueckblick-auf-aktionen-der-letzten-tage)
* Schwäbische: [Nach Baumbesetzung auf dem Hochschulcampus: RWU bekommt Klimamanager](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-nach-baumbesetzung-auf-dem-hochschulcampus-rwu-bekommt-klimamanager-_arid,11461560.html)

## 23.1.2022
* Schwäbische: [Klimaaktivisten steigen erneut auf Bäume in der Ravensburger Altstadt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaaktivisten-steigen-erneut-auf-baeume-in-der-ravensburger-altstadt-_arid,11461160.html)

## 22.1.2022
* Die Bildschirmzeitung: [Klimaaktivist*innen wollen Stadt mit erneuter dezentraler Baumbesetzung zum Handeln bewegen](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/8115-klimaaktivist-innen-wollen-stadt-mit-erneuter-dezentraler-baumbesetzung-zum-handeln-bewegen)

## 20.1.2022
* Die Bildschirmzeitung: [Anwohnerinnen und Aktivistinnen aus der Besetzung im Altdorfer Wald installieren Kunstaktion in Kiesgrube bei Oberankenreute](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/8103-anwohnerinnen-und-aktivistinnen-aus-der-besetzung-im-altdorfer-wald-installieren-kunstaktion-in-kiesgrube-bei-oberankenreute)

## 13.1.2022
* Ravensburger Spectrum: [Zweiter "Klimaprozess" vor dem Ravensburger Amtsgericht: "Eine Verurteilung wäre eine Niederlage für den Rechtsstaat" ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3963638/zweiter-klimaprozess-vor-dem-ravensburger-amtsgericht-eine-verurteilung-war)
* Schwäbische: [Baumbesetzung in der Ravensburger Innenstadt hat juristische Folgen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-baumbesetzung-in-der-ravensburger-innenstadt-hat-juristische-folgen-_arid,11457559.html)

## 8.1.2022
* Die Bildschirmzeitung: [Banneraktion in Ulm](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/7984-banneraktion-in-ulm)

## 2.1.2022
* Schwäbische: [Neuer regionaler Krimi: Alles dreht sich um den Kiesabbau im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wolfegg_artikel,-neuer-regionaler-krimi-alles-dreht-sich-um-den-kiesabbau-im-altdorfer-wald-_arid,11453296.html)

## 30.12.2021
* BLIX: [Zukunft ungewiss](https://www.diebildschirmzeitung.de/blix/bildung/7914-zukunft-ungewiss)

## 29.12.2021
* Kontext Wochenzeitung: [Den Protest in die Städte tragen](https://www.kontextwochenzeitung.de/gesellschaft/561/den-protest-in-die-staedte-tragen-7918.html)

## 28.12.2021
* Schwäbische: [Aufgeheizte Debatte um Kies, Wald und Klima](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-aufgeheizte-debatte-um-kies-wald-und-klima-_arid,11451356.html)

## 17.12.2021
* Schwäbische: [Demonstranten gehen in Ravensburg wieder fürs Klima auf die Straße](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-demonstranten-gehen-in-ravensburg-wieder-fuers-klima-auf-die-strasse-_arid,11449073.html)

## 12.12.2021
* Schwäbische: [Klimaaktivisten in Ravensburg: „Wir machen so lange Stress, bis die Stadt handelt“](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaaktivisten-in-ravensburg-wir-machen-so-lange-stress-bis-die-stadt-handelt-_arid,11446799.html)
* Ravensburger Spectrum: [KLIMA: RAVE-BERLIN-IRLAND-RAVE / Doch der Kreis hat sich längst nicht geschlossen, im Gegenteil / Klimaretter "Sam" statt Fürst "Dan" . . . und was Beethoven mit dem "Klima" zu tun hat.](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3941584/klima-rave-berlin-irland-rave--kreis-hat-sich-langst-nicht-geschlossen--kli)

## 11.12.2021
* Schwäbische: [Klimaaktivisten beenden Protest in Innenstadt](https://www.schwaebische.de/home_artikel,-klimaaktivisten-besetzen-erneut-baeume-in-innenstadt-_arid,11446562.html)

## 10.12.2021
* Schwäbische: [Buchen pflanzen im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wolfegg_artikel,-buchen-pflanzen-im-altdorfer-wald-_arid,11446214.html)

## 2.12.2021
* Rabensburger Spectrum: [Klima-Sam nun auch international - Grüße aus Dublin an die Ravensburger Schussenstrasse und in den Altdorfer Wald](https://ravensburger-spectrum.mozello.de/welt/params/post/3933505/klima-sam-nun-auch-international---gruse-aus-dublin-an-die-schussen)

## 1.12.2021
* Schwäbische: [Klimaaktivisten erhalten den Widerstandspreis der Räuberhöhle](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaaktivisten-erhalten-preis-eine-holzleiter-und-500-euro-als-lohn-fuer-protest-_arid,11442604.html)

## 28.10.2021
* SWR Aktuell: [Gründer des Klimacamps im Altdorfer Wald zu Sozialstunden verurteilt](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/gruender-des-klimacamps-im-altdorfer-wald-vor-amtsgericht-ravensburg-100.html)
* Schwäbische: [18-jähriger Baumbesetzer und Klimaaktivist in Ravensburg zu Sozialstunden verurteilt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-zu-sozialstunden-verurteilt-_arid,11429087.html)

## 27.10.2021
* Schwäbsche: [Klimaschützer halten Mahnwache vor dem Ravensburger Amtsgericht](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaschuetzer-halten-mahnwache-vor-ravensburger-amtsgericht-ab-_arid,11428415.html)

## 11.10.2021
* Schwäbische: [Stadt braucht mehr Schatten, Grün und Wasser](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-braucht-mehr-schatten-gruen-und-wasser-_arid,11422024.html)
* Schwäbische: [Besuch bei den Klimaaktivisten in Grund](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-besuch-bei-den-klimaaktivisten-in-grund-_arid,11421788.html)

## 9.10.2021
* BLIX: [Mehr miteinander und über das Klima reden](https://www.diebildschirmzeitung.de/diebildschirmzeitung/diebildschirmzeitung-le/6798-mehr-miteinander-und-ueber-das-klima-reden)

## 30.9.2021
* Schwäbische: [Klimaaktivisten spielen Katz und Maus mit der Polizei](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/sigmaringen_artikel,-klimaaktivisten-loesen-feuerwehreinsatz-aus-wer-bezahlt-das-_arid,11417610.html)

## 29.9.2021
* Kontext Wochenzeitung: [Der schwarze Fluss hat Ufer](https://www.kontextwochenzeitung.de/politik/548/der-schwarze-fluss-hat-ufer-7749.html)
* BLIX: [Wahlkampf bis zum Schluss](https://www.diebildschirmzeitung.de/blix/aktuell/6658-wahlkampf-bis-zum-schluss)

## 26.9.2021
* Schwäbische: [Klimaaktivisten erklimmen Basilika in Weingarten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-klimaaktivisten-erklimmen-basilika-in-weingarten-_arid,11415718.html)

## 24.9.2021
* Schwäbische: [Klimastreik in Sigmaringen: Aktivisten lassen sich bei gewagter Aktion von der Polizei nicht aufhalten](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/sigmaringen_artikel-klimastreik-in-sigmaringen-polizei-und-feuerwehr-stoppen-gewagte-protestaktion-ueber-der-donau-_arid,11415223.html)
* IDEA: [Ulm: Polizei verhindert Aktion von Klima-Aktivisten am Münster](https://www.idea.de/artikel/ulm-polizei-verhindert-aktion-von-klima-aktivisten-am-muenster)

## 23.9.2021
* all-in.de: [Neuer Versuch: Klimaaktivisten wollen auf andere Kirche klettern](https://www.all-in.de/c-lokales/neuer-versuch-klimaaktivisten-wollen-auf-andere-kirche-klettern_a5131139)

## 22.9.2021
* Schwäbische: [Ravensburger Klima-Aktivisten bei Kletter-Plänen am Ulmer Münster von Polizei „geerdet“](https://www.schwaebische.de/landkreis/alb-donau-kreis/ulm_artikel,-polizei-verhindert-klima-protest-auf-dem-ulmer-muenster-_arid,11414227.html)
* Augsburger Allgemeine: [Warum die Klimaaktivisten nicht aufs Ulmer Münster kletterten](https://www.augsburger-allgemeine.de/neu-ulm/Ulm-Warum-die-Klimaaktivisten-nicht-aufs-Ulmer-Muenster-kletterten-id60617221.html)
* SWR: [Keine Kletteraktion von Klimaaktivisten am Ulmer Münster](https://www.swr.de/swraktuell/baden-wuerttemberg/ulm/kletteraktion-muenster-klima-banner-ulm-100.html)

## 21.9.2021
* Regio TV: [Klimaaktivisten planen auf das Ulmer Münster zu Klettern](https://www.regio-tv.de/mediathek/video/klimaaktivisten-planen-auf-das-ulmer-muenster-zu-klettern/)
* all-in.de: [Klima-Aktivisten wollten auf Ulmer Münster klettern: Aktion kurz vor Beginn abgeblasen](https://www.all-in.de/ulm/c-lokales/klima-aktivisten-wollten-auf-ulmer-muenster-klettern-aktion-kurz-vor-beginn-abgeblasen_a5130938)
* Schwäbische: [Aktivisten wollen an Ulmer Münster-Fassade gegen CDU protestieren](https://www.schwaebische.de/landkreis/alb-donau-kreis/ulm_artikel,-aktivisten-wollen-aufs-ulmer-muenster-klettern-_arid,11413853.html)
* Augsburger Allgemeine: [Klimaaktivisten wollen aufs Ulmer Münster klettern und Banner enthüllen](https://www.augsburger-allgemeine.de/neu-ulm/Ulm-Klimaaktivisten-wollen-aufs-Ulmer-Muenster-klettern-und-Banner-enthuellen-id60611016.html)
* Augsburger Allgemeine: [Klimaaktivisten wollen aufs Ulmer Münster klettern: So reagiert die Polizei](https://www.augsburger-allgemeine.de/neu-ulm/Ulm-Klimaaktivisten-wollen-aufs-Ulmer-Muenster-klettern-So-reagiert-die-Polizei-id60610461.html)
* Augsburger Allgemeine: [Klimaaktivisten wollen aufs Münster klettern: "Müssen schneller sein als die Polizei"](https://www.augsburger-allgemeine.de/neu-ulm/Ulm-Klimaaktivisten-wollen-aufs-Muenster-klettern-Muessen-schneller-sein-als-die-Polizei-id60609946.html)
* Augsburger Allgemeine: [Klimaaktivisten wollen aufs Ulmer Münster klettern und Banner enthüllen](https://www.augsburger-allgemeine.de/neu-ulm/Ulm-Klimaaktivisten-wollen-aufs-Ulmer-Muenster-klettern-und-Banner-enthuellen-id60608536.html)
* Donau 3 FM: [ULM: KLIMA-AKTIVISTEN PLANEN KLETTERAKTION AN MÜNSTER-FASSADE](https://www.donau3fm.de/ulm-klima-aktivisten-wollen-an-muenster-fassade-demonstrieren-297627/)

## 18.9.2021
* Schwäbische: [Bürgermeister sieht Kiesabbau weiterhin als Zukunftsthema](https://www.schwaebische.de/landkreis/landkreis-ravensburg/baienfurt_artikel,-buergermeister-sieht-kiesabbau-weiterhin-als-zukunftsthema-_arid,11412332.html)

## 10.9.2021
* Schwäbische: [Naturschutzverbände kritisieren Pläne zu neuem Baugebiet „Schwärze“](https://www.schwaebische.de/landkreis/alb-donau-kreis/rottenacker_artikel,-neuer-aerger-um-das-baugebiet-schwaerze-_arid,11409727.html)

## 9.9.2021
* Schwäbische: [Räumungs-Urteil für Hambacher Forst – das bedeutet es für die Baumbesetzung im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-raeumungs-urteil-fuer-hambacher-forst-das-bedeutet-es-fuer-die-baumbesetzung-im-altdorfer-wald-_arid,11409233.html)

## 4.9.2021
* Schwäbische: [Regionalverband will 40 neue Windräder für Region Bodensee-Oberschwaben](https://www.schwaebische.de/landkreis/bodenseekreis/friedrichshafen_artikel,-regionalverband-40-neue-windraeder-sollen-in-der-region-bodensee-oberschwaben-gebaut-werden-_arid,11406231.html)

## 2.9. 2021
* Schwäbische: [„Initiative B30“ will Klarheit von Bundestagskandidaten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/bad-waldsee_artikel,-initiative-b30-will-klarheit-von-bundestagskandidaten-_arid,11406665.html)

## 28.8.2021
* Schwäbische: [Karikatur von Rainer Weishaupt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-die-karikatur-der-woche-fuer-ravensburg-und-die-region-_arid,11404020.html)

## 27.8.2021
* Schwäbische: [Ravensburger Baumkletterer muss kein Bußgeld zahlen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumkletterer-muss-kein-bussgeld-zahlen-_arid,11404370.html)

## 23.8.2021
* Schwäbische: [Ravensburger Klimaaktivist klettert aufs Brandenburger Tor](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-vom-einem-stadtbaum-in-ravensburg-aufs-brandenburger-tor-in-berlin-_arid,11402746.html)

## 22.8.2021
* Schwäbische: [Agnieszka Brugger würde Kiesgrube bei Vogt gerne verhindern](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-agnieszka-brugger-wuerde-kiesgrube-bei-vogt-gerne-verhindern-_arid,11402055.html) 

## 14.8.2021
* Schwäbische: [Kiesunternehmer Mohr äußert sich zu Kritik an Abbauplänen im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-kiesunternehmer-mohr-wir-muessen-uns-auf-alles-gefasst-machen-_arid,11399400.html)

## 12.8.2021
* Regio TV: [Diskussion um Kiesabbau im Tettnanger Wald](https://www.schwaebische.de/landkreis/bodenseekreis/tettnang_video,-diskussion-um-kiesabbau-im-tettnanger-wald-_vidid,160767.html)

## 30.7.2021
* Schwäbische: [Klimaaktivsten bleiben trotz Unwetter und Regen im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-trotz-unwetter-und-regen-klimaaktivsten-bleiben-im-altdorfer-wald-_arid,11393656.html)

## 25.7.2021
* Schwäbische: [Dieses Wochenende gehört das Lauratal den Radlern](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-ein-wochenende-fuer-radler-im-lauratal-_arid,11391679.html)

## 23.7.2021
* Schwäbische: [Den Geheimnissen des Altdorfer Waldes auf der Spur](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-den-geheimnissen-des-altdorfer-waldes-auf-der-spur-_arid,11391139.html)

## 19.7.2021
* Schwäbische: [Rohstoffindustrie begrüßt Verabschiedung des Regionalplans](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-rohstoffindustrie-begruesst-verabschiedung-des-regionalplans-_arid,11388964.html)

## 16.7.2021
* Schwäbische: [Wolfgang Heine ist neuer Regionalverbandsdirektor](https://www.schwaebische.de/landkreis/bodenseekreis/friedrichshafen_artikel,-wolfgang-heine-ist-neuer-regionalverbandsdirektor-_arid,11388358.html)

## 14.7.2021
* Schwäbische: [Debatte über Einsatz von Recyclingbaustoffen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/leutkirch_artikel,-debatte-ueber-einsatz-von-recyclingbaustoffen-_arid,11387110.html)

## 5.7.2021
* Schwäbische: [Klimawandel: In Baienfurt steigen die Temperaturen besonders stark](https://www.schwaebische.de/landkreis/landkreis-ravensburg/baienfurt_artikel,-baienfurt-naehert-sich-einer-erwaermung-um-zwei-grad-_arid,11383019.html)

## 4.7.2021
* Schwäbische: [Mit Tröten gegen den Regionalplan](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-mit-troeten-gegen-den-regionalplan-_arid,11383098.html)

## 3.7.2021
* Schwäbische: [Mit Tröten und Traktoren gegen den Regionalplan - Das war die Demo am Samstag](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-demonstration-gegen-regionalplan-in-ravensburg-_arid,11382772.html)
* Schwäbische: [Demos auf der Autobahn und Waldbesetzung durch Klimaaktivisten: Dürfen die das?](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-autobahn-demos-und-waldbesetzung-was-ist-erlaubt-_arid,11382527.html)
* Schwäbische: [Bildergalerie: Demontration gegen den Regionalplan](https://www.schwaebische.de/home_galerie,-demontration-gegen-den-regionalplan-_galid,304535.html)
* Schwäbische: [Demontration gegen den Regionalplan](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_video,-demontration-gegen-den-regionalplan-_vidid,160206.html)

## 2.7.2021
* Schwäbische: [„Robin Wood“ unterstützt Protest im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-robin-wood-unterstuetzt-protest-im-altdorfer-wald-_arid,11382311.html)

## 1.7.2021
* Schwäbische: [Protest gegen Regionalplan geht in Ravensburg weiter](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-protest-gegen-regionalplan-geht-in-ravensburg-weiter-_arid,11382090.html)

## 30.6.2021
* Schwäbische: [Fahrraddemo am Sonntag auf der A96 und in Wangen abgesagt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wangen_artikel,-fahrraddemo-am-sonntag-auf-der-a96-und-in-wangen-abgesagt-_arid,11381480.html)

## 29.6.2021
* Schwäbische: [FDP: Radlerdemo auf der A96 ist „Geiselhaft für Autofahrer“](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wangen_artikel,-fdp-radlerdemo-auf-der-a96-ist-geiselhaft-fuer-autofahrer-_arid,11381025.html)

## 28.6.2021
* Schwäbische: [Kiesabbau bei Grund bleibt trotz Gegenwind im Regionalplan](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wolfegg_artikel,-kiesabbau-bei-grund-bleibt-im-regionalplan-_arid,11380663.html)

## 25.6.2021
* Schwäbische: [Wangen verbietet erneut Radfahrer-Demo auf der A96](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wangen_artikel,-stadt-wangen-verbietet-erneut-radfahrer-demo-auf-der-a-96-_arid,11379610.html)
* Schwäbische: [Verwaltungsgericht erlaubt nächtliches Klimacamp in Ravensburg](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-verwaltungsgericht-erlaubt-naechtliches-klimacamp-in-ravensburg-_arid,11379435.html)
* Schwäbische: [Protest bleibt ohne Erfolg: Versammlung beschließt neuen Regionalplan](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/pfullendorf_artikel,-protest-bleibt-ohne-erfolg-versammlung-beschliesst-neuen-regionalplan-_arid,11379676.html)
* Schwäbische: [Die Klimaaktivisten schaffen auf der Autobahn nichts – außer Ärger](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wangen_artikel,-die-klimaaktivisten-schaffen-auf-der-autobahn-nichts-ausser-aerger-_arid,11379691.html)

## 24.6.2021
* Schwäbische: [Klimacamp Pfullendorf: Aktivisten protestieren in der Innenstadt gegen Kiesabbau](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/pfullendorf_artikel,-klimacamp-pfullendorf-aktivisten-protestieren-in-der-innenstadt-gegen-kiesabbau-_arid,11379275.html)

## 23.6.2021
* Schwäbische: [Teilnehmer von Diskussionsrunde stellen sich gegen den Regionalplan](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-teilnehmer-stellen-sich-gegen-den-regionalplan-_arid,11378620.html)

## 22.6.2021
* Schwäbische: [Verwaltungsgericht lässt Klimaaktivisten in Ravensburg abblitzen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-verwaltungsgericht-laesst-klimaaktivisten-in-ravensburg-abblitzen-_arid,11377707.html)
* Schwäbische: [Klimaaktivisten planen weitere Autobahn-Demo bei Wangen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wangen_artikel,-klimaaktivisten-planen-bei-wangen-weitere-autobahn-demo-_arid,11378104.html)

## 21.6.2021
* Schwäbische: [Klimaaktivisten demonstrieren auf der A96: Sind Autobahnen der richtige Ort für Protest?](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wangen_artikel,-klimaaktivisten-demonstrieren-auf-der-a-96-lange-staus-sind-die-folge-_arid,11377405.html)
* Schwäbische: [Gesellschaft und Wirtschaft brauchen einander](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-gesellschaft-und-wirtschaft-brauchen-einander-_arid,11377210.html)

## 20.6.2021
* Schwäbische: [Sechs Gemeinden beziehen Stellung zum Regionalplan und machen ihre Forderungen deutlich](https://www.schwaebische.de/landkreis/bodenseekreis/tettnang_artikel,-buergermeister-aus-sechs-gemeinden-beziehen-stellung-zum-regionalplan-_arid,11376743.html)

## 18.6.2021
* Schwäbische: [Baumhaus bei Oberankenreute wieder abgebaut](https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-baumhaus-bei-oberankenreute-wieder-abgebaut-_arid,11376507.html)
* Schwäbische: [Naturschützer kritisieren neues Baugebiet](https://www.schwaebische.de/landkreis/landkreis-biberach/bad-schussenried_artikel,-naturschuetzer-kritisieren-neues-baugebiet-in-hopferbach-_arid,11376665.html)

## 17.6.2021
* Schwäbische: [Fronten zum Regionalplan für die Region Bodensee-Oberschwaben verhärten sich](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-fronten-zum-regionalplan-fuer-die-region-bodensee-oberschwaben-verhaerten-sich-_arid,11376053.html)
* Schwäbische: [Es kommt auf die Umsetzung an](https://www.schwaebische.de/home_artikel,-es-kommt-auf-die-umsetzung-an-_arid,11376054.html)
* Schwäbische: [Zweites Baumcamp im Altdorfer Wald - Darum machen auch Anwohner mit](https://www.schwaebische.de/landkreis/landkreis-ravensburg/schlier_artikel,-zweites-baumcamp-im-altdorfer-wald-_arid,11376169.html)

## 16.6.2021
* Schwäbische: [Bildergalerie: Hohes Polizeiaufgebot, friedliche Demonstranten, so liefen die Proteste gegen den Regionalplan bisher](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_galerie,-bildergalerie-hohes-polizeiaufgebot-friedliche-demonstranten-so-liefen-die-proteste-gegen-den-reg-_galid,304508.html)
* Schwäbische: [Mit Video: Klimaaktivistinnen ziehen in Horgenzell fast blank](https://www.schwaebische.de/landkreis/landkreis-ravensburg/horgenzell_artikel,-warum-klimaaktivistinnen-in-horgenzell-fast-blank-ziehen-_arid,11375812.html)

## 15.6.2021
* Schwäbische: [Fraktionschefs von CDU, SPD und Freien Wählern verteidigen Regionalplan](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-fraktionschefs-von-cdu-spd-und-freien-waehlern-verteidigen-regionalplan-_arid,11375239.html)

## 14.6.2021
* Schwäbische: [Klimaaktivisten campieren in Horgenzell](https://www.schwaebische.de/landkreis/landkreis-ravensburg/horgenzell_artikel,-klimaaktivisten-campieren-in-horgenzell-_arid,11374775.html)
* Schwäbische: [Bürger protestieren gegen geplanten Kalkabbau im Donautal](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/sigmaringen_artikel,-buerger-protestieren-gegen-geplanten-kalkabbau-im-donautal-_arid,11374426.html)
* Schwäbische: [Falsche Prioritäten? Über den Regionalplan entscheiden fast nur Männer](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ueber-den-regionalplan-entscheiden-fast-nur-maenner-_arid,11374298.html)
* Schwäbische: [200 Demonstranten protestieren gegen geplanten Kalkabbau im Donautal](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/sigmaringen_artikel,-rund-200-personen-demonstrieren-gegen-kalkabbau-im-donautal-_arid,11374841.html)
* Schwäbische: [Suche nach Alternativen für umstrittene Kiesgrube im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-auf-der-suche-nach-alternativen-fuer-die-umstrittene-kiesgrube-_arid,11374794.html)
* Regio TV: [Rund 200 Demonstranten protestieren gegen den geplanten Kalkabbau im Donautal](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/sigmaringen_video,-rund-200-demonstranten-protestieren-gegen-den-geplanten-kalkabbau-im-donautal-_vidid,159896.html)

## 8.6.2021
* Schwäbische: [Altdorfer Wald: Jetzt schalten sich auch kirchliche Organisationen in die Debatte ein](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-altdorfer-wald-jetzt-schalten-sich-auch-kirchliche-organisationen-in-die-debatte-ein-_arid,11372231.html)

## 7.6.2021
* Schwäbische: [Sigmaringer demonstrieren gegen den Kalkabbau](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/sigmaringen_artikel,-sigmaringer-demonstrieren-gegen-den-kalkabbau-_arid,11371636.html)
* 7.6.2021 [Klimaziele „krachend verfehlt“: Warum Grünen-Politiker den Regionalplanentwurf kritisch sehen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaziele-krachend-verfehlt-warum-gruenen-politiker-den-regionalplanentwurf-kritisch-sehen-_arid,11371796.html)

## 6.6.2021
* Schwäbische: [Wie die Baumbesetzer-Szene sich in Süddeutschland ausbreitet](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-wie-die-baumbesetzer-szene-sich-in-sueddeutschland-ausbreitet-_arid,11371578.html)
* Schwäbische: [Baumbesetzer wollen in den Weingartenwald zurückkehren](https://www.schwaebische.de/landkreis/bodenseekreis/meersburg_artikel,-baumbesetzer-wollen-in-den-weingartenwald-zurueckkehren-_arid,11369995.html)
* Schwäbische: [Radfahrer demonstrieren auf der Bundesstraße B30 für mehr Klimaschutz](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-radfahrer-demonstrieren-auf-der-bundesstrasse-b30-fuer-mehr-klimaschutz-_arid,11371610.html)

## 5.6.2021
* Schwäbische: [Für Ravensburgs Oberbürgermeister ist klar: Die Zeit der Einfamilienhäuser ist im Prinzip vorbei](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-ob-rapp-plaediert-fuer-entschleunigtes-wachstum-_arid,11370950.html)

## 4.6.2021
* Schwäbische: [Für Fahrraddemo muss B 30 bei Ravensburg gesperrt werden](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-fuer-fahrraddemo-muss-b30-bei-ravensburg-gesperrt-werden-_arid,11370952.html)

## 3.6.2021
* Schwäbische: [Baumbesetzung bei Hagnau: Wer sich freut, wer sich ärgert - und wem jetzt vielleicht Konsequenzen drohen](https://www.schwaebische.de/landkreis/bodenseekreis/meersburg_artikel,-so-sehen-akteure-aus-der-region-die-baumbesetzung-im-weingartenwald-_arid,11369992.html) ([Foto](/pressespiegel/schwaebische-11369992.jpeg))

## 2.6.2021
* Ravensburger Spectrum: [Salem: Jetzt nicht für Affen(berg), sondern auch für aktive Homo Sapiens](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3120272/salem-jetzt-nicht-fur-affen-sondern-auch-fur-aktive-homo-sapiens)
* Südkurier: [Klimaaktivisten protestieren jetzt auch in Salem gegen den Regionalplan](https://www.suedkurier.de/region/bodenseekreis/salem/klimaaktivisten-protestieren-jetzt-auch-in-salem-gegen-den-regionalplan;art372491,10823114) ([Foto](/pressespiegel/suedkurier-372491,10823114.jpeg))
* Schwäbische: [Klimaaktivisten weiten ihren Protest nach Salem aus](https://www.schwaebische.de/landkreis/bodenseekreis/salem_artikel,-klimaaktivisten-weiten-ihren-protest-nach-salem-aus-_arid,11370196.html) ([Foto](/schwaebische-11370196.jpeg))

## 1.6.2021
* Ravensburger Spectrum: [Fronleichnam (3. Juni 2021) im "Altdorfer Wald" . . .](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3115555/fronleichnam-3-juni-2021-im-altdorfer-wald---)
* Ravensburger Spectrum: [BILD DES TAGES: Modellprojekt - vom Staat gefördert, oder: Nach uns die Sintflut ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3114996/bild-des-tages-modellprojekt---vom-staat-gefordert-oder-nach-uns-die-sintfl)

## 31.5.2021
* Ravensburger Spectrum: [An MdB Olaf Scholz: Bundestagskandidatur von Heike Engelhardt - Mit einem RAF-Vergleich in den Bundestag ?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3111226/)
* Ravensburger Spectrum: [KLIMASTREIT: Jenseits von Eden - oder: Von den "Guten" und den "Bösen"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3110564/klimastreit-jenseits-von-eden---oder-von-den-guten-und-den-bosen)
* Blix Mai/Juni 2021: Auf die Bäume! ([Foto](/pressespiegel/blix-2021-0506.jpeg))

## 29.5.2021
* Schwäbische: [Online-Veranstaltungen zum Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-online-veranstaltungen-zum-altdorfer-wald-_arid,11368766.html)

## 28.5.2021
* Ravensburger Spectrum: [Klimastreit: Ex-Ante Verurteilung durch die Institutionen aufgrund erschreckender juristischer Defizite und Leugnung von Fakten](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3099083/klimastreit-ex-ante-verurteilung-durch-die-institutionen-aufgrund-erschreck)
* Ravensburger Spectrum: [Entschärfend aktualisiert: KLIMASTREIT // Missbrauch der Machtstellung - Verdacht auf "Üble Nachrede" - § 186 StGB](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3096521/klimastreit-missbrauch-der-machtstellung-und-praktizierte--militante-demokr)
* Südkurier: [Wald haben die Klimaaktivisten nicht gerettet, stattdessen ein Lebensgefühl gepflegt](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/wald-haben-die-klimaaktivisten-nicht-gerettet-stattdessen-ein-lebensgefuehl-gepflegt;art410936,10819386) ([Foto](/pressespiegel/suedkurier-410936,10819386.jpeg))
* Schwäbische: [Ravensburger Polizei wehrt sich gegen neue Vorwürfe der Klimaaktivisten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-polizei-wehrt-sich-gegen-vorwuerfe-der-klimaaktivisten-_arid,11368658.html) ([Foto](/pressespiegel/schwaebische-11368658.jpeg))

## 26.5.2021
* SWR Aktuell: [Streit um Kiesabbau im Altdorfer Wald soll sachlicher werden](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/online-diskussion-um-altdorfer-wald-100.html)
* Südkurier: [Klimaaktivisten geben Camp im Weingartenwald auf. Doch der Protest gegen die Planung der B 31-neu soll weitergehen](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/klimaaktivisten-geben-camp-im-weingartenwald-auf;art410936,10817211) ([Foto](/pressespiegel/suedkurier-410936,10817211.jpeg))
* Ravensburger Spectrum: [KLIMA: Des einen "lässliche Sünde", ist des anderen große Wohltat - Über das gespaltene Denken und Handeln der Ordnungsbehörden](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3090679/klima-des-einen-lassliche-sunde-ist-des-anderen-grose-wohltat---uber-das-ge)
* Ravensburger Spectrum: [KLIMASTREIT wird zum KLIMAKRIEG: Sich den Fakten und nicht den verbalen Verlautbarungen und blendenden Medaillen verpflichtet wissen.](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3089950/klimastreit-oder-klimakrieg-sich-den-fakten-und-nicht-den-verbalen-verlautb)
* Ravensburger Spectrum: [KLIMA: Während Wolfram Frommlet de-eskaliert, rüstet die Polizei zum verbalen Krieg !!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3084074/klima-wahrend-wolfram-frommlet-de-eskaliert-rustet-die-polizei-zum-verbalen)
* Schwäbische: [Protest wegen B31-neu schnell vorbei - Baumbesetzer räumen ihr Lager](https://www.schwaebische.de/landkreis/bodenseekreis/meersburg_artikel,-baumbesetzer-raeumen-ihr-lager-im-weingartenwald-_arid,11367926.html) ([Foto](/pressespiegel/schwaebische-11367926.jpeg))
* Schwäbische: [Besetzung wegen B31-neu: Klimaaktivisten bauen ihr Lager wieder ab](https://www.schwaebische.de/landkreis/bodenseekreis/meersburg_artikel,-klimaaktivisten-bauen-ihr-lager-wieder-ab-_arid,11367615.html) ([Foto](/pressespiegel/schwaebische-11367615.jpeg))
* Schwäbische: [Ravensburger Klimaaktivisten reagieren auf Kritik an Protestaktion](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-klimaaktivisten-verteidigen-zivilen-ungehorsam-_arid,11367677.html) ([Foto](/pressespiegel/schwaebische-11367677.jpeg))

## 25.5.2021
* Ravensburger Spectrum: [KLIMASTREIT: Vom "Phantom der Spaltung" und dem wirklichen, über den Aktivi schwebende "Schwert der Macht" ! --- KlimAAktivist/innen reagieren auf offenen Brief des Polizeipräsidenten ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3077054/klimastreit-vom-phantom-der-spaltung-und-dem-wirklichen-uber-uns-schwebende)
* Ravensburger Spectrum: [Klimastreit: Weingartenwald bei Salem am See besetzt - "Bevor die Bundesregierung unseren Wald roden kann, muss sie uns erst räumen."](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3079571/klimastreit-weingartenwald-bei-salem-am-see-besetzt---bevor-die-bundesregie)
* Südkurier: [Klimaaktivisten besetzen seit Montag den Weingartenwald im Bodenseekreis](https://www.suedkurier.de/region/bodenseekreis/meersburg/klimaaktivisten-besetzen-seit-montag-den-weingartenwald-im-bodenseekreis;art372493,10816791) ([Foto](/pressespiegel/suedkurier-372493,10816791.jpeg))
* Südkurier: [Geht es hier noch ums Klima? In Ravensburg wird Kritik an der von Baumbesetzern gewählten Form des Protests laut](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/geht-es-hier-noch-ums-klima-in-ravensburg-wird-kritik-an-der-von-baumbesetzern-gewaehlten-form-des-protests-laut;art410936,10815331) ([Foto](/pressespiegel/suedkurier-410936,10815331.jpeg))
* Schwäbische: [Nach Altdorfer Wald und Ravensburg - Nun besetzen Klimaaktivisten Bäume an der B31-neu](https://www.schwaebische.de/landkreis/bodenseekreis/meersburg_artikel,-klimaaktivisten-wollen-bau-der-b-31-neu-verhindern-_arid,11367551.html) ([Foto](/pressespiegel/schwaebische-11367551.jpeg))
* Schwäbische: [Klimaaktivisten besetzen Weingartenwald am Bodensee](https://www.schwaebische.de/sueden/baden-wuerttemberg_video,-klimaaktivisten-besetzen-weingartenwald-am-bodensee-_vidid,159580.html)

## 24.5.2021
* Ravensburger Spectrum: [KLIMASTREIT: Ravensburger Bürger fordert Heike Engelhardt auf, sich für ihren RAF-Vergleich zu entschuldigen. - Antwort = vielsagend nichtssagend ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3074494/klimastreit-ravensburger-burger-fordert-heike-engelhardt-sich-fur-ihren-raf)
* Ravensburger Spectrum: [Ravensburg: Die erschreckende Wahrheit über "unsere" Presse und unsere Gesellschaft - Robin Halles hoffentlich letzter öffentlicher Kommentar im "Südfinder"!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3073520/ravensburg-die-erschreckende-wahrheit-uber-unsere-presse-und-unsere-gesells)
* Schwäbische: [Ravensburger Polizeipräsident äußert sich zu umstrittenem Protest der Klimaaktivisten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-polizeipraesident-aeussert-sich-zu-umstrittenem-protest-der-klima-aktivisten-_arid,11367115.html) ([Foto](/pressespiegel/schwaebische-11367115.jpeg))

## 23.5.2021
* Ravensburger Spectrum: [Pfingsten: Wenn der Geist Gottes nicht zwischen kalten Kirchenmauern, sondern im "Altdorfer Wald" weht . . .](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3066053/pfingsten-wenn-der-geist-gottes-nicht-zwischen-kalten-kirchenmauern-sondern)
* Schwäbische: [Regionalplanerin plädiert für mehr Verzicht](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-regionalplanerin-plaediert-fuer-mehr-verzicht-_arid,11366235.html) ([Foto](/pressespiegel/schwaebische-11366235.jpeg) (Vorsicht: Hier wird wie üblich versucht, Verantwortung auf Einzelpersonen abzuschieben. Unser Politik adressiert stets die Politik. Diese muss Rahmenbedingungen schaffen, sodass es allen Bürger\*innen zeitlich und finanziell möglich ist, sich klimafreundlich zu verhalten.)

## 22.5.2021
* SWR Aktuell: [Sendung 19:30 Uhr vom 22.5.2021](https://www.ardmediathek.de/video/swr-aktuell-baden-wuerttemberg/sendung-19-30-uhr-vom-22-5-2021/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzE0NjcyNzY/) (Zeitstempel 20:47)
* Ravensburger Spectrum: ["KLIMASCHUTZ IST KEIN VERBRECHEN" - Protest-Aktion eines Klimavaters auf dem Ravensburger Marienplatz](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3063862/klimaschutz-ist-kein-verbrechen---ist-auch-diese--aktion-eine-straftat)
* Ravensburger Spectrum: [RAVENSBURGER KLIMASTREIT: Vom friedlichen politischen Kritiker zum militanten politischen Gefangenen - Eine weitere Analyse der aktuellen Ereignisse](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3062490/)
* Bild: [Hier hängt ein Professor ab](https://www.bild.de/bild-plus/regional/stuttgart/stuttgart-aktuell/wolfgang-ertel-62-kaempft-fuers-klima-hier-haengt-ein-professor-ab-76477892.bild.html)
* Schwäbische: [Karikatur von Rainer Weishaupt](/pressespiegel/2021-05-22-schwaebische.jpeg): Noch hatten die Restaurants in dieser Woche geschlossen. Wer da lecker auswärts essen wollte, der musste sich schon etwas ganz Besonderes einfallen lassen. Geheimtipp: Im Polizeirevier Friedrichshafen serviert Sternekoch Yve Sturmär außergewöhnlich feine Speisen. Und die Küche erfüllt dabei auch alle Sonderwünsche.

## 20.5.2021
* Deutschlandfunk Dlf-Magazin: [Auf die Bäume - Streit um Kiesabbau in Oberschwaben](https://srv.deutschlandradio.de/dlf-audiothek-audio-teilen.3265.de.html?mdm:audio_id=927181)
* Deutschlandfunk Länderreport: [Es knirscht im Getriebe - Streit um Kiesabbau in Oberschwaben](https://srv.deutschlandradio.de/dlf-audiothek-audio-teilen.3265.de.html?mdm:audio_id=927054)
* SWR Aktuell: [Wissenschaftsministerin zeigt sich beeindruckt von Baumbesetzung an Hochschule](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/baumbesetzung-an-hochschule-ravensburg-weingarten-100.html)
* Ravensburger Spectrum: [KLIMASTREIT: Stefan Weinert schreibt an die beiden Bundestagsabgeordneten Martin Gerster und Martin Schulz folgenden Brief - "Diese Entgleisung ist in der Nachkriegs-BRD nicht tragbar."](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3053331/klimakrise-stefan-weinert-schreibt-an-die-beiden-bundestagsabgeordneten-mar)
* Ravensburger Spectrum: [Klimastreit: OFFENER BRIEF AN DEN CHEFREDAKTEUR DER SCHWÄBISCHEN ZEITUNG und die GESCHÄFTSFÜHRUNG "SCHWÄBISCH MEDIA": - "Wir leben nicht im Jahre 1969 und schon gar nicht im Jahre 1939 und auch nicht 1919. Gott sei es gedankt!"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3051769/klimastreit-offener-brief-an-den-chefredakteur-der-schwabischen-zeitung-und)
* Ravensburger Spectrum: [Klimastreit: Stefan Weinert schreibt Leserbrief für "Unser Ländle4Future", der an die Fraktionsvorsitzende der SPD im Ravensburger Gemeinderat gerichtet ist: "Ein Stich in das Herz der Eltern der jungen Aktivi!"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3052085/)
* Ravensburger Spectrum: [KLIMASTREIT: Stefan Weinert schreibt an die beiden Bundestagsabgeordneten Martin Gerster und Martin Schulz folgenden Brief - "Diese Entgleisung ist in der Nachkriegs-BRD nicht tragbar."](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3053331/klimakrise-stefan-weinert-schreibt-an-die-beiden-bundestagsabgeordneten-mar)
* Ravensburger Spectrum: [Der Ravensburger Autor und Journalist, Wolfram Frommlet, schreibt einen Newsletter zu Heike Engelhardts (SPD) Vergleich: " 'Friedliche Klimaschützer/innen' und 'Mörder der RAF' ".](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3053875/klimastreit-der-ravensburger-autor-und-journalist-wolfram-frommlet-schreibt)
* Schwäbische: [Baumbesetzer: SPD-Politikerin kritisiert „zunehmende Radikalisierung der Szene“](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-baumbesetzer-spd-politikerin-kritisiert-zunehmende-radikalisierung-der-szene-_arid,11365673.html) ([Foto](/pressespiegel/schwaebische-11365673.jpeg))

## 19.5.2021
* Ravensburger Spectrum: [Klimastreit: WER PARKHÄUSER BAUT . . . Ein weiterer Widerspruch auf dem Weg zu CO-Null !!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3046333/klimastreit-wer-parkhauser-baut----ein-weiterer-widerspruch-auf-dem-weg-zu-)
* Ravensburger Spectrum: [KLIMASTREIT: 10.000 Bäume sollen im Süden von München "gemessert" werden --- Aktivi: "Nicht mit uns!"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3046939/)
* Ravensburger Spectrum: [OFFENER BRIEF AN DEN OBERBÜRGERMEISTER DER STADT MÜNCHEN, HERRN DIETER REITER - - - Keine Teilrodung des "Forst Kasten" im Südwesten von München](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3047144/)
* Ravensburger Spectrum: [KLIMASTREIT: Auch Bürger Albert Hagn schreibt Brief an den CDU-Vorsitzenden Christoph Sitta: "Es ist erschütternd, dass Sie die Klimaaktivist/innen als Spaßvögel betrachten."](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3048141/auch-burger-albert-hagn-schreibt-brief-an-den-cdu-vorsitzenden-christoph-si)
* Ravensburger Spectrum: [Klimastreit: PARENTS FOR FUTURE SCHREIBEN EINEN OFFENEN BRIEF ZU DEN unguten und entgleisenden Ereignissen RUND UM DIE AKTIONEN DER AKTIVI AM 14. und 15. Mai 2021 --- Professor Ertel erhält persönliche Antwort von der Wissenschaftsministerin BaWü](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3049197/parents-for-future-schreiben-einen-offenen-brief-zu-den-geschehnissen-des-w)
* Wochenblatt-Online: [Nach Räumung von Aktivisten: Stimmung ist aufgeheizt](https://www.wochenblatt-news.de/nach-raeumung-von-aktivisten-stimmung-ist-aufgeheizt/)
* Südfinder: Die Jugendlichen brauchen Grenzen
* Schwäbische: [Polizei lässt Banner der Klimaaktivisten abhängen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/bad-waldsee_artikel,-polizei-laesst-banner-der-klimaaktivisten-abhaengen-_arid,11364909.html) ([Foto](/pressespiegel/schwaebische-11365673.jpeg))
* Schwäbische: [Streit um neuen Regionalplan: Ab wann gefährdet Wachstum die eigenen Lebensgrundlagen?](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-klimahoelle-oder-notwendige-entwicklung-streit-um-den-regionalplan-fuer-bodensee-oberschwaben-_arid,11364587.html) ([Foto](/pressespiegel/schwaebische-11364587.jpeg))

## 18.5.2021
* Ravensburger Spectrum: [KLIMA-ABC: "KLIMA & WALD" (Teil 2: WASSER) für Ravensburg, Passau und die ganze gebeugte Welt](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3041931/)
* Ravensburger Spectrum: [Klimaaktivist/innen und die "Grünen" in 88212 Ravensburg OFFENER BRIEF AN DIE KANZLERKANDIDATIN DER PARTEI "DIE GRÜNEN" - FRAU ANNALENA BAERBOCK, MdB](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3042350/)
* Ravensburger Spectrum: [KLIMASTREIT - - - OFFENER BRIEF AN DEN VORSITZENDEN DER CDU RAVENSBURG - HERRN CHRISTOPH SITTA "Sie pervertieren die Geschehnisse und treten doch wie ein ge-rechter Richter auf."](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3043707/klimastreit------)
* Ravensburger Spectrum: [KLIMASTREIT & RECHTSBRUCH: Amtsträger sollten in Zukunft für ihr Versagen in Handschellen abgeführt werden. - Sam in einer Linie mit Hannah Arendt und Jürgen Habermas . . .](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3044201/)
* Schwäbische: [Jetzt ermittelt die Ravensburger Staatsanwaltschaft gegen die Klimaaktivisten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-jetzt-ermittelt-die-ravensburger-staatsanwaltschaft-gegen-die-klimaaktivisten-_arid,11364588.html) ([Foto](/pressespiegel/schwaebische-11364588.jpeg))
* Schwäbische: [Die Ravensburger CDU kritisiert: Den Klimaaktivisten geht es um Provokation](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-die-ravensburger-cdu-kritisiert-den-klimaaktivisten-geht-es-um-provokation-_arid,11364794.html)

## 17.5.2021
* Ravensburger Spectrum: [Umwelt, die bald keine mehr ist: "KLIMALISTE BADEN-WÜRTTEMBERG" meldet sich zu Wort . . .](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3034253/umwelt-die-bald-keine-mehr-ist-klimaliste-baden-wurttemberg-meldet-sich-zu-)
* Ravensburger Spectrum: [Freiheitslied der Ravensburger Klimaschützer/innen](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3034780/freiheitslied-der-ravensburger-klimaschutzerinnen)
* Ravensburger Spectrum: [KlimAAktivist/innen in "Häfler" Polizeigewahrsam: WIE ES WIRKLICH WAR (die halb nackte Wahrheit) ... Betroffener berichtet - Mit neuen exklusiven Fotos vom KlimASamstag](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3035988/klimaaktivistinnen-in-polizeigewahrsam-wie-es-de-facto-wirklich-war-)
* Wochenblatt-Online: [Stadt Ravensburg zeigt kein Verständnis für Klima-Protestaktion](https://www.wochenblatt-news.de/stadt-ravensburg-zeigt-kein-verstaendnis-fuer-klima-protestaktion/)
* Schwäbische: [Klimaaktivisten beklagen Behandlung in Zelle - Polizei bezieht Stellung](https://www.schwaebische.de/landkreis/bodenseekreis/friedrichshafen_artikel,-klimaaktivisten-berichten-von-ihrer-zeit-in-der-zelle-_arid,11364460.html) ([Foto](/pressespiegel/schwaebische-11364460.jpeg))
* Schwäbische: [Klimaaktivisten erweisen dem Klima einen Bärendienst](https://www.schwaebische.de/landkreis/bodenseekreis/friedrichshafen_artikel,-klimaaktivisten-erweisen-dem-klima-einen-baerendienst-_arid,11364461.html) ([Foto](/pressespiegel/schwaebische-11364461.jpeg))
* Schwäbische: [Ravensburger Klimaaktivisten um 20 Uhr aus Polizeigewahrsam entlassen](https://www.schwaebische.de/landkreis/bodenseekreis/friedrichshafen_artikel,-baumbesetzer-werden-in-fn-festgehalten-_arid,11364063.html) ([Foto](/pressespiegel/schwaebische-11364063.jpeg))
* Stadt Ravensburg: [Stadt Ravensburg nimmt Stellung zu den Klimaschutz-Protestaktionen am 15. Mai 2021](https://www.ravensburg.de/rv/aktuelles/2021/stellungnahme-klimaschutzaktivisten.php)

## 16.5.2021
* Ravensburger Spectrum: [KLIMASTREIT: Während sich die Ravensburger Stadtverwaltung mit Polizei, Feuerwehr, THW und STASI-Methoden munitioniert, glänzen die jungen KlimAAktivist/innen mit juristischer Fachkenntnis!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3029420/klimastreit-wahrend-sich-die-ravensburger-stadtverwaltung-mit-polizei-feuer)
* Ravensburger Spectrum: [Ravensburger "KlimaRat" im Licht der KlimAAktivist/innen](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3029919/ravensburger-klimarat-im-licht-der-klimaaktivistinnen)
* Ravensburger Spectrum: [Zeitzeuge berichtet spannend und erhellend über die Klimacamp-Räumung am 15. Mai 2021](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3030242/)
* Ravensburger Spectrum: ["Die AKTIVI sind frei, wer konnt' es verhindern?!" - Sam und Freunde wurden ab 20 Uhr heute Abend entlassen](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3032106/die-aktivi-sind-frei-wer-konnt-es-verbieten---sam-und-freunde-wurden-ab-20-)
* Aktionsbündnis Zukunftsfähiger Regionalplan: [Eine verstörende Entdeckung](https://regionbodenseeoberschwaben.blogspot.com/2021/05/eine-verstorende-entdeckung.html)
* Schwäbische: [Illegale Baumbesetzung: Der harte Kern der Demonstranten muss in Gewahrsam](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-hoehenspezialisten-der-polizei-beenden-illegale-baumbesetzung-_arid,11363963.html)
* Wochenblatt-News: [Klimaaktivisten durch SEK geräumt](https://www.wochenblatt-news.de/klimaaktivisten-durch-sek-geraeumt/)
* Wochenblatt-News: [Klimaaktivisten in Ravensburg durch SEK geräumt](https://www.youtube.com/watch?v=eBf_rzJDGv0)

## 15.5.2021
* Ravensburger Spectrum: [KLIMASTREIT +++EIL+++ // Traversen und Hängematten über der Ravensburger Schussenstrasse](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3024395/klimastreit-eil--traversen-und-hangematten-uber-der-ravensburger-schussenst)
* Ravensburger Spectrum: [KLIMASTREIT: Vogelgezwitscher, frische Luft und Umweltgesänge // Schussenstraße wird für ein paar Stunden zum Paradies . . .](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3026111/)
* Ravensburger Spectrum: [KlimAAktivist/innen: Der große Unterschied zwischen "Schwäbisch" und "Spectrum" --- Polizei- und Amtsblatt oder Zeitung?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3026860/klimastreit-berichterstattung-der-grose-unterschied-zwischen-schwabisch-und)
* Ravensburger Spectrum: [KlimAAktivisten bis Sonntag 20 Uhr in Friedrichshafener Polizeigewahrsam](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3027645/alarm-und-sos-klimaaktivisten-3-in-friedrichshafen-in-polizeigewahrsam---al)
* Wochenblatt-News: [Klimaaktivisten besetzen Schussenstraße in Ravensburg](https://www.wochenblatt-news.de/klimaaktivisten-besetzen-schussenstrasse-in-ravensburg/)
* Schwäbische: [Klimaprotest mit Stahlseil: SEK holt Aktivisten in Schussenstraße wieder auf den Boden](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaaktivisten-spannen-seil-ueber-schussenstrasse-offenbar-besetzung-geplant-_arid,11363843.html) ([Foto](/pressespiegel/schwaebische-11363843.jpeg))
* Schwäbische: [SEK-Spezialkräfte holen Klimaaktivisten in Ravensburg aus Bäumen und von einem Stahlseil herunter](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_video,-sek-spezialkraefteholen-klimaaktivisten-in-ravensburg-aus-baeumen-und-von-einem-stahlseil-herunter-_vidid,159462.html)
* Stuttgarter Zeitung: [Spezialisten der Polizei beenden illegale Baumbesetzung](https://www.stuttgarter-zeitung.de/inhalt.klimaaktivisten-in-ravensburg-spezialisten-der-polizei-beenden-illegale-baumbesetzung.4293da32-1e50-45c8-9b80-1ba8beef7dcc.html)
* Badische Zeitung: [Polizei-Höhenspezialisten beenden illegale Baumbesetzung](https://www.badische-zeitung.de/polizei-hoehenspezialisten-beenden-illegale-baumbesetzung)
* all-in.de: [Auf Hängematten über der Straße: Polizei räumt illegale Baumbesetzung in Ravensburg](https://www.all-in.de/c-polizei/auf-haengematten-ueber-der-strasse-polizei-raeumt-illegale-baumbesetzung-in-ravensburg_a5115381)

## 14.5.2021
* AGORA-LA: [Hofberichterstattung zum Regionalplan](https://agora-la.org/2021/05/14/hofberichterstattung-zum-regionalplan/)
* Ravensburger Spectrum: [KLIMASTREIT: Machtkampf mit Wunden, oder "Don Sam's Sieg gegen die Gebetsmühlen"?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3018490/klimastreit-machtkampf-mit-wunden-oder-don-sams-sieg-gegen-die-gebetsmuhlen)
* Ravensburger Spectrum: [KLIMA-ABC: "KLIMA & WALD" (Teil 1: Strahlung und Temperatur) für Ravensburg, Passau und die ganze gebeugte Welt . . .](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3020588/klima-abc-klima--wald-1-fur-ravensburg-passau-und-die-ganze-gebeugte-welt--)
* Ravensburger Spectrum: [KLIMASTREIT: Werden die Handys der Klimaktivist/innen und derer, die mit ihnen kommunizieren, abgehört?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/3021157/klimastreit-werden-die-handys-der-klimaktivistinnen-und-derer-die-mit-ihnen)
* Schwäbische: [Klimaaktivisten besetzen erneut Baum in Ravensburg](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaaktivisten-besetzen-erneut-baum-in-ravensburg-_arid,11363667.html) ([Foto](/pressespiegel/schwaebische-11363667.jpeg))

## 13.5.2021
* Schwäbische: [Stadt Ravensburg befürchtet erneut Baumbesetzungen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-ravensburg-befuerchtet-erneut-baumbesetzungen-_arid,11363271.html) ([Foto](/pressespiegel/schwaebische-11363271.jpeg))
* Schwäbische: [Warum Landwirte den neuen Regionalplan nicht fürchten müssten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-warum-landwirte-den-neuen-regionalplan-nicht-fuerchten-muessten-_arid,11362514.html) ([Foto](/pressespiegel/schwaebische-11362514.jpeg))

## 12.5.2021
* Südkurier: [Weil ihm zu viel geheizt wird: Ein Professor protestiert in Weingarten mit Klima-Aktivisten in luftiger Höhe gegen die eigene Hochschule](https://www.suedkurier.de/baden-wuerttemberg/weil-ihm-zu-viel-geheizt-wird-ein-professor-protestiert-in-weingarten-mit-klima-aktivisten-in-luftiger-hoehe-gegen-die-eigene-hochschule;art417930,10807226)

## 11.5.2021
* Schwäbische: [Professor protestiert mit Baumbesetzern an Hochschule](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-professor-protestiert-mit-baumbesetzern-an-hochschule-_arid,11362406.html) ([Foto](/pressespiegel/schwaebische-11362406.jpeg))
* Schwäbische: [Polizei verhindert neues Baumhaus auf dem Hochschulcampus in Weingarten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-polizei-verhindert-neues-baumhaus-auf-dem-hochschulcampus-in-weingarten-_arid,11362338.html) ([Foto](/pressespiegel/schwaebische-11362338.jpeg))
* Süddeutsche: [Professor protestiert mit Baumbesetzern an Hochschule](https://www.sueddeutsche.de/politik/demonstrationen-weingarten-professor-protestiert-mit-baumbesetzern-an-hochschule-dpa.urn-newsml-dpa-com-20090101-210511-99-553162)
* Zeit: [Professor protestiert mit Baumbesetzern an Hochschule](https://www.zeit.de/news/2021-05/11/professor-protestiert-mit-baumbesetzern-an-hochschule)
* Badisches Tagblatt: [Professor protestiert mit Baumbesetzern an Hochschule](https://www.badisches-tagblatt.de/Nachrichten/Professor-protestiert-mit-Baumbesetzern-an-Hochschule-87474.html)
* Wochenblatt-News: [Aktivisten wollten Baumhaus an Hochschule errichten](https://www.wochenblatt-news.de/aktivisten-wollten-baumhaus-an-hochschule-errichten/)
* FAZ: [Wenn der Professor im Baum hängt](https://www.faz.net/aktuell/karriere-hochschule/hoersaal/aktion-an-baum-ravensburger-professor-protestiert-mit-klimaaktivisten-17338101.html)
* SWR Aktuell: [Klimaschützer kritisieren Hochschule Ravensburg-Weingarten](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/klimaaktivisten-kritisieren-energieverbrauch-hochschule-weingarten-102.html)
* Bodenseeradio RV SWR4

## 10.5.2021
* Regio TV: [Besetzung im Altdorfer Wald](https://www.regio-tv.de/mediathek/179250)
* Neues Deutschland: [Waldmenschen](https://www.neues-deutschland.de/artikel/1151841.umweltschutz-waldmenschen.html)
* Ravensburger Spectrum: [DER "WALD" IM SPANNUNGSFELD GESELLSCHAFTLICHER ANSPRÜCHE -Leonardo Fibonacci bei der "Altdorfer-Wald-Szene"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2999297/der-wald-im-spannungsfeld-gesellschaftlicher-anspruche--leonardo-fibonacci-)
* AGORA-LA: [Muttertag der anderen Art](https://agora-la.org/2021/05/10/muttertag-der-anderen-art/)

## 9.5.2021
* Ravensburger Spectrum: [KLIMARAT für RAVENSBURG: Offener Protestbrief an den Oberbürgermeister Dr. Rapp](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2995760/)
* Ravensburger Spectrum: [KLIMASTREIT: Liebe zu "Mutter Erde", oder "Vaterlandsliebe"?](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2995475/klimastreit-dank-an-mutter-erde-oder-lieb-vaterland)
* Ravensburger Spectrum: [KLIMASTREIT: VERSAGEN DER POLITIK ZWINGT ZUM "kategorischen Klima-Imperativ" - - -Auch die Passauer Baumbesetzer/innen bekommen ihre Professor/innen ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2993042/klimastreit-versagen-der-politik-zwingt-zum-kategorischen-klima-imperativ--)
* SatireSenf: [TS56/21: „Marketing“-Offensive des Regionalverbands mithilfe der SchwäZ: Franke heißt jetzt Kießling](https://satiresenf.de/ts56-21-marketing-offensive-des-regionalverbands-mithilfe-der-schwaez-franke-heisst-jetzt-kiessling/)

## 8.5.2021
* Schwäbische: [Was hinter dem Regionalplan für Oberschwaben steckt und wo er Einfluss nimmt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-was-der-regionalplan-fuer-oberschwaben-alles-steuert-_arid,11360742.html) ([Foto](/pressespiegel/schwaebische-11360742.jpeg))
* Ravensburger Spectrum: [Die verhängnisvolle Umweltformel: Windkraft = Windrad = viel Zement = noch mehr Kies = Zementwerke sind Giftschleudern === Große Umweltsünde](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2987745/die-verhangnisvolle-umweltformel-windkraft--windrad--viel-zement--noch-mehr)

## 6.5.2021
* Schwäbische: [Soziologe: „Die Demonstrationen zeigen, dass man mit dieser Gesellschaft nicht zufrieden ist“](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-soziologe-diese-kiesgrube-ist-ein-brennglas-fuer-unsere-gesellschaft-_arid,11360091.html) ([Foto](/pressespiegel/schwaebische-11360091.jpeg))

## 5.5.2021
* Kontextwochenzeitung: [Zoff um Kies unter Bäumen](https://www.kontextwochenzeitung.de/schaubuehne/527/zoff-um-kies-unter-baeumen-7472.html)
* Südfinder: Klimaaktivisten: Zoff mit der Polizei +++ Schlimme Vorwürfe ([Foto](/pressespiegel/suedfinder-2021-05-05.jpeg))
* Ravensburger Spectrum: [Ravensburg hat sich den UMWELTSCHUTZ auf die Fahne geschrieben - Politiker beim Wort nehmen ist keine Satire!!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2967341/ravensburg-hat-sich-den-umweltschutz-auf-die-fahne-geschrieben---politiker-)
* Ravensburger Spectrum: [KLIMASTREIT im Südwesten und Südosten - oder: "Deutschland macht Sinn -- Chile ist hin!"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2967107/klimastreit-im-sudwesten-und-sudosten---oder-deutschland-macht-sinn--chile-)
* AGORA-LA: [Klimaprotest im Altdorfer Wald](https://agora-la.org/2021/05/05/klimaprotest-im-altdorfer-wald/)
* Schwäbische: [Argumente statt aggressive Effekthascherei](https://www.schwaebische.de/landkreis/landkreis-ravensburg/bad-waldsee_artikel,-argumente-statt-aggressive-effekthascherei-_arid,11359676.html) ([Foto](/pressespiegel/schwaebische-11359676.jpeg))

## 4.5.2021
* Schwäbische: [Polizei über Klimaprotest: „Das ist eine neue Protestform, die wir so noch nicht hatten“](https://www.schwaebische.de/landkreis/landkreis-ravensburg/bad-waldsee_artikel,-polizeisprecher-ueber-klimaprotest-das-ist-eine-neue-protestform-die-wir-hier-so-noch-nicht-hatte-_arid,11359675.html) ([Foto](/pressespiegel/schwaebische-11359675.jpeg))

## 3.5.2021
* SatireSenf: [HInfo54: Satire-Workshop: Hier die Regionalplan-Satire ohne Pfusch und Staatsschutz](https://satiresenf.de/hinfo54-satire-workshop-hier-die-regionalplan-satire-ohne-pfusch-und-staatsschutz/)
* BLIX: [Nach verstrichenem Ultimatum: Klimacamper\*innen beginnen Klimaaktionswochen mit Banner am Ravensburger Rathaus](https://www.diebildschirmzeitung.de/diebildschirmzeitung/diebildschirmzeitung-le/4798-nach-verstrichenem-ultimatum-klimacamper-innen-beginnen-klimaaktionswochen-mit-banner-am-ravensburger-rathaus)
* Schwäbische: [Nach Kieswerk-Blockade: Klima-Aktivisten werfen Polizei „gefährliche Körperverletzung“ vor](https://www.schwaebische.de/landkreis/landkreis-ravensburg/bad-waldsee_artikel,-klimaaktivisten-werfen-polizei-gefaehrliche-koerperverletzung-vor-_arid,11359403.html) ([Foto](/pressespiegel/schwaebische-11359403.jpeg))
* Südkurier: [Klima-Aktivisten im Altdorfer Wald werfen der Polizei vor, „mutwillig Menschenleben zu gefährden“ – die Beamten schildern die Geschichte ganz anders](https://www.suedkurier.de/baden-wuerttemberg/klima-aktivisten-im-altdorfer-wald-werfen-der-polizei-vor-mutwillig-menschenleben-zu-gefaehrden-die-beamten-schildern-die-geschichte-ganz-anders;art417930,10799701)

## 2.5.2021
* Schwäbische: [Gegen Kiesabbau: Der Altdorfer Wald wird zum Ort des Protests](https://www.schwaebische.de/home_artikel,-gegen-kiesabbau-der-altdorfer-wald-wird-zum-ort-des-protests-_arid,11359034.html) ([Foto](/pressespiegel/schwaebische-11359034.jpeg))
* SatireSenf: [TS53/21: Brodelndes Oberschwaben: Noch viel mehr Holzpimmel, aber bitte keine Empörung mehr!](https://satiresenf.de/ts53-21-brodelndes-oberschwaben-noch-viel-mehr-holzpimmel-aber-bitte-keine-empoerung-mehr/)

## 1.5.2021
* Ravensburger Spectrum: [Klimastreit: Fiktion (Hirschgraben) und Wirklichkeit (Marienplatz)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2944077/ravensburg-fiktive-holzkreuz-aktion-im-hirschgraben)
* Ravensburger Spectrum: [Klimastreit-Satire -- Jetzt in Ihrer Apotheke: Dr. Altdorfs Globuli gegen das Waldsterben](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2945043/klimastreit-satire-----jetzt-in-ihrer-apotheke-dr-altdorfs-globuli-gegen-da)

## 29.4.2021
* SWR Aktuell: [Klimaaktivisten blockieren Zufahrten von Kieswerken](https://www.ardmediathek.de/video/swr-aktuell-baden-wuerttemberg/sendung-17-00-uhr-vom-29-4-2021/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzE0NTQwNTM/) (Zeitstempel 1:49)
* Schwäbische: [Klima-Aktivisten versperren Zufahrt zu Kiesgruben - Polizei mit Großaufgebot im Einsatz](https://www.schwaebische.de/landkreis/landkreis-ravensburg/bad-waldsee_artikel,-klimaaktivisten-versperren-zufahrt-zu-kiesgruben-_arid,11357930.html) ([Foto](/pressespiegel/schwaebische-11357930.jpeg))
* Schwäbische: [Rund 100 Teilnehmer bei Wald-Demo in Wolfegg](https://www.schwaebische.de/landkreis/landkreis-ravensburg/wolfegg_artikel,-rund-100-teilnehmer-bei-wald-demo-in-wolfegg-_arid,11358240.html) ([Foto](/pressespiegel/schwaebische-11358240.jpeg))
* Ravensburger Spectrum: [KLIMASTREIT: In Hängematten über'm Asphalt, statt zwischen Party und Palmen - In der Ruhe liegt die Kraft! Fünf (5) Aktivist/innen hinter Gitter!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2930967/)
* Ravensburger Spectrum: [DU & ich = WIR sind "DIE STADT"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2932998/du--ich--wir-sind-die-stadt)
* Ravensburger Spectrum: [Klimastreit: BAUM bewirbt sich um einen Arbeitsplatz - hervorragende Professionen, doch ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2933939/klimastreit-baum-bewirbt-sich-um-einen-arbeitsplatz---hervorragende-profess)
* Südkurier: [Polizei nimmt Klima-Aktivisten bei Kieswerk-Blockade vorläufig fest: Wie geht es weiter mit dem Protest-Camp im Altdorfer Wald?](https://www.suedkurier.de/baden-wuerttemberg/polizei-nimmt-klima-aktivisten-bei-kieswerk-blockade-vorlaeufig-fest-wie-geht-es-weiter-mit-dem-protest-camp-im-altdorfer-wald;art417930,10797224)

## 28.4.2021
* SatireSenf: [TS52/21: Altdorfer Waldbesetzer: Korrekturen und Ergänzungen zu zwei SchwäZ-Artikeln](https://satiresenf.de/ts52-21-altdorfer-waldbesetzer-korrekturen-und-ergaenzungen-zu-zwei-schwaez-artikeln/)

## 27.4.2021
* BLIX: [Auf die Bäume!](https://www.diebildschirmzeitung.de/blix/aktuell/4721-auf-die-baeume)

## 25.4.2021
* FAZ: [CDU in Baden-Württemberg: Kein Verlass mehr auf die Mehrheit](https://www.faz.net/aktuell/politik/inland/cdu-in-baden-wuerttemberg-kein-verlass-mehr-auf-die-mehrheit-17309259.html)

## 24.4.2021
* Schwäbische: [Karikatur von Rainer Weishaupt](/pressespiegel/2021-04-24-schwaebische.jpeg): Oberschwaben in 20 Jahren: Der Regionalplan ist nach den schlimmsten Befürchtungen der Kiesabbaugegner und Klimaaktivisten Realität geworden. Die Wirtschaft brummt dank des Reichtums an Kies. Vogt hat sich zu einer Metropole wie im Ruhrgebiet entwickelt und deswegen sogar einen Autobahnanschluss bekommen.
* Südkurier: [„Ich finde das Camp klasse, obwohl es illegal ist“: Warum ein Ravensburger Professor mit einer Vorlesung im Altdorfer Wald das Image der Klimaaktivisten verbessern will](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/ich-finde-das-camp-klasse-obwohl-es-illegal-ist-warum-ein-ravensburger-professor-mit-einer-vorlesung-im-altdorfer-wald-das-image-der-klimaaktivisten-verbessern-will;art410936,10792347) ([Foto](/pressespiegel/2021-04-30-suedkurier.pdf))

## 23.4.2021
* Schwäbische: [Leben im Baumhausdorf: Klimaaktivisten kämpfen weiter für den Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-leben-im-baumhausdorf-klimaaktivisten-kaempfen-fuer-den-altdorfer-wald-_arid,11355857.html) ([Foto](/pressespiegel/schwaebische-11355857.jpeg))
* Ravensburger Spectrum: [Klimastreit: Vom "Zaubertrank" der Aldorfer Waldbesetzer/innen - Den Wald vor lauter Banner nicht mehr sehen ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2896901/klimastreit-vom-zaubertrank-der-aldorfer-waldbesetzerinnen)

## 22.4.2021
* Schwäbische: [Streit um Kies: Wie viel Gestein aus der Region geht wirklich ins Ausland?](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-streit-um-kies-wie-viel-gestein-aus-der-region-geht-wirklich-ins-ausland-_arid,11355265.html) ([Foto](/pressespiegel/schwaebische-11355265.jpeg))
* Schwäbische: [Bauparagraf 13b - der „Flächenfraß-Paragraf“](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-bauparagraf-13b-heizt-den-den-flaechenverbrauch-an-_arid,11355303.html) ([Foto](/pressespiegel/schwaebische-11355303.jpeg))

## 21.4.2021
* Ravensburger Spectrum: [KLIMASTREIT: Offener Brandbrief an den Landtagsabgeordneten der FDP, Herrn Klaus Hoher - "Ihr Gebaren gegen die auf demokratischem Fundament demonstrierenden Umweltaktivisten"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2884482/klimastreit-offener-brandbrief-an-den-landtagsabgeordneten-der-fdp-herrn-kl)
* Ravensburger Spectrum: [KLIMASTREIT: MdL Klaus Hoher antwortet dem Spectrum: "Als Rechtsstaatspartei werden wir darauf dringen, ..."](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2887223/)
* SatireSenf: [TS49/21: MdL Klaus Hoher (FDP): Mit der standschießlichen Entrechtung der Altdorfer Waldbesetzer geht aber auch gar nichts vorwärts?](https://satiresenf.de/ts49-21-mdl-klaus-hoher-fdp-mit-der-standschiesslichen-entrechtung-der-altdorfer-waldbesetzer-geht-aber-auch-gar-nichts-vorwaerts/)
* Tagblatt: [Widerstand in den Bäumen](https://www.tagblatt.de/Nachrichten/Widerstand-in-den-Baeumen-498314.html)

## 20.4.2021
* SatireSenf: [TS48/21: Regionalplan betreffs Meichle + Mohr: Meiner ist der Längste, aber wir sagen nix dazu!](https://satiresenf.de/ts48-21-regionalplan-betreffs-meichle-mohr-meiner-ist-der-laengste-aber-wir-sagen-nix-dazu/)
* SWR Aktuell: [Kritik an Waldbesetzung durch Klimaaktivisten](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/kritik-an-waldbesetzern-im-altdorfer-wald-bei-ravensburg-100.html)
* Tag24: [Klimaaktivisten besetzen Wald in Vogt: Endet die Besetzung wie im Hambacher Forst?](https://www.tag24.de/nachrichten/regionales/baden-wuerttemberg/klimaaktivisten-besetzen-wald-aus-protest-gegen-rodung-fuer-kiesabbau-1928350)
* Süddeutsche: [Klimaaktivisten besetzen Wald im Kreis Ravensburg](https://www.sueddeutsche.de/wissen/umweltpolitik-waldburg-klimaaktivisten-besetzen-wald-im-kreis-ravensburg-dpa.urn-newsml-dpa-com-20090101-210420-99-271932)
* Zeit: [Klimaaktivisten besetzen Wald im Kreis Ravensburg](https://www.zeit.de/news/2021-04/20/klimaaktivisten-besetzen-wald-im-kreis-ravensburg)
* Schwäbische: [Klimaaktivisten besetzen Wald im Kreis Ravensburg](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-klimaaktivisten-besetzen-wald-im-kreis-ravensburg-_arid,11353993.html) ([Foto](/pressespiegel/schwaebische-11353993.jpeg))
* Bild: [Klimaaktivisten besetzen Wald im Kreis Ravensburg](https://www.bild.de/regional/stuttgart/stuttgart-aktuell/im-kreis-ravensburg-klimaaktivisten-besetzen-wald-76126432.bild.html)

## 19.4.2021
* Ravensburger Spectrum: [Online-Klima aus dem Wald in alle Welt: Professor Ertel zwischen Alb und Anden](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2871595/klima-online-aus-dem-wald-in-alle-welt-professor-ertel-zwischen-alb-und-and)
* Ravensburger Spectrum: [NABU Ravensburg reagiert auf "Alarm aus dem LK Biberach"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2872689/)

## 18.4.2021
* Ravensburger Spectrum: [Altdorfer Klimastreit: Von Bannern an Baggern - und Bodenschichten ohne Bäume](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2866330/altdorfer-klimastreit-von-bannern-an-baggern-und-bodenschichten-ohne-baumen)
* Ravensburger Spectrum: [ALARM aus dem LK Biberach - Ausgeraubte Kiesgrube wird als "Paradies" verkauft - und der "Schotter" fließt weiter ... Über das wahre Gesicht der CDU und der GRÜNEN!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2867110/kiesabbau--holle-oder-paradies-)

## 17.4.2021
* Ravensburger Spectrum: [Klima Oberschwaben: CDU = Banner weg! - Klimaschützer/innen = Banner again !!](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2859886/klima-oberschwaben-cdu--banner-weg-klimaschutzerinnen--banner-again-)
* Ravensburger Spectrum: [Polizei verhindert "1.000 Kühe-Stall-Protest-Banner" - Dreifacher Wortbruch der vereidigten Bürgermeister](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2860417/ravensburg-bullen-verhindern-1000-kuhe-stall-protest-banner---dreifacher-wo)
* Tagblatt: [Der Flächenfraß-Paragraph](https://www.tagblatt.de/Nachrichten/Der-Flaechenfrass-Paragraph-497874.html)

## 16.4.2021
* Schwäbische: [Professor im Klimaprotest: Vorlesung aus dem Baumhaus](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-professor-im-klimaprotest-vorlesung-aus-dem-baumhaus-_arid,11352743.html) ([Foto](/pressespiegel/schwaebische-11352743.jpeg))
* Ravensburger Spectrum: [Altdorfer Baumbesetzung verpasst der CDU Bannbulle](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2852572/)
* Ravensburger Spectrum: [Auch Ravensburg befeuert den "1.000-Kühe-Stall" in Ostrach - Bannerkaktion der Aktivist/innen auf dem Marienplatz](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2855145/auch-ravensburg-befeuert-den-1000-kuhe-stall-in-ostrach---bannerkaktion-der)
* SatireSenf: [TS46/21: Banner der Wahrheit vor dem Haus des CDU Kreisverbands Ravensburg](https://satiresenf.de/ts46-21-banner-der-wahrheit-vor-dem-haus-des-cdu-kreisverbands-ravensburg/)
* Schwäbische: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.schwaebische.de/sueden/baden-wuerttemberg_artikel,-klimaaktivisten-bringen-banner-an-cdu-gebaeude-an-_arid,11352596.html) ([Foto](/pressespiegel/schwaebische-11352596.jpeg))
* Süddeutsche: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.stuttgarter-zeitung.de/inhalt.ravensburg-klimaaktivisten-bringen-banner-an-cdu-gebaeude-an.e776c863-69d3-4d84-94a7-eba641d97f85.html)
* Zeit: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.zeit.de/news/2021-04/16/klimaaktivisten-bringen-banner-an-cdu-gebaeude-an)
* Badisches Tagblatt: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.badisches-tagblatt.de/Nachrichten/Klimaaktivisten-bringen-Banner-an-CDU-Gebaeude-an-83790.html)
* Wochenblatt-News: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.wochenblatt-news.de/klimaaktivisten-bringen-banner-an-cdu-gebaeude-an/)
* Stuttgarter Zeitung: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.stuttgarter-zeitung.de/inhalt.ravensburg-klimaaktivisten-bringen-banner-an-cdu-gebaeude-an.e776c863-69d3-4d84-94a7-eba641d97f85.html)
* Landtag von Baden-Württemberg: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.landtag-bw.de/home/aktuelles/dpa-nachrichten/2021/April/KW15/Freitag/783037e5-077c-47dd-84c2-70048ac3.html)
* RTL: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.rtl.de/cms/klimaaktivisten-bringen-banner-an-cdu-gebaeude-an-4742301.html)
* Tag24: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.tag24.de/stuttgart/politik-wirtschaft/klimaaktivisten-bringen-banner-an-cdu-gebaeude-in-ravensburg-an-1923785)
* ZVW: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.zvw.de/baden-w%C3%BCrttemberg/klimaaktivisten-bringen-banner-an-cdu-geb%C3%A4ude-an_arid-341711)
* t-online.de: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.t-online.de/region/id_89856648/klimaaktivisten-bringen-banner-an-cdu-gebaeude-an.html)
* n-tv: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.n-tv.de/regionales/baden-wuerttemberg/Klimaaktivisten-bringen-Banner-an-CDU-Gebaeude-an-article22494488.html)
* bnn: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://bnn.de/nachrichten/baden-wuerttemberg/klimaaktivisten-bringen-banner-an-cdu-gebaeude-an)
* Welt: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.welt.de/regionales/baden-wuerttemberg/article230427671/Klimaaktivisten-bringen-Banner-an-CDU-Gebaeude-an.html)
* Neckar Chronik: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.neckar-chronik.de/Nachrichten/Klimaaktivisten-bringen-Banner-an-CDU-Gebaeude-an-497743.html)
* Schwäbisches Tagblatt: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.tagblatt.de/Nachrichten/Klimaaktivisten-bringen-Banner-an-CDU-Gebaeude-an-497743.html)
* Stimme: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://www.stimme.de/suedwesten/nachrichten/pl/klimaaktivisten-bringen-banner-an-cdu-gebaeude-an;art19070,4473561)
* Flipboard: [Klimaaktivisten bringen Banner an CDU-Gebäude an](https://flipboard.com/@rtl_de/unsere-neuesten-artikel-hmm584f6z/klimaaktivisten-bringen-banner-an-cdu-geb-ude-an/a-kRvIHCC3RsqCGYcE_AuYBw%3Aa%3A3293091934-f84b5488d4%2Frtl.de)

## 13.4.2021
* Ravensburger Spectrum: [Ravensburger Klimakampf: Wenn der Goliath den David unterschätzt ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2835768/klimakampf-wenn-der-goliath-den-david-unterschatzt-)
* Ravensburger Spectrum: [Klima-Kampf&Krieg: Altdorfer Baumbesetzer/innen werden mit Lärm, Gepöbel und Feuerwerk attackiert!](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2836418/)
* Ravensburger Spectrum: [KLIMAPROTEST: Nötigung - das Schwert der Macht gegen alles, was stört ... Wenn dem Staat und der Wirtschaft nichts mehr einfällt, dann ist es ... "Nötigung" :( !!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2838590/klimaprotest-notigung---das-schwert-der-macht-gegen-alles-was-stort--wenn-d)
* Schwäbische: [Verfolgungsjagd nach Feuerwerkskörperwurf](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-fahrzeug-bei-vogt-beworfen-polizei-ermittelt-_arid,11351082.html) ([Foto](/pressespiegel/schwaebische-11351082.jpeg))
* Schwäbische: [Baumbesetzer im Altdorfer Wald wurden angegriffen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-altdorfer-wald-angriff-auf-baumbesetzer-_arid,11351503.html) ([Foto](/pressespiegel/schwaebische-11351503.jpeg))
* SatireSenf: [TS44/21: Neues zur Pfusch-Satire: Aktivisten des Klimacamp Ravensburg outen sich und verteilen den inkriminierten Text weiter](https://satiresenf.de/ts44-21-neues-zur-pfusch-satire-aktivisten-des-klimacamp-ravensburg-outen-sich-und-verteilen-den-inkriminierten-text-weiter/)

## 12.4.2021
* Schwäbische: [Blockade Kieswerk Grenis: Jetzt wird ermittelt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-blockade-kieswerk-grenis-jetzt-wird-ermittelt-_arid,11350924.html) ([Foto](/pressespiegel/schwaebische-11350924.jpeg))
## 9.4.2021

* Schwäbische: [Gegen Mega-Gewerbegebiet im Schussental: Petition erreicht Stuttgart](https://www.schwaebische.de/landkreis/landkreis-ravensburg/baienfurt_artikel,-gegen-mega-gewerbegebiet-im-schussental-petition-erreicht-stuttgart-_arid,11349742.html) ([Foto](/pressespiegel/schwaebische-11349742.jpeg))

## 8.4.2021
* Schwäbische: [Klima-Aktivisten blockieren Kieswerk in Grenis](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-klimaaktivisten-blockieren-kieswerk-in-grenis-_arid,11349654.html) ([Foto](/pressespiegel/schwaebische-11349654.jpeg))
* Ravensburger Spectrum: [Grenis/Altdorfer Wald: Aktivist/innen legen Aspahltwerk lahm](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2804097/grenisaltdorfer-wald-aktivistinnen-legen-aspahltwerk-lahm)

## 7.4.2021
* SWR Landesschau: [ab Zeitstempel 21:30](https://www.ardmediathek.de/video/swr-aktuell-baden-wuerttemberg/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzE0NDEwMTU/)
* Schwäbische: [2700 Beschwerden gegen Regionalplan für Oberschwaben](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-2700-beschwerden-gegen-regionalplan-fuer-oberschwaben-_arid,11349063.html) ([Foto](/pressespiegel/schwaebische-11349063.jpeg))
* Südkurier: [Warum Klima-Aktivisten den Altdorfer Wald im Kreis Ravensburg mit Baumhäusern besetzen und damit für Diskussionsstoff sorgen](https://www.suedkurier.de/baden-wuerttemberg/warum-klima-aktivisten-den-altdorfer-wald-im-kreis-ravensburg-mit-baumhaeusern-besetzen-und-damit-fuer-diskussionsstoff-sorgen;art417930,10777957) ([Foto](/pressespiegel/schwaebische-10777957.jpeg))

## 2.4.2021
* Ravensburger Spectrum: [Altdorfer Wald: HUPEN für den KLIMASCHUTZ - "Regionalverband und CDU sollten sich schämen."](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2766508/altdorfer-wald-hupen-fur-den-klimaschutz%20---cdu-und-regionalverband-sollten-)

## 31.3.2021
* Ravensburger Spectrum: [Ravensburger Klimarat von hochdotierten Experten gemäß Gemeindeordnung Ba-Wü rechtlich nicht zulässig](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2753982/)

## 30.3.2021
* Schwäbische: [Streit um Kosten des künftigen Ravensburger Klimarats](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-streit-um-kosten-des-kuenftigen-ravensburger-klimarats-_arid,11346605.html) ([Foto](/pressespiegel/schwaebische-11346605.jpeg))
* SatireSenf: [TS38/21: Regionalplan-Kritiker: Sonnenschein und subkutane Reden zum Petitionsstart – Aufruf zum zivilen Ungehorsam – Regionalpresse schweigt](https://satiresenf.de/ts38-21-regionalplan-kritiker-sonnenschein-und-subkutane-reden-zum-petitionsstart-aufruf-zum-zivilen-ungehorsam-regionalpresse-schweigt/)
* Ravensburger Spectrum: ["Wer auf die Politik v€rtraut, der hat auf Sand g€baut" - Molldietetunnel: Chefsache oder Phallussymbol?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2749105/wer-auf-die-politik-vrtraut---der-hat-auf-sand-gbaut-----oberschwabische-re)

## 29.3.2021
* Südwest Presse: [Protestcamp und Petition gegen Kiesabbau](https://www.swp.de/suedwesten/kiesabbau-in-oberschwaben-petitionen-protestcamp_-baumbesetzung-55989050.html) ([Foto](/pressespiegel/2021-03-29-swp.jpeg))
* Badische Zeitung: [15.000 Bäume sollen in einem Wald einer Kiesgrube weichen](https://www.badische-zeitung.de/mit-baumhaeusern-gegen-kiesbagger--200960663.html) ([Foto](/pressespiegel/2021-03-29-badische.jpeg))

## 24.3.2021
* Schwäbische: [Kiesabgabe rückt erstmal in weite Ferne](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-kiesabgabe-rueckt-erstmal-in-weite-ferne-_arid,11344465.html) ([Foto](/pressespiegel/schwaebische-11344465.jpeg))
* Schwäbische: [Weingartens OB Ewald lehnt Kiesabbau im Altdorfer Wald ab](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-ewald-kein-kiesabbau-im-altdorfer-wald-_arid,11343442.html) ([Foto](/pressespiegel/schwaebische-11343442.jpeg))

## 23.3.2021
* Ravensburger Spectrum: [Petition "Rauchende Schornsteine zu zeitgemäßen Bedingungen"](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2715566/petition-rauchende-schornsteine-zu-zeitgemasen-bedingungen)

## 22.3.2021
* Schwäbische: [Globaler Klimastreik auch in Ravensburg](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-globaler-klimastreik-auch-in-ravensburg-_arid,11342433.html) ([Foto](/pressespiegel/schwaebische-11342433.jpeg))

## 21.3.2021
* @traeumenvonbaeumen: [Instagram](https://www.instagram.com/tv/CMsX5rFANnm/)

## 18.3.2021
* Schwäbische: [Wissenschaftler: Status Quo verursacht allein in Ravensburg jedes Jahr Millionenschäden](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-wissenschaftler-status-quo-verursacht-allein-in-ravensburg-jedes-jahr-millionenschaeden-_arid,11341613.html) ([Foto](/pressespiegel/schwaebische-11341613.jpeg))

## 17.3.2021
* SatireSenf: [TS34/21: Pfusch-Satire über Regionalplan: Staatsschutz ermittelt wegen Urkundenfälschung](https://satiresenf.de/ts34-21-pfusch-satire-ueber-regionalplan-staatsschutz-ermittelt-wegen-urkundenfaelschung/)

## 15.3.2021
* Ravensburger Spectrum: [Einst haben die Kerls auf den Bäumen gehockt ... Stirbt der Baum - stirbt auch der Mensch !!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2693846/einst-haben-die-kerls-auf-den-baumen-gehockt---)

## 14.3.2021
* Ravensburger Spectrum: ["Parents for Future" laden zum Aufmarsch und Kundgebunge ein - Umweltthema darf nicht "untergehen"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2693235/parents-for-future-laden-zum-aufmarsch-und-kundgebunge-ein---umweltthema-da)

## 13.3.2021
* Ravensburger Spectrum: [Vollltext Stellungnahme des Petitionsausschusses in Sachen "Altdorfer Wald" (Petition aus 2019)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2690011/landtagswahl-kleine-entscheidungshilfe-gefallig-https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2690011/landtagswahl-kleine-entscheidungshilfe-gefallig-)
* Ravensburger Spectrum: [Ravensburger Klima: Guter Rat muss nicht teuer sein!](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2689321/ravensburger-klima-guter-rat-ist-teuer-)
* Ravensburger Spectrum: [Klimarat - Am Beispiel der Stadt Köln: Wie er funktionieren könnte ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2690536/klimarat-am-beispiel-der-stadt-koln-wie-er-funktionieren-konnte-)

## 12.3.2021
* Schwäbische: [Anzeige gegen Ravensburger Klima-Aktivisten wurde fallengelassen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-demo-an-besetztem-baum-in-ravensburg-war-nicht-rechtswidrig-_arid,11339421.html) ([Foto](/pressespiegel/schwaebische-11339421.jpeg))

## 11.3.2021
* Schwäbische: Klimarat schaut der Stadt bald auf die Finger
* Ravensburger Spectrum: [Offizielle Dokumente beweisen: "Schuld" in SACHEN zu hohen KIESABBAUS in der REGION ist nicht der Regionalverband allein, sondern primär das GRÜNE UMWELTMINISTERIUM!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2682955/regierungsdokument-beweist-schuld-in-sachen-kiesabbau-in-der-region-ist-nic)
* Ravensburger Spectrum: [Bürger aus dem Kreis Ravensburg reicht Petition "FFH für den gesamten Altdorfer Wald" bei der EU ein](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2683430/ravensburger-burger-reicht-petition-ffh-im-altdorfer-wald-bei-der-eu-ein)

## 10.3.2021
* Kontextwochenzeitung: [Kies bringt Kies](https://www.kontextwochenzeitung.de/gesellschaft/519/kies-bringt-kies-7363.html)
* CrimethInc: [The Forest Occupation Movement in Germany](https://crimethinc.com/2021/03/10/the-forest-occupation-movement-in-germany-tactics-strategy-and-culture-of-resistance)
* Ravensburger Spectrum: [Interview mit einem BAUM, der für den LANDTAG in Stuttgart kandidiert](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2678950/interview-mit-dem-baum-der-fur-den-landtag-in-stuttgart-kandidiert)

## 9.3.2021
* ANF: [Stadt kassiert erste Schlappe gegen Ravensburger Klimacamp](https://anfdeutsch.com/Oekologie/ravensburger-klimacamp-stadt-kassiert-erste-schlappe-24943)
* Ravensburger Spectrum: [Die Stadt Ravensburg muss erste rechtliche Doppel-Schlappe in ihrem Kampf gegen Ravensburger Klimagerechtigkeitsaktivisten kassieren!!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2677349/die-stadt-ravensburg-muss-erste-rechtliche-doppel-schlappe-in-ihrem-kampf-g)
* BUND Kressbronn: [Willkommen im Alti - Klimacamp im Altdorfer Wald](http://bund-kressbronn.de/de/KlimacampAltdorferWald)

## 8.3.2021
* Schwäbische: [Regionalplan im Weingartener Gemeinderat: Katastrophe oder Zukunftsvision?](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-regionalplan-im-weingartener-gemeinderat-katastrophe-oder-zukunftsvision-_arid,11337787.html) ([Foto](/pressespiegel/schwaebische-11337787.jpeg))

## 7.3.2021
* Schwäbische: [Umstrittener Kiesabbau im Altdorfer Wald: So stehen die Ravensburger Kandidaten dazu](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-der-altdorfer-wald-als-wahlkampfthema-_arid,11336876.html) ([Foto](/pressespiegel/schwaebische-11336876.jpeg))
* Ravensburger Spectrum: [Neues aus dem "WÜHLKREIS 69" (Ravensburg/Tettnang) -- Wenn Satire und Ernst aufeinander treffen und wenn Kies zu "Kies" wird!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2672415/neues-aus-dem-wuhlkreis-69-ravensburgtettnang---wenn-satire-und-ernst-aufei)
* Ravensburger Spectrum: [Große Teile des ALTDORFER WALDS sind (!) seit JANUAR 2019 ALS "FAUNA-UND FLORA-SCHUTZGEBIET-FFH der EU" AUSGEWIESEN -- Proteste zu RECHT!!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2672728/alarm-altdorger-wald-ist-eu-fauna-und-flora-hab itat-und-seit-januar-als-sch)

## 5.3.2021
* Schwäbische: [Karikatur von Rainer Weishaupt](/pressespiegel/2021-03-02-schwaebische.jpeg): Fuchs und Has und dem ganzen Rest ist es einfach zu viel: Alle streiten sich um ihr Zuhause. Die Kieser wollen es platt machen und nach Rohstoff buddeln; die Aktivisten ziehen kurzerhand einfach selber ein, um dagegen zu protestieren. Bis klar ist, was jetzt wirklich mit dem Wald bei Grund passiert, ziehen die Tiere provisorisch schon mal um. Wenn da bloß mal kein Windpark kommt ...
* Regio TV: [Klimaaktivisten besetzen Altdorfer Wald](https://www.regio-tv.de/mediathek/video/klimaaktivisten-besetzen-altdorfer-wald/)
* Ravensburger Spectrum: [Die Geschichte des Altdorfer Waldes aus der Sicht eines Zeitzeugen aus der ersten Hälfte des 19. Jahrhunderts](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2667762/)

## 4.3.2021
* SWR Landesschau: [Protest gegen Kiesabbau im Altdorfer Wald](https://www.swrfernsehen.de/landesschau-bw/landesschau-baden-wuerttemberg-vom-432021-100.html)
* Schwäbische: [Waldbesetzer bekommen Zuwachs im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-waldbesetzer-bekommen-zuwachs-_arid,11336277.html) ([Foto](/pressespiegel/schwaebische-11336277.jpeg))
* Ravensburger Spectrum: [Ravensburger Bürger stellt seinen Bürgermeistern **21.315,84 Euro** in Rechnung !! // Verletzung EU-Umweltrecht (FFH) und Steuergeldermissbrauch](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2664720/ravensburger-burger-stellt-seinen-burgermeister n-2131584-euro-in-rechnung-) ([Foto](/pressespiegel/schwaebische-84 .jpeg))

## 3.3.2021
* Schwäbische: [Klimaaktivisten protestieren auf Vordach des Kongresszentrums in Weingarten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-klimaaktivisten-protestieren-auf-vordach-des-kongresszentrums-in-weingarten-_arid,11335856.html) ([Foto](/pressespiegel/schwaebische-11335856.jpeg))
* Südfinder: Ravensburg macht Ernst! Sachbeschädigung: Aktivisten müssen 5.000 Euro zahlen ([Foto](/pressespiegel/2021-03-03-suedfinder.jpeg))
* ANF: [Besetzer des Altdorfer Walds laden ein zum offenene Austausch](https://anfdeutsch.com/Oekologie/besetzer-des-altdorfer-walds-laden-ein-zum-offenene-austausch-24808)
* Ravensburger Spectrum: [WEINGARTEN wird zu BAUMGARTEN - BANNERAKTION am KUKO - "Ihr da oben, wir da unten?" - Den Spieß umdrehen ...](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2661557/weingarten-wird-zu-baumgarten---banneraktion-am-kukoz)
* Ravensburger Spectrum: [Altdorfer Wald muss bleiben - Aktivist/innen besetzen Ort der Begehrlichkeiten](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2661092/altdorfer-wald-muss-bleiben---aktivistinnen-besetzen-ort-der-begehrlichkeit)

## 2.3.2021
* Schwäbische: [Hunderte von Beschwerden gegen Regionalplan gehen ein](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-hunderte-von-beschwerden-gegen-regionalplan-_arid,11335127.html) ([Foto](/pressespiegel/schwaebische-11335127.jpeg))

## 28.2.2021
* Südkurier: [Baumbesetzer in Ravensburg sollen 4000 Euro für Räumung zahlen](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/baumbesetzer-in-ravensburg-sollen-4000-euro-fuer-raeumung-zahlen;art410936,10747408) ([Foto](/pressespiegel/schwaebische-10747408.jpeg))
* Wochenblatt-Online: [Klimagerechtigkeitsaktivist\*innen besetzen Altdorfer Wald](https://wochenblatt-online.de/klimagerechtigkeitsaktivistinnen-besetzen-altdorfer-wald/)
* Ravensburger Spectrum: [Ravensburger Oberbürgermeister besetzt den "Hirschgraben 2" und hängt Banner auf -- Zeigen, wer Herr im Hause ist ! (Satire)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2652234/ravensburger-oberburgermeister-besetzt-den-hirschgraben-2---zeigen-wer-herr)

## 27.2.2021
* Schwäbische: [Bündnis gegen Kiesabbau macht sich im Altdorfer Wald breit](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-buendnis-gegen-kiesabbau-macht-sich-im-altdorfer-wald-breit-_arid,11334128.html) ([Foto](/pressespiegel/schwaebische-11334128.jpeg))
* SWR Aktuell: [Klimaaktivisten besetzen Bäume im Altdorfer Wald](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/klimaaktivisten-besetzen-baeume-im-altdorfer-wald-100.html)

## 26.2.2021
* Schwäbische: [Ein zweiter Hambacher Forst? Aktivisten besetzen den Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-ein-zweiter-hambacher-forst-aktivisten-besetzen-den-altdorfer-wald-_arid,11333867.html) ([Foto](/pressespiegel/schwaebische-11333867.jpeg))
* Schwäbische: [Fridays-for-Future-Aktivisten demonstrieren auf der Ravensburger Schussenstraße](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stockender-verkehr-auf-der-schussenstrasse-durch-klimaschuetzer-_arid,11333862.html) ([Foto](/pressespiegel/schwaebische-11333862.jpeg))
* Ravensburger Spectrum: [Aus aktuellem Anlass: Warum Jugendliche tun, was sie tun -- Inklusive Ausflug in die Hirnforschung](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2646996/)
* Ravensburger Spectrum: [Klimagerechtigkeitsaktivist\*innen besetzen Altdorfer Wald](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2647139/)
* Ravensburger Spectrum: [Redebeiträge, Lieder, viel Beifall, friedliche "Schlachtrufe" und ein Banner: Fridays for Future mobilisiert 100 Bürger/innen](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2647963/redebeitrage-lieder-viel-beifall-friedlichen-schlachtrufe-und-ein-banner-fr)
* Wochenblatt-News: [Klimaaktivisten besetzen Altdorfer Wald](https://www.wochenblatt-news.de/klimaaktivisten-besetzen-altdorfer-wald/)
* BLIX: [Altdorfer Wald besetzt – Zwei neue Baumhäuser errichtet](https://www.diebildschirmzeitung.de/diebildschirmzeitung/4019-altdorfer-wald-besetzt-zwei-neue-baumhaeuser-errichtet)

## 25.2.2021
* Schwäbische: [Fridays-for-Future-Aktivisten wollen Schussenstraße blockieren](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-fridays-for-future-aktivisten-wollen-schussenstrasse-blockieren-_arid,11333389.html) ([Foto](/pressespiegel/schwaebische-11333389.jpeg))
* Ravensburger Spectrum: [Wer "GRÜN" wählt sollte nicht vergessen, dass auch GURKEN grün sind! // Zur Präsentation von Manne Lucha](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2644512/)
* Ravensburger Spectrum: [Ravensburger Fridays-for-Future-Aktivist\*innen blockieren die Ravensburger Schussenstraße mit Gehzeugen](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2645419/)

## 24.2.2021
* Ravensburger Spectrum: [Albert Hagn äußert sich zum "Ärger über Postwurfsendung Regionalplan"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2642651/albert-hagn-ausert-sich-zum-arger-der-postwurfsendung-regionalplan)

## 23.2.2021
* Schwäbische: [Regionalplan sorgt für Unmut bei Klimaaktivisten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/amtzell_artikel,-regionalplan-sorgt-fuer-unmut-bei-klimaaktivisten-_arid,11332447.html) ([Foto](/pressespiegel/schwaebische-11332447.jpeg))
* Schwäbische: [Ravensburger Baumbesetzer erhalten Rechnung der Stadt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-erhalten-rechnung-der-stadt-_arid,11332277.html) ([Foto](/pressespiegel/schwaebische-11332277.jpeg))
* Schwäbische: [Aufregung um gefälschten Rundbrief zum Regionalplan - Strafanzeige gegen Unbekannt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-gefaelschter-rundbrief-zum-regionalplan-sorgt-fuer-furore-_arid,11331882.html) ([Foto](/pressespiegel/schwaebische-11331882.jpeg))
* Wochenblatt-News: [Klimaaktivisten hissen Banner im Amtzell](https://www.wochenblatt-news.de/klimaaktivisten-hissen-banner-im-amtzell/)
* Ravensburger Spectrum: [Postwurfsendung des Regionalverbands Ravensburg-Oberschwaben // "verwirrend irritierend aufschlussreich" - FAKE? oder FAKT!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2639335/postwurfsendung-des-regionalverbands-ravensburg-oberschwaben-und-die-berich)
* SatireSenf: [TS27/21: Fake-Satire: Regionalverband Bodensee-Oberschwaben antwortet](https://satiresenf.de/ts27-21-fake-satire-regionalverband-bodensee-oberschwaben-antwortet/)

## 22.2.2021
* Schwäbische: [Samuel Bosch und Wolfgang Ertel zu Gast beim Weingartener Jugendgemeinderat](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-samuel-bosch-und-wolfgang-ertel-zu-gast-beim-weingartener-jugendgemeinderat-_arid,11331805.html) ([Foto](/pressespiegel/2021-02-22-schwaebische.jpeg)) ([Foto](/pressespiegel/schwaebische-11331805.jpeg))
* Ravensburger Spectrum: [Ravensburger Baumcamper/innen in Amtzell: Statt in die eigenen Knie, gehen sie anderen auf's Dach ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2635581/)

## 20.2.2021
* Ravensburger Spectrum: [Juristisch ausgebildeter Bürgermeister stellt Ravensburger Baumcamper zu Unrecht Vollstreckungsbescheid zu](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2632396/juristisch-ausgebildeter-burgermeister-stellt-ravensburger-baumcamper-zu-un)
* SatireSenf: [TS25/21: WICHTIG und EILT: SaSe-Distanzierungserklärung zu einer FAKE-Postwurfsendung des Regionalverbands Bodensee-Oberschwaben](https://satiresenf.de/ts25-21-wichtig-und-eilt-sase-distanzierungserklaerung-zu-einer-fake-postwurfsendung-des-regionalverbands-bodensee-oberschwaben/)

## 18.2.2021
* Schwäbische: [Wissenschaftler: Regionalplan für Region Bodensee-Oberschwaben passt nicht zu Klimazielen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-wissenschaftler-regionalplan-fuer-region-bodensee-oberschwaben-passt-nicht-zu-klimazielen-_arid,11330127.html) ([Foto](/pressespiegel/2021-02-18-schwaebische.jpeg)) ([Foto](/pressespiegel/schwaebische-11330127.jpeg))
* Schwäbische: [Ravensburger Baumbesetzer widersprechen der Stadtverwaltung](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-widersprechen-der-stadtverwaltung-_arid,11330327.html) ([Foto](/pressespiegel/2021-02-18-schwaebische.jpeg)) ([Foto](/pressespiegel/schwaebische-11330327.jpeg))

## 17.2.2021
* Schwäbische: [Klima-Aktivisten hängen Banner am Untertor auf](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-auf-_arid,11329632.html) ([Foto](/pressespiegel/schwaebische-11329632.jpeg))
* Schwäbische: [Allianz wehrt sich gegen Kritik von Klimaschützern](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/sigmaringen_artikel,-allianz-wehrt-sich-gegen-kritik-von-klimaschuetzern-_arid,11330158.html) ([Foto](/pressespiegel/2021-02-17-schwaebische-2.jpeg)) ([Foto](/pressespiegel/schwaebische-11330158.jpeg))
* Ravensburger Spectrum: [Jugend: Baum-Camp oder Bahnhof-Szene ..?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2622937/ravensburg-baum-camp-oder-bahnhof-szene-)
* BLIX: [FDP: Rebellion gegen den Regionalplan ist ein Bündnis für Stillstand](https://www.diebildschirmzeitung.de/diebildschirmzeitung/diebildschirmzeitung-le/3873-fdp-rebellion-gegen-den-regionalplan-ist-ein-buendnis-fuer-stillstand)
* BLIX: [SPD fordert Kommunikation und Verständnis für Klimaneutralität](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/3874-spd-fordert-kommunikation-und-verstaendnis-fuer-klimaneutralitaet)

## 16.2.2021
* Schwäbische: [„Kein Wachstum um jeden Preis“ - Umweltschützer rebellieren gegen Regionalplan](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-kein-wachstum-um-jeden-preis-umweltschuetzer-rebellieren-gegen-regionalplan-_arid,11329117.html) ([Foto](/pressespiegel/2021-02-16-schwaebische.jpeg)) ([Foto](/pressespiegel/schwaebische-11329117.jpeg))
* BLIX: [Ravensburger Klimacamp startet mit weiteren Banner-Aktionen und hat bisher keine Rechnung erhalten](https://www.diebildschirmzeitung.de/diebildschirmzeitung/diebildschirmzeitung-le/3858-ravensburger-klimacamp-startet-mit-weiteren-banner-aktionen-und-hat-bisher-keine-rechnung-erhalten)

## 15.2.2021
* SatireSenf: [CDU Kreis Sigmaringen: Lügt die SchwäZ oder lädt die CDU Presse nur selektiv ein?](https://satiresenf.de/ts22-21-cdu-kreis-sigmaringen-luegt-die-schwaez-oder-laedt-die-cdu-presse-nur-selektiv-ein/)

## 14.2.2021
* Evangelisches Gemeindeblatt für Württemberg: Druck aus luftigen Höhen ([Foto](/pressespiegel/2021-evangelisches-gemeindeblatt.pdf))
* alibi -- studentischer Kulturverein e.V.: [alibi-Nicht-Live-Stream](https://youtu.be/eF3Lib2EHjE?t=3268) (Zeitstempel 54:28)

## 11.2.2021
* SWR4 Friedrichshafen

## 10.2.2021
* Ravensburger Spectrum: [Wiederholte manipulative, unwahre Berichterstattung der "Schwäbischen" (hier konkret: "Baumbesetzung")](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2606345/wiederholte-manipulative-unwahre-berichterstattung-der-schwabischen-hier-ko)

## 9.2.2021
* Schwäbische: [Baumbesetzung: Stadt muss Polizeieinsatz bezahlen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-baumbesetzung-stadt-muss-polizeieinsatz-bezahlen-_arid,11326276.html) ([Foto](/pressespiegel/schwaebische-11326276.jpeg))
* Schwäbische: [Stadt Ravensburg schickt Rechnung an Baumbesetzer](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-ravensburg-schickt-rechnung-an-baumbesetzer-_arid,11326797.html) ([Foto](/pressespiegel/schwaebische-11326797.jpeg))
* Ravensburger Spectrum: [Räumung Baumcamp: Die Frage ist nicht "ob", sondern "wann": Rücktritt des Ravensburger Bürgermeisters und anderer Verantwortlicher gefordert](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2604031/raumung-baumcamp-die-frage-ist-nicht-ob-sondern-wann-rucktritt-des-ravensbu)

## 8.2.2021
* Schwäbische: [Klimaaktivisten demonstrieren friedlich gegen Regionalplan](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaaktivisten-demonstrieren-friedlich-gegen-regionalplan-_arid,11325800.html) ([Foto](/pressespiegel/2021-02-08-schwaebische.jpeg)) ([Foto](/pressespiegel/schwaebische-11325800.jpeg))

## 7.2.2021
* Ravensburger Spectrum: [Brisant: Ravensburger Baumbesetzer/innen- Thema im Landtag BW](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2598816/brisant-ravensburger-baumbesetzerinnen--thema-im-landtag-bw)

## 6.2.2021
* Schwäbische: [Karikatur von Rainer Weishaupt](/pressespiegel/2021-02-06-schwaebische.jpeg): In den künftigen Ravensburger Wohngebieten ist kein Platz für alle 500 Interessenten. Kein Grund zur Enttäuschung, neue Ideen braucht das Land. Die Etagenwohnung ist künftig der Schlüssel zum Glück: Wer frei von Höhenangst ist, könnte unter fachkundiger Anleitung seinen Traum vom Eigenheim so doch noch verwirklichen.
* SWR Aktuell: [Ravensburger Klimaaktivisten protestieren gegen Regionalplan und steigen Regionalverband aufs Dach](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/protest-gegen-regionalplan-bodensee-oberschwaben-102.html)
* Ravensburger Spectrum: [Junge Umweltaktivist/innen: Hausdach statt Baumkrone besetzt](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2596822/junge-umweltaktivistinnen-hausdach-statt-baumkrone-besetzt)
* Wochenblatt-online: [Klimaaktivist\*innen verhüllen Regionalverbandsgebäude](https://wochenblatt-online.de/klimaaktivistinnen-verhuellen-regionalverbandsgebaeude/)
* BLIX: [Klimaaktivist\*innen verhüllen Regionalverbandsgebäude, um auf von dort ausgehende Klimazerstörung hinzuweisen](https://www.diebildschirmzeitung.de/diebildschirmzeitung/diebildschirmzeitung-le/3745-klimaaktivist-innen-verhuellen-regionalverbandsgebaeude-um-auf-von-dort-ausgehende-klimazerstoerung-hinzuweisen)

## 2.2.2021
* Wochenblatt-online: [Baumbesetzer zur Rechen­schaft ziehen](https://wochenblatt-online.de/baumbesetzer-zur-rechenschaft-ziehen/)

## 1.2.2021
* Schwäbische: [Klima-Aktivisten bauen Baumcamp in Ravensburg ab](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klima-aktivisten-bauen-baumcamp-in-ravensburg-ab-_arid,11322887.html) ([Foto](/pressespiegel/schwaebische-11322887.jpeg))

## 31.1.2021
* Ravensburger Spectrum: [Baumcamp weg - Ultimatum da](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2581836/baumcamp-weg---ultimatum-da)

## 30.1.2021
* Schwäbische: [Ravensburger Baumbesetzer ziehen Bilanz: „Wir erlebten großen Zuspruch“](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-ziehen-bilanz-wir-erlebten-grossen-zuspruch-_arid,11322187.html) ([Foto](/pressespiegel/2021-01-30-schwaebische.jpeg))

## 29.1.2021
* Ravensburger Spectrum: [Kundgebung der "Baumhausaktivist/innen"](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2577470/pressekonferenz-der-baumhausaktivistinnen-morgen -samstag-30-januar-2021-14-)

## 28.1.2021
* Regio TV Bodensee: [Nach einer Absprache mit der Stadt werden die Klimaaktivisten ihr Baumhaus am Rande der Ravensburger Innenstadt freiwillig räumen – der Protest soll an anderer Stelle weitergehen.](https://www.regio-tv.de/mediathek/video/regio-tv-bodensee-journal-28-01-2021/)

## 26.1.2021
* Ravensburger Spectrum: ["Klimaschutz" in Ravensburg ist nur lautes Getöse, geblasen aus Hörnern von Absichten](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2567796/)

## 23.1.2021
* Schwäbische: [Karikatur von Rainer Weishaupt](/pressespiegel/schwaebische-2021-01-23.jpeg)

## 22.1.2021
* Wochenblatt: [Klimacamper\*innen pausieren ihr Baumhaus und stellen Stadt Ultimatum](https://www.wochenblatt-news.de/klimacamperinnen-pausieren-ihr-baumhaus-und-stellen-stadt-ultimatum/)
* Schwäbische: [Ravensburger Baumbesetzer räumen ihr Lager](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-raeumen-ihr-lager-_arid,11319583.html) ([Foto](/pressespiegel/schwaebische-11319583.jpeg))
* Schwäbische: [Stadtrat und Vater einer Baumbesetzerin fordert mehr Klimaschutz](https://www.schwaebische.de/landkreis/landkreis-ravensburg/bad-waldsee_artikel,-stadtrat-und-vater-einer-baumbesetzerin-fordert-mehr-klimaschutz-_arid,11318989.html) ([Foto](/pressespiegel/schwaebische-11318989.jpeg))
* Ravensburger Spectrum: [Ravensburg Anno Domini 2034 -- so ... ? Oder so ... ?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2557756/ravensburg-anno-domini-2034---so---oder-so--)
* Ravensburger Spectrum: [+++ Baumbesetzung +++ 5G +++ Corona +++ WEM GEHÖRT DIE STADT? - AUSZUG AUS EINER BACHELOR-ARBEIT (hoch spannend und aufschlussreich)](https://ravensburger-spectrum.mozello.de/deutschland-1/params/post/2558364/wem-gehort-die-stadt---auszug-aus-einer-bachelor-arbeit-hoch-spannend-und-a)

## 21.1.2021
* Schwäbische: [Keine Einigung zwischen Stadt und Baumbesetzern](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-keine-einigung-zwischen-stadt-und-baumbesetzern-_arid,11318553.html) ([Foto](/pressespiegel/2021-01-21-schwaebische.jpeg))
* Stuttgarter Zeitung: [Kein Friede dieser Hütte](https://www.stuttgarter-zeitung.de/inhalt.baumbesetzer-im-beschaulichen-ravensburg-kein-friede-dieser-huette.a2c532e1-60ba-493a-9782-a0d93c874f5a.html?reduced=true) ([Foto](/pressespiegel/2021-01-21-stuttgarter-zeitung.pdf))
* seemoz: [Ohne Bäume keine Träume](https://www.seemoz.de/lokal_regional/ohne-baeume-keine-traeume/)

## 20.1.2021
* Ravensburger Spectrum: [Baumhausdiskurs - Wem gehört eigentlich der öffentliche Raum? - Schlaglicht auf eine "Grauzone" ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2550419/baumhausdiskurs---wem-gehort-eigentlich-der-offentliche-raum---schlaglicht-)

## 19.1.2021
* Wir und jetzt (Blogeintrag): [Baumhausklimacamp in Ravensburg](https://wirundjetzt.org/baumhausklimacamp-in-ravensburg/)

## 15.1.2021
* Regio TV Bodensee: [Nachrichten vom 15.1.2020, Beginn bei 7:51](https://www.regio-tv.de/mediathek/video/regio-tv-bodensee-journal-15-01-2021/)
* Schwäbische: [Ravensburger Baumbesetzer erstmals im Gespräch mit der Stadt: Das sind die Streitpunkte](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-erstmals-im-gespraech-mit-der-stadt-das-sind-die-streitpunkte-_arid,11316775.html) ([Foto](/pressespiegel/2021-01-15-schwaebische.jpeg))
* Schwäbische: Baumbesetzer laufen Gefahr, Sympathie zu verspielen ([Foto](/pressespiegel/2021-01-15-schwaebische-2.jpeg))
* Süddeutsche: [Ravensburger Baumbesetzer dürfen erstmal bleiben](https://www.sueddeutsche.de/politik/demonstrationen-ravensburg-ravensburger-baumbesetzer-duerfen-erstmal-bleiben-dpa.urn-newsml-dpa-com-20090101-210115-99-41940)
* Wochenblatt-online: [Ravensburger Baumbesetzer dürfen erstmal bleiben](https://wochenblatt-online.de/ravensburger-baumbesetzer-duerfen-erstmal-bleiben/)

## 14.1.2021
* Schwäbische: [Zwischen Ravensburgs Oberbürgermeister und den Baumbesetzern kracht es](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-zwischen-ravensburgs-oberbuergermeister-und-den-baumbesetzern-kracht-es-_arid,11316370.html) ([Foto](/pressespiegel/2021-01-14-schwaebische.jpeg))
* Ravensburger Spectrum: [Umweltschutz: Der Ravensburger Weg ist unantastbar. (Über das Gepräch OB vs Samuel)](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2532846/umweltschutz-der-ravensburger-weg-ist-unantastbar-uber-das-geprach-ob-vs-sa)

## 13.1.2021
* KONTEXT:Wochenzeitung: [Ohne Bäume keine Träume](https://www.kontextwochenzeitung.de/schaubuehne/511/ohne-baeume-keine-traeume-7243.html)
* Ravensburger Spectrum: [Baumbesetzung (BB): Die Machtspiele des PIK-Buben (OB)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2526635/baumbesetzung-bb-die-machtspiele-des-pik-buben-ob)

## 11.1.2021
* Regio TV: [Klimacamp in Ravensburg: Aktivisten trotzen der Kälte](https://www.regio-tv.de/mediathek/video/klimacamp-in-ravensburg-aktivisten-trotzen-der-kaelte/)

## 10.1.2021
* Schwäbische: [Wipfel des Widerstandes: Zu Besuch bei den Ravensburger Klimaaktivisten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-wipfel-des-widerstandes-zu-besuch-bei-den-ravensburger-klimaaktivisten-_arid,11314736.html) ([Foto](/pressespiegel/schwaebische-2021-01-10.jpeg))

## 8.1.2021
* Schwäbische: [Ravensburger Baumbesetzer fordern Zugeständnisse von der Stadtspitze](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-fordern-zugestaendnisse-von-der-stadtspitze-_arid,11314131.html) ([Foto](/pressespiegel/2021-01-08-schwaebische.pdf))
* Schwäbische: [Vier Fragen an die Ravensburger Baumbesetzer](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_video,-vier-fragen-an-die-ravensburger-baumbesetzer-_vidid,157865.html)

## 7.1.2021
* SWR Landesschau: [Klimaschützer bauen Baumhaus in Ravensburg](https://www.ardmediathek.de/ard/video/landesschau-baden-wuerttemberg/klimaschuetzer-bauen-baumhaus-in-ravensburg/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzEzODE4NjI/) (mit Anmoderation hier: [Mediathek](https://www.ardmediathek.de/swr/video/landesschau-baden-wuerttemberg/landesschau-baden-wuerttemberg-vom-7-1-2020/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzEzODE4OTM/))

## 5.1.2021
* Der Westallgäuer: Klimaaktivisten nisten sich in Baumhaus ein ([Foto](/pressespiegel/2021-01-05-westallgaeuer.pdf))
* BLIX: [„Wir brauchen die jungen Menschen in den Bäumen“](https://www.diebildschirmzeitung.de/blix/aktuell/3381-wir-brauchen-die-jungen-menschen-in-den-baeumen)

## 4.1.2021
* SWR: [Polizei duldet vorerst neues Klimaschutz-Baumhaus in Ravensburg](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/baumhaus-klimacamp-in-ravensburg-geraumt-100.html)

## 3.1.2021
* Schwäbische: [Polizeipräsident verteidigt Kompromiss mit Klima-Aktivisten nach erneuter Baumbesetzung](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-polizeipraesident-verteidigt-kompromiss-mit-klima-aktivisten-nach-erneuter-baumbesetzung-_arid,11312469.html) ([Foto](/pressespiegel/schwaebische-2021-01-03.jpeg))

## 2.1.2021
* Schwäbische: [Klima-Aktivisten besetzen erneut einen Baum in Ravensburg](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klima-aktivisten-besetzen-erneut-einen-baum-in-ravensburg-_arid,11311887.html) ([Foto](/pressespiegel/schwaebische-2021-01-02.jpeg))

## 1.1.2021
* Bild: [Aktivisten besetzen Baum](https://www.bild.de/regional/stuttgart/stuttgart-aktuell/klimaschuetzer-aktivisten-besetzen-baum-74699230.bild.html)

## 31.12.
* Schwäbische: [Klima-Aktivisten besetzen erneut einen Baum in Ravensburg](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klima-aktivisten-besetzen-erneut-einen-baum-in-ravensburg-_arid,11311753.html) ([Foto](/pressespiegel/2020-12-31-schwaebische.jpeg))
* Südkurier: [Polizei räumt Baumhaus-Klimacamp in Ravensburg. Nun wollen die Aktivisten klagen](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/polizei-raeumt-baumhaus-klimacamp-in-ravensburg-nun-wollen-die-aktivisten-klagen;art410936,10701777)
* Süddeutsche: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.sueddeutsche.de/politik/demonstrationen-ravensburg-klimaaktivisten-besetzen-schon-wieder-baum-in-ravensburg-dpa.urn-newsml-dpa-com-20090101-201231-99-862013)
* Badisches Tagblatt: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.badisches-tagblatt.de/Nachrichten/Klimaaktivisten-besetzen-schon-wieder-Baum-in-Ravensburg-69173.html)
* Zeit: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.zeit.de/news/2020-12/31/klimaaktivisten-besetzen-schon-wieder-baum-in-ravensburg)
* Stimme: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.stimme.de/suedwesten/nachrichten/pl/klimaaktivisten-besetzen-schon-wieder-baum-in-ravensburg;art19070,4433893)
* Landtag von Baden-Württemberg: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.landtag-bw.de/home/aktuelles/dpa-nachrichten/2020/Dezember/KW53/Donnerstag/23c434a3-6bd7-4b0e-b163-60583fbc.html)

## 30.12.
* Schwäbische: [SEK holt Klimaaktivisten nach 18 Tagen von Baum an Ravensburger Schussenstraße](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-sek-holt-klimaaktivisten-nach-18-tagen-von-baum-an-ravensburger-schussenstrasse-_arid,11311541.html) ([Foto](/pressespiegel/2020-12-30-schwaebische.jpeg))
* SWR: [Klimaschutz-Baumhaus in Ravensburg von Polizei geräumt](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/baumhaus-klimacamp-in-ravensburg-geraumt-100.html)
* SWR Aktuell (18:00 Uhr): [Klimaschutz-Baumhaus in Ravensburg von Polizei geräumt](https://www.ardmediathek.de/swr/video/swr-aktuell-baden-wuerttemberg/klimaschutz-baumhaus-in-ravensburg-von-polizei-geraeumt/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzEzNzc2NzQ/) (mit Anmoderation hier: [Mediathek](https://www.ardmediathek.de/swr/video/swr-aktuell-baden-wuerttemberg/sendung-18-00-uhr-vom-30-12-2020/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzEzNzc2Nzc/), Zeitstempel 4:13)
* Badische Zeitung: [Klimaaktivisten besetzen wochenlang Baum in Ravensburg](https://www.badische-zeitung.de/klimaaktivisten-besetzen-wochenlang-baum-in-ravensburg--199217771.html)
* Zeit: [Klimaaktivisten besetzen wochenlang Baum in Ravensburg](https://www.zeit.de/news/2020-12/30/klimaaktivisten-besetzen-wochenlang-baum-in-ravensburg)
* Wochenblatt: [Räumung des Ravensburger Klimacamps in Baumkrone](https://www.wochenblatt-news.de/raeumung-des-ravensburger-klimacamps-in-baumkrone/)
* Ravensburger Spectrum: [Pressekonferenz der Ravensburger Baumbesetzer (Teil 1 – Prolog)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2438292/pressekonferenz-der-ravensburger-baumbesetzer), [Teil 2 (mit Video)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2438890/uber-die-demo-pressekonferenz-der-ravensburger-baumbestzer-teil-2-2)
* Die Bildschirmzeitung: [Stadt Ravensburg lässt Baumhaus polizeilich räumen](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/3365-stadt-ravensburg-laesst-baumhaus-polizeilich-raeumen)
* Donau 3 FM: [Ravensburg: Polizei holt 17-jährigen Aktivisten von Baum](https://www.donau3fm.de/ravensburg-polizei-holt-17-jaehrigen-aktivisten-von-baum-157356/)
* Landtag von Baden-Württemberg: [Klimaaktivisten besetzen wochenlang Baum in Ravensburg](https://www.landtag-bw.de/home/aktuelles/dpa-nachrichten/2020/Dezember/KW53/Mittwoch/6f520321-a54f-4416-a1b6-3ad0b06a.html)
* Welt: [Klimaaktivisten besetzen wochenlang Baum in Ravensburg](https://www.welt.de/regionales/baden-wuerttemberg/article223483416/Klimaaktivisten-besetzen-wochenlang-Baum-in-Ravensburg.html)

## 29.12.
* Schwäbische: [Stadt lässt Ravensburger Klimacamp räumen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-laesst-ravensburger-klimacamp-raeumen-_arid,11311295.html) ([Foto](/pressespiegel/schwaebische-11311295.jpeg))
* Ravensburger Spectrum: [Skandal oder Sensation? – Mitglied der Ravensburger "Klimakommission" wird selbst zum Baumbesetzer!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2431340/)
* Ravensburger Spectrum: [Ravensburg Eilmeldung: Baumhaus wird geräumt!!! Für zwei Aktivisten rücken 14 Großraumwagen der Göppinger Polizei an ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2432583/ravensburg-eilmedlung-baumhaus-wird-geraumt)

## 28.12.
* Schwäbische: Kultur leben ([Foto](/pressespiegel/2020-12-28-schwaebische.jpeg))
* Ravensburger Spectrum: [Ravensburg: Ein Professor (61) besetzt statt Pult einen Baum – "Dozent für praktischen Umweltschutz"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2425526/ravensburg-ein-professor-besetzt-statt-pult-einen-baum-solidaritat-uber-alt)

## 27.12.
* Ravensburger Spectrum: [Heute Morgen: Interview mit Ravensburger Baumbesetzer/Baumschützer](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2418722/interview-heute-morgen-mit-ravensburger-baumbesetzerbaumschutzer)

## 25.12.
* Ravensburger Spectrum: [Tree alive – Offener Brief an die Ravensburger Baumschützer ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2408268/tree-alive---offener-brief-an-die-ravensburger-baumschutzer-)

## 24.12.
* Schwäbische: [Ravensburger Klima-Aktivisten wollen auch über Weihnachten im Baum bleiben](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-klima-aktivisten-wollen-auch-ueber-weihnachten-im-baum-bleiben-_arid,11309774.html) ([Foto](/pressespiegel/2020-12-24-schwaebische.jpeg))

## 20.12.
* Schwäbische: [Stadt Ravensburg will Aktivisten zum Aufgeben bewegen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-ravensburg-will-aktivisten-zum-aufgeben-bewegen-_arid,11308373.html) ([Foto](/pressespiegel/2020-12-20-schwaebische.jpeg))

## 18.12.
* Radio 7

## 16.12.
- Südkurier: [Mit einer Baumbesetzung in Ravensburg protestieren Jugendliche gegen die Zerstörung von Wäldern für den Straßenbau](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/mit-einer-baumbesetzung-in-ravensburg-protestieren-jugendliche-gegen-die-zerstoerung-von-waeldern-fuer-den-strassenbau;art410936,10691560)

## 14.12.
* Wochenblatt: [Junge Aktivisten errichten Klimacamp in Baumkrone](https://www.wochenblatt-news.de/junge-aktivisten-errichten-klimacamp-in-baumkrone/)

## 13.12.

* Schwäbische: [Ravensburger Klima-Aktivisten fordern Einhaltung des Pariser Klimaabkommens](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-klima-aktivisten-fordern-einhaltung-des-pariser-klimaabkommens-_arid,11305454.html) ([Foto](/pressespiegel/schwaebische-11305454.jpeg))
* Radio 7
