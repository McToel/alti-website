---
title: "Kontakt"
date: 2023-03-29T09:26:55+02:00
draft: false
description: Kontaktmöglichkeiten in die Waldbesetzung im Altdorfer Wald (Alti)
---
# Kontaktmöglichkeiten zur Waldbesetzung
Du möchtest Kontakt mit Menschen in der Waldbesetzung aufnehmen oder Dich mit Unterstützer:innen zu dem Thema auszutauschen? Hier sind Deine Möglichkeiten:

## Mail
[<i class="bi bi-envelope-at"></i> baumbesetzung.ravensburg@gmail.com](mailto:baumbesetzung.ravensburg@gmail.com)

## Instagram
[<i class="bi bi-instagram"></i> @baumbesetzung.ravensburg](https://www.instagram.com/baumbesetzung.ravensburg/)

## Telegram
[<i class="bi bi-telegram"></i> Info-Ticker](https://t.me/s/altdorfer_wald)
(nur wichtige Ankündigungen)\
[<i class="bi bi-telegram"></i> Soli-Gruppe](https://t.me/+Sv8dMIv1OEjRQ6DJ)
(wichtige Ankündigungen und Austausch)

## WhatsApp
[<i class="bi bi-whatsapp"></i> Soli-Gruppe auf WhatsApp](https://chat.whatsapp.com/Ir1uR6O54FyAYML3CBgEQP)

## Anruf
[<i class="bi bi-phone"></i> +4915908156028](tel:+4915908156028)\
[<i class="bi bi-phone"></i> +4917695110311](tel:+4917695110311)
