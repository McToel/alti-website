---
title: "Waldbesetzung Altdorfer Wald"
linkTitle: "Start"
date: 2023-03-23T18:08:48+01:00
draft: false
description: Waldbesetzung gegen die Abholzung des Altdorfer Walds für eine Kiesgrube
---

# Waldbesetzung im Altdorfer Wald

Der Konzern [Meichle+Mohr](https://www.meichle-mohr.de/) möchte einen Teil des Altdorfer Walds roden und dort eine 90 Meter tiefe Kiesgrube errichten.

Der Altdorfer Wald ist das größte zusammenhängende Waldgebiet in Oberschwaben und einer der entscheidenden Faktoren für eine lebenswerte Heimat im Landkreis Ravensburg. Er liefert Sauerstoff zum Atmen, filtert Hunderttausende Tonnen an Staub und CO₂, ist Helfer beim Klimaschutz, hat starken Einfluss auf das regionale Klima, speichert Regenwasser in unvorstellbaren Mengen, liefert Trinkwasser allerbester Qualität, ist Naherholungsgebiet, und ein wichtiger Faktor für die Forstwirtschaft vor Ort.

**Wir stellen uns der gewaltvollen Zerstörung des Altdorfer Walds in den Weg!**

## Aktuelles aus dem Alti

{{< list-page page="/blog" number="3" fade="true" >}}

## Aktuelle Pressemitteilungen

{{< list-page page="/presse/pressemitteilungen" number="3" fade="true" >}}

## Anreise in den Altdorfer Wald

Du möchtest uns im Alti besuchen? Mega cool. Wie Du zu uns findest, findest Du hier:

<a href="{{< ref "Anreise.md">}}" class="btn btn-primary">Anreise in den Alti</a>

## Kontakt in die Waldbesetzung

Du willst Kontakt mit uns aufnehmen, uns etwas Fragen, oder oder oder? Mach's doch einfach! Hier sind Deine Kontaktmöglichkeiten:

<a href="{{< ref "Kontakt.md">}}" class="btn btn-primary">Kontaktmöglichkeiten</a>

## Frühere Aktionen

Frühere Aktionen sind teilweise auf der [früheren Website zum Ravensburger Klimacamp](https://classic-ravensburg.klimacamp-augsburg.de/) verzeichnet.

{{< instantclick >}}
