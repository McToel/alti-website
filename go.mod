module gitlab.com/McToel/alti-website

// replace gitlab.com/McToel/climatecrisis-theme => /home/mctoel/dev/climatecrisis-theme

go 1.19

require (
	github.com/gohugoio/hugo-mod-bootstrap-scss/v5 v5.20300.20400 // indirect
	github.com/gohugoio/hugo-mod-jslibs-dist/popperjs/v2 v2.21100.20000 // indirect
	github.com/twbs/bootstrap v5.3.3+incompatible // indirect
	github.com/twbs/icons v1.11.3 // indirect
	gitlab.com/McToel/climatecrisis-theme v0.0.0-20241118110509-a62e85545bc1 // indirect
)
